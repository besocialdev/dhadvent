tinymce.PluginManager.add('add_banner', function(editor, url) {
  return;
  
  editor.addButton('add_banner', {
    text: 'Bannerkód beillesztése',
    icon: false,
    onclick: function() {
    	editor.insertContent('[cikk_tartalom_banner]');
    }
  });

  editor.addMenuItem('add_banner', {
    text: 'Bannerkód beillesztése',
    context: 'tools',
    onclick: function() {
    	editor.insertContent('[cikk_tartalom_banner]');
    }
  });

  return {
    getMetadata: function () {
      return  {
        name: "Bannerkód beillesztése",
        url: ""
      };
    }
  };
});