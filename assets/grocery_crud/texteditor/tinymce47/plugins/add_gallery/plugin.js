tinymce.PluginManager.add('add_gallery', function(editor, url) {
	return;
	
  editor.addButton('add_gallery', {
    text: 'Galéria beillesztése',
    icon: false,
    onclick: function() {
    	editor.insertContent('[gallery]');
    }
  });

  editor.addMenuItem('add_gallery', {
    text: 'Galéria beillesztése',
    context: 'tools',
    onclick: function() {
    	editor.insertContent('[gallery]');
    }
  });

  return {
    getMetadata: function () {
      return  {
        name: "Galéria beillesztése",
        url: ""
      };
    }
  };
});