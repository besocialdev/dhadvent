$(function() {

	var tinymce_path = default_texteditor_path+'/tinymce47/';
	var images_upload_url = js_vars.base_url + js_vars.fileupload_slug;
	//console.log(images_upload_url);
	var tinymce_options = {
		script_url : tinymce_path +"tinymce.min.js",
		theme : "modern",
		language: 'hu_HU',
		plugins : "paste add_banner add_gallery code print preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern",

		toolbar1: 'paste|add_banner|add_gallery|formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | code',
		extended_valid_elements : 'iframe[width|height|src|frameborder|allow|allowfullscreen] img[class|src|border=0|alt|title|hspace|vspace|align|onmouseover|onmouseout|name]',

		relative_urls: false,
		images_upload_url: images_upload_url,
		images_upload_base_path: '',
		images_upload_credentials: true,

		theme_advanced_resizing : false,
		theme_advanced_resize_horizontal: false,
		theme_advanced_resizing_max_width: '100%',
		theme_advanced_resizing_min_height: '500',
		width: '100%',
		height: '500',
	};

	$('textarea.texteditor').tinymce(tinymce_options);
	var minimal_tinymce_options = $.extend({}, tinymce_options);
	minimal_tinymce_options.theme = "simple";
	$('textarea.mini-texteditor').tinymce(minimal_tinymce_options);
});