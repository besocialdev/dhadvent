jQuery(document).ready(function(){

    // *******************
    // config
    // *******************
    var page_wrapper = $("#wrapper");

        var days_available = 0;
        if($( "input.my_datepicker[name=game_end_date]" ).data("days_available")>0){
            days_available = $( "input.my_datepicker[name=game_end_date]" ).data("days_available") ;
        }


        var dateFormat = "yy-mm-dd",
          from = $( "input.my_datepicker[name=game_start_date" )
            .datepicker({
              //defaultDate: 0,
              changeMonth: true,
              numberOfMonths: 1,
              minDate: 0,
              dateFormat: dateFormat
            })
            .on( "change", function() {
              date = new Date(getDate( this ));
              to.datepicker( "option", "minDate", getDate( this ) );
              to.datepicker( "option", "maxDate", date.addDays(days_available-1) );

            }),
          to = $( "input.my_datepicker[name=game_end_date" ).datepicker({
            //defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            minDate: 0,
            maxDate: "+"+days_available+"days",
            dateFormat: dateFormat
          })
          .on( "change", function() {
            //from.datepicker( "option", "minDate", getDate( this ) );
          });

        function getDate( element ) {
          var date;
          try {
            date = $.datepicker.parseDate( dateFormat, element.value );
          } catch( error ) {
            date = null;
          }

          return date;
        }

        Date.prototype.addDays = function(days) {
            var date = new Date(this.valueOf());
            date.setDate(date.getDate() + days);
            return date;
        }

        var facebook_pixel_id_help = '<p>A Facebook pixel azonosító egy 15 számjegyből álló azonosító, melyet a Facebook által adott kódban a következő piros X-ekkel jelölt részeken lehet megtalálni.</p><p>Fontos, hogy a fenti mezőbe, nem kell a teljes scriptet bemásolni, csak a 15 számjegyből álló azonosítót.<p><div><img style="max-width: 100%;" src="'+base_url+'assets/static/backend/images/facebook_pixel_id_help.png"></div>';
        var google_analytics_id_help = '<p>A Google Analytics ID egy egyedi azonosító, melyet a Google által generált kódban kódban a következő piros "GA_AZONOSITO" helyén lehet megtalálni.</p><p>Fontos, hogy a fenti mezőbe, nem kell a teljes scriptet bemásolni, csak ezt az azonosítót kell megadni.<p><div><img style="max-width: 100%;" src="'+base_url+'assets/static/backend/images/google_analytics_id_help.png"></div>';

    // global help:

    $('.meta_title_form_group').after('<div class="form-group field-info"><div class="col-sm-3 control-label"></div><div class="col-sm-9">Felhasználók számára is látható szöveg – a böngésző fülén jelenik meg. Pár szó hosszúságú, az oldal tartalmát leíró szöveget célszerű megadni.</div></div>');
    $('.meta_description_form_group').after('<div class="form-group field-info"><div class="col-sm-3 control-label"></div><div class="col-sm-9">Az oldal 1-2 mondatos leírása. Felhasználók a keresők találati listájában látják ezt a szöveget, az oldalon alapvetően nem jelenik meg.</div></div>');

    //$('.').after('<div class="form-group field-info"><div class="col-sm-3 control-label"></div><div class="col-sm-9">txt</div></div>');

    $('.site_status_form_group').after('<div class="form-group field-info"><div class="col-sm-3 control-label"></div><div class="col-sm-9">Ha bekapcsolod, az oldal publikus felülete csak adminisztrátortoknak lesz elérhető. A felhasználók egy általános tájékoztató üzenetet látnak.</div></div>');
    $('.fb_appsecret_form_group').after('<div class="form-group field-info"><div class="col-sm-3 control-label"></div><div class="col-sm-9">Ha használsz Facebook bejelentkezést, itt add meg az alkalmazás ID és Secret kódokat.</div></div>');
    $('.enable_prev_days_form_group').after('<div class="form-group field-info"><div class="col-sm-3 control-label"></div><div class="col-sm-9">A felhasználók visszamenőleg is megnézhetik a napokat?</div></div>');
        $('.login_required_setting_form_group').after('<div class="form-group field-info"><div class="col-sm-3 control-label"></div><div class="col-sm-9"><b>Amennyiben bejelentkezés nélkül is lehet játszani, abban az esetben a kvíz típusú játékmód és a kuponok nem használhatóak! Ezek regisztrációhoz kötött funkciók.</b></div></div>');
        $('.facebook_pixel_id_form_group').after('<div class="form-group field-info"><div class="col-sm-3 control-label"></div><div class="col-sm-9"><p>Cookie szint: 3 (reklám/remarketing)</p> '+facebook_pixel_id_help+'</div></div>');
        $('.google_analytics_id_form_group').after('<div class="form-group field-info"><div class="col-sm-3 control-label"></div><div class="col-sm-9"><p>Cookie szint: 1-3 (a játékos választásától függ)</p> '+google_analytics_id_help+'</div></div>');
    $('.game_end_modal_content_form_group').after('<div class="form-group field-info"><div class="col-sm-3 control-label"></div><div class="col-sm-9">A játék végén megjelenő tájékoztató ablak tartalma. Minden felhasználónak egyszer jelenik meg.</div></div>');
    $('.social_twitter_form_group').after('<div class="form-group field-info"><div class="col-sm-3 control-label"></div><div class="col-sm-9">Ha megadnál külső social oldal URL-eket, itt teheted meg.</div></div>');

        // játéknap típusnál megjelenő help-ek, csak az jelenik meg ami ki van választva!
    $('.type_form_group').after('<div class="form-group field-info type-info texteditor hidden"><div class="col-sm-3 control-label"></div><div class="col-sm-9">Használja a megjelenő szerkesztőt a tartalom feltöltéséhez. Beszúrható leírás, képek, linkek stb.</div></div>');
        $('.type_form_group').after('<div class="form-group field-info type-info puzzle hidden"><div class="col-sm-3 control-label"></div><div class="col-sm-9">Töltsön fel képet és adja meg hány darabra legyen vágva (sor és oszlop külön)<br><br><span style="color: #cc0000; font-style: normal;"><b>Figyelem!</b> Ha nincs feltöltve kép vagy nincs megadva valamelyik érték, akkor a játéknap automatikusan <u>inaktív</u> állapotban kerül elmentésre!</span></div></div>');
    $('.type_form_group').after('<div class="form-group field-info type-info wintime hidden"><div class="col-sm-3 control-label"></div><div class="col-sm-9">Speciális játék típus. A megadott nyerő időpontokban játszó játékosok automatikusan megnyerik az időponthoz tartozó nyereményt. A többi játékos a tényleges tartalom mezőben lévő tartalamat látja.</div></div>');
    $('.type_form_group').after('<div class="form-group field-info type-info youtube hidden"><div class="col-sm-3 control-label"></div><div class="col-sm-9"><u>Youtube játék típus:</u> elegendő egyetlen youtube linket megadni, de a szerkesztőben további tartalom is hozzáadható a naphoz (leírás, kép, egyéb információ). </div></div>');
    $('.type_form_group').after('<div class="form-group field-info type-info quiz hidden"><div class="col-sm-3 control-label"></div><div class="col-sm-9"><u>Kvíz:</u> egy speciális játék típus. Találós kérdést lehet feltenni, amire maximum 5 lehetséges válaszból kell eltalálni a jót (nem kötelező 4 rossz választ megadni, lehet kevesebb is). A jó választ az első mezőben kell megadni, de a játékosnak véletlenszerűen jelenik meg az adott napon a megadott válaszlehetőségek között. Helyes és hibás válasz esetén is megadható,h ogy milyen üzenet jelenjen meg a tippelés után.</div></div>');


    $('.cpm_layer_form_group').after('<div class="form-group field-info"><div class="col-sm-3 control-label"></div><div class="col-sm-9">A honlap alján megjelenő sáv szövege.</div></div>');
    $('.cpm_modal_infotext_form_group').after('<div class="form-group field-info"><div class="col-sm-3 control-label"></div><div class="col-sm-9">A felugró ablakban megjelenő hosszabb tájékoztató szöveg.</div></div>');
    $('.cpm_level3_form_group').after('<div class="form-group field-info"><div class="col-sm-3 control-label"></div><div class="col-sm-9">A különböző szinteken betöltött sütik leírásai.</div></div>');

    $('.og_title_form_group').after('<div class="form-group field-info"><div class="col-sm-3 control-label"></div><div class="col-sm-9">Facebook megosztáskor megjelenő címsor. Kicsit hosszabb lehet, mint a meta cím, akár a játék neve is megadható itt (40 karakternél hosszabbat nem érdemes).</div></div>');
    $('.og_description_form_group').after('<div class="form-group field-info"><div class="col-sm-3 control-label"></div><div class="col-sm-9">Facebook megosztáskor megjelenő üzenet szövege. Bár felső korlát nincs, de kb. 300 karakternél hosszabb szöveget nem érdemes megadni.</div></div>');
    $('.og_image_form_group').after('<div class="form-group field-info"><div class="col-sm-3 control-label"></div><div class="col-sm-9">Facebook megosztáskor megjelenő kép. A formátuma jpg vagy png legyen, a mérete pedig 1200x630px.</div></div>');

    $('.puzzle_image_form_group').after('<div class="form-group field-info type-info puzzle hidden"><div class="col-sm-3 control-label"></div><div class="col-sm-9">A kép feltöltés után automatikusan átméretezésre kerül, a szélesség és a magasság legfeljebb 640px lesz, a képarány nem változik az átméretezés során. <br><br><b>Figyelem!</b> Ha nincs feltöltve kép vagy nincs megadva valamelyik érték, akkor a játéknap automatikusan inaktív állapotba kerül elmentésre!</div></div>');


    $(document).on('click','.mode-info-table-title',function(){
        $('.mode-info-table').removeClass("hidden").slideToggle();
    });


    if( page_wrapper.hasClass('mainpage') ){

    }

        // beállítások oldal
    if( page_wrapper.hasClass('cms-setup') ){

            // A süti pdf feltöltés csak akkor jelenjen meg, ha aktív a modal
            check_cookie_modal_upload_visibility();
            $("select[name=cookie_modal_enabled]").change(function() {
                check_cookie_modal_upload_visibility();
            });

            // A süti pdf feltöltés csak akkor jelenjen meg, ha aktív a modal
            function check_cookie_modal_upload_visibility()
            {
                if($('select[name=cookie_modal_enabled]').val()=='0') {
                    $(".cookie_pdf_form_group").hide();
                }
                if($('select[name=cookie_modal_enabled]').val()=='1') {
                    $(".cookie_pdf_form_group").show();
                }
            }


            // mailchimp beállítások csak akkor jelennek meg ha a mailchimp opció van kiválasztva
            check_mailchimp_settings_visibility();
            $("select[name=newsletter_api]").change(function() {
                check_mailchimp_settings_visibility();
            });

            // mailchimp beállítások csak akkor jelennek meg ha a mailchimp opció van kiválasztva
            function check_mailchimp_settings_visibility()
            {
                $(".mcapi_apikey_form_group").hide();
                $(".mcapi_list_id_form_group").hide();

                if($('select[name=newsletter_api]').val()=='2') {
                    $(".mcapi_apikey_form_group").show();
                    $(".mcapi_list_id_form_group").show();
                }
            }

        }


        // játéknapok
    if( page_wrapper.hasClass('gamedays') ){
        $('.cal_day_form_group').before('<div class="fieldset">Játék nap és típus beállítása.</div>');
        $('.title_form_group').before('<div class="fieldset">Nyilvánosan megjelenő szövegek. Ezeket a felhasználó regisztráció nélkül is látja.</div>');
                $('.puzzle_image_form_group').before('<div class="fieldset form-pre-title puzzle">Töltse fel a puzzle képet</div>');
        $('.body_form_group').before('<div class="fieldset form-pre-title texteditor">Szöveges, képes eredmény oldal tartalma. Csak bejelentkezés után látható.</div>');
        $('.youtube_link_form_group').before('<div class="fieldset form-pre-title youtube_link hidden">YouTube URL beillesztése. Csak bejelentkezés után látható.</div>');
        $('.quiz_question_form_group').before('<div class="fieldset form-pre-title quiz hidden">Kvíz beállítások. Csak bejelentkezés után látható.</div>');
        $('.wintime_success_form_group').before('<div class="fieldset form-pre-title wintime">Nyerő időpont beállítások.</div>');
        $('.wintime_success_form_group').after('<div class="form-group field-info form-pre-title wintime"><div class="col-sm-3 control-label"></div><div class="col-sm-9">Ez az üzenet a játék oldalon jelenik meg, az e-mail értesítő szövegét a beállítások menüben adhatod meg. A nyeremény nevét a {prize} helyőrzővel adhatod meg.</div></div>');
                $('.coupon_1_form_group').before('<div class="fieldset">Kupon</div><div class="fieldset_subhelp"><span>(*több feltöltött kupon esetén véletlenszerűen jelenik meg egy kupon a feltöltöttek közül)</span></div>');
                $('.coupon_1_form_group').after('<div class="form-group field-info"><div class="col-sm-3 control-label"></div><div class="col-sm-9">A feltöltött kép automatikusan maximum 400x400 px méretűre méreteződik. </div></div>');
                $('.coupon_2_form_group').after('<div class="form-group field-info"><div class="col-sm-3 control-label"></div><div class="col-sm-9">A feltöltött kép automatikusan maximum 400x400 px méretűre méreteződik. </div></div>');
                $('.coupon_3_form_group').after('<div class="form-group field-info"><div class="col-sm-3 control-label"></div><div class="col-sm-9">A feltöltött kép automatikusan maximum 400x400 px méretűre méreteződik. </div></div>');

                hide_gamedays_type_groups();
                show_gamedays_type_groups($( "#field-type" ).val() );

        $( "#field-type" ).change(function() {
                        hide_gamedays_type_groups();
                        show_gamedays_type_groups($(this).val());
            
        });

                if($('select[name=puzzle_pcx]').val()=='')
                {
//                    var f = $("select[name=puzzle_pcx] option:first").val();
//                    console.log('aaA:'+f); // az első érték üres
                    $('select[name=puzzle_pcx]').val(3);
                    $('select[name=puzzle_pcx]').trigger("chosen:updated");
                    $('select[name=puzzle_pcy]').val(2);
                    $('select[name=puzzle_pcy]').trigger("chosen:updated");
                }


    }

        // egyedi editor
        if (page_wrapper.hasClass('cms-setup') || page_wrapper.hasClass('gamedays')){
                /**
                 * a CKEditor-ból a ClassicEditor van letöltve (nem Inline és nem Baloon, mert azok külön letölthető buildek)
                 * https://ckeditor.com/docs/ckeditor5/latest/builds/guides/overview.html#classic-editor
                 *
                 * quick start
                 * https://ckeditor.com/docs/ckeditor5/latest/builds/guides/quick-start.html#classic-editor
                 *
                 * config
                 * https://ckeditor.com/docs/ckeditor5/latest/builds/guides/quick-start.html#classic-editor
                 *
                 * image-styles
                 * https://ckeditor.com/docs/ckeditor5/latest/features/image.html#image-styles
                 */
                var simpleConfig = {
                    toolbar: {
                        items: [
                            'heading',
                            '|',
                            //'image', // az egész image upload csak a fizetős verzióban elérhető :/
                            'bold',
                            'italic',
                            'link',
                            '|',
                            'undo',
                            'redo'
                        ]
                    },
                    image: {
                        toolbar: [
                            'imageStyle:full',
                            'imageStyle:side',
                            '|',
                            'imageTextAlternative'
                        ]
                    },
                    language: 'hu' // ? nem tűnik működőkéépesnek, lehet  vmi nyelvi fájl nincs letöltve
                };


//                ClassicEditor
//                    .create( document.querySelector( '#field-quiz_question' ) ,ClassicEditor.simpleConfig )
//                    .catch( error => {
//                        console.error( error );
//                    } );

/***
                if($("#field-quiz_success").length>0){
                    ClassicEditor
                        .create( document.querySelector( '#field-quiz_success' ) , simpleConfig )
                        .catch( error => {
                            console.error( error );
                        } );
                }

                if($("#field-quiz_failure").length>0){
                    ClassicEditor
                        .create( document.querySelector( '#field-quiz_failure' ) ,simpleConfig )
                        .catch( error => {
                            console.error( error );
                        } );
                }
// */

// cookie
                if($("#field-cpm_modal_infotext").length>0){
                ClassicEditor
                    .create( document.querySelector( '#field-cpm_modal_infotext' ) , simpleConfig )
                    .catch( error => {
                        console.error( error );
                    } );
                }

                if($("#field-cpm_level1").length>0){
                ClassicEditor
                    .create( document.querySelector( '#field-cpm_level1' ) , simpleConfig )
                    .catch( error => {
                        console.error( error );
                    } );
                }

                if($("#field-cpm_level2").length>0){
                ClassicEditor
                    .create( document.querySelector( '#field-cpm_level2' ) , simpleConfig )
                    .catch( error => {
                        console.error( error );
                    } );
                }

                if($("#field-cpm_level3").length>0){
                ClassicEditor
                    .create( document.querySelector( '#field-cpm_level3' ) , simpleConfig )
                    .catch( error => {
                        console.error( error );
                    } );
                }
        }

        function hide_gamedays_type_groups(){
            $('.form-pre-title').addClass("hidden");
            $('.type-info').addClass("hidden");
            $('.body_form_group, .youtube_link_form_group, .quiz_question_form_group, .quiz_success_form_group, .quiz_failure_form_group, ._quiz_answers_input_possibilities__form_group, .type-info').addClass('hidden');
            $('.puzzle_image_form_group, .puzzle_pcx_form_group, .puzzle_pcy_form_group').addClass('hidden');
        }

        function show_gamedays_type_groups(type){
            //console.log("wintime:"+type);
            if(type=='text'){
                $('.type-info.texteditor').removeClass('hidden');
                $('.form-pre-title.texteditor').removeClass("hidden");
                $('.body_form_group').removeClass('hidden');
            }else if(type=='youtube'){
                $('.type-info.youtube').removeClass('hidden');
                $('.form-pre-title.youtube_link').removeClass("hidden");
                $('.form-pre-title.texteditor').removeClass("hidden");
                $('.body_form_group').removeClass('hidden');
                $('.youtube_link_form_group').removeClass('hidden');
            }else if(type=='quiz'){
                $('.type-info.quiz').removeClass('hidden');
                $('.form-pre-title.quiz').removeClass("hidden");
                $(".quiz_question_form_group, .quiz_success_form_group, .quiz_failure_form_group, ._quiz_answers_input_possibilities__form_group").removeClass("hidden");
            } if(type=='puzzle'){
                $('.type-info.puzzle').removeClass('hidden');
                $('.form-pre-title.puzzle').removeClass("hidden");
                $('.form-pre-title.texteditor').removeClass("hidden");
                $('.body_form_group').removeClass('hidden');
                $(".puzzle_image_form_group, .puzzle_pcx_form_group, .puzzle_pcy_form_group").removeClass("hidden");
            }else if(type=='wintime'){
                $('.type-info.wintime').removeClass('hidden');
            }else{

            }
        }

    if( page_wrapper.hasClass('skin') ){

                var page_wire_1 = '<div class="pwire_cont"><div class="pwire_head"><div class="pwire_logo">LOGO</div><div class="pwire_menucont"><span class="pwire_menuact">active</span><span class="pwire_menu">menu</span><span class="pwire_menu">menu</span></div></div><div class="pwire_body"><div class="pwire_bodyleft"><span class="pwire_maintext">kiemelt terület címsor</span><span class="pwire_subtext">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</span></div><div class="pwire_bodyright"><span>NYITÓKÉP</span></div><div class="pwire_bodybottom"><span class="pwire_maintext">Ez egy blokk főcímsor</span></div></div></div>';
                var font_sample_box = '<div class="selected_skin_mainarea_heading_font_sample">Mintaszöveg: árvíztűrő tükörfúrógép</div>';
                var button_sample = '<div class="btn_sample_outer"><div class="btn_sample btn-def_sample">Gomb minta</div></div>';
                var button_2_sample = '<div class="btn_sample_outer"><div class="btn_sample btn-2-def_sample">Játék gomb</div></div>';

                // Montserrat betűtípus a gomb mintához
                $('head').append('<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">');


        $('.skin_main_color_form_group').before('<div class="fieldset">Alap színek és betűtípus</div>');
                //$('.skin_main_color_form_group').find("#field-skin_main_color").after(page_wire_1);
                $('.skin_mainarea_heading_font_type_form_group').find("#field_skin_mainarea_heading_font_type_chosen").after(font_sample_box);
        //$('.skin_mainarea_heading_color_form_group').before('<div class="fieldset">Kiemelt területek (főoldal és játék oldal felső része)</div>');
                $('.skin_mainarea_text_color_form_group').find("#field-skin_mainarea_text_color").after(page_wire_1);
        $('.skin_btn_background_form_group').before('<div class="fieldset">Gombok</div>');
        $('.skin_btn_text_color_form_group').find("#field-skin_btn_text_color").after(button_sample);
        $('.skin_btn_2_text_color_form_group').find("#field-skin_btn_2_text_color").after(button_2_sample);
        $('.skin_bottom_svg_color_form_group').before('<div class="fieldset">Lábléc</div>');

                colorize_sample();

                //ha megváltozik a szín kódja a beviteli mezőben
                $('#field-skin_main_color, #field-skin_mainarea_heading_color, #field-skin_mainarea_text_color, #field-skin_btn_background, #field-skin_btn_text_color, #field-skin_btn_2_background, #field-skin_btn_2_text_color').on('change',function(e){
                    //alert('Changed');
                    colorize_sample();
                });

                function colorize_sample(){
                    $(".pwire_body").css({"background-color":$("#field-skin_main_color").val()});
                    $(".pwire_menuact").css({"color":$("#field-skin_main_color").val()});
                    $(".pwire_maintext").css({"color":$("#field-skin_mainarea_heading_color").val()});
                    $(".pwire_subtext").css({"color":$("#field-skin_mainarea_text_color").val()});

                    $(".btn-def_sample").css({"color":$("#field-skin_btn_text_color").val(), "background":$("#field-skin_btn_background").val()});
                    $(".btn-2-def_sample").css({"color":$("#field-skin_btn_2_text_color").val(), "background":$("#field-skin_btn_2_background").val()});

                }

                var mainID = jQuery($("#field-skin_mainarea_heading_font_type")).attr('id');
                GoogleFontSelect($("#field-skin_mainarea_heading_font_type"), mainID, '.pwire_maintext, .selected_skin_mainarea_heading_font_sample');


                $("#field-skin_mainarea_heading_font_type").chosen().change( function(){
                    var mainID = jQuery(this).attr('id');
                    GoogleFontSelect(this, mainID, '.pwire_maintext, .selected_skin_mainarea_heading_font_sample');
                } );

                //http://jsfiddle.net/3eWmJ/
                function GoogleFontSelect(select_selector, mainID, previewer_selector) {

                    var _selected = $(select_selector).val(); //get current value - selected and saved
                    var _linkclass = 'style_link_' + mainID;
                    var _previewer = previewer_selector;

                    console.log(_selected);
                    console.log(_previewer);

                    if (_selected) { //if var exists and isset

                        //Check if selected is not equal with "Select a font" and execute the script.
                        if (_selected !== 'none' && _selected !== '') {

                            //remove other elements crested in <head>
                            $('.' + _linkclass).remove();

                            //replace spaces with "+" sign
                            var the_font = _selected.replace(/\s+/g, '+');

                            //add reference to google font family
                            $('head').append('<link href="https://fonts.googleapis.com/css?family=' + the_font + '" rel="stylesheet" type="text/css" class="' + _linkclass + '">');

                            //show in the preview box the font
                            $(_previewer).css('font-family', _selected + ', sans-serif');

                        } else {

                        }

                    }

                }


        $("#field-skin_main_color").spectrum({
            showInput: true,
            //flat:true,
            preferredFormat: "hex",
        });

        $("#field-skin_card_modal_tile_color").spectrum({
            showInput: true,
            //flat:true,
            preferredFormat: "hex",
        });

        $("#field-skin_mainarea_heading_color").spectrum({
            showInput: true,
            //flat:true,
            preferredFormat: "hex",
        });

        $("#field-skin_mainarea_text_color").spectrum({
            showInput: true,
            //flat:true,
            preferredFormat: "hex",
        });
        $("#field-skin_btn_background").spectrum({
            showInput: true,
            //flat:true,
            preferredFormat: "hex",
        });
        $("#field-skin_btn_text_color").spectrum({
            showInput: true,
            //flat:true,
            preferredFormat: "hex",
        });
        $("#field-skin_btn_2_background").spectrum({
            showInput: true,
            //flat:true,
            preferredFormat: "hex",
        });
        $("#field-skin_btn_2_text_color").spectrum({
            showInput: true,
            //flat:true,
            preferredFormat: "hex",
        });
        $("#field-skin_bottom_svg_color").spectrum({
            showInput: true,
            //flat:true,
            preferredFormat: "hex",
        });
    }

    // *******************
    // generate menu slug:
    // *******************
    if( page_wrapper.hasClass('cms-pages') ){
        var menu_slug = $( "#field-menu_slug" );
        if( menu_slug.val() == '' ){
            $( "#field-title" ).focusout(function() {
                if( menu_slug.val() == '' ){
                    var new_str = remove_spec_char( $( this ).val() );
                    menu_slug.val(new_str);
                }
            });
        }

        $('.cms_block_mainpage_visible_form_group').before('<div class="fieldset">Az oldalhoz tartozó blokk beállításai</div>');
        $('.cms_page_block_pre_title_form_group').before('<div class="fieldset">A blokk tartalmi beállításai</div>');
        $('.menu_zone_header_form_group').before('<div class="fieldset">Menü beállítások</div>');
            $('#field_prizes_block_type_chosen').after('<div style="font-size:11px; padding: 5px;">Megj: amennyiben ez a beállítás "Slider", abban az esetben "A blokk tartalmi beállításai" egység beállításait itt nem kell kitölteni</div>');
    }

    if( page_wrapper.hasClass('users') ){
        // only in edit view:
        if( parseInt(primary_key) > 0 ){
            $('.games_nn_form_group').after('<div class="form-group field-info"><div class="col-sm-3 control-label"></div><div class="col-sm-9">A játékos által megnézett napok. További információk <a href="'+user_game_url+'">Játékok menüpontban</a> érhetőek el.</div></div>');
        }
    }

    // *******************
    // CMS pages
    // *******************
    if( page_wrapper.hasClass('cms-pages') ){

        $('.meta_title_form_group').before('<div class="fieldset">Meta adatok</div>');
        $('.og_title_form_group').before('<div class="fieldset">Facebook megosztás adatok</div>');

        // save preview
        $('.preview-btn').click(function() {
            var curr_id = parseInt( $(this).attr("data-id") );
            $.ajax({
                type: "POST",
                url: base_url + 'admin/pages/save_preview?id=' + curr_id,
                dataType: 'json',
                data: $('#crudForm').serialize(),
                cache: false,
                success: function(obj){
                    if(obj.error_msg == '#OK#')
                    {
                        // redirect to preview
                        var redir_url = base_url + 'preview/page/' + curr_id;
                        $("<a>").attr("href", redir_url).attr("target", "_blank")[0].click();
                    }
                    else
                    {
                        alert(obj.error_msg);
                    }
                },
                error: function(ex,ey,ez){
                    alert('Hiba az előnézet mentése során. Próbálja újra.');
                }
            });
            return false;
        });

        // show live url:
        if (document.getElementById('live-url')) {

            $('#live-url a').click(function(e) {
                e.preventDefault();
                var curr_id = parseInt( $("#curr_id").val() );
                $.ajax({
                    type: "POST",
                    url: base_url + 'admin/pages/get_live_url?id=' + curr_id,
                    dataType: 'json',
                    data: '',
                    cache: false,
                    success: function(obj){
                        if(obj.error_msg == '#OK#')
                        {
                            var redir_url = obj.result;
                            $("<a>").attr("href", redir_url).attr("target", "_blank")[0].click();
                        }
                        else
                        {
                           // console.log(obj.error_msg);
                        }
                    },
                    error: function(ex,ey,ez){
                      //  console.log('Hiba az url lekérése során.');
                      //  console.log(ex);
                      //  console.log(ey);
                      //  console.log(ez);
                    }
                });
                return false;
            });
        }
    }

    // default meta:
    if( page_wrapper.hasClass('meta') ){
        $('.meta_title_form_group').before('<div class="fieldset">Meta adatok</div>');
        $('.og_title_form_group').before('<div class="fieldset">Facebook megosztás adatok</div>');
    }

    // settings:
    if( page_wrapper.hasClass('cms-setup') ){
        $('.page_name_form_group').before('<div class="fieldset">Játék beállítások</div>');
        $('.email_from_form_group').before('<div class="fieldset">E-mail beállítások</div>');
                $('.email_subject_coupon_form_group').before('<div class="fieldset">Kupon e-mail beállítások</div>');
        $('.smtp_host_form_group').before('<div class="fieldset">SMTP beállítások</div>');
        //$('.tracking_code_lvl1_form_group').before('<div class="fieldset">Tracking kódok</div>');
                //$('.facebook_pixel_id_form_group').before('<div class="fieldset">Facebook pixel és Google Analytics azonosítók</div>');
                $('.cookie_modal_enabled_form_group').before('<div class="fieldset">Süti beállítások</div>');
        $('.newsletter_api_form_group').before('<div class="fieldset">Hírlevél beállítások</div>');
                $('.mailgun_api_key_form_group').before('<div class="fieldset"><b>BESOCIAL extra</b> - Mailgun beállítások</div>');
        $('.extra_css_form_group').before('<div class="fieldset"><b>BESOCIAL extra</b> - Extra kódok</div>');

                // lebegő gomb, hogy ne kelljen az oldal aljára görgetni a mentéshez
                var button_parent = $( "#form-button-save" ).parent();
                $( "#form-button-save" ).clone().appendTo(button_parent).attr("id","form-button-save-floating").css({"position":"fixed", "right":"25px", "top":"288px"});
    }


    // *******************
    // post categories
    // *******************
    if( page_wrapper.hasClass('post-categories') ){
        $('.meta_title_form_group').before('<div class="fieldset">Meta adatok</div>');
        $('.og_title_form_group').before('<div class="fieldset">Facebook megosztás adatok</div>');
    }

    if( page_wrapper.hasClass('mainpage') ){

        new Morris.Line({
          // ID of the element in which to draw the chart.
          element: 'regstat',
          // Chart data records -- each entry in this array corresponds to a point on
          // the chart.
          data: chartdata,
          // The name of the data record attribute that contains x-values.
          xkey: 'day',
          // A list of names of data record attributes that contain y-values.
          ykeys: ['value'],
          // Labels for the ykeys -- will be displayed when you hover over the
          // chart.
          labels: ['Regisztrált']
        });

    }



    // *******************
    // posts
    // *******************
    if( page_wrapper.hasClass('posts') ){

        $('.meta_title_form_group').before('<div class="fieldset">Meta adatok</div>');
        $('.og_title_form_group').before('<div class="fieldset">Facebook megosztás adatok</div>');

        // save preview
        $('.preview-btn').click(function() {
            var curr_id = parseInt( $(this).attr("data-id") );
            $.ajax({
                type: "POST",
                url: base_url + 'admin/posts/save_preview?id=' + curr_id,
                dataType: 'json',
                data: $('#crudForm').serialize(),
                cache: false,
                success: function(obj){
                    if(obj.error_msg == '#OK#')
                    {
                        // redirect to preview
                        var redir_url = base_url + 'preview/post/' + curr_id;
                        $("<a>").attr("href", redir_url).attr("target", "_blank")[0].click();
                    }
                    else
                    {
                        alert(obj.error_msg);
                    }
                },
                error: function(ex,ey,ez){
                    alert('Hiba az előnézet mentése során. Próbálja újra.');
                }
            });
            return false;
        });

        if (document.getElementById('live-url')) {

            // show live url:
            $('#live-url a').click(function(e) {
                e.preventDefault();
                var curr_id = parseInt( $("#curr_id").val() );
                $.ajax({
                    type: "POST",
                    url: base_url + 'admin/posts/get_live_url?id=' + curr_id,
                    dataType: 'json',
                    data: '',
                    cache: false,
                    success: function(obj){
                        if(obj.error_msg == '#OK#')
                        {
                            var redir_url = obj.result;
                            $("<a>").attr("href", redir_url).attr("target", "_blank")[0].click();
                        }
                        else
                        {
                           // console.log(obj.error_msg);
                        }
                    },
                    error: function(ex,ey,ez){
                      //  console.log('Hiba az url lekérése során.');
                      //  console.log(ex);
                      //  console.log(ey);
                      //  console.log(ez);
                    }
                });
                return false;
            });

        }

    }

    // *******************
    // posts searchű
    // *******************
    if( page_wrapper.hasClass('posts_search') ){

        $( ".datepicker" ).datepicker({
            dateFormat: "yy-mm-dd"
        });

        // set search cookies
        $( ".searchform" ).submit(function( event ) {

            var s_keyword_content = $("#s_keyword_content").val();
            var s_keyword_title = $("#s_keyword_title").val();
            var s_status = $("#s_status").val();
            var s_datefrom = $("#s_datefrom").val();
            var s_dateto = $("#s_dateto").val();
            var s_cat_id = $("#s_cat_id").val();
            var my_posts = $("#my_posts").val();

            setCookie('mcms_ps_keyword', s_keyword_content, 10);
            setCookie('mcms_ps_keywordtitle', s_keyword_title, 10);
            setCookie('mcms_ps_status', s_status, 10);
            setCookie('mcms_ps_dateto', s_dateto, 10);
            setCookie('mcms_ps_datefrom', s_datefrom, 10);
            setCookie('mcms_ps_cat_id', s_cat_id, 10);
            setCookie('mcms_my_posts', my_posts, 10);

            return true;
        });

        // read search cookies
        if( getCookie('mcms_ps_keyword') ) {
            $("#s_keyword_content").val(getCookie('mcms_ps_keyword'));
        }
        if( getCookie('mcms_ps_keywordtitle') ) {
            $("#s_keyword_title").val(getCookie('mcms_ps_keywordtitle'));
        }
        if( getCookie('mcms_ps_status') ) {
            $("#s_status").val(getCookie('mcms_ps_status'));
        }
        if( getCookie('mcms_ps_datefrom') ) {
            $("#s_datefrom").val(getCookie('mcms_ps_datefrom'));
        }
        if( getCookie('mcms_ps_dateto') ) {
            $("#s_dateto").val(getCookie('mcms_ps_dateto'));
        }
        if( getCookie('mcms_ps_cat_id') ) {
            $("#s_cat_id").val(getCookie('mcms_ps_cat_id'));
        }
        if( getCookie('mcms_my_posts') ) {
            $("#my_posts").val(getCookie('mcms_my_posts'));
        }

        // reset search cookies
        jQuery(document).on('click','.resetcookie',function(e){
            e.preventDefault();

            $("#s_keyword_content").val('');
            $("#s_keyword_title").val('');
            $("#s_status").val('');
            $("#s_datefrom").val('');
            $("#s_dateto").val('');
            $("#s_cat_id").val('');
            $("#my_posts").val('');

            setCookie('mcms_ps_keywordtitle', '', -1);
            setCookie('mcms_ps_keyword', '', -1);
            setCookie('mcms_ps_status', '', -1);
            setCookie('mcms_ps_dateto', '', -1);
            setCookie('mcms_ps_datefrom', '', -1);
            setCookie('mcms_ps_cat_id', '', -1);
            setCookie('mcms_my_posts', '', -1);
        });
    }


    // *******************
    // gallery
    // *******************
    if( page_wrapper.hasClass('gallery') ){

        // sort:
        $( "tbody.sortable" ).sortable({
            handle: '.sortable-handle',
            update: function( ) {
                var sort = '';
                var curr_id = parseInt( $("#curr_id").val() );
                $( ".img-row" ).each(function( index ) {
                    sort += parseInt($( this ).attr('data-imgid')) + ',';
                });

                var data_str = 'curr_id=' + curr_id + "&sort=" + sort;
                console.log(data_str);

                $.ajax({
                    type: "POST",
                    url: base_url + 'admin/gallery/update_gallery_order',
                    dataType: 'json',
                    data: data_str,
                    cache: false,
                    success: function(obj){
                        if(obj.error_msg == '#OK#')
                        {
                            setTimeout(function(){
                                var uri = base_url + 'admin/gallery/edit/' + curr_id + '?u=1';
                                window.location = uri;
                            }, 0);
                        }
                        else
                        {
                            console.log(obj.error_msg);
                            alert("Hiba a sorrend módosítása során.");
                        }
                    },
                    error: function(ex,ey,ez){
                      //  console.log('Hiba az url lekérése során.');
                      //  console.log(ex);
                      //  console.log(ey);
                      //  console.log(ez);
                    }
                });

                return false;

            }
        });

        // delete image:
        $('.gal_img_delete').click(function() {

            if( confirm('Kép végleges törlése? A művelet nem vonható vissza.') ){
                var id = parseInt( $(this).attr("data-id") );
                var gal_id = parseInt( $(this).attr("data-gal-id") );

                setTimeout(function(){
                    var del_url = base_url + 'admin/gallery/delete_gallery_image/' + id + '/' + gal_id;
                    window.location = del_url;
                }, 0);
            }

            return false;
        });


        // update image:
        $('.gal_img_update').click(function() {

            var id = parseInt( $(this).attr("data-id") );
            var gal_id = parseInt( $(this).attr("data-gal-id") );
            var title = $("#galimg_title_"+id).val();
            var subtitle = $("#galimg_subtitle_"+id).val();
            var desc = $("#galimg_desc_"+id).val();
            var pic_url = $("#galimg_pic_url_"+id).val();
            var icon = $("#galimg_icon_"+id).val();

            var data_str = "id=" + id + "&gal_id=" + gal_id + "&title=" + encodeURIComponent(title) + "&subtitle=" + encodeURIComponent(subtitle) + "&desc=" + encodeURIComponent(desc);
            data_str += "&pic_url=" + encodeURIComponent(pic_url);
            data_str += "&icon=" + encodeURIComponent(icon);

            $.ajax({
                type: "POST",
                url: base_url + 'admin/gallery/update_gallery_image',
                dataType: 'json',
                data: data_str,
                cache: false,
                success: function(obj){
                    if(obj.error_msg == '#OK#')
                    {
                        alert("Az adatok mentése sikerült.");
                    }
                    else
                    {
                        // console.log(obj.error_msg);
                        alert("Hiba a mentés során");
                    }
                },
                error: function(ex,ey,ez){
                  //  console.log('Hiba az url lekérése során.');
                  //  console.log(ex);
                  //  console.log(ey);
                  //  console.log(ez);
                }
            });

            return false;
        });

    }

});

function generate_wintimes(){
    if( confirm("Időpontok létrehozása?") ){
        window.location.href = base_url + "admin/wintimes/gen?h=234225445";
    }
}

function delete_wintimes( ){
    if( confirm("Időpontok törlése? Figyelem, minden időpont törlésre kerül, akkor is, ha tartozik hozzá nyertes felhasználó! A művelet nem vonható vissza.") ){
        window.location.href = base_url + "admin/wintimes/del?h=234225445";
    }
}

function gen_winners(){
    if( confirm("Nyertesek sorsolása? Figyelj rá, hogy a korábbi sorsolás eredményét felülírod. Nem visszavonható művelet.") ){
        winner_nr = parseInt($("#winner_nr").val());
        weighted = $("select[name='weighted']").val();
        window.location.href = base_url + "admin/winnergen/gen?h=234225445&winner_nr=" + winner_nr + "&weighted="+weighted;
    }
}

/**
 * remove spec. characters from string
 */
function remove_spec_char(str)
{
    str = str.toLowerCase();
    str=str.replace(/[óőö]/ig,"o");
    str=str.replace(/[úűü]/ig,"u");
    str=str.replace(/á/ig,"a");
    str=str.replace(/é/ig,"e");
    str=str.replace(/í/ig,"i");
    str = str.replace(/[^a-z0-9]/g, '-');

    while( str.charAt( 0 ) === '-' )
        str = str.slice( 1 );

    while( str.charAt( parseInt(str.length) - 1 ) === '-' ){
        str = str.slice(0, -1);
    }
    str=str.replace(/-+/ig,"-");

    return str;
}

function title3_counter()
{
    var current = parseInt(document.getElementById("field-title3").value.length);
    document.getElementById("title3-counter").innerHTML = current + "/60";
    return false;
}

function setCookie(cname, cvalue, exdays){
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname){
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return false;
}