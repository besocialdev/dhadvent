// origin: Piers Rueb

$(document).ready(function(){ 

	//  globals

	var tileClicked = false;
	var firstTileClicked; 
	var secondTileClicked; 
	var topPosFir = 0;
	var leftPosFir = 0;
	var topPosSec = 0;
	var leftPosSec = 0;
	var timer;
	var moves = 0;
	var secs = 0;

        setTimeout(function(){
                $(".puzzletempcover").animate({
                    opacity: 0,
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0,
                }, 1000, function () {
                    $(".puzzletempcover").css("display","none");
                });
                shuffle_elements();
                timer = setInterval(function(){ 
                    secs++ ;
                }, 1000);
        }, 2000);
        
        
	//  play the game

	$(document).on("click",".pieces",function(e){

                //$(this).css("pointer-events",'none');
                //e.preventDefault();
                //$( this ).off( e );

                $('.pieces').css('z-index',1);
                $(this).css('z-index',2);
                
		if(tileClicked == false){  //  if no tile is clicked

			//  set variables
			firstTileClicked = $(this).attr('id');
			topPosFir = parseInt($(this).css('top')); 
			leftPosFir = parseInt($(this).css('left')); 
                        
			//  highlight tile
			$(this).addClass('glow');
			tileClicked = true;
                        
		} else{  //  if you've clicked a tile
                    
			//  set variables
			secondTileClicked = $(this).attr('id');
			topPosSec = parseInt($(this).css('top')); 
			leftPosSec = parseInt($(this).css('left'));
                        
                        $(".puzzletempcover").css({"display":"block","opacity":"1","top":"-20px","right":"-20px","bottom":"-20px","left":"-20px"});
                        
                        $(".puzzletempcover").animate({
                            opacity: 0,
                            top: 0,
                            right: 0,
                            bottom: 0,
                            left: 0,
                        }, 700, function () {
                            $(".puzzletempcover").css("display","none");
                        });
                        
			//  animations
			$('#' + firstTileClicked).css({'top' : topPosSec , 'left' : leftPosSec});
			$('#' + secondTileClicked).css({'top' : topPosFir , 'left' : leftPosFir});

			//  remove the glow and reset the first tile
			$('.pieces').removeClass('glow');
			tileClicked = false;

			//  test for the win

			setTimeout(function(){
                                result = checkState();
				if( result === true ){
					$('p.puzzleresult').text('Gratulálunk! ' + moves + ' lépésből megoldottad, ' + secs + ' másodperc alatt!!');
					$('article').addClass('glow-2');
					moves = 0;
                                        secs = 0;
                                        clearInterval(timer);
				}
			}, 1000);

			//  increment the move counter
			moves++

		}
                
                //$(this).prop('disabled', true);
                
                

	});  //  end the click function

        function checkState()
        {
            var res = true;
            $( ".pieces" ).each(function( index ) {
                t = String(parseInt($(this).css("top")));
                l = String(parseInt($(this).css("left")));
                def = String($(this).data("pos"));
                current = String(l+t);
                
                if(def !== current){
                    res = false;
                }
            });
            
            return res;
        }
        
        function shuffle_elements()
        {
            var elements = [] ;
            var original = [] ;
            $( ".pieces" ).each(function( index ) {
                elements[index] = $(this).attr("id");
                original[index] = $(this).attr("id");
            });
            var shuffled = shuffle_array(elements);
            //console.log(shuffled.equals(original));
            //ellenőrzés, hogy biztosan meg van e keverve
            if(shuffled.equals(original))
            {
                var i = 0;
                while (i < 10) { // max 10x próbáljuk meg összekeverni
                    //console.log("i: "+i);
                    shuffled = shuffle_array(elements);
                    if(shuffled.equals(original)===false){
                        break;
                    }
                    i++;
                }                 
            }
            $( ".pieces" ).each(function( index ) {
                //$('#').css({top: 340, left: 0});
                id = shuffled[index];
                newtop = $("#"+id).css("top");
                newleft = $("#"+id).css("left");
                $(this).css({"top":newtop, "left":newleft});
            });
        }
        
        function shuffle_array(array) {
          var currentIndex = array.length, temporaryValue, randomIndex;

          // While there remain elements to shuffle...
          while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
          }

          return array;
        }        
        
        
        // Warn if overriding existing method
        if(Array.prototype.equals)
            console.warn("Overriding existing Array.prototype.equals. Possible causes: New API defines the method, there's a framework conflict or you've got double inclusions in your code.");
        // attach the .equals method to Array's prototype to call it on any array
        Array.prototype.equals = function (array) {
            // if the other array is a falsy value, return
            if (!array)
                return false;

            // compare lengths - can save a lot of time 
            if (this.length != array.length)
                return false;

            for (var i = 0, l=this.length; i < l; i++) {
                // Check if we have nested arrays
                if (this[i] instanceof Array && array[i] instanceof Array) {
                    // recurse into the nested arrays
                    if (!this[i].equals(array[i]))
                        return false;       
                }           
                else if (this[i] != array[i]) { 
                    // Warning - two different object instances will never be equal: {x:20} != {x:20}
                    return false;   
                }           
            }       
            return true;
        }
        // Hide method from for-in loops
        Object.defineProperty(Array.prototype, "equals", {enumerable: false});
        
});



