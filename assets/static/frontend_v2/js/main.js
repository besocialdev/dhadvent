/**
 * main js ...
 */

var reg_validator;

jQuery(document).ready(function(){


	$(document).click(function(event){
		if(!$(event.target).parents('.calendar-modal').length){
            if($('.calendar-modal').data("daytype")=='puzzle'&&$(".puzzleresult").html()=="")
            {
                if(confirm('Biztosan abbahagyod? Az eddigi lépéseid elvesznek.'))
                {
                    close_calendar_modal();
                }
            }
            else
            {
                close_calendar_modal();
            }
		}
	});

        /**
         * reg betöltés
         */
        if($("#regisztracio").length>0){
            load_registration_form();
        }

        /**
         * naptár init
         */
        if($(".calendar_here").length>0){
            load_calendar();
        }

        var selected_answer_id = false;
        $(document).on('click','.quiz-radio',function(e){
            e.preventDefault();
            $(".quiz-radio").removeClass('checked');
            $(this).addClass('checked');
            $('.btn--quiz').removeAttr('disabled');
            selected_answer_id = parseInt( $(this).attr('data-answer-id') );
        });

        $(document).on('click','.btn--quiz',function(e){

            $('.btn--quiz').addClass('disabled');
            var day_id = $('.quiz-content').data("day-id");
            $.get("ajax-quiz/"+day_id+"/"+selected_answer_id,function(ret){
                ret = JSON.parse(ret);

                correct_answer_id = ret.id;
                result_content = ret.result;

                $(".quiz-radio").removeClass('checked');
                $(".quiz-radio").removeClass('is-correct');
                $(".quiz-radio").removeClass('is-wrong');

                $('[data-answer-id="'+correct_answer_id+'"]').addClass('is-correct');

                if( selected_answer_id != correct_answer_id )
                    $('[data-answer-id="'+selected_answer_id+'"]').addClass('is-wrong');

                setTimeout(function(){
                    $('html, body').animate({
                        scrollTop: $(".calendar-modal-inner").offset().top-100
                    }, 400);
                    $('.quiz-content').html(result_content);
                }, 2000);
            })
            return false;

        });


        $(document).on('click','.coupon_sendmail_link',function(e){
            e.preventDefault();
            var hash = $(this).data("hash");
            $.get("kupon-letoltes/"+hash,function(ret){
                ret = JSON.parse(ret);
                if(ret.error!=="")
                {
                    ret.message = 'Hiba a kupon kiküldésekor, kérjük lépjen kapcsolatba velünk';
                }
                $(document).find(".coupon_sendmail_cont").html(ret.message);
            })
            return false;
        });


	// scrollmagic panels
	/*var controller = new ScrollMagic.Controller({
		globalSceneOptions: {
			triggerHook: 'onLeave',
			duration: "200%"
		}
	});
	var slides = document.querySelectorAll(".panel");
	for (var i=0; i<slides.length; i++) {
		new ScrollMagic.Scene({
				triggerElement: slides[i]
			})
			.setPin(slides[i], {pushFollowers: false})
			.addTo(controller);
	}*/
	//----

	if( parseInt(js_vars.game_is_active) == 0 ){
		$("#game_end_modal").modal();
	}

	/**
	 * get document width
	 * @type integer
	 */
	var document_width = parseInt($( document ).width());

	/**
	 * settings by doc. width
	 * @type Boolean
	 */
	var is_mobile = is_mobile_by_width();
	$(window).resize(function() {
		is_mobile = is_mobile_by_width();
	}).resize();

	/**
	 * default autos scroll speed
	 * @type int
	 */
	var scroll_speed = 800;
	if( true === is_mobile ){
		scroll_speed = 1000;
	}

	/**
	 * default scroll offset
	 * @type Number
	 */
	var scroll_offset = 80;
	if( true === is_mobile ){
		scroll_offset = 80;
	}

	/**
	 * ScrollMAgic default controller
	 * @type ScrollMagic
	 */
	var sm_ontroller = new ScrollMagic.Controller();

	if ($('.sm-fade-in').length > 0) {
		$('.sm-fade-in').each(function () {
			new ScrollMagic.Scene({
				triggerElement: this,
				triggerHook: 0.9,
				reverse: false
			})
			.setClassToggle(this, 'sm-animate')
			.addTo(sm_ontroller);
		});
	}

	if ( false === is_mobile && $('.sm-padding').length > 0) {
		$('.sm-padding').each(function () {
			new ScrollMagic.Scene({
				triggerElement: 'body',
				triggerHook: 0,
				offset: 100,
				reverse: true
			})
			.setClassToggle(this, 'sm-animate')
			.addTo(sm_ontroller);
		});
	}

	if ($('.calendar-item--current').length > 0) {
		spin('.calendar-item--current', 500);
		$('.calendar-item--current').hover(function() {
			if( ! $(this).hasClass('opened') ){
				$('.lock-anim').addClass('animate');
			}
			//spin('.calendar-item--current', 1000);
		}, function() {
			$('.lock-anim').removeClass('animate');
		});
	}

	// custom cookie modal:
	jQuery(document).on('click','.accept_all',function(e){
		e.preventDefault();
		set_cookie('cpm_cookie_o', 3, 365);
		location.reload();
	});
	if( ! get_cookie('cpm_cookie_o') ){
		$('.c-layer').removeClass("hidden");
	}

	// tooltips:
	$('[data-toggle="tooltip"]').tooltip();

	// alert:
	$('.bell, .signout').hover(function() {
                hover_fill_color = $(this).data("fill-color-hover") ? $(this).data("fill-color-hover") : '#553CD8';
		$(this).children('path').attr('fill', hover_fill_color); /*#2F80ED*/
	}, function() {
                default_fill_color = $(this).data("fill-color-default") ? $(this).data("fill-color-default") : '#D2D2D2';
		$(this).children('path').attr('fill', default_fill_color);
	});

	setTimeout(function(){ $('#pushalert-ticker').addClass('hidden'); }, 400);
	$('.btn-alert-icon').click(function(event) {
		$('#pushalert-ticker').removeClass("hidden");
		$('#pushalert-ticker').trigger('click');
	});

	// scroll:
	jQuery(document).on('click','.scrollto',function(e){
		e.preventDefault();
		var to = $(this).attr('data-scrollto');
		$('html, body').animate({
			scrollTop: $("#"+to).offset().top-scroll_offset
		}, scroll_speed, 'linear');
		if(  to == 'how-to-play' ){
			$("#first_name").focus();
			spin('.form-signin', 1000);
		}

		if( cpm_cookie == 3 ){

		}
	});

	// autoscroll:
	if( js_vars.scrollto != '' ){
		$('html, body').animate({
			scrollTop: $("#"+js_vars.scrollto).offset().top-scroll_offset
		}, scroll_speed, 'linear');
		if(  js_vars.scrollto == 'how-to-play' ){
			$("#first_name").focus();
			spin('.form-signin', 1000);
		}
	}
	if( true === is_mobile ){
		$( ".i001" ).addClass('hidden');
	} else {
		$(window).scroll(function() {

			$( ".i001" ).addClass('moveback');
			var top  = window.pageYOffset || document.documentElement.scrollTop;
			if( parseInt( top ) == 0 ){
				$( ".i001" ).removeClass('moveback');
			}
		});
	}

	// close nav:
	var navMain = $(".navbar-collapse");
        navMain.on("click", "a:not([data-toggle])", null, function () {
        navMain.collapse('hide');
    });

	// countdown:
	if( $('body').hasClass('naptar') ){
		var countDownDate = new Date();
		countDownDate.setHours(24,0,0,0);
		countdown_tomorrow(countDownDate.getTime());
		var x = setInterval(function() {
			countdown_tomorrow(countDownDate.getTime());
		}, 1000);
	}

        // reg:
        $('.register').dblclick(function(e){
		e.preventDefault();
		return false;
	});
	reg_validator = $(".form-signin").validate({
		rules: {
			email_address: {
				required: true,
				email: true,
				maxlength: 255
			},
			first_name:{
				required: true,
				maxlength: 255
			},
			last_name:{
				required: true,
				maxlength: 255
			},
			accept_tos:{
				required: true
			},
			accept_privacy:{
				required: true
			}
		},
		messages: {
			email_address: {
				required: 'Kötelező mező.',
				email: 'Az e-mail cím nem megfelelő.',
				maxlength: 'Max. 255 karakter.'
			},
			first_name:{
				required: 'Kötelező mező.',
				maxlength: 'Max. 255 karakter.'
			},
			last_name:{
				required: 'Kötelező mező.',
				maxlength: 'Max. 255 karakter.'
			},
			accept_tos:{
				required: 'Kötelező mező.'
			},
			accept_privacy:{
				required: 'Kötelező mező.'
			}
		},
		ignore: [],
		submitHandler: function(form) {

			// btn:
			var btn_html = $('.register').html();
			$('.register').prop("disabled", true);
			$('.register').html("Kis türelmet ...");

			// xhr:
			/*$.ajax({
				type: "POST",
				url: '',
				dataType: 'json',
				data: new FormData($('#form-signin')[0]),
				processData: false,
	        	contentType: false,
				cache: false,
				success: function(obj){
					$('.register').html(btn_html);
					$('#csrf_token').val(obj.csrf_token);
				},
				error: function(ex,ey,ez){
					$('.register').html(btn_html);
					//console.log(ex,ey,ez);
				}
			});*/

			return false;
		}
	});

	// game; close item:
    jQuery(document).on('click','.calendar-modal-close, .calendar-modal-close--mobile, .calendar-modal-close--in-text',function(e){
        e.preventDefault();
        if($('.calendar-modal').data("daytype")=='puzzle'&&$(".puzzleresult").html()=="")
        {
            if(confirm('Biztosan abbahagyod? Az eddigi lépéseid elvesznek.'))
            {
                close_calendar_modal();
            }
        }
        else
        {
            close_calendar_modal();
        }
    });

	// video size:
	//video_resize();
});

function load_registration_form(){
    $.get("jatek-urlap",function(ret){
        $("#regisztracio").html(ret);
        $(".sm-fade-in").css("opacity","1");
    });
}

function load_calendar(){
    $.get("ajax-calendar",function(r){
        $(".calendar_here").html(r);
        init_calendar();
    });
}

/**
 * last day in a row user clicked
 * @param day_clicked int
 * @return int
 */
function get_item_after(day_clicked){

	var dw = parseInt($( document ).width());
	var cols = 0;
	if( dw <= 575 ){
		cols = 2;
	} else if( dw > 575 && dw <= 991 ){
		cols = 3;
	} else {
		cols = 6;
	}

	index = $("#day-"+day_clicked).index()+1;

	if( index <= cols ){
		cc = cols;
	} else {
//		while( index%cols != 0 ){
//			index++;
//		}
		cc = index;
	}

	if($(".calendar-col:last-child").index()<cols)
		return day_clicked;

	return $(".calendar-col:nth-child("+cc+")").attr("id").replace("day-","")
}

/**
 * resize embed videos
 * @param  {string} fluid_el
 * @return
 */
function video_resize(fluid_el){

	var $allVideos = $("iframe, object:not(.bnr), embed:not(.bnr)");
	$fluidEl = $(fluid_el);
	$allVideos.each(function() {
		$(this).attr('data-aspectratio', this.height / this.width).removeAttr('height').removeAttr('width');
	});
	$(window).resize(function() {
		var newWidth = parseInt($fluidEl.width());
		$allVideos.each(function() {
			var $el = $(this);
			$el.width(newWidth).height(newWidth * $el.attr('data-aspectratio'));
		});
	}).resize();
}

/**
 * calendar item close
 */
function close_calendar_modal(){
	$('.calendar-modal').remove();
	$('.calendar-item').removeClass('opened');
	if( parseInt($( document ).width()) <= 991 ){
		$('.calendar-col').show(100);
	} else {
		$('.calendar-col').removeClass('opacity_02');
		$('.calendar-col').removeClass('opacity_10');
	}
}

/**
 * add spin animation
 */
function spin(el, timeout){
	var dw = parseInt($( document ).width());
	if( dw > 991 ){
		$(el).addClass('spinning');
		setTimeout(function(){
			$(el).removeClass('spinning');
		}, timeout);
	}
}

/**
 * countdown
 */
function countdown_tomorrow(countDownDate){
	var now = new Date().getTime();
	var distance = countDownDate - now;
	var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
	var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
	var seconds = Math.floor((distance % (1000 * 60)) / 1000);

	if( hours <= 9 ){
		hours = '0' + hours;
	}
	if( minutes <= 9 ){
		minutes = '0' + minutes;
	}
	if( seconds <= 9 ){
		seconds = '0' + seconds;
	}

	$('.countdown').html(hours + ':' + minutes + ':' + seconds);

	if (distance < 0) {
		clearInterval(x);
		$('.countdown').html('00:00:00');
	}
}

function get_cookie(name){
	var doc_cookie = document.cookie;
	var pref = name + "=";
	var begin = doc_cookie.indexOf("; " + pref);

	if (begin == -1){
		begin = doc_cookie.indexOf(pref);
		if (begin !== 0) {
			return null;
		}
	} else {
		begin += 2;
	}

	var end = document.cookie.indexOf(";", begin);
	if (end == -1) end = doc_cookie.length;

	return unescape(doc_cookie.substring(begin + pref.length, end));
}

/**
 * set new cookie
 */
function set_cookie(name, value, expire, path, domain, secure, expireinhour){
	var extra = "";
	var exdate = new Date();

	if(expire){
		if(expireinhour) exdate.setTime(exdate.getTime()+(3600000*expire));
		else exdate.setDate(exdate.getDate()+expire);
	} else {
		exdate.setDate(exdate.getDate()+1);
	}

	extra = "; expires=" + exdate.toGMTString() +
	((path) ? "; path=" + path : "") +
	((domain) ? "; domain=" + domain : "") +
	((secure) ? "; secure" : "");

	document.cookie = name + "=" + escape(value) + extra;
}

function is_mobile_by_width(){
	var document_width = parseInt($( document ).width());
	if( document_width <= 991 ){
		return true;
	}
	return false;
}

/**
 * reg
 */
/// ? duplikálva van valószínűleg véletlenül
function submitGameForm(){
    if( cpm_cookie == 3 ){}
    $("#email_address").val($("#email_address").val().trim())
    $.post("jatek-urlap",$('.form-signin').serialize())
    .done(function(ret){
        $("#regisztracio").html(ret)
    })
    $(".form-content").html("Egy pillanat...")
    return false;
}


function getRegFrom(){
    if( cpm_cookie == 3 ){ // üres
    }
    $.get("get-reg-form",function(ret){
        if( cpm_cookie == 3 ){ // üres
        }
        $("#regisztracio").html(ret);
        $(".sm-fade-in").css("opacity","1")
    });
}

function getLoginFrom(){
    if( cpm_cookie == 3 ){ // üres
    }
    $.get("get-login-form",function(ret){
        $("#regisztracio").html(ret);
        $(".sm-fade-in").css("opacity","1");
    });
}

function submitGameForm(){
    if( cpm_cookie == 3 ){ // üres
    }
    $("#email_address").val($("#email_address").val().trim());
    $.post("jatek-urlap",$('.form-signin').serialize())
    .done(function(ret){
        if(ret.indexOf("SUCCESS_LOGIN")!=-1)
            $("#day-<?=$day->id;?> .calendar-item").click();
        else
            $("#regisztracio").html(ret);
    })
    $(".form-content").html("Egy pillanat...")
    return false;
}

function init_calendar(){
        if ($('.calendar-item--current').length > 0) {
            spin('.calendar-item--current', 500);
            $('[data-toggle="tooltip"]').tooltip();
        }

        var is_mobile = is_mobile_by_width();
        var scroll_offset = 80;
        var scroll_speed = 800;

        if( is_mobile ){
            scroll_offset = 80;
            scroll_speed = 1000;
        }

        jQuery(document).on('click','.calendar-item',function(e){
            e.preventDefault();

            var day_clicked = parseInt($(this).attr('data-day'));

            if(
            	(   $(this).hasClass('calendar-item--adm') ||
                    $(this).hasClass('calendar-item--guest') ||
                    $(this).hasClass('calendar-item--prv') ||
                    $(this).hasClass('calendar-item--current') ||
                    $(this).hasClass('viewed')
                )
                && !$(this).hasClass('opened') ){

            	 $(this).addClass('viewed');
                 $(this).removeClass('not-viewed');

                close_calendar_modal();
                var parentid = $(this).parent().attr('id');

                if( parentid !== "undefined" ){
                    var parent = $("#"+parentid);
                    var item_after = get_item_after(day_clicked);
                    if( item_after != 0 ){
                        $.get("ajax-day/"+day_clicked,function(ret){

                            ret = $.parseJSON(ret);
                            //else
                            {
                                $("#day-"+item_after).after( ret.html );
                                $(".calendar-modal").fadeIn(600);

                                if( parseInt( ret.viewed ) == 1 ){
                                    $("#winfo_" + day_clicked).html("<span class='item-type'>"+ret.GAME_DAY_VIEWED_MESSAGE+"</span>");
                                    //$("a[data-day='" + day_clicked + "'] .open.btn-def").html("BBBB");
                                }

                                if( true === is_mobile ){
                                    $('.calendar-col').hide(100);
                                } else {
                                    $('.calendar-col').addClass('opacity_02');
                                    parent.addClass('opacity_10');
                                }

                                if(true === ret.is_guest)
                                {
                                    load_registration_form();
                                }


                                setTimeout(function(){
                                    $('html, body').animate({
                                        scrollTop: $(".calendar-modal").offset().top-scroll_offset
                                    }, scroll_speed, 'linear', function(){
                                        setTimeout(function(){
                                            $('.calendar-modal-inner .preloader').addClass('hidden');
                                            $('.calendar-modal-content').addClass('animate');
                                        }, 200);
                                    });

                                            video_resize('.calendar-modal-content-section .jumbotron');
                                            $('.editor-content img').removeAttr('width').removeAttr('height').addClass('img100');

                                }, 100);

                                $(this).addClass('opened');
                                $(this).addClass('viewed');
                                $(this).removeClass('not-viewed');

                                $('[data-toggle="tooltip"]').tooltip("hide");

                                if( cpm_cookie == 3 ){
                                }

                            }


                        })
                    }
                }
            }
        });
    }

/**
 * Facebook SDK :: share content
 * @param  string link
 */
function share(link){
	var obj = {
		method: 'feed',
		link: link,
		picture: $("meta[property='og:image']").attr("content"),
		name: 'behome.hu',
		caption: $("meta[property='og:title']").attr("content"),
		description: $("meta[property='og:description']").attr("content")
	};
	function callback(response){}
	FB.ui(obj, callback);
}
