<div id="wrapper" class="cms-pages users">
    
    <?php
		echo $backend_menu;
    ?>

    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row" style="margin-bottom: 20px;">
                <div class="col-lg-12">
                    <h1 class="page-header" style="position: relative;">
                        <?php echo $pagetitle; ?>
                       <?php $this->load->view('backend/_company_logo'); ?>
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url('admin'); ?>"><?php echo lang('dashboard'); ?></a>
                        </li>
                        <li class="active">
                            <i class="fa fa-user"></i> <a href="<?php echo base_url( $this->config->item('admin_menu_items')['users']['slug'] ); ?>"><?php echo lang('page_sidebar_users'); ?></a>
                        </li>
                    </ol>
                </div>
            </div>

             <?php if($primary_key > 0){}else{ ?>
	           <div class="row">
                        <?php if(true === $frontend_settings['login_required']) : ?>
		           	<div class="col-lg-10">
		           		 <div class="alert alert-info" role="alert">
		           		 	<i class="fa fa-info-circle"></i>
		       				<div style="display: inline; padding-left:10px;">
		       					<strong>Játékosok:</strong> Az oldalon regisztrált felhasználók listája. A játékos lista itt exportálható például hírlevél küldéshez, vagy sorsoláshoz. Ebben a listában minden felhasználó csak egyszer fog szerepelni. Ha a felhasználó lista helyett az egyes játékokat szeretnéd megnézni, azt a <strong><i>Játékok</i></strong> menüpontban találod. Vagy <a href="<?php echo base_url('admin/games/crud'); ?>">ide</a> is kattinthatsz.
		       				</div>
						</div>
		           	</div>
                        <?php else: ?>
                            <div class="col-lg-10">
                                <div role="alert" class="alert alert-danger">
                                    <i class="fa fa-info-circle"></i>
                                    <div style="display: inline; padding-left:10px;">
                                        <strong>Figyelem:</strong> a játék jelenleg "Vendég" módban fut, regisztráció és bejelentkezés nélkül is lehet használni. Ebben az esetben a játékosok menüpont nem elérhető.
                                    </div>
                                </div>
                            </div>
                       <?php endif; ?>
	           </div> <?php } ?>

           <?php /* <div class="row">
                <div class="col-lg-10">
                  <?php
                  	echo $crud_output->output;
                  ?>
                </div>
                <div class="col-lg-2">
                 	<?php echo $sidebar_info; ?>
                </div>
            </div>*/ ?>


             <div class="row">
                <div class="col-lg-10">

                    <?php if ($primary_key > 0) { ?>

                        <ul class="nav nav-tabs" role="tablist">
                            <?php /*<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Alap beállítások</a></li>*/ ?>
                            <?php /* if( $curr_id == 2 ){ ?>
                              <li role="presentation"><a href="#caf_gal" aria-controls="caf_gal" role="tab" data-toggle="tab">Galériák</a></li>
                              <?php } */ ?>
                        </ul>


                        <div class="tab-content" style="border: 1px solid #ddd; border-top: 0;">

                            <div role="tabpanel" class="tab-pane active" id="home">
                                <?php
                                echo $crud_output->output;
                                ?>
                            </div>

                            <?php if (0) { ?>
                                <div role="tabpanel" class="tab-pane" id="caf_gal">
                                    <div style="padding: 10px;">

                                    </div>
                                </div>
                            <?php } ?>

                        </div>

                    <?php } else {
                        ?> 
							<div class="crud-list-container">
								<?php echo $crud_output->output; ?>
							</div>
                        <?php
                    } ?>



                </div>
                <div class="col-lg-2">
                    <?php echo $sidebar_info; ?>
                </div>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript">
	var primary_key = '<?php echo $primary_key; ?>';
	var user_game_url = '<?php echo base_url('admin/games/crud'); ?>';
</script>