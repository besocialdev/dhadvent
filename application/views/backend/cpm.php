<div id="wrapper" class="cms-setup">
    
    <?php
		echo $backend_menu;
    ?>

    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row" style="margin-bottom: 20px;">
                <div class="col-lg-12">
                    <h1 class="page-header" style="position: relative;">
                        <?php echo $pagetitle; ?>
                        <?php $this->load->view('backend/_company_logo'); ?>
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url('admin'); ?>"><?php echo lang('dashboard'); ?></a>
                        </li>
                        <li class="active">
                            <i class="fa fa-cog"></i> <?php echo lang('page_sidebar_cpm'); ?>
                        </li>
                    </ol>
                </div>
            </div>


            <div class="row">
	           	<div class="col-lg-10">
	           		 <div class="alert alert-info" role="alert">
	           		 	<i class="fa fa-info-circle"></i>
	       				<div style="display: inline; padding-left:10px;">
	       					<strong>Süti engedélyezés ablak beállítások:</strong> Itt állíthatod be a felhasználóknak megjelenő süti ablak szövegeit. Az ablak csak akkor jelenik meg, ha a beállításoknál azt engedélyezted. A süti beállításokról pár infó: <br>
	       					<ul>
	       						<li>A <b>Beállítások</b> menüben tudod bekapcsolni, hogy a süti tájékoztató megjelenjen-e a felhasználóknak. Ha igen, akkor az első látogatáskor az oldal alsó sávjában megjelenik egy információs sáv, illetve egy további beállítások funkció.</li>
	       						<li>A felhasználó beállíthatja, hogy milyen szintű sütiket fogad el az oldalon. Figyelj rá, hogy a beállításoknál a mérőkódokat is ez alapján kell beállítani.</li>
	       						<li>Az oldal által használt sütikről feltölthetsz egy tájékoztató pdf-et, ez a süti beállítások ablakban lesz elérhető.</li>
	       						<li>A süti beállítások funkció az oldal láblécében folyamatosan elérhető.</li>
	       					</ul>
	       				</div>
					</div>
	           	</div>
           </div>


           <div class="row">
                <div class="col-lg-10">

                    <?php if (1) { ?>

                        <ul class="nav nav-tabs" role="tablist">
                           
                        </ul>


                        <div class="tab-content" style="border: 1px solid #ddd; border-top: 0;">

                            <div role="tabpanel" class="tab-pane active" id="home">
                                <?php
                                echo $crud_output->output;
                                ?>
                            </div>

                            <?php if (0) { ?>
                                <div role="tabpanel" class="tab-pane" id="caf_gal">
                                    <div style="padding: 10px;">

                                    </div>
                                </div>
                            <?php } ?>

                        </div>

                    <?php } else {
                        ?> 
							<div class="crud-list-container">
								<?php echo $crud_output->output; ?>
							</div>
                        <?php
                    } ?>



                </div>

            </div>

        </div>
    </div>
</div>