<!DOCTYPE html>
<html lang="<?php echo lang('page_language_code'); ?>">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php echo lang('cms_title'); ?>">
	<title><?php echo lang('cms_title'); ?></title>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&display=swap&subset=latin-ext" rel="stylesheet">
	<link href="<?php echo base_url('assets/static/backend/css/bootstrap.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/static/backend/css/sb-admin.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/static/backend/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/static/backend/css/cms-admin.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/static/backend/css/cms-admin-login.css'); ?>" rel="stylesheet">
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<div id="wrapper">

	    <div id="page-wrapper">
	        <div class="container-fluid">

	            <div class="row">
	                <div class="col-lg-12">

						<div style="margin-bottom: 30px;">
							<?php /*<img style="max-width: 200px;" src="<?php echo base_url('assets/static/backend/images/logologin.png'); ?>">*/ ?>
							<img style="max-width: 200px;" src="<?php echo base_url('assets/bda6f-logo.png'); ?>">
						</div>

	                 	<div id="login_form_cont">
							<h1>
								<b>be</b>Calendar
							</h1>
	                 		<form class="" method="post" action="<?php echo base_url('admin/dologin'); ?>">
								<div class="row">
									<div class="col-sm-12">
										<div class="row login-form-row">
											<div class="col-sm-12">
												<?php if( isset($error_msg) ): ?>
												<div class="alert alert-danger">
													<?php echo lang($error_msg); ?>
												</div>
												<?php endif; ?>
												<label for="admin_email" class="sr-only">
													<?php echo lang('page_label_email'); ?>
												</label>
												<input type="text" id="admin_email" name="admin_email" class="form-control" placeholder="<?php echo lang('page_label_email'); ?>" required="" autofocus="">	
											</div>
										</div>



										<div class="row login-form-row">
											<div class="col-sm-12">
												<label for="admin_password" class="sr-only">
													<?php echo lang('page_label_password'); ?>
												</label>
												<input type="password" id="admin_password" name="admin_password" class="form-control" placeholder="<?php echo lang('page_label_password'); ?>" required="">
											</div>
										</div>

										<div class="row login-form-row">
											<div class="col-sm-12">
												<button type="submit" class="btn btn-success btn-lg"> <i class="fa fa-check fa-fw"></i> <?php echo lang('page_login_button'); ?></button>
						                    </div>
										</div>
									</div>
								</div>
							</form>
	                 	</div>
	                </div>
	            </div>

	        </div>
	    </div>
	</div>
	<script src="<?php echo base_url('assets/static/backend/js/jquery.js'); ?>"></script>
    <script src="<?php echo base_url('assets/static/backend/js/bootstrap.min.js'); ?>"></script>
</body>
</html>