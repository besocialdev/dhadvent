    <script src="<?php echo base_url('assets/static/backend/js/jquery.js'); ?>"></script>
    <script src="<?php echo base_url('assets/static/backend/js/bootstrap.min.js'); ?>"></script>
	
	<?php 
		if( isset($crud_output->js_files) )
		{
			foreach($crud_output->js_files as $crud_js_file)
			{
				?>
				<script src="<?php echo $crud_js_file; ?>"></script>
				<?php
			}
		}
	?>
	<script type="text/javascript">
		var base_url = '<?php echo base_url(); ?>';
	</script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
	<script src="<?php echo base_url('assets/static/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/static/vendors/colorpicker/spectrum.js'); ?>"></script>
	<script src="<?php echo base_url('assets/static/backend/js/cms-admin.js?v=3'); ?>"></script>
</body>
</html>