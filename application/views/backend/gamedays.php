<div id="wrapper" class="cms-pages gamedays">

    <?php
    echo $backend_menu;
    ?>

    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row" style="margin-bottom: 20px;">
                <div class="col-lg-12">
                    <h1 class="page-header" style="position: relative;">
                       	Játék napok
                        <?php $this->load->view('backend/_company_logo'); ?>
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url('admin'); ?>"><?php echo lang('dashboard'); ?></a>
                        </li>
                        <li class="active">
                            <i class="fa fa-calendar"></i> <a href="<?php echo base_url($this->config->item('admin_menu_items')['gamedays']['slug']); ?>"><?php echo lang('page_sidebar_gamedays'); ?></a>
                        </li>
                    </ol>
                </div>
            </div>
            <div class="alert alert-info col-lg-10" role="alert">
                <div class="mode-info-table-title"><h4>Jelenlegi játéktípus: <strong><?=(true===$this->frontend_settings['login_required'] ? 'Bejelenthezéshez és regisztrációhoz kötött játékmód' : 'Vendég játékmód' ); ?></strong></h4><span style="margin-left: 10px;" class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></div>
                <div class="mode-info-table">
                    <div class="alert alert-warning" role="alert">A játékmód típusát a <a href="<?php echo base_url('admin/setup/edit/1'); ?>" class="alert-link">beállítások</a> között tudja megváltoztatni.</div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Funkciók</th>
                                <th>Bejelentkezéshez kötött mód<?=(true===$this->frontend_settings['login_required'] ? '<span class="checked_symbol">&check;</span>' : '' ); ?></th>
                                <th>Vendég játékmód<?=(true===$this->frontend_settings['login_required'] ? '' : '<span class="checked_symbol">&check;</span>' ); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Játéktípus: Szöveges képes tartalom</td>
                                <td><span class="checked_symbol">&check;</span></td>
                                <td><span class="checked_symbol">&check;</span></td>
                            </tr>
                            <tr>
                                <td>Játéktípus: Youtube videó</td>
                                <td><span class="checked_symbol">&check;</span></td>
                                <td><span class="checked_symbol">&check;</span></td>
                            </tr>
                            <tr>
                                <td>Játéktípus: Puzzle</td>
                                <td><span class="checked_symbol">&check;</span></td>
                                <td><span class="checked_symbol">&check;</span></td>
                            </tr>
                            <tr>
                                <td>Játéktípus: Kvíz</td>
                                <td><span class="checked_symbol">&check;</span></td>
                                <td><span class="cross_symbol">&cross;</span></td>
                            </tr>
                            <tr>
                                <td>Kuponok használata</td>
                                <td><span class="checked_symbol">&check;</span></td>
                                <td><span class="cross_symbol">&cross;</span></td>
                            </tr>
                        </tbody>
                        </table>
                    </div>
                </div>
            </div> 

             <?php if($curr_id > 0){
             	?>
					 <div class="row">
				           	<div class="col-lg-10">
				           		 <div class="alert alert-info" role="alert">
				           		 	<i class="fa fa-info-circle"></i>
				       				<div style="display: inline; padding-left:10px;">
				       					<strong>Játék nap szerkesztése:</strong> Válaszd ki, melyik napon legyen elérhető a megadott tartalom a felhasználóknak. Egy kártyát csak egy naphoz rendelhetsz. Ezután add meg, milyen típusú kártyát hozol létre. Ettől függ, milyen tartalom jelenik meg a játékosoknak. A következők közül választhatsz: <br>
				       					<ul>
				       						<li><b>Szöveges, képes oldal:</b> szerkesztővel hozhatsz létre egyedi tartalmat, tölthetsz fel képet, esetleg kupont.</li>
				       						<li><b>Youtube Videó:</b> a szöveges oldal típusnál is van lehetőség videót beilleszteni, de ha mást nem szeretnél megjeleníteni, ez talán kényelmesebb. :). A szöveges tartalom nem jelenik meg a játékosoknak.</li>
				       						<li><b>Puzzle:</b> egyszerű puzzle játék. Egy képet kell feltölteni és megadni, hogy hány sorra/oszlopra darabolva készüljön belőle puzzle. A sor/oszlop értékeket érdemes 3 és 5 értékekek közé állítani (2x2 részre osztott puzzle túl egyszerű, azonban 5x5nél több részre vágva már nehezen kirakható, így elrontja a játékélményt)</li>
				       						<li><?=(true===$this->frontend_settings['login_required'] ? '<span>' : '<span style="font-style: italic; color: #aaaaaa;">' ); ?><b>Kvíz:</b> Speciális típus, itt meg kell adnod a kvíz kérdést, illetve a helyes, és a hibás válaszokat. Legfeljebb négy hibás választ írhatsz. A válaszok a felhasználóknak véletlen sorrendben jelennek meg. Ezeken kívül írd le, milyen üzenetet kapon a felhasználó, ha eltalálta a helyes választ, és mit akkor, ha nem.</span> <strong style="font-size: 11px;">A kvíz csak "Bejelentkezéshez és regisztrációhoz kötött játékmód"ban használható.</strong></li>
				       						<li style="list-style-type: none; margin-top: 10px; margin-left:-12px;"><?=(true===$this->frontend_settings['login_required'] ? '<span>' : '<span style="font-style: italic; color: #aaaaaa;">' ); ?><b>+1 extra: kuponok</b><br>A kuponok használata: egy adott játéknapon akár kupon is ajándékozható a játékosnak, mely az adott nap kinyitásakor aktiválódik. Jelenleg 3 különböző kupont lehet egy játéknaphoz feltölteni, amelyek közül 1 kupont véletlenszerűen választ ki a rendszer és ezt kapja a játékos. Amennyiben csak 1 kupon kerül feltöltésre, abban az esetben éretelmszerűen minden játékos ugyanazt a kupont kapja az adott napot kinyitva.<br>A kupon(oka)t kép formátumban kell feltölteni.<br> A nap nyitásával megszerzett kupont azonnal megnézheti a játékos, vagy emailben kiküldheti magának a regisztrációkor megadott email címére.</span> <strong style="font-size: 11px;">A kuponok csak "Bejelentkezéshez és regisztrációhoz kötött játékmód"ban használható.</strong></li>
				       					</ul>

				       				</div>
								</div>
				           	</div>
			           </div>
             	<?php


             }else{ ?>
	           <div class="row">
		           	<div class="col-lg-10">
		           		 <div class="alert alert-info" role="alert">
		           		 	<i class="fa fa-info-circle"></i>
		       				<div style="display: inline; padding-left:10px;">
		       					<strong>Játéknapok:</strong> Az alkalmazás lelke. :) Itt állíthatod be a játékban megjelenő kártyákat. Add meg, hogy a kártya melyik napon legyen aktív, illetve állítsd be a kártya tartalmát. Az első kártya létrehozásához kattints a <b><i>Hozzáadás</i></b> gombra itt lentebb.
                                   <?php if($crud_state == 'add'): ?>
                                   <br>
                                   <ul>
                                        <li><b>Szöveges, képes oldal:</b> szerkesztővel hozhatsz létre egyedi tartalmat, tölthetsz fel képet, esetleg kupont.</li>
                                        <li><b>Youtube Videó:</b> a szöveges oldal típusnál is van lehetőség videót beilleszteni, de ha mást nem szeretnél megjeleníteni, ez talán kényelmesebb. :). A szöveges tartalom nem jelenik meg a játékosoknak.</li>
                                        <li><b>Puzzle:</b> egyszerű puzzle játék. Egy képet kell feltölteni és megadni, hogy hány sorra/oszlopra darabolva készüljön belőle puzzle. A sor/oszlop értékeket érdemes 3 és 5 értékekek közé állítani (2x2 részre osztott puzzle túl egyszerű, azonban 5x5nél több részre vágva már nehezen kirakható, így elrontja a játékélményt)</li>
                                        <li><?=(true===$this->frontend_settings['login_required'] ? '<span>' : '<span style="font-style: italic; color: #aaaaaa;">' ); ?><b>Kvíz:</b> Speciális típus, itt meg kell adnod a kvíz kérdést, illetve a helyes, és a hibás válaszokat. Legfeljebb négy hibás választ írhatsz. A válaszok a felhasználóknak véletlen sorrendben jelennek meg. Ezeken kívül írd le, milyen üzenetet kapon a felhasználó, ha eltalálta a helyes választ, és mit akkor, ha nem.</span> <strong style="font-size: 11px;">A kvíz csak "Bejelentkezéshez és regisztrációhoz kötött játékmód"ban használható.</strong></li>
                                        <li style="list-style-type: none; margin-top: 10px; margin-left:-12px;"><?=(true===$this->frontend_settings['login_required'] ? '<span>' : '<span style="font-style: italic; color: #aaaaaa;">' ); ?><b>+1 extra: kuponok</b><br>A kuponok használata: egy adott játéknapon akár kupon is ajándékozható a játékosnak, mely az adott nap kinyitásakor aktiválódik. Jelenleg 3 különböző kupont lehet egy játéknaphoz feltölteni, amelyek közül 1 kupont véletlenszerűen választ ki a rendszer és ezt kapja a játékos. Amennyiben csak 1 kupon kerül feltöltésre, abban az esetben éretelmszerűen minden játékos ugyanazt a kupont kapja az adott napot kinyitva.<br>A kupon(oka)t kép formátumban kell feltölteni.<br> A nap nyitásával megszerzett kupont azonnal megnézheti a játékos, vagy emailben kiküldheti magának a regisztrációkor megadott email címére.</span> <strong style="font-size: 11px;">A kuponok csak "Bejelentkezéshez és regisztrációhoz kötött játékmód"ban használható.</strong></li>
                                </ul>
                                <?php endif; ?>
		       				</div>
						</div>
		           	</div>
	           </div> <?php } ?>

            <div class="row">
                <div class="col-lg-10">

                    <?php if ($curr_id > 0) { ?>

                        <ul class="nav nav-tabs" role="tablist">
                            <?php /*<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Alap beállítások</a></li>*/ ?>
                            <?php /* if( $curr_id == 2 ){ ?>
                              <li role="presentation"><a href="#caf_gal" aria-controls="caf_gal" role="tab" data-toggle="tab">Galériák</a></li>
                              <?php } */ ?>
                        </ul>


                        <div class="tab-content" style="border: 1px solid #ddd; border-top: 0;">

                            <div role="tabpanel" class="tab-pane active" id="home">
                                <?php
                                echo $crud_output->output;
                                ?>
                            </div>

                            <?php if (0) { ?>
                                <div role="tabpanel" class="tab-pane" id="caf_gal">
                                    <div style="padding: 10px;">

                                    </div>
                                </div>
                            <?php } ?>

                        </div>

                    <?php } else {
                        ?> 
							<div class="crud-list-container">
								<?php echo $crud_output->output; ?>
							</div>
                        <?php
                    } ?>



                </div>
                <div class="col-lg-2">
                    <?php //echo $sidebar_info; ?>

                    <?php if($curr_id > 0){ ?>
					<input type="hidden" value="<?php echo $curr_id; ?>" id="curr_id">
					<div class="panel panel-default sidebar">
					    <div class="panel-heading">
					        <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> <?php echo lang('func'); ?></h3>
					    </div>
					    <div class="panel-body">
					       <ul>
					       		<li>
					       			<a href="<?php echo base_url('naptar') ?>" target="_blank">
					       				Játék oldal megnyitása &raquo;
					       			</a>
					       		</li> 
					       </ul>
					    </div>
					</div>
					<?php } ?>

                </div>
            </div>

        </div>
    </div>
</div>