<div id="wrapper" class="cms-pages">

    <?php
    echo $backend_menu;
    ?>

    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row" style="margin-bottom: 20px;">
                <div class="col-lg-12">
                    <h1 class="page-header" style="position: relative;">
                        <?php echo $pagetitle; ?>
                        <?php $this->load->view('backend/_company_logo'); ?>
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url('admin'); ?>"><?php echo lang('dashboard'); ?></a>
                        </li>
                        <li class="active">
                            <i class="fa fa-trophy"></i> <a href="<?php echo base_url($this->config->item('admin_menu_items')['prizes']['slug']); ?>"><?php echo lang('page_sidebar_prizes'); ?></a>
                        </li>
                    </ol>
                </div>
            </div>

            <?php if($curr_id > 0){}else{ ?>
           <div class="row">
	           	<div class="col-lg-10">
	           		 <div class="alert alert-info" role="alert">
	           		 	<i class="fa fa-info-circle"></i>
	       				<div style="display: inline; padding-left:10px;">
	       					<strong>Nyeremények:</strong> Az itt beállított nyeremények a publikus felület főoldalán jelennek meg egy lapozható galériában. Új nyeremény beállításához katt a <b><i>hozzáadás</i></b> gombra. A nyeremény darabszámát csak abban az esetben töltsd ki, ha nyerő időponthoz szeretnéd társítani a nyereményeket.
	       				</div>
					</div>
	           	</div>
           </div> <?php } ?>

            <div class="row">
                <div class="col-lg-10">

                    <?php if ($curr_id > 0) { ?>

                        <ul class="nav nav-tabs" role="tablist">
                            <?php /*<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Alap beállítások</a></li>*/ ?>
                            <?php /* if( $curr_id == 2 ){ ?>
                              <li role="presentation"><a href="#caf_gal" aria-controls="caf_gal" role="tab" data-toggle="tab">Galériák</a></li>
                              <?php } */ ?>
                        </ul>


                        <div class="tab-content" style="border: 1px solid #ddd; border-top: 0;">

                            <div role="tabpanel" class="tab-pane active" id="home">
                                <?php
                                echo $crud_output->output;
                                ?>
                            </div>

                            <?php if (0) { ?>
                                <div role="tabpanel" class="tab-pane" id="caf_gal">
                                    <div style="padding: 10px;">

                                    </div>
                                </div>
                            <?php } ?>

                        </div>

                    <?php } else {
                        ?> 
							<div class="crud-list-container">
								<?php echo $crud_output->output; ?>
							</div>
                        <?php
                    } ?>


                </div>
                <div class="col-lg-2">
                   
                   <?php /*<?php if($curr_id > 0){ ?>
					<input type="hidden" value="<?php echo $curr_id; ?>" id="curr_id">
					<div class="panel panel-default sidebar">
					    <div class="panel-heading">
					        <h3 class="panel-title">
					        	Nyerő időpontok
					        </h3>
					    </div>
					    <div class="panel-body">
					       dfsfdf
					    </div>
					</div>
					<?php } ?>*/ ?>

                </div>
            </div>

        </div>
    </div>
</div>