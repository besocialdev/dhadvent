<div class="panel panel-default sidebar">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> <?php echo $sb_title; ?></h3>
    </div>
    <div class="panel-body">
       <ul>
       		<li>
       			<b><?php echo lang('widget_shortcode') ?></b> <?php echo $widget_shortcode; ?>
       		</li>
       </ul>
    </div>
</div>