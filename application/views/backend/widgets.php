<div id="wrapper">
    
    <?php
		echo $backend_menu;
    ?>

    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header" style="position: relative;">
                        <?php echo $pagetitle; ?>
                       <?php $this->load->view('backend/_company_logo'); ?>
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url('admin'); ?>"><?php echo lang('dashboard'); ?></a>
                        </li>
                        <li class="active">
                            <i class="fa fa-bolt"></i> <a href="<?php echo base_url( $this->config->item('admin_menu_items')['widgets']['slug'] ); ?>"><?php echo lang('page_sidebar_widgets'); ?></a>
                        </li>
                    </ol>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-10">
                  <?php
                  	echo $crud_output->output;
                  ?>
                </div>
                <div class="col-lg-2">
                 	<?php echo $sidebar_info; ?>
                </div>
            </div>

        </div>
    </div>
</div>