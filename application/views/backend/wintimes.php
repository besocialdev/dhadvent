<?php 
die('---***---');
/* // wintime - nyerő időpont törlés */
?>
<div id="wrapper" class="cms-pages">

    <?php
    echo $backend_menu;
    ?>

    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row" style="margin-bottom: 20px;">
                <div class="col-lg-12">
                    <h1 class="page-header" style="position: relative;">
                        <?php echo $pagetitle; ?>
                        <?php $this->load->view('backend/_company_logo'); ?>
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url('admin'); ?>"><?php echo lang('dashboard'); ?></a>
                        </li>
                        <li class="active">
                            <i class="fa fa-trophy"></i> <?php echo lang('page_sidebar_wintimes'); ?></a>
                        </li>
                    </ol>
                </div>
            </div>

            <?php if($curr_id > 0){}else{ ?>
           <div class="row">
	           	<div class="col-lg-10">
	           		 
	           		<?php if($_GET && isset($_GET['genmsg'])){ ?>
					<div class="alert alert-success" role="alert">
	       				<div style="display: inline; padding-left:10px;">
	       					<b>Rendben</b>, <?php echo (int)$_GET['genmsg'] ?> darab időpont lett létrehozva.
	       				</div>
					</div>
					<?php } ?>

					<?php if($_GET && isset($_GET['delmsg'])){ ?>
					<div class="alert alert-success" role="alert">
	       				<div style="display: inline; padding-left:10px;">
	       					<b>Rendben</b>, minden feltöltött időpont törölve lett. Hozz létre újakat, vagy importálj csv fájlból.
	       				</div>
					</div>
					<?php } ?>

					<?php if($_GET && isset($_GET['importmsg'])){ ?>
						
						<?php if( $_GET['importmsg'] == 'err'){ ?>
							<div class="alert alert-danger" role="alert">
			       				<div style="display: inline; padding-left:10px;">
			       					<b>Hopp</b>, valami nem sikerült. :) Ellenőrizd a feltöltött fájt és próbáld újra. 
			       				</div>
							</div>
						<?php } ?>

						<?php if( (int)$_GET['importmsg'] > 0 ){ ?>
							<div class="alert alert-success" role="alert">
			       				<div style="display: inline; padding-left:10px;">
			       					<b>Rendben</b>, <?php echo (int)$_GET['importmsg'] ?> darab időpont lett létrehozva.
			       				</div>
							</div>
						<?php } ?>

					<?php } ?>

					

	           		 <div class="alert alert-info" role="alert">
	           		 	<i class="fa fa-info-circle"></i>
	       				<div style="display: inline; padding-left:10px;">
	       					<strong>Nyerő időpontok:</strong> Speciális játék típus esetén használható funkció. A feltöltött nyereményekhez beállíthatsz 1-1 véletlenszerű időpontot. A megadott időpont után játszó első felhasználó automatikusan megnyeri a nyereményt. Ilyen típusú játékot így állíthatsz be egyszerűen: <br>
	       					<ul> 
	       						<li>A <a href="<?php echo base_url('admin/prizes/crud'); ?>" target="_blank">nyeremények beállításainál</a> minden nyereményhez add meg a megnyerhető darabszámot. A nyeremények státuszát állítsd aktívra.</li>
	       						<li>Állítsd be a játék kezdő és vége dátumát a <a href="<?php echo base_url('admin/setup/edit/1') ?>" target="_blank">Beállítások</a> oldalon.</li>
	       						<li>Szintén a <a href="<?php echo base_url('admin/setup/edit/1') ?>" target="_blank">Beállítások</a> oldalon add meg a felhasználónak kimenő e-mail értesítés tárgyát és szövegét.</li>
	       						
	       						<?php /*<li>Kattints a <strong>Nyerő időpontok létrehozása</strong> gombra. Ezzel automatikusan létrejönnek az időpontok, minden aktív nyereményhez annyi, ahány darab megnyerhető. Minden időpont véletlenszerűen jön létre a játék kezdő és vége időpont között, illetve minden időpont egy nyereményhez is lesz társítva.</li>*/ ?>

	       						<?php /*<li>Figyelj rá, hogy ezután ha változik a játék kezdő, vagy vége időpontja, esetleg a nyeremények darabszáma, az időpontok nem frissülnek automatikusan. Ha a játék indulása előtt változnak ezek, töröld ki a generált időpontokat, és hozz létre újakat.  </li>*/ ?>

	       						<li>
	       							Célszerű összeállítani, hogy milyen és mennyi nyerőidőpontot szeretnél beállítani, valamint hogy milyen és mennyi nyereményt szeretnél azokhoz rendelni. A nyerőidőpontokat és a hozzájuk tartozó nyeremény id-kat (ami a Nyeremények oldalon található lista utolsó oszlopában fellelhető) egy csv fájl segítségével töltheted fel az alkalmazásba. Egy minta fájlt <a href="https://calendarapp.thepromobuilder.com/tutorial/csv_minta.csv" target="_blank">itt</a> találsz. 
	       						</li>

	       						<li>Figyelj rá, hogy ezután ha változik a játék kezdő, vagy vége időpontja, esetleg a nyeremények darabszáma, az időpontok nem frissülnek automatikusan. Ha a játék indulása előtt változnak ezek, töröld ki a generált időpontokat, és töltsd fel újra a megfelelő listát.  </li>

	       						<li>Egy felhasználó naponta legfeljebb egyszer nyerhet, de a további játéknapokon továbbra is van lehetősége nyerni.</li>
	       						<li>Ha van olyan nyeremény, amihez nem szeretnél időpontot társítani (pl. a játék végén sorsolt főnyeremény), annak a darabszámát állítsd nullára.</li>
	       						<li>A játék nap létrehozásnál válaszd a nyerő időpont típust és add meg, milyen üzenetet kapjon a nyertes felhasználó. Aki épp nem nyer, a "Tényleges tartalom" mezőben megadott tartalmat látja.   </li>
	       					</ul>
	       				</div>
					</div>
					<?php /*<div class="panel panel-default sidebar">
					    <div class="panel-heading">
					        <h3 class="panel-title">
					        	Nyerő időpontok létrehozása
					        </h3>
					    </div>
					    <div class="panel-body">
					       
					       <div style="margin-bottom: 20px;">
					       		Jelenleg összesen <strong><?php echo $all_time; ?></strong> darab időpont van feltöltve.
					       </div>
							
						   <?php if($all_time == 0): ?>
					       <button class="btn btn-info" onclick="generate_wintimes();">
					       		Nyerő időpontok létrehozása
					       </button> <?php endif; ?>

					       <?php if($all_time > 0): ?>
					       <button class="btn btn-danger" onclick="delete_wintimes();">
					       		Nyerő időpontok törlése
					       </button> <?php endif; ?>

					    </div>
					</div>*/ ?>

					<div class="panel panel-default sidebar">
					    <div class="panel-heading">
					        <h3 class="panel-title">
					        	Nyerő időpontok importálása
					        </h3>
					    </div>
					    <div class="panel-body">
							
						   <?php if($all_time == 0): ?>
						       
						       <div class="alert alert-info" role="alert">
						       		<div style="margin-bottom: 10px;">
						       			<b>
						       				<?php /*Ha külső forrásból szeretnél időpontokat importálni, töltsd fel itt CSV formátumban.*/ ?> Információk a fájl felépítéséről:
						       			</b>
						       		</div>
						       		<ul>
						       			<li>A fájl kódolása UFT-8 legyen</li>
						       			<li>Az egyes mezőket pontosvesszővel válaszd el.</li>
						       			<li>Egy sorba egy időpontot írj.</li>
						       			<li>
						       				Az oszlopok ebben a sorrendben legyenek: időpont (1970-01-01 00:00:00 formátumban); Nyeremény azonosító <br>
						       				Egy minta fájlt <a href="https://calendarapp.thepromobuilder.com/tutorial/csv_minta.csv" target="_blank">itt</a> találsz.
						       			</li>
						       			<li>A nyeremény azonosítót a nyeremények menüpontban találod</li>
						       		</ul>
						       </div>
						       <form action="<?php echo base_url('admin/wintimes/import') ?>" method="post" enctype="multipart/form-data">
						       		
						       		<div style="margin-bottom: 20px;">
						       			<input type="file" name="csv">
						       		</div>
						       		<div>
						       			<input type="submit" class="btn btn-default" value="Nyerő időpontok importálása">
						       		</div>

						       </form>
						       
					   		<?php endif; ?>

					       <?php if($all_time > 0): ?>
					       	<p>Jelenleg vannak nyerő időpontok létrehozva. Importálás előtt töröld ezeket.</p>
					       <?php endif; ?>

					    </div>
					</div>
	           	</div>
	           	
           </div> <?php } ?>

            <div class="row">
                <div class="col-lg-10">

                    <?php if ($curr_id > 0) { ?>

                        <ul class="nav nav-tabs" role="tablist">
                            <?php /*<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Alap beállítások</a></li>*/ ?>
                            <?php /* if( $curr_id == 2 ){ ?>
                              <li role="presentation"><a href="#caf_gal" aria-controls="caf_gal" role="tab" data-toggle="tab">Galériák</a></li>
                              <?php } */ ?>
                        </ul>


                        <div class="tab-content" style="border: 1px solid #ddd; border-top: 0;">

                            <div role="tabpanel" class="tab-pane active" id="home">
                                <?php
                                echo $crud_output->output;
                                ?>
                            </div>

                            <?php if (0) { ?>
                                <div role="tabpanel" class="tab-pane" id="caf_gal">
                                    <div style="padding: 10px;">

                                    </div>
                                </div>
                            <?php } ?>

                        </div>

                    <?php } else {
                        ?> 
							<div class="crud-list-container">
								<?php echo $crud_output->output; ?>
							</div>
                        <?php
                    } ?>


                </div>

                
            </div>

        </div>
    </div>
</div>