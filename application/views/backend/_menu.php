 <!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo base_url('admin'); ?>">
        	<?php echo $page_name; ?>
        </a>
    </div>

    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav"> 
    	<li>
    		<a href="https://calendarapp.thepromobuilder.com/tutorial/calendar_user_guide_v1.pdf" target="_blank">
    			Segítség &raquo;
    		</a>
    	</li>
    	<li>
            <a href="<?php echo base_url(); ?>" target="_blank"><i class="fa fa-eye fa-fw"></i> <?php echo lang('page_topnav_visit_site'); ?></a>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo trim(strip_tags($this->session->userdata('admin_name'))); ?> <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="<?php echo base_url('admin/my-profile/edit/' . $this->session->userdata('id')); ?>"><i class="fa fa-fw fa-user"></i> <?php echo lang('page_topnav_myprofile'); ?></a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="<?php echo base_url('admin/logout'); ?>"><i class="fa fa-fw fa-power-off"></i> <?php echo lang('page_menu_logout'); ?></a>
                </li>
            </ul>
        </li>
    </ul>

    <!-- Sidebar Menu Items -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <?php 
            	echo $menu_items_html;
            ?>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>