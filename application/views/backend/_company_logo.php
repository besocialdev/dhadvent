<?php
	if( $frontend_settings['default_logo'] != '' ){
		if( file_exists( FCPATH . 'assets'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'files' . DIRECTORY_SEPARATOR . $frontend_settings['default_logo'] ) ){
			?>
				<a href="<?php echo base_url(); ?>" target="_blank" class="mainlogo">
					<img src="<?php echo base_url('assets/uploads/files/' . $frontend_settings['default_logo']); ?>" style="max-width:110px; position: absolute; right:0;">
				</a>
			<?php
		}
	}
?>