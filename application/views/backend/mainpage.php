<div id="wrapper" class="mainpage">
   	<?php
    	echo $backend_menu;
    ?>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-lg-12">
                    <h1 class="page-header" style="position: relative;">
						<?php echo htmlspecialchars($frontend_settings['page_name']); ?>
						<?php $this->load->view('backend/_company_logo'); ?>
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url('admin') ?>"><?php echo lang('dashboard'); ?></a>
                        </li>
                    </ol>
                </div>
            </div>

            <?php if( (int)$frontend_settings['site_status'] == 0 ){ ?>
				<div class="alert alert-danger" role="alert">
					<strong>Figyelem!</strong> A karbantartás funkció jelenleg aktív, az oldalt csak adminisztrátorok láthatják. <a href="<?php echo base_url('admin/setup/edit/1'); ?>">Beállítások</a>
				</div>
            <?php } ?>

            <!--
            <?php if( $project_info['data']['status'] != 'ongoing' || (int)$project_info['data']['days_total'] <=0 ){ ?>
				<div class="alert alert-danger" role="alert">
					<strong>Figyelem!</strong> A játékot a felhasználók csak a promóciós időszak elindítása után láthatják. Adminisztrátorként bejelentkezve a játék felület elérhető marad. A játék időszak elindításához kattints <a href="<?php echo base_url('admin/activate'); ?>">ide</a>.
				</div>
            <?php } ?>
            -->

            <div class="alert alert-danger" role="alert">
                <h4 style="font-weight: normal; margin-top: 0px;">Figyelem!</h4><span style="font-size: 13px; font-weight: normal;">Amíg az adminisztrációs felületen be van jelentkezve, addig a felhasználói oldalon aktívként látható a játék, akkor is ha az nem aktív állapotban van. Ellenőrzéshez érdemes inkognitó módban tesztelni, vagy egy másik olyan böngészőben ahol nincs bejelentkezve az adminisztrációs felületre.</span>
            </div>


            <div class="alert alert-info" role="alert">
                <div class="mode-info-table-title"><h4>Jelenlegi játéktípus: <strong><?=(true===$this->frontend_settings['login_required'] ? 'Bejelenthezéshez és regisztrációhoz kötött játékmód' : 'Vendég játékmód' ); ?></strong></h4><span style="margin-left: 10px;" class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></div>
                <div class="mode-info-table">
                    <div class="alert alert-warning" role="alert">A játékmód típusát a <a href="<?php echo base_url('admin/setup/edit/1'); ?>" class="alert-link">beállítások</a> között tudja megváltoztatni.</div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Funkciók</th>
                                <th>Bejelentkezéshez kötött mód<?=(true===$this->frontend_settings['login_required'] ? '<span class="checked_symbol">&check;</span>' : '' ); ?></th>
                                <th>Vendég játékmód<?=(true===$this->frontend_settings['login_required'] ? '' : '<span class="checked_symbol">&check;</span>' ); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <td>Oldal beállítások</td>
                                <td><span class="checked_symbol">&check;</span></td>
                                <td><span class="checked_symbol">&check;</span></td>
                            </tr>
                            <tr>
                            <td>Nyeremények blokk létrehozás</td>
                                <td><span class="checked_symbol">&check;</span></td>
                                <td><span class="checked_symbol">&check;</span></td>
                            </tr>
                            <tr>
                            <td>Játékos regisztráció / bejelentkezés</td>
                                <td><span class="checked_symbol">&check;</span></td>
                                <td><span class="cross_symbol">&cross;</span></td>
                            </tr>
                            <tr>
                            <td>Játékosok menüpont</td>
                                <td><span class="checked_symbol">&check;</span></td>
                                <td><span class="cross_symbol">&cross;</span></td>
                            </tr>
                            <tr>
                                <td>Játékok menüpont</td>
                                <td><span class="checked_symbol">&check;</span></td>
                                <td><span class="cross_symbol">&cross;</span></td>
                            </tr>
                            <tr>
                                <td>Játéknapok menüpont</td>
                                <td><span class="checked_symbol">&check;</span></td>
                                <td><span class="checked_symbol">&check;</span></td>
                            </tr>
                            <tr>
                                <td>Játéknap típus: Szöveges képes tartalom</td>
                                <td><span class="checked_symbol">&check;</span></td>
                                <td><span class="checked_symbol">&check;</span></td>
                            </tr>
                            <tr>
                                <td>Játéknap típus: Youtube videó</td>
                                <td><span class="checked_symbol">&check;</span></td>
                                <td><span class="checked_symbol">&check;</span></td>
                            </tr>
                            <tr>
                                <td>Játéknap típus: Puzzle</td>
                                <td><span class="checked_symbol">&check;</span></td>
                                <td><span class="checked_symbol">&check;</span></td>
                            </tr>
                            <tr>
                                <td>Játéknap típus: Kvíz</td>
                                <td><span class="checked_symbol">&check;</span></td>
                                <td><span class="cross_symbol">&cross;</span></td>
                            </tr>
                            <tr>
                                <td>Kuponok használata</td>
                                <td><span class="checked_symbol">&check;</span></td>
                                <td><span class="cross_symbol">&cross;</span></td>
                            </tr>


                        </tbody>
                        </table>
                    </div>
                </div>
            </div>


            <div class="alert alert-info" role="alert">
                
                    <!-- aktiválás blokk -> core/MCMS_controller.php -->
                    <div class="activate_message" style="margin-bottom: 10px;">
                        <?php 
                            echo $message_main; // ennek mindig lennie kell, a többi opcionális
                            echo (!empty($message_valid_from) ? $message_valid_from : '' );
                            echo (!empty($message_days_total) ? $message_days_total : '' );
                            echo (!empty($message_days_available) ? $message_days_available : '' );
                        ?>
                    </div>
                    
                    A játék állapotának módosításához <a href="<?php echo base_url('admin/activate'); ?>">kattintson ide</a>
            
                    <?php //echo (!empty($game_status) ? $game_status : ''); ?>
                    <!--
				<b>Játék kezdő időpont:</b> <?php echo $frontend_settings['game_start_date'] . ' ' . $frontend_settings['game_start_time']; ?> <br>
				<b>Játék vége időpont:</b> <?php echo $frontend_settings['game_end_date'] . ' ' . $frontend_settings['game_end_time']; ?> 
                    -->
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-file fa-5x"></i>
                                </div>
                            	<div class="col-xs-9 text-right">
                                    <div>
                                    	<a style="color:#fff;" href="<?php echo base_url( $this->config->item('admin_menu_items')['pages']['slug'] ); ?>">
                                    		<strong><?php echo lang('page_sidebar_sites'); ?></strong>
                                    	</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="<?php echo base_url( $this->config->item('admin_menu_items')['pages']['slug'] ); ?>">
                            <div class="panel-footer">
                                <span class="pull-left"><?php echo lang('all'); ?></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-fw fa-user fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>
                                    	<a style="color:#fff;" href="<?php echo base_url( 'admin/users/crud' ); ?>">
                                    		<strong>Játékosok</strong>
                                    	</a>
                                        <?php if($frontend_settings["login_required"]): ?>
                                            <div class="mainpage_registered_players">Regisztrált játékosok száma: <b><?php echo (!empty($users_count) ? $users_count : 0 ); ?> fő</b></div>
                                        <?php else: ?>
                                            <div class="mainpage_registered_players">a játék jelenleg "Vendég" módban fut</div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="<?php echo base_url( 'admin/users/crud' ); ?>">
                            <div class="panel-footer">
                                <span class="pull-left"><?php echo lang('all'); ?></span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>


                <div class="col-lg-4 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-fw fa-cog fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div>
                                    	<a style="color:#fff;" href="<?php echo base_url('admin/setup/edit/1'); ?>">
                                    		<strong>Játék beállítások</strong>
                                    	</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="<?php echo base_url('admin/setup/edit/1'); ?>">
                            <div class="panel-footer">
                                <span class="pull-left">Játék beállítások</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                
            </div>
            
            <?php if($frontend_settings["login_required"]): ?>
            <div class="row">
                <div class="col-lg-12">
                   <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-user"></i> Regisztráció statisztika</h3>
                        </div>
                        <div class="panel-body">
							<div id="regstat"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                   <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-user"></i> Legutóbbi regisztrációk</h3>
                        </div>
                        <div class="panel-body">
                           <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Név</th>
                                        <th>E-mail cím</th>
                                        <th>Regisztráció dátuma</th>
                                        <th>Részletek</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   <?php 
                                    	if( is_array($widget1) )
						            	{
						            		foreach ($widget1 as $id => $widget1) 
						            		{
						            			echo $widget1['html'];
						            		}
						            	}
                                    ?>
                                </tbody>
                            </table>
                            <div class="text-right">
                                <a href="<?php echo base_url( 'admin/users/crud' ); ?>">
                                	<?php echo lang('all'); ?> <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <?php endif; // $frontend_settings["login_required"] ?>
            
        </div>
    </div>
</div> 
<script type="text/javascript">
	/*var chartdata = [
		{ day: '2008', value: 20 },
		{ day: '2009', value: 10 },
		{ day: '2010', value: 5 },
		{ day: '2011', value: 5 },
		{ day: '2012', value: 20 }
	];*/

	var chartdata = <?php echo $chart_reg; ?>;
</script>