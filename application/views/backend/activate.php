<div id="wrapper" class="activate">
    <?php
    echo $backend_menu;
    ?>
    <div id="page-wrapper">
        <div class="container-fluid">
            
            <!-- kezdőlap blokk -->
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-lg-12">
                    <h1 class="page-header" style="position: relative;">
                        <?php echo (isset($title) ? $title : ''); ?>
                        <?php $this->load->view('backend/_company_logo'); ?>
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url('admin') ?>"><?php echo lang('dashboard'); ?></a>
                        </li>
                    </ol>
                </div>
            </div>

            <!-- infó blokk -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-info" role="alert">
                        <i class="fa fa-info-circle"></i>
                        <div style="display: inline; padding-left:10px;">
                            <strong>Játék aktiválása:</strong> Itt állítsd be a játékhoz megvásárolt játék napok kezdő dátumát. A játék a megadott napon indul el. A lejárat előtt értesítést küldünk a játék állapotáról.
                        </div>
                    </div>
                </div>
            </div> 

            <!-- aktiválás blokk -> core/MCMS_controller.php -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <?php echo (isset($title) ? $title : "Rendszerüzenet"); ?>
                            </h3>
                        </div>
                        <div class="panel-body">

                            <div class="activate_message" style="margin-bottom: 10px;">
                                <?php 
                                    echo $message_main; // ennek mindig lennie kell, a többi opcionális
                                    echo (!empty($message_valid_from) ? $message_valid_from : '' );
                                    echo (!empty($message_days_total) ? $message_days_total : '' );
                                    echo (!empty($message_days_available) ? $message_days_available : '' );
                                ?>
                            </div>
                            
                            <?php (!empty($form_type) ? $this->load->view("backend/activate/".$form_type) : ''); ?>

                        </div>
                    </div>
                </div>
            </div>            

        </div>
    </div>
</div>