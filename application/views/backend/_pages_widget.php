<div class="panel panel-default sidebar">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> <?php echo lang('info'); ?></h3>
    </div>
    <div class="panel-body">
       <ul>
       		<li>
       			<b><?php echo lang('create_date'); ?>: </b> <br><?php echo $w_create_date; ?>
       		</li>
       </ul>
    </div>
</div>

<?php if($curr_id > 0){ ?>
<input type="hidden" value="<?php echo $curr_id; ?>" id="curr_id">
<div class="panel panel-default sidebar">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> <?php echo lang('func'); ?></h3>
    </div>
    <div class="panel-body">
       <ul>
       		<li id="live-url">
       			<a data-curr-id="<?php echo $curr_id; ?>" href="javascript:;" target="_blank"><?php echo lang('live_url'); ?></a>
       		</li> 
       </ul>
    </div>
</div>
<?php } ?>