<div id="wrapper" class="cms-pages">
    
    <?php
		echo $backend_menu;
    ?>

    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header" style="position: relative;">
                        Hopp
						<?php $this->load->view('_company_logo'); ?>
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url('admin'); ?>"><?php echo lang('dashboard'); ?></a>
                        </li>
                    </ol>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-10">
                  <p style="margin:20px 0;"><b>Az oldal megtekintéséhez nincs megfelelő jogosultság.</b></p>
                </div>
            </div>

        </div>
    </div>
</div>