<div class="panel panel-default sidebar">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-long-arrow-right fa-fw"></i> <?php echo lang('info'); ?></h3>
    </div>
    <div class="panel-body">
       <ul>
       		<li>
       			<b><?php echo lang('date_reg'); ?>: </b> <br><?php echo $w_create_date; ?>
       		</li>
       </ul>
    </div>
</div>