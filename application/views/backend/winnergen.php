<div id="wrapper" class="cms-pages">

    <?php
    echo $backend_menu;
    ?>

    <div id="page-wrapper">
        <div class="container-fluid">

            <div class="row" style="margin-bottom: 20px;">
                <div class="col-lg-12">
                    <h1 class="page-header" style="position: relative;">
                        <?php echo $pagetitle; ?>
                        <?php $this->load->view('backend/_company_logo'); ?>
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url('admin'); ?>"><?php echo lang('dashboard'); ?></a>
                        </li>
                        <li class="active">
                            <i class="fa fa-trophy"></i> <?php echo lang('page_sidebar_winnergen'); ?></a>
                        </li>
                    </ol>
                </div>
            </div>

            <?php if(1){ ?>
           <div class="row">
	           	<div class="col-lg-10">

	           		<?php if($_GET && isset($_GET['genmsg'])){ ?>
					<div class="alert alert-success" role="alert">
	       				<div style="display: inline; padding-left:10px;">
	       					<b>Rendben</b>, <?php echo (int)$_GET['genmsg'] ?> nyertest sorsoltunk.
	       				</div>
					</div>
					<?php } ?>

	           		 <div class="alert alert-info" role="alert">
	           		 	<i class="fa fa-info-circle"></i>
	       				<div style="display: inline; padding-left:10px;">
	       					<strong>Nyertes sorsolás:</strong> A regisztrált, aktív játékosok közül tudsz megadott számú nyertest sorsolni. Add meg, hogy hány nyertest szeretnél kisorsolni, majd kattints a <b>Nyertes sorsolás</b> gombra. Minden sorsolás felülírja az azt megelőzőt, ne félj használni az export lehetőséget. :) A sorsolás így működik: <br>
	       					<ul>
	       						<li>Csak az aktív felhasználók között sorsol.</li>
	       						<li>Minden regisztrált felhasználó azonos eséllyel lesz kisorsolva.</li>
	       						<li>Egy felhasználó maximum egyszer lesz kisorsolva, függetlenül attól, hány játék napon játszott.</li>
	       					</ul>
	       					Ha ettől eltérő sorsolásra van szükség, válaszd az export lehetőséget.
	       				</div>
					</div>

					<div class="panel panel-default sidebar">
					    <div class="panel-heading">
					        <h3 class="panel-title">
					        	Nyertes sorsolás
					        </h3>
					    </div>
                        <div class="panel-body">
                            <div style="margin-bottom: 15px;">
                                <label>
                                    Nyertesek száma:
                                </label> <br>
                                <input class="form-control" type="text" name="winner_nr" id="winner_nr" value="1" maxlength="10">
                            </div>
                            <div style="margin-bottom: 15px;">
                                <label>
                                    Sorsolás típusa:
                                </label> <br>
                                <select name="weighted" class="form-control">
                                    <option value="igen">Súlyozott (játékok száma és hírlevélfeliratkozás)</option>
                                    <option value="nem">Mindenki egyszer</option>
                                </select>
                            </div>
                            <div>
                                <button class="btn btn-info" onclick="gen_winners();">
                                    Nyertes sorsolás
                               </button>
                            </div>
                        </div>
					</div>
	           	</div>

           </div> <?php } ?>

            <div class="row">
                <div class="col-lg-10">

                    <?php if (1) { ?>

                        <ul class="nav nav-tabs" role="tablist">
                            <?php /*<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Alap beállítások</a></li>*/ ?>
                            <?php /* if( $curr_id == 2 ){ ?>
                              <li role="presentation"><a href="#caf_gal" aria-controls="caf_gal" role="tab" data-toggle="tab">Galériák</a></li>
                              <?php } */ ?>
                        </ul>


                        <div class="tab-content" style="border: 1px solid #ddd; border-top: 0;">

                            <div role="tabpanel" class="tab-pane active" id="home">
                                <?php
                                echo $crud_output->output;
                                ?>
                            </div>

                            <?php if (0) { ?>
                                <div role="tabpanel" class="tab-pane" id="caf_gal">
                                    <div style="padding: 10px;">

                                    </div>
                                </div>
                            <?php } ?>

                        </div>

                    <?php } else {
                        ?>
							<div class="crud-list-container">
								<?php echo $crud_output->output; ?>
							</div>
                        <?php
                    } ?>


                </div>
            </div>

        </div>
    </div>
</div>