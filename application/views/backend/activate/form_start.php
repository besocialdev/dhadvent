<form method="post" id="activate_form" action="<?php echo base_url('admin/activate/status') ?>" onsubmit="return confirm('Játék időszak indítása?');">
    <div class="" style="margin-bottom: 20px; display: inline-block;">
        <label>Kezdő dátum</label> <br>
        <input type="text" class=" form-control my_datepicker" name="game_start_date" placeholder="Kezdő dátum kiválasztása" value="<?php echo date('Y-m-d'); ?>">
    </div>
    <div class="" style="margin-bottom: 20px; display: inline-block;">
        <label>Befejezés dátuma</label> <br>
        <input type="text" class=" form-control my_datepicker" name="game_end_date" data-days_available="<?php echo (isset($days_available) ? $days_available : 0) ?>" placeholder="Utolsó játéknap kiválasztása" value="<?php echo date('Y-m-d'); ?>">
    </div><br>
    <div>
        <input type="hidden" name="action" value="start" />
        <input type="submit" class="btn btn-success" value="Aktiválás">
    </div>
</form>

