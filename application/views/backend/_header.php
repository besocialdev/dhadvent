<!DOCTYPE html>
<html lang="<?php echo lang('page_language_code'); ?>">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php echo htmlspecialchars($frontend_settings['page_name']); ?>">
	<title><?php echo htmlspecialchars($frontend_settings['page_name']); ?></title>
	<link href="<?php echo base_url('assets/static/vendors/jquery-ui/jquery-ui.min.css'); ?>" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&display=swap&subset=latin-ext" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
	
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url("favicons"); ?>/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url("favicons"); ?>/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url("favicons"); ?>/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url("favicons"); ?>/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url("favicons"); ?>/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url("favicons"); ?>/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url("favicons"); ?>/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url("favicons"); ?>/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url("favicons"); ?>/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url("favicons"); ?>/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url("favicons"); ?>/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url("favicons"); ?>/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url("favicons"); ?>/favicon-16x16.png">
	<link rel="manifest" href="<?php echo base_url("favicons"); ?>/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo base_url("favicons"); ?>/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	
	<?php 
		if( isset($crud_output->css_files) )
		{
			foreach($crud_output->css_files as $crud_css_file)
			{
				?>
				<link type="text/css" rel="stylesheet" href="<?php echo $crud_css_file; ?>" />
				<?php
			}
		}
	?>
	
	<link href="<?php echo base_url('assets/static/vendors/colorpicker/spectrum.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/static/backend/css/bootstrap.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/static/backend/css/sb-admin.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/static/backend/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/static/backend/css/cms-admin.css'); ?>" rel="stylesheet">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<script type="text/javascript">
		var js_vars = {
			'base_url': '<?php echo $this->config->item('base_url'); ?>',
			'upload_path': '<?php echo $this->config->item('backend__upload_path'); ?>',
			'fileupload_slug': '<?php echo $this->config->item('backend__fileupload_slug'); ?>'
		};
		var imageupdate  = 0;
	</script>
</head>
<body>