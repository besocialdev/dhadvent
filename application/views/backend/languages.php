<div id="wrapper">
    
    <?php
		echo $backend_menu;
    ?>

    <div id="page-wrapper">
        <div class="container-fluid">

           <div class="row" style="margin-bottom: 20px;">
                <div class="col-lg-12">
                    <h1 class="page-header" style="position: relative;">
                        <?php echo $pagetitle; ?>
                        <?php $this->load->view('backend/_company_logo'); ?>
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url('admin'); ?>"><?php echo lang('dashboard'); ?></a>
                        </li>
                        <li class="active">
                           Social beállítások
                        </li>
                    </ol>
                </div>
            </div>

            <div class="row">
	           	<div class="col-lg-10">
	           		 <div class="alert alert-info" role="alert">
	           		 	<i class="fa fa-info-circle"></i>
	       				<div style="display: inline; padding-left:10px;">
	       					<strong>Social beállítások:</strong> Itt állíthatsz be alapértelmezett Facebook megosztás szöveget és képet, illetve a keresőknek (pl. google) szóló úgynevezett meta adatokat. Ezek a beállítások a publikus felület minden oldalára vonatkoznak, de ha szeretnél, minden oldalnak külön-külön is megadhatod ezeket az <b><i>oldalak</i></b> menüpontban.
	       				</div>
					</div>
	           	</div>
           </div>

           <?php /* <div class="row">
                <div class="col-lg-10">
                  <?php
                  	echo $crud_output->output;
                  ?>
                </div>
            </div>*/ ?>



             <div class="row">
                <div class="col-lg-10">

                    <?php if (1) { ?>

                        <ul class="nav nav-tabs" role="tablist">
                           
                        </ul>


                        <div class="tab-content" style="border: 1px solid #ddd; border-top: 0;">

                            <div role="tabpanel" class="tab-pane active" id="home">
                                <?php
                                echo $crud_output->output;
                                ?>
                            </div>

                            <?php if (0) { ?>
                                <div role="tabpanel" class="tab-pane" id="caf_gal">
                                    <div style="padding: 10px;">

                                    </div>
                                </div>
                            <?php } ?>

                        </div>

                    <?php } else {
                        ?> 
							<div class="crud-list-container">
								<?php echo $crud_output->output; ?>
							</div>
                        <?php
                    } ?>



                </div>

            </div>

        </div>
    </div>
</div>