<div class="cookieModal hidden">
    <input type="hidden" name="cookie_level" id="cookie_level" value="3">
    <div class="table">
        <div class="cell">
            <div class="panel">
                <h2>A weboldalon használt ún. sütikkel (cookie-kal) kapcsolatos információk</h2>
                <p>
                	{{cpm_modal_infotext}}
                </p>
                <div class="buttons">
                    <div class="accept accept_cookie button">Elfogadom a sütik használatát</div>
                    <div class="link settings">Süti beállítások megjelenítése</div>
                    <div class="clear"></div><a class="link" href="{{cookie_pdf}}" target="_blank" title="">Süti tájékoztató letöltése (.pdf)</a></div>
            </div>
            <div class="panel settingsPanel">
                <h2>A weboldalon használt ún. sütik (cookie-k) beállítása</h2>
                <p>
                	{{cpm_modal_infotext}}
                </p>
                <div class="separator"></div>
                <div class="cookieRange clearfix">
                    <div class="col-6 toLeft">
                        <div class="theSlider"></div>
                        <div class="withPadding">
                            <div class="theSliderMobile"></div>
                        </div>
                        <div class="ranges">
                            <div class="txt top">Alap működés</div>
                            <div class="txt middle">Statisztikai sütik</div>
                            <div class="txt bottom">Statisztikai és reklám célú sütik</div>
                        </div>
                        <div class="accept accept_cookie button">Kiválasztott sütik használatát elfogadom</div>
                    </div>
                    <div class="col-6 toLeft">
                        <div class="steps ">
                            {{cpm_level1}}
                            <a class="link" href="{{cookie_pdf}}" target="_blank" title="">Süti tájékoztató letöltése (.pdf)</a></div>
                        <div class="steps">
                            {{cpm_level2}}
                            <a class="link" href="{{cookie_pdf}}" target="_blank" title="">Süti tájékoztató letöltése (.pdf)</a></div>
                        <div class="steps visible">
                            {{cpm_level3}}
                            <a class="link" href="{{cookie_pdf}}" target="_blank" title="">Süti tájékoztató letöltése (.pdf)</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>