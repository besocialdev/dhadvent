<?php defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->view("backend/_header");
$this->load->view("backend/" . $current_content);
$this->load->view("backend/_footer");

?>