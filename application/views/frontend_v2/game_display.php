<section class="game-display featured featured--bg2 align-center fixed-header--double sm-fade-in">
    <div class="container">
        <div class="jumbotron">
            <h2 class="main_title">
                <?php echo strip_tags($cmsdata['title']); ?>
            </h2>
            <p class="lead">
               <?php echo strip_tags($cmsdata['lead']); ?>
            </p>
        </div>
    </div>
</section>
<section class="game-container sm-fade-in">
    <div class="container">
        <div class="row calendar_here"></div>
    </div>
</section>
