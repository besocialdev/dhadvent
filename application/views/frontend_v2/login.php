<section class="cms-content fixed-header--double sm-fade-in">
    <div class="container">
    	<div class="form-content2">
	        <h1 class="form-title"><?php echo $cmsdata['title']; ?></h1>
	        <form action="" method="post" novalidate="" class="loginform">
	            <?php echo (!$success)?"<div class='form_error2'>". lang('LOGIN_ERROR') ."</div>":"";?>

	            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
					
				<div class="frm-row">
					<label for="email_address"><?php echo lang('FORM_EMAIL');?></label><br />
					<input class="form-input" type="email" id="email_address" name="email_address" value="<?php echo $this->input->post("email_address")?>" required="" />
				</div>

				<div class="frm-row">
					<label for="password"><?php echo lang('FORM_PASSWORD');?></label><br />
					<input class="form-input" type="password" id="password" name="password" value="" required="" />
				</div>
				
				<div class="frm-row">
					<input class="btn btn-def btn-inv" type="submit" value="<?php echo lang('LOGIN_BTN'); ?>">
	            </div>

	            <a href="<?php echo base_url('elfelejtett-jelszo'); ?>"><?php echo lang('FORGOTTEN_PWD'); ?></a><br />
	            <a href="<?php echo base_url('fb-login'); ?>"><?php echo lang('LOGIN_WITH_FB'); ?></a>
	        </form>
        </div>
    </div>
</section>