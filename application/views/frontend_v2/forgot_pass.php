<section class="cms-content fixed-header--double sm-fade-in">
    <div class="container">
    	<div class="form-content2">
    		
    		<h1 class="form-title"> <?php echo $cmsdata['title']; ?> </h1>
	        <form action="" method="post" novalidate="">
	            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo  $this->security->get_csrf_hash();?>">
                    <input style="display: none !important;" type="hidden" name="becaptcha" value="">

				<div class="frm-row">
					<?php if(form_error("email_address")): ?><span style="display:block;" class="form_error form_error2"><?php echo lang('INVALID_EMAIL');?></span><?php endif; ?>
					<label for="email_address"><?php echo lang('FORM_EMAIL'); ?></label><br />
	           		<input class="form-input" type="email" name="email_address" id="email_address" value="<?php echo $this->input->post("email_address");?>" required="" />
				</div>

				<div class="frm-row">
					<input class="btn btn-def btn-inv" type="submit" value="<?php echo lang('NEXT_STEP'); ?>">
				</div>
	        </form>
    	</div>
    </div>
</section>