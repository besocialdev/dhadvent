<section class="cms-content fixed-header--double sm-fade-in ">
    <div class="container">
    	<div class="form-content2">
    		<h1 class="form-title"><?php echo $type=="datamod"?lang('MODIFY_ACCOUNT'):lang('CREATE_ACCOUNT')?></h1>
	        <form action="" method="post" novalidate="">
	            <input type="hidden" name="<?php echo  $this->security->get_csrf_token_name();?>" value="<?php echo  $this->security->get_csrf_hash();?>">
                    <input style="display: none !important;" type="hidden" name="becaptcha" value="">
	            
	            <div class="frm-row">
	            	
		            <label for="email_address"><?php echo lang('FORM_EMAIL');?></label><br />
		            <input class="form-input" type="email" id="email_address" name="email_address" value="<?php echo $user_data["email_address"]?>" required="" />
		            <?php if(form_error("email_address")): ?><span class="form_error2"><?php echo form_error("email_address");?></span><?php endif;?>
	            </div>

	            <div class="frm-row">
	            	
		            <label for="last_name"><?php echo lang('FORM_LASTNAME');?></label><br />
		            <input class="form-input" type="text" name="last_name" id="last_name" value="<?php echo $user_data["last_name"]?>" required="" />
		            <?php if(form_error("last_name")): ?><span class="form_error2"><?php echo form_error("last_name");?></span><?php endif;?>
	            </div>

	            <div class="frm-row">
	            	 
		            <label for="first_name"><?php echo lang('FORM_FIRSTNAME');?></label><br />
		            <input class="form-input" type="text" name="first_name" id="first_name" value="<?php echo $user_data["first_name"]?>" required="" />
		            <?php if(form_error("first_name")): ?><span class="form_error2"><?php echo form_error("first_name");?></span><?php endif;?>
	            </div>

	            <div class="frm-row">
	            	
		            <label for="password"><?php echo lang('FORM_PASSWORD');?></label> 
		            <?php echo $type=="datamod"?"(ha nem akarod módosítani, hagyd üresen)":""?><br />
		            <input class="form-input" type="password" name="password" id="password" />
		            <?php if(form_error("password")): ?><span class="form_error2"><?php echo form_error("password");?></span><?php endif;?>
	            </div>

	            <div class="frm-row">
	            	
		            <label for="passconf"><?php echo lang('FORM_PASSWORD_AGAIN');?></label><br />
		            <input class="form-input" type="password" name="passconf" id="passconf" />
		             <?php if(form_error("passconf")): ?><span class="form_error2"><?php echo form_error("passconf");?></span><?php endif;?>
	            </div>
                    
                    <?php if($type=="register"): ?>
                    <div class="frm-row">
                        <input type="checkbox" name="accept_tos" id="accept_tos" <?php echo $this->input->post("accept_tos") ? "checked" : ""; ?>> 
                        <label for="accept_tos">
                             <?php echo str_replace("{url_token_game_rules}", "jatekszabalyzat", lang('ACCEPT_GAME_RULES')); ?>
                        </label>
                        <?php if(form_error("accept_tos")): ?><span class="form_error2"><?php echo form_error("accept_tos"); ?></span><?php endif; ?>
                    </div>                    
                    <div class="frm-row">
                        <input type="checkbox" name="accept_privacy" id="accept_privacy" <?php echo $this->input->post("accept_privacy") ? "checked" : ""; ?>> 
                        <label for="accept_privacy">
                             <?php echo str_replace("{url_token_privacy_policy}", "adatvedelmi-nyilatkozat", lang('ACCEPT_PRIVACY_POLICY')); ?>
                        </label>
                        <?php if(form_error("accept_privacy")): ?><span class="form_error2"><?php echo form_error("accept_privacy"); ?></span><?php endif; ?>
                    </div>                    
                    <?php endif; ?>
                    
	            <div class="frm-row">
	            	<input type="checkbox" name="newsletter" id="newsletter" <?php echo (isset($user_data["newsletter"]) && $user_data["newsletter"]=='igen') ? 'checked' : ''; ?> > 
                        <label for="newsletter">
                            <?php echo lang('FORM_SUBSCRIBE_NEWSLETTER');?>
                        </label>
	            </div>

	            <div class="frm-row">
			<input class="btn btn-def btn-inv" type="submit" value="<?php echo $type=="datamod"?lang('FORM_SUBMIT_BUTTON_MODIFY_ACCOUNT'):lang('FORM_SUBMIT_BUTTON_CREATE_ACCOUNT')?>">
	            </div>
	        </form>
    	</div>
    </div>
</section>