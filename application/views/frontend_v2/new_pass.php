<section class="cms-content fixed-header--double sm-fade-in">
    <div class="container">
    	<div class="form-content2">
    		
    		<h1 class="form-title"><?php echo lang('NEW_PASS'); ?></h1>
	        <form action="" method="post" novalidate="">
	            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                    <input style="display: none !important;" type="hidden" name="becaptcha" value="">
				<div class="frm-row">
					<?php if(form_error("password")): ?><span style="display:block;" class="form_error form_error2"><?php echo form_error("password"); ?></span><?php endif; ?>
					<label for="password"><?php echo lang('PASSWORD'); ?></label><br />
	           		<input class="form-input" type="password" name="password" id="password" required="" />
				</div>


				<div class="frm-row">
					<?php if(form_error("passconf")): ?><span style="display:block;" class="form_error form_error2"><?php echo form_error("passconf");?></span><?php endif; ?>
					<label for="passconf"><?php echo lang('PASSWORD_2'); ?></label><br />
	           		<input class="form-input" type="password" name="passconf" id="passconf" required="" />
				</div>

				<div class="frm-row">
					<input class="btn btn-def btn-inv" type="submit" value="<?php echo lang('NEXT_STEP'); ?>">
				</div>
	        </form>
    	</div>
    </div>
</section>