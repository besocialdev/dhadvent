<section class="cms-content-top-section fixed-header--double sm-fade-in">
        <div class="container">
        	<div class="col-xs-12">
        		<h1><?php echo $cmsdata['title'] ?></h1>
                <?php 
                if(!empty($cmsdata['content'])):
                    echo $cmsdata['content'];
                endif; 
                ?>
        	</div>

        </div>
</section>

<section class="featured featured--with-image cms-block sm-fade-in sm-animate">
<?php 
    if( /*!empty($cmsdata['cover_image']) &&*/ ($cmsdata['cms_block_mainpage_visible']==2 || $cmsdata['cms_block_mainpage_visible']==3))
    {
        if($cmsdata['menu_slug']=='nyeremenyek' && $prizes_slider_enabled){
?>
                <section class="featured align-center sm-fade-in sm-animate swiper-container-outer<?php echo ($cms_block_type == 'image_on_left' ? ' image-on-leftside' : '' ) ?>">
                    <div class="container">
                    <div class="swiper-container" style="position: relative;">
                            <div class="swiper-wrapper">
                            <?php
                            foreach ($prizes_list as $kk => $vv) {
                            ?>
                                <div class="swiper-slide">
                                    <?php
                                        $prize_data = array(
                                            'cmsdata' => $vv
                                        );
                                        $this->load->view($settings['theme']."/cms_content/".$cms_block_type,$prize_data); 

                                    ?>
                                </div>
                            <?php
                            }
                            ?>
                            </div> <!-- .swiper-wrapper -->
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>                    

                    </div> <!-- .swiper-container -->
                    </div> <!-- .container -->
                </section>
            <?php
        }
        else 
        {
            // admin felületen lehet állítani, hogy a kép melyik oldalra kerüljön (csak CMS oldal, enum tipusú mezőben tárolódik)
            $this->load->view($settings['theme']."/cms_content/$cms_block_type"); 
        }
    }
?>
</section>