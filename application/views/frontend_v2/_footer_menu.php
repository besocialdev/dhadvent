<footer class="footer">
        <section class="footer--top">
                <div class="container">
                    
                <div class="fb-share-button" data-href="<?php echo base_url(); ?>" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url(); ?>&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Megosztom</a></div>
                
               <?php /* <?php foreach ($menu_footer as $k => $v): //$menu_header (MCMS_Controller -> construct) ?>
                    <a class="footer-menu-item footer-menu-<?php echo $v['menu_slug']; ?>" href="<?php echo $v['menu_slug']; ?>"><?php echo $v['title']; ?></a>
                <?php endforeach; ?>*/ ?>

                <?php 
                foreach ($menu_footer as $k => $v) 
                { 
                ?>
                    <a class="footer-menu-item footer-menu-<?php echo $v['menu_slug']; ?>" href="<?php echo base_url($v['menu_slug']); ?>">
                            <?php echo strip_tags(stripslashes($v['title'])); ?>
                    </a>
                <?php 
                } 
                ?>
                <!-- süti beállítások -->
                <a href="javascript:void(0);" class="cpm_modal_open">
                    <?php echo lang('cookie_settings'); ?>
                </a>
                </div>
        </section>

		<section class="footer--bottom">
		    <div class="container">
		       <?php echo lang('copy'); ?><?php date("Y"); ?> <?php echo lang('besocial'); ?>
		    </div>
		</section>
</footer>