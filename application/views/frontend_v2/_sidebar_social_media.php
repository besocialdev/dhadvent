<?php 
if( 
	strlen($settings['social_fb']) > 0 ||
	strlen($settings['social_insta']) > 0 ||
	strlen($settings['social_twitter']) > 0 ) { 
?>
		<aside class="share">
		    <div class="share-container">
				
				<?php if( strlen($settings['social_fb']) > 0 ) { ?>
			        <div class="share-item share-facebook mb-min">
			            <a target="_blank" href="<?php echo $settings['social_fb']; ?>">
			                <img src="<?php echo base_url('assets/static/'.$settings['theme'].'/images/share_facebook.svg')?>">
			            </a>
			        </div> <?php } ?>
				
				
				<?php if( strlen($settings['social_insta']) > 0 ) { ?>
			        <div class="share-item share-instagram mb-min">
			            <a target="_blank" href="<?php echo $settings['social_insta']; ?>">
			                <img src="<?php echo base_url('assets/static/'.$settings['theme'].'/images/share_instagram.svg')?>">
			            </a>
			        </div> <?php } ?>
				
				<?php if( strlen($settings['social_twitter']) > 0 ) { ?>
			        <div class="share-item share-twitter">
			            <a target="_blank" href="<?php echo $settings['social_twitter']; ?>">
			                <img src="<?php echo base_url('assets/static/'.$settings['theme'].'/images/share_twitter.svg')?>">
			            </a>
			        </div> <?php } ?>

		    </div>
		</aside>
<?php } ?>