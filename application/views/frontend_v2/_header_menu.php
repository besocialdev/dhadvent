<?php if ($slug == '') { ?>
    <?php /* <i class="icon i001"><img src="<?php echo base_url('assets/static/'.$settings['theme'].'/icons/003.svg'); //ajándék ?>"></i> */ ?>
    <i class="icon i001" id="icontop"><img id="svg01" src="<?php echo base_url('assets/static/'.$settings['theme'].'/icons/doboz.svg'); //ajándék  ?>"></i>
<?php } ?>

<header class="header">
    <nav class="menu-top navbar navbar-default navbar-expand-lg navbar-light bg-white border-bottom noselect sm-padding">
        <div class="container">

            <a class="navbar-brand" href="<?php echo base_url(); ?>">
                <?php
                if (strlen($settings['default_logo']) && file_exists(FCPATH . 'assets' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . $settings['default_logo'])) {
                    ?>
                    <img style="max-width: 180px;" alt="<?php echo strip_tags(stripslashes($settings['page_name'])); ?>" src="<?php echo base_url('assets/uploads/files/' . $settings['default_logo']); ?>">
                        <?php
                    } else
                        echo strip_tags(stripslashes($settings['page_name']));
                    ?>
                    <?php //echo strip_tags(stripslashes($settings['page_name'])); // vagy külön mező? logo_text van neki db-ben ha a page_name másra is kellene ?>
            </a>

            <?php if (strlen($settings['pushalert']) > 0): ?>
                <a class="show-mobile trans btn-alert-icon" href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('TURN_ON_NOTIFICATION'); ?>" alt="<?php echo lang('TURN_ON_NOTIFICATION'); ?>">
                    <svg class="bell" width="23" height="25" viewBox="0 0 23 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M11.5 25C12.875 25 14 23.875 14 22.5H9C9 23.875 10.125 25 11.5 25ZM19.625 17.5V10.625C19.625 6.75 17 3.625 13.375 2.75V1.875C13.375 0.875 12.5 0 11.5 0C10.5 0 9.625 0.875 9.625 1.875V2.75C6 3.625 3.375 6.75 3.375 10.625V17.5L0.875 20V21.25H22.125V20L19.625 17.5Z" fill="#D2D2D2"/>
                    </svg>
                </a>
            <?php endif; ?>
            <a class="btn btn-def btn-play--mobile show-mobile trans scrollto" href="javascript:void(0);" data-scrollto="how-to-play">
                <i class="fa fa-calendar-check-o"></i>
            </a>

            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="icon-bar top-bar"></span>
                <span class="icon-bar middle-bar"></span>
                <span class="icon-bar bottom-bar"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link trans <?php echo base_url() == current_url() ? ' active' : ''; ?>" href="<?php echo base_url(); ?>">
                            <?php echo lang('HOMEPAGE'); ?>
                        </a>
                    </li>

                    <!-- fejléc menü elemek -->
                    <?php
                    foreach ($menu_header as $k => $v): //$menu_header (MCMS_Controller -> construct)
                        $activeclass = base_url() . $v['menu_slug'] == current_url() ? ' active' : '';
                        ?>
                        <li class="nav-item">
                            <a class="nav-link trans <?php echo $activeclass; ?>" href="<?php echo $v['menu_slug']; ?>"><?php echo $v['title']; ?></a>
                        </li>
                    <?php endforeach; ?>

                    <?php if (strlen($settings['pushalert']) > 0): ?>
                        <li class="nav-item hide-desctop">
                            <a class="btn trans btn-alert-icon" href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('TURN_ON_NOTIFICATION'); ?>" alt="<?php echo lang('TURN_ON_NOTIFICATION'); ?>">
                                <svg class="bell" width="23" height="25" viewBox="0 0 23 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M11.5 25C12.875 25 14 23.875 14 22.5H9C9 23.875 10.125 25 11.5 25ZM19.625 17.5V10.625C19.625 6.75 17 3.625 13.375 2.75V1.875C13.375 0.875 12.5 0 11.5 0C10.5 0 9.625 0.875 9.625 1.875V2.75C6 3.625 3.375 6.75 3.375 10.625V17.5L0.875 20V21.25H22.125V20L19.625 17.5Z" fill="#D2D2D2"/>
                                </svg>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if (true === $game_is_active) { ?>
                        <li class="nav-item">
                            <?php if (base_url() == current_url() && $this->account->is_guest() && $settings['login_required'] ): //nyitóoldalon götgetés, egyébként link ?>
                                <a class="btn btn-def btn-def-secondary-colors btn-play btn-play--mobile2 trans scrollto" href="javascript:void(0);" data-scrollto="how-to-play">
                                <?php else: ?>
                                <a class="btn btn-def btn-def-secondary-colors btn-play btn-play--mobile2 trans" href="<?php echo base_url('naptar'); ?>">
                                <?php endif; ?>
                                    <i class="fa fa-calendar-check-o"></i>&nbsp;&nbsp;<?php echo lang('GAME'); ?>
                                </a> <!-- ! -->
                        </li> <?php } ?>

                    <?php if (!$this->account->is_guest() && $settings['login_required']==true ): // kilépés gomb! ?>
                        <li class="nav-item">
                            <a class="btn trans btn-alert-icon" href="kijelentkezes" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('SIGN_OUT'); ?>" alt="<?php echo lang('SIGN_OUT'); ?>">
                                <span class="show-mobile"><?php echo lang('SIGN_OUT'); ?></span>
                                    <?php echo load_custom_svg('sign_out.svg'); ?>
                            </a>
                        </li>
                    <?php endif; ?>

                </ul>

                <?php if (strlen($settings['pushalert']) > 0): ?>
                    <div class="show-mobile infobar">
                        <?php echo lang('TIPP1'); ?>
                        <svg class="bell-mobile" width="23" height="25" viewBox="0 0 23 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11.5 25C12.875 25 14 23.875 14 22.5H9C9 23.875 10.125 25 11.5 25ZM19.625 17.5V10.625C19.625 6.75 17 3.625 13.375 2.75V1.875C13.375 0.875 12.5 0 11.5 0C10.5 0 9.625 0.875 9.625 1.875V2.75C6 3.625 3.375 6.75 3.375 10.625V17.5L0.875 20V21.25H22.125V20L19.625 17.5Z" fill="#D2D2D2"/>
                        </svg>
                        <?php echo lang('TIPP2'); ?>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    </nav>
</header>
