<section class="featured featured--with-image cms-block  sm-fade-in">
    <div class="container">
        <?php if(!empty($cmsdata['last_item'])): ?>
            <i class="icon i001 sm-fade-in"><img src="<?php echo base_url('assets/static/'.$settings['theme'].'/icons/003.svg'); //003.svg:ajándék ?>"></i>
            <i class="icon i002 sm-fade-in"><img src="<?php echo base_url('assets/static/'.$settings['theme'].'/icons/001.svg'); //001.svg:controller ?>"></i>
            <i class="icon i003 sm-fade-in"><img src="<?php echo base_url('assets/static/'.$settings['theme'].'/icons/002.svg'); //003.svg:ajándék ?>"></i>
        <?php endif; ?>        
        <div class="row">
            <div class="col-12 col-lg-6">
                <a href="" class="inl-blk prize-img">
                    <img src="<?php echo $cmsdata['cover_image'] ?>" class="img100">
                </a>
            </div>
            <div class="col-12 col-lg-6">
                <div class="jumbotron">
                    <div class="prize-slides">
                        <div class="subtitle"><?php echo $cmsdata['cms_page_block_pre_title'] ?></div>
                        <h2 class="cms_title"><?php echo $cmsdata['cms_page_block_title'] ?></h2>
                        <p class="lead prize-lead">
                            <?php echo $cmsdata['cms_page_block_content'] ?>
                        </p>
                    </div>

                </div>
            </div>

        </div>
    </div>
</section>