<section class="featured featured--with-image cms-block  sm-fade-in">
    <div class="container">
    	
        <div class="row">
            <div class="col-12 col-lg-6 prize-text-cont">
                <div class="jumbotron">
                    <div class="prize-slides">
                        <div class="subtitle"><?php echo $cmsdata['cms_page_block_pre_title'] ?></div>
                        <h2 class="cms_title"><?php echo $cmsdata['cms_page_block_title'] ?></h2>
                        <p class="lead prize-lead">
                            <?php echo $cmsdata['cms_page_block_content'] ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6 prize-img-cont">
                <a href="" class="inl-blk prize-img">
                    <img src="<?php echo $cmsdata['cover_image'] ?>" class="img100">
                </a>
            </div>                        
        </div>
    </div>
</section>