<?php if(!empty($intro_block[0])): ?>
	<section class="featured featured-full-width-bg fixed-header sm-fade-in intro-block">
	    <div class="container">
	        <div class="row">
	            <div class="col-12 col-lg-5">
	                <div class="jumbotron">
	                    <h2 class="main_title">
	                        <?php echo $intro_block[0]['cms_page_block_title'] ?>
	                    </h2>
	                    <p class="lead">
	                        <?php echo $intro_block[0]['cms_page_block_content'] ?>
	                    </p>
	                    <?php if( true === $game_is_active ){ ?>
	                    <a class="btn btn-def btn-lg btn--mob100 scrollto" href="javascript:void(0);" role="button" data-scrollto="how-to-play">
	                        <img src="<?php echo base_url('assets/static/style_settings/chevron-down.svg') ?>" alt="">&nbsp;&nbsp;
	                        <?php echo lang('BUTTON_NEXT'); ?>
	                    </a><?php } ?>

	                </div>
	            </div>
	            <div class="col-12 col-lg-7 p0 mainpage-intro-imgcont-outer">
	                <?php /*<i class="icon i001 sm-fade-in"><img src="<?php echo base_url('assets/static/'.$settings['theme'].'/icons/001.svg'); //001.svg:controller ?>"></i>*/ ?>
	                <i class="icon i002 sm-fade-in"><img src="<?php echo base_url('assets/static/'.$settings['theme'].'/icons/003.svg'); //003.svg:ajándék ?>"></i>
	                <a href="javascript:void(0);" style="cursor: initial!important;" class="inl-blk mainpage-intro-imgcont">
	                    <img src="<?php echo $intro_block[0]['main_page_image'] ?>" class="img100">
	                </a>
	            </div>
	        </div>
	    </div>
	</section>
<?php endif; ?>

<section class="featured featured--bg2 align-center sm-fade-in how-to-play-block" id="how-to-play">
    <div class="container">
        <i class="icon i001"><img src="<?php echo base_url('assets/static/'.$settings['theme'].'/icons/002.svg'); //002.svg:szív ?>"></i>
        <i class="icon i002"><img src="<?php echo base_url('assets/static/'.$settings['theme'].'/icons/poker.svg'); //002.svg:szív ?>"></i>
        <div class="jumbotron">
            <h2 class="main_title">
                <?php /*<?php echo lang('HOW_TO_PLAY_BLOCK_TITLE'); ?>*/ ?>
                <?php echo strip_tags($cmsdata['fooldal_also_blokk_cim']); ?>
            </h2>
            <p class="lead">
                <?php /*<?php echo lang('HOW_TO_PLAY_BLOCK_LEADTEXT'); ?>*/ ?>
                 <?php echo $cmsdata['fooldal_also_blokk_szoveg']; ?>
            </p>
        </div>
    </div>
</section>


<section class="reg-form-cont align-center sm-fade-in" <?php echo ( true === $game_is_active ? ' id="regisztracio"' : "" ) ?>>
<?php if(false === $game_is_active): ?>
        <div class="container">
                <form class="form-signin" style="padding-top: 8%;">
                        <h3><?php echo $settings['game_end_modal_title']; ?></h3>
                        <p>
                                <?php echo $settings['game_end_modal_content']; ?>
                        </p>
                </form>
        </div>
<?php endif; ?>
</section>



<?php
/**
 * A nyitóoldalon a megadott oldalak blokk részei megjelenhetnek
 * cms_block_mainpage_visible
 * sorrend: cms_block_mainpage_order
 * balos vagy jobbos elrendezés: cms_block_type ('image_on_left','image_on_right')
 */
if(!empty($mainpage_cms_blocks))
{
    foreach ($mainpage_cms_blocks as $k => $v) {
        $view_data = array(
                'cmsdata' => $v
            );
        ?>
            <section class="featured align-center sm-fade-in sm-animate prizes-title">
                <div class="container">
                    <div class="jumbotron">
                        <h2 class="main_title upper">
                            <?php echo '- '.$v['title'].' -'; ?>
                        </h2>
                    </div>

                    <?php if($v['id'] == 36){ ?>
                        <div class="win-icons" style="position: relative;">
                            <i class="icon i001 sm-fade-in" style="position: absolute; top:-120px; right: 0;"><img src="<?php echo base_url('assets/static/'.$settings['theme'].'/icons/poker.svg'); //003.svg:ajándék  ?>"></i>
                            <i class="icon i002 sm-fade-in" style="position: absolute;  top: -120px;  max-width: 100px;  left: 20%;  transform: rotate(-20deg);"><img src="<?php echo base_url('assets/static/'.$settings['theme'].'/icons/doboz.svg'); //001.svg:controller  ?>"></i>
                        </div>
		            <?php } ?>
                </div>
            </section>

        <?php

        if($v['menu_slug']=='nyeremenyek' && $prizes_slider_enabled){
            ?>
                <section class="featured align-center sm-fade-in sm-animate swiper-container-outer<?php echo ($v['cms_block_type'] == 'image_on_left' ? ' image-on-leftside' : '' ) ?>">
                    <div class="container">
                    <div class="swiper-container" style="position: relative;">
                            <div class="swiper-wrapper">
                            <?php
                            foreach ($prizes_list as $kk => $vv) {
                            ?>
                                <div class="swiper-slide">
                                    <?php
                                        $prize_data = array(
                                            'cmsdata' => $vv
                                        );
                                        $this->load->view($settings['theme']."/cms_content/".$v['cms_block_type'],$prize_data);
                                    ?>
                                </div>
                            <?php
                            }
                            ?>
                            </div> <!-- .swiper-wrapper -->
                            <!-- Add Pagination -->
                            <div class="swiper-pagination"></div>
                            <!-- Add Arrows -->
                            <div class="swiper-buttons">
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>
                    </div> <!-- .swiper-container -->
                    </div> <!-- .container -->
                </section>
            <?php
        }
        else
        {
            $this->load->view($settings['theme']."/cms_content/".$v['cms_block_type'],$view_data);
        }
    } //
}

?>



