<div <?php if($tooltip): ?> data-toggle="tooltip" data-placement="top" title="<?php echo $tooltip;?>"<?php endif; ?> class="calendar-col col col-6 col-sm-4 col-lg-2 mb-half" id="day-<?php echo $day_id; ?>">
    <a href="javascript:void(0);" data-day="<?php echo $day_id; ?>" class="calendar-item <?php echo $prev_days_class; ?> calendar-item--<?php echo  $class; ?> <?php echo  $day_viewed ? "viewed" : "not-viewed"; ?> ">
        <span id="winfo_<?php echo $day_id; ?>">
            <?php
                if( $day_viewed )
                    echo "<span class='item-type'>". lang("GAME_DAY_VIEWED_MESSAGE")."</span>";
                elseif( $class=="past"&&($prev_days_class=="calendar-item--prv"||$prev_days_class=="calendar-item--adm") )
                    echo "<span class='item-type only-border'>". lang("GAME_DAY_OPEN_MESSAGE")."</span>";
            ?>
        </span>
        <span class="day"><?php echo $day_no; ?></span>
        <?php if($class=="current"||$class=="next") echo "<span class='day-str'>".$day_str."</span>"; ?>
        <span class="lock">&nbsp;</span>
        <?php if($class=="next"): ?><span class="countdown">00:00:00</span><?php endif; ?>
        <?php if($class=="current"): ?>
            <span class="open btn btn-def"><?php echo lang($day_viewed?"GAME_DAY_VIEWED_MESSAGE":"GAME_DAY_OPEN_MESSAGE"); ?></span>
        <?php endif; ?>
    </a>
</div>