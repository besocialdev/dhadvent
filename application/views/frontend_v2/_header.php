<?php
// pre($settings);
// pre($cmsdata);
?>
<!DOCTYPE html>
<html lang="<?php echo strip_tags( $settings['lang_code'] ); ?>">
	<head>
		<meta charset="UTF-8">
		<meta name="description" content="<?php echo (!empty($cmsdata['meta_description']) ? $cmsdata['meta_description'] : '' ); ?>">
		<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no,maximum-scale=1.0">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta content='<?php echo $meta_index; ?>' name='robots'>
		<meta property="og:type" content="website">
		<meta property="og:site_name" content="<?php echo strip_tags($cmsdata['og_title']); ?>">
		<meta name="csrf_token" content="<?php echo $this->security->get_csrf_hash(); ?>">
        <?php echo $html_header;  ?>

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<!--<link href="https://fonts.googleapis.com/css?family=Montserrat+Alternates:400,700|Montserrat:400,700|Noto+Sans:400,700&display=swap&subset=latin-ext" rel="stylesheet">-->

        <link href="https://fonts.googleapis.com/css?family=Covered+By+Your+Grace|Montserrat:300,400,700&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo base_url('assets/static/vendors/bootstrap/css/bootstrap.min.css?v=1.0')?>">

		<?php // get spec font: ?>
		<?php if( $settings['skin_mainarea_heading_font_type'] != '' ){ ?>
			<?php $settings['skin_mainarea_heading_font_type'] = str_replace(' ', '+', $settings['skin_mainarea_heading_font_type']); ?>
        	<link href="https://fonts.googleapis.com/css?family=<?php echo $settings['skin_mainarea_heading_font_type']; ?>&display=swap" rel="stylesheet">
        <?php } ?>

        <?php if(isset($prizes_slider_enabled) && $prizes_slider_enabled===true): ?>
        	<link rel="stylesheet" href="<?php echo base_url('assets/static/vendors/swiper-master/dist/css/swiper.min.css?v=1.0')?>">
        <?php endif; ?>

		<link rel="stylesheet" href="<?php echo base_url('assets/static/'.$settings['theme'].'/css/style.css?v=1.0')?>">
		<link rel="stylesheet" href="<?php echo base_url('assets/static/'.$settings['theme'].'/css/temp.css?v=1.2')?>">	<?php // @todo ?>
		<link rel="stylesheet" href="<?php echo base_url('assets/static/style_settings.css?v=1.0')?>">	<?php // adminon beallitott ertekek ebbe kerulnek ?>

		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url('favicons')?>/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('favicons')?>/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('favicons')?>/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('favicons')?>/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('favicons')?>/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('favicons')?>/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url('favicons')?>/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('favicons')?>/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('favicons')?>/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url('favicons')?>/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url('favicons')?>/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url('favicons')?>/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('favicons')?>/favicon-16x16.png">
		<link rel="manifest" href="<?php echo base_url('favicons')?>/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?php echo base_url('favicons')?>/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		<?php
                        // cookie modal kód
			if( strlen( $settings['cookie_codes'] ) > 0 ){
				echo $settings['cookie_codes'];
			}
                        
			// pushalert script:
			if( strlen( $settings['pushalert'] ) > 0 ){
				echo "\r\n".$settings['pushalert'];
			}

			// custom css:
			if( strlen( $settings['extra_css'] ) > 0 ){
				echo "\r\n".$settings['extra_css'];
			}

		?>
	</head>
	<body <?php echo 'class="'.$slug.'"' ?>>
            
                <div id="fb-root"></div>
                <script async defer crossorigin="anonymous" src="https://connect.facebook.net/hu_HU/sdk.js#xfbml=1&version=v6.0"></script>

		<?php if( isset($modal_success) && strlen($modal_success) > 0 ){ ?>
			<style type="text/css">
				.flash_msg{
					text-align: center;
					padding: 20px;
					font-size:large;
					background: #28a745;
					color:#fff;
				}
				#icontop{
					display: none!important;
				}
			</style>
			<div class="flash_msg" id="flash_msg">
				<?php echo $modal_success; ?>
			</div>
			<script type="text/javascript">
				setTimeout(function(){
					document.getElementById("flash_msg").style.display = "none";
					document.getElementById("icontop").style.display = "block";
				}, 1800);
			</script>
		<?php } ?>