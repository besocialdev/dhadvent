<div class="container">
    <form class="form-signin" method="post" action="" novalidate="novalidate" onsubmit="return submitGameForm();">
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
        <input style="display: none !important;" type="hidden" name="becaptcha" value="">
        <i class="icon i001"><img src="<?php echo base_url('assets/static/'.$settings['theme'].'/icons/002.svg'); //szív  ?>"></i>
        <h3><?php echo (!empty($settings['reg_title']) ? $settings['reg_title'] : lang('FORM_MAIN_TITLE_PLAY_FOR_REWARDS')); ?></h3>

        <?php if(isset($validation_errors)): ?>
            <span class="form_error"><?php echo $validation_errors ?></span>
        <?php endif; ?>


        <div class="form-content">
            <?php if($step=="email"): ?>


            <div class="input-row">
                <label for="email_address" class="sr-only"><?php echo lang('FORM_EMAIL'); ?></label>
                <?php if(form_error("email_address")): ?><span class="form_error"><?php echo form_error("email_address"); ?></span><?php endif; ?>
                <input type="text" value="<?php echo $this->input->post("email_address"); ?>" id="email_address" name="email_address" class="form-control" placeholder="<?php echo lang('FORM_EMAIL'); ?>">
            </div>

            <?php elseif($step=="login"): ?>
            <?php /*   <input type="hidden" id="email_address" name="email_address" value="<?=$this->input->post("email_address");?>"> */ ?>
            <div class="input-row">
                <label for="email_address" class="sr-only"><?php echo lang('FORM_EMAIL'); ?></label>
                <?php if(form_error("email_address")): ?><span class="form_error"><?php echo form_error("email_address"); ?></span><?php endif;?>
                <input type="text" value="<?php echo $this->input->post("email_address"); ?>" id="email_address" name="email_address" class="form-control" placeholder="<?php echo lang('FORM_EMAIL'); ?>">
            </div>
            <div class="input-row">
                <?php if(form_error("password")): ?><span class="form_error"><?php echo form_error("password"); ?></span><?php endif; ?>
                <label for="password" class="sr-only"><?php echo lang('FORM_PASSWORD'); ?></label>
                <input type="password" id="password" name="password" class="form-control" placeholder="<?php echo lang('FORM_PASSWORD'); ?>">
            </div>
            <?php elseif($step=="register"): ?>
            <?php /* <input type="hidden" id="email_address" name="email_address" value="<?=$this->input->post("email_address");?>"> */ ?>
            <div class="input-row">
                <label for="email_address" class="sr-only"><?php echo lang('FORM_EMAIL'); ?></label>
                <?php if(form_error("email_address")): ?><span class="form_error"><?php echo form_error("email_address"); ?></span><?php endif; ?>
                <input type="text" value="<?php echo $this->input->post("email_address"); ?>" id="email_address" name="email_address" class="form-control" placeholder="<?php echo lang('FORM_EMAIL'); ?>">
            </div>
            <div class="input-row">
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <?php if(form_error("last_name")): ?><span class="form_error"><?php echo form_error("last_name"); ?></span><?php endif; ?>
                        <label for="last_name" class="sr-only"><?php echo lang('FORM_LASTNAME'); ?></label>
                        <input type="text" id="last_name" value="<?php echo $this->input->post("last_name"); ?>" name="last_name" class="form-control" placeholder="<?php echo lang("FORM_LASTNAME"); ?>" autofocus="">
                    </div>
                    <div class="input-col col-12 col-lg-6">
                        <?php if(form_error("first_name")): ?><span class="form_error"><?php echo form_error("first_name"); ?></span><?php endif; ?>
                        <label for="first_name" class="sr-only"><?php echo lang("FORM_FIRSTNAME"); ?></label>
                        <input type="text" id="first_name" value="<?php echo $this->input->post("first_name"); ?>" name="first_name" class="form-control" placeholder="<?php echo lang("FORM_FIRSTNAME"); ?>">
                    </div>
                </div>
            </div>
            <div class="input-row">
                <?php if(form_error("password")): ?><span class="form_error"><?php echo form_error("password"); ?></span><?php endif; ?>
                <label for="password" class="sr-only"><?php echo lang('FORM_PASSWORD'); ?></label>
                <input type="password" id="password" name="password" class="form-control" placeholder="<?php echo lang('FORM_PASSWORD'); ?>">
            </div>
            <div class="input-row">
                <?php if(form_error("passconf")): ?><span class="form_error"><?php echo form_error("passconf"); ?></span><?php endif; ?>
                <label for="passconf" class="sr-only"><?php echo lang('FORM_PASSWORD_AGAIN'); ?></label>
                <input type="password" id="passconf" name="passconf" class="form-control" placeholder="<?php echo lang('FORM_PASSWORD_AGAIN'); ?>">
            </div>

            <div class="input-row checkbox">
                <label>
                    <?php if(form_error("accept_tos")): ?><span class="form_error"><?php echo form_error("accept_tos"); ?></span><?php endif; ?>
                    <input id="accept_tos" name="accept_tos" type="checkbox" <?php echo $this->input->post("accept_tos") ? "checked" : ""; ?>>
                    <?php echo str_replace("{url_token_game_rules}", "jatekszabalyzat", lang('ACCEPT_GAME_RULES')); ?>
                    <span class="checkmark"></span>
                </label>
            </div>
            <div class="input-row checkbox">
                <label>
                    <?php if(form_error("accept_privacy")): ?><span class="form_error"><?php echo form_error("accept_privacy"); ?></span><?php endif; ?>
                    <input id="accept_privacy" name="accept_privacy" type="checkbox" <?php echo $this->input->post("accept_privacy") ? "checked" : ""; ?>>
                    <?php echo str_replace("{url_token_privacy_policy}", "adatvedelmi-nyilatkozat", lang('ACCEPT_PRIVACY_POLICY')); ?>
                    <span class="checkmark"></span>
                </label>
            </div>
            <?php if ($settings['newsletter_api'] > 0){ ?>
                <div class="input-row checkbox">
                    <label>
                        <input id="newsletter" name="newsletter" type="checkbox" <?php echo $this->input->post("newsletter") ? "checked" : ""; ?>>
                        <?php echo lang('FORM_SUBSCRIBE_NEWSLETTER'); ?>
                        <span class="checkmark"></span>
                    </label>
                </div>
            <?php } else //if ($settings['newsletter_api'] > 0) ág vége
                  {  ?>
                <input id="newsletter" name="newsletter" type="hidden" value="0">
            <?php } ?>

            <?php endif; // elseif($step=="register"): vége  ?>
        </div>
        <button class="btn btn-def btn-def-secondary-colors btn-lg btn--mob100 register" type="submit"><?php echo lang('BUTTON_NEXT'); ?>!</button>
        <?php if ($step == 'login') { ?>
            <div style="display: block; margin-top: 20px;">
                <a style="font-weight: bold;" href="javascript:void(0);" class="get-reg-form" onclick="getRegFrom();">
                    Regisztrálok
                </a> |
                <a style="" target="_blank" href="<?php echo base_url('elfelejtett-jelszo'); ?>"><?php echo lang('FORGOTTEN_PWD'); ?></a>
            </div>
        <?php } ?>

        <?php if ($step == 'register') { ?>
            <div style="display: block; margin-top: 20px;">
                <a style="" href="javascript:void(0);" class="get-login-form" onclick="getLoginFrom();">
                    Belépés
                </a>
            </div>
        <?php } ?>

    </form>
</div>
