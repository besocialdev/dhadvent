<?php if(empty($puzzle_data['error'])): ?>
<link rel="stylesheet" href="assets/static/frontend_v2/css/puzzle.css">
<?php echo $day->body;?>
<p class="puzzleresult"></p>
<article style="<?php echo $puzzle_data['global']['inline_css'] ?>">
    <div class="puzzletempcover visible"></div>
    <?php foreach ($puzzle_data['pieces'] as $k => $v): ?>
        <div id="piece-<?php echo $k; ?>" class="pieces" style="<?php echo $v['inline_css'] ;?>" data-pos="<?php echo $v['left'].$v['top'] ?>" data-fbasename="<?php echo $v['file_basename'] ;?>"></div>
    <?php endforeach; ?>
</article>
<script src="assets/static/frontend_v2/js/puzzle.js"></script>
<?php else: echo $puzzle_data['error']; endif;?>