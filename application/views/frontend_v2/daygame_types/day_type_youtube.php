<?php
    //youtube típus esetén az adminon a mező ellenőrizve van ugyanezzel a regexp kifejezéssel, hogy csak valós youtube linket adhat meg (elvileg)
    preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $day->youtube_link, $matches);
    $youtube_id = (!empty($matches[1]) ? $matches[1] : '' );
?>
    <p class="video-content">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $youtube_id;?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </p>
    <?php echo $day->body;?>
