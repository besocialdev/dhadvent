<div class="quiz-content" data-day-id="<?php echo $day->id; ?>">
    <div class="quiz-question">
        <h2><?php echo htmlspecialchars_decode($quiz_data["quiz_question"]); ?></h2>
    </div>
    <?php

    // kitoltes: még nem látta vagy látta de nem válaszolt
    if (!$viewed || $viewed->game_result == "") {
        ?>


        <form method="post" id="quiz_form" action="">
            <div class="quiz-answers clearfix">
                <?php
                $this->db->order_by("id", "RANDOM");
                $options = $this->db->get_where("calendar_quiz", array("day_id" => $day->id, "body <>" => ''))->result();
                if ($options) {
                    foreach ($options as $o) {
                        ?>
                        <div class="quiz-radio clearfix" data-answer-id="<?php echo  $o->id; ?>">
                            <span class="quiz-answers--text"><?php echo  $o->body; ?></span>
                        </div><br>
                        <?php
                    }
                }
                ?>
            </div>
            <div class="quiz-answer-button-cont">
                <input  type="button" class="btn btn-def btn-lg btn--quiz" value="<?php echo lang("SHOW_QUIZ_RESULT_BUTTON_TITLE"); ?>" disabled />
            </div>
            <input type="hidden" name="answer" value="" id="answer" />
        </form>
    <?php
    }
    // eredmeny megjelenítés ha már válaszolt
    else {

        //echo $this->load->view($this->frontend_theme."game_quiz_result",array("day"=>$day,"is_correct"=>$viewed->game_result),true);
        ?>
        


            <form onsubmit="return false;">
                <div class="quiz-answers clearfix">
                    <?php
                    $user_answer_id = $viewed->answer_id; // user melyiket valasztotta

                    $options = $this->db->get_where("calendar_quiz", array("day_id" => $day->id, "body <>" => ''))->result();
                    if ($options) {
                        foreach ($options as $o) {
                            if (( (int) $o->is_correct == 1)) {
                                ?>
                                <div class="quiz-radio quiz-radio--answered is-correct clearfix">
                                    <span class="quiz-answers--text"><?php echo $o->body; ?></span>
                                </div><br>
                                <?php
                            }
                            if ($o->id == $user_answer_id && (int) $viewed->game_result == 0) {
                                ?>
                                <div class="quiz-radio quiz-radio--answered is-wrong clearfix">
                                    <span class="quiz-answers--text"><?php echo $o->body; ?></span>
                                </div><br>
                                <?php
                            }
                        }
                    }
                    ?>
                </div>
            </form>

        <?php
    }
    ?>
</div>