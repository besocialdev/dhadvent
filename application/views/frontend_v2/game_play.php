<?php if( (int)$redirect == 1 ){ ?>
	<script type="text/javascript">
		window.location.href = '<?php echo base_url("naptar"); ?>';
	</script>
<?php } else { ?>
	<div class="container is-logged" data-status="SUCCESS_LOGIN">
	    <form class="form-signin" method="post" action="" novalidate="novalidate">
                <h3><?php echo (!empty($settings['reg_title']) ? $settings['reg_title'] : lang('FORM_MAIN_TITLE_PLAY_FOR_REWARDS')) ;?></h3>
	        <a class="btn btn-def btn-def-secondary-colors btn-play btn-play--mobile2 trans" href="<?php echo base_url('naptar'); ?>">
	                <i class="fa fa-calendar-check-o"></i>&nbsp;&nbsp;<?php echo lang('GAME'); ?>
	        </a> <!-- ! -->
	    </form>



	</div>
<?php } ?>