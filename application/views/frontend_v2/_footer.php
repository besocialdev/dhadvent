		<?php if( $settings['cookie_modal_enabled'] == 1 ){ ?>
		<div class="c-layer hidden">
			<div class="c-layer-inner">
				<div class="container">
					<?php echo strip_tags($settings['cpm_layer']); ?>&nbsp;&nbsp;
					<a href="javascript:void(0);" class="cpm_modal_open"><?php echo lang('FOOTER_COOKIE_SETTINGS'); ?></a> <br>
					<div style="text-align: right;">
						<a class="btn btn-def accept_all">
							<?php echo lang('OK'); ?>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="cpm_container" style="display: none;"></div>
		<?php } ?>

		<?php
			if( false === $game_is_active && ! isset($_COOKIE['cl_gameend']) ){ setcookie("cl_gameend", 1);
		?>
			  <div class="modal" id="game_end_modal">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h4 class="modal-title"><?php echo strip_tags($settings['game_end_modal_title']); ?></h4>
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				      </div>
				      <div class="modal-body">
				       <?php echo strip_tags($settings['game_end_modal_content']); ?>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-def" data-dismiss="modal"><?php echo lang('CLOSE'); ?></button>
				      </div>
				    </div>
				  </div>
				</div>
		<?php } ?>

		<?php if(base_url('naptar')!=current_url()): ?><div class="bottomcover"></div> <?php endif; ?>

		<script type="text/javascript">
			var js_vars = {
				'scrollto': '<?php if( $_GET && isset($_GET['scrollto']) ){ echo trim($_GET['scrollto']); } ?>',
				'game_is_active': '<?php echo ( $game_is_active == true) ? 1:0; ?>',
			};
		</script>

		<script src="<?php echo base_url('assets/static/vendors/jquery-3.4.1.min.js?v=1.0')?>"></script>
                <?php if(isset($prizes_slider_enabled) && $prizes_slider_enabled===true): ?>
                <script src="<?php echo base_url('assets/static/vendors/swiper-master/dist/js/swiper.min.js')?>"></script>
                <?php endif; ?>
		<!-- cookie -->
		<script src="<?php echo base_url('assets/static/vendors/cookie_modal/cpm.plugin.jquery.js?v=1.0')?>"></script>
		<script>
			$(document).ready(function(){
				var cpm = $('.cpm_container').cookie_policy_modal({
					container: '.cpm_container',
					cookie_name: 'cpm_cookie_o',
					cookie_expire: 365,	// days
					cust_btn: '.cpm_modal_open',
					modal_html: '<?php echo escape_javascript_text( $cookie_modal_html ); ?>',
					//modal_html: '<?php echo escape_javascript_text(file_get_contents(APPPATH."/views/_cookie_modal.php")); ?>',
					css_url: '<?php echo base_url('assets/static/vendors/cookie_modal/cpm.css?v=1.011')?>'
				});

				// 1 (default):
				if( parseInt(cpm_cookie) == 1 ){

				}

				// 1,2 (default, statistics):
				if( parseInt(cpm_cookie) == 2 ){

				}

				// 1,2,3 (default, statistics, advertisement):
				if( parseInt(cpm_cookie) == 3 ){

				}
                                <?php if(isset($prizes_slider_enabled) && $prizes_slider_enabled===true): ?>
                                var mySwiper = new Swiper ('.swiper-container', {
                                    slidesPerView: 1,
                                    loop: true,
                                    autoHeight: true,
                                    grabCursor: true,
                                    pagination: {
                                      el: '.swiper-pagination',
                                      clickable: true,
                                    },
                                    autoplay: {
                                      delay: 4000,
                                      disableOnInteraction: false,
                                    },
                                    navigation: {
                                      nextEl: '.swiper-button-next',
                                      prevEl: '.swiper-button-prev',
                                    }
                                });
                                <?php endif; ?>
			});
		</script> <!-- end cookie -->

		<script src="<?php echo base_url('assets/static/vendors/popper.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/static/vendors/jquery.validate/jquery.validate.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/static/vendors/scroll_magic/ScrollMagic.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/static/vendors/bootstrap/js/bootstrap.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/static/'.$settings['theme'].'/js/main.js'); ?>"></script>

                <?php if (isset($js_to_load) && is_array($js_to_load)) : foreach ($js_to_load as $row): ?>
                    <script type="text/javascript" src="<?php echo base_url('assets/jstatic/'.$settings['theme'].'/js/'.$row); ?>">
                <?php endforeach; endif;
                    if( strlen( $settings['extra_js'] ) > 0 ){
                            echo $settings['extra_js'];
                    }
		?>
	</body>
</html>