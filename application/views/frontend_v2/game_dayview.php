<div class="calendar-modal mb-half" data-daytype="<?php echo $day->type ?>">
    <div class="calendar-modal-inner">
        <a class="calendar-modal-close" href="javascipt:;">
            <!-- <img src="assets/static/<?php echo $settings['theme']; ?>/images/modal-close.svg" /> -->
            <?php echo load_custom_svg('modal-close.svg'); ?>
        </a>
        <div class="calendar-modal-title">
            <svg class="lock-open-svg" width="14" height="18" viewBox="0 0 14 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M13.4218 9.32806C13.203 9.10924 12.9374 9.00002 12.6248 9.00002H3.99987V5.25006C3.99987 4.4218 4.2929 3.71486 4.87887 3.12889C5.46489 2.54304 6.17186 2.25006 7.00005 2.25006C7.82815 2.25006 8.53525 2.543 9.12101 3.12889C9.70703 3.71486 10.0001 4.42184 10.0001 5.25006C10.0001 5.45312 10.0742 5.62888 10.2225 5.7773C10.3712 5.92576 10.547 6.00001 10.7498 6.00001H11.5002C11.7032 6.00001 11.8789 5.92576 12.0276 5.7773C12.1756 5.62888 12.25 5.45312 12.25 5.25006C12.25 3.80459 11.7363 2.56849 10.7091 1.54107C9.68174 0.513608 8.44531 0 7.00005 0C5.55465 0 4.31835 0.513608 3.29089 1.54103C2.26359 2.56833 1.74994 3.80455 1.74994 5.25002V8.99998H1.37499C1.06259 8.99998 0.796896 9.10945 0.578121 9.32802C0.359346 9.54659 0.25 9.81236 0.25 10.1249V16.875C0.25 17.1876 0.359387 17.4533 0.578121 17.672C0.796896 17.8905 1.06259 18 1.37499 18H12.6248C12.9374 18 13.2032 17.8905 13.4218 17.672C13.6403 17.4533 13.7499 17.1876 13.7499 16.875V10.1249C13.7501 9.8124 13.6406 9.54692 13.4218 9.32806Z" fill="#ffffff"/>
            </svg>
            <span class="calendar-modal-title-day">
                <?php echo date("j",strtotime($day->cal_day));?>,
                <span class="calendar-modal-title-text"><?php echo get_day_name_hu($day->cal_day);?></span>
            </span>
            <span class="calendar-modal-title-text calendar-modal-title-text--long">
                <?php echo $day->title;?>
            </span>
            <?php 
            if(!empty($settings['fb_appid'])): ?>
            <!-- Facebook shareBtn -->
            <div style="float: right;">
                <a href="javascript:void(0);" onclick="share('<?php echo base_url("naptar#".$day->cal_day); ?>');">
                    <img alt="Megosztás" title="Megosztás" style="max-width:110px;" src="<?php echo base_url('assets/static/frontend_v2/images/bs_calendar_fb_share_button.png') ?>">
                </a>
            </div>
            <?php endif; ?>
        </div>
        <div class="calendar-modal-content">
            <section class="featured featured--bg2 align-center calendar-modal-content-section">
                <div class="container">
                    <div class="jumbotron">
                        <h2><?php echo $day->headline_text;?></h2>
                        <p class="lead"><?php echo $day->subtitle;?></p>
                        <?php if($this->account->is_guest() && $settings['login_required']==true){ ?>
                            <div class="reg-form-cont align-center sm-fade-in" id="regisztracio"></div>
                        <?php
                        }
                        else{
                            
                            switch ($day->type) {
                                case "image":
                                case "text":
                                case "youtube":
                                case "quiz":
                                default:
                                    // minden esetben úgy van elnevezve a fájl, hogy a megfelelő view-t töltse be
                                    $this->load->view($settings['theme']."/daygame_types/day_type_".$day->type,array("day"=>$day)); 
                                    break;
                            }
                        } ?>
                    </div>
                    <?php if(!empty($day->coupon_url_download_hash)):  // csak bejelentkezés után érhető el a funkció, vendég módban nem használható funkció ?>
                        <div class="coupon_download_cont">
                            <a href="<?php echo base_url("kupon-letoltes/".$day->coupon_url_download_hash); ?>" title="">Kupon letöltése</a>
                        </div>
                    <?php endif; ?>
                    
                    <?php if(!empty($day->coupon_url_email_hash)):  // csak bejelentkezés után érhető el a funkció, vendég módban nem használható funkció ?>
                        <div class="coupon_sendmail_cont">
                            <a href="javascript:void(0)" class="coupon_sendmail_link" data-hash="<?php echo $day->coupon_url_email_hash; ?>" title="">E-mail-ben kérem a kupont</a>
                        </div>
                    <?php endif; ?>
                </div>
            </section>
        </div>
        <div class="preloader"></div>
        <a class="calendar-modal-close--mobile" href="javascript:;">
            <!-- <img src="assets/static/<?php echo $settings['theme']; ?>/images/modal-close.svg" /> -->
            <?php echo load_custom_svg('modal-close.svg'); ?>
        </a>
    </div>
</div>