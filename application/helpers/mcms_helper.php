<?php defined('BASEPATH') OR exit('No direct script access allowed');

if( ! function_exists('escape_javascript_text') ){
	/**
	 * try to escape html text to be valid in js var
	 */
	function escape_javascript_text($string) { 
	    return str_replace("\n", '\n', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)$string), "\0..\37'\\"))); 
	} 
}

if( ! function_exists('_str') ){
	function _str($str){
		return trim(strip_tags(strtolower(addslashes($str))));
	}
}

if( ! function_exists('search_sql') ){
	/**
	 * Generate sql for search forms
	 * 
	 * @param  [string] $type  		search type: like, 
	 * @param  [string] $sql_str 	original sql string
	 * @param  [string] $value   	field value
	 * @param  [array] 	$fields  	array of db fields
	 * @return string
	 */
	function search_sql($type, $sql_str, $value, $fields)
	{
		$value = strip_tags($value);
		$ret = '';
		
		switch ($type) {
			case "like":
				$value = mb_strtoupper($value);
				$c = 0;

				$ret .= ( $sql_str == '' ) ? "" : " AND ";
				$ret .= "(";

				foreach ($fields as $k => $field)
				{
					$ret .= ( $c == 0 ) ? " UCASE($field) LIKE '%$value%'" : " OR UCASE($field) LIKE '%$value%'";
					$c++;
				}

				$ret .= ")";
				break;
			
			case "=":
			case "<=":
			case ">=":
				$ret .= ( $sql_str == '' ) ? $fields[0] . $type . "'" . $value ."'" : " AND " . $fields[0] . $type . "'" . $value ."'";
				break;

			default:
				break;
		}

		return $ret;
	}
}

if( ! function_exists('pre') )
{
	/**
	 * Show an array
	 *
	 * @param array $arr 
	 * @param boolean $exit
	 * @return void
	 */
	function pre($arr, $exit = FALSE){
		if( is_array($arr) ){
			echo '<pre>';
			print_r($arr);
			echo '</pre>';
		}  else  {
			echo 'not an array ...';
		}

		if($exit){
			exit;
		}
	}
}

if( ! function_exists('load_custom_svg') )
{
        function load_custom_svg($svg_filename, $params=array()) {
            
            $return = '';
            
                $default_svg_path = 'assets/static/frontend_v2/images/';
                $custom_svg_path = 'assets/static/style_settings/';
                
                if(file_exists($custom_svg_path.$svg_filename)){
                    $return = file_get_contents(($custom_svg_path.$svg_filename));
                }
                else {
                    $return = file_get_contents(($default_svg_path.$svg_filename));
                }
            
            return $return;
            
        }
}


if( ! function_exists('isvalidtext') )
{
	/**
	 * Validate text (hungarian spec. chars)
	 *
	 * @param string $str 
	 * @param int $min
	 * @param int $max
	 * @return boolean
	 */
	function isvalidtext($str, $min = 0, $max = 255)
	{
		return preg_match("#^[a-zA-Z0-9íéáőúűöüóÍÉÁŐÚÖÜÓŰ\-\' \,\.\!\?\-\:\+\(\)\#\&\{\}\']{".$min.",".$max."}$#i",$str);
	}
}

if( ! function_exists('isvalidemail') )
{
	/**
	 * Validate e-mail address
	 *
	 * @param string $str
	 * @return boolean
	 */
	function isvalidemail($str)
	{
		return preg_match("#^[A-Z0-9+._-]+@[A-Z0-9][A-Z0-9.-]{0,63}[A-Z0-9]\.[A-Z.]{2,8}$#i",$str);
	}
}

if( ! function_exists('isvalidnumber') )
{
	/**
	 * Validate dec. number
	 *
	 * @param string $str
	 * @param integer $max Max char nr.
	 * @return boolean
	 */
	function isvalidnumber($str, $max)
	{
		return preg_match("#^[0-9]{1,$max}$#i",$str);
	}
}

if( ! function_exists('isvalidurl') )
{
	/**
	 * Validate URL by PHP filter_var()
	 *
	 * @param string $str
	 * @return boolean
	 */
	function isvalidurl($str)
	{
		if ( ! filter_var($str, FILTER_VALIDATE_URL) === FALSE) 
		{
			return TRUE;
		}
		return FALSE;
	}
}

if( ! function_exists('isvalidzip') )
{
	/**
	 * Validate Hungarian ZIP code
	 *
	 * @param string $str
	 * @return boolean
	 */
	function isvalidzip($str)
	{
		return preg_match("#^[0-9]{4}$#i",$str);
	}
}

if( ! function_exists('isvalidphone') )
{
	/**
	 * Validate Hungarian phone nr
	 *
	 * @param string $str
	 * @return boolean
	 */
	function isvalidphone($str)
	{
		$re1 = "/^[\\+][0-9]{1,2}[\\-\\ \\(\\)]{0,2}[0-9]{1,2}[\\-\\ \\(\\)]{0,2}[0-9\\-]{6,20}$/im"; 
	    $re2 = "/^06[\\-\\ \\(\\)]{0,2}[0-9]{1,2}[\\-\\ \\(\\)]{0,2}[0-9\\-]{6,10}$/im";

	    if( ! preg_match($re1,$str) )
	    {
	        if( ! preg_match($re2,$str) )
	        {
	            return FALSE;
	        }
	    }
	    return true;
	}
}

if( ! function_exists('isvaliddate') )
{
	/**
	 * Validate date format (YYYY-MM-DD)
	 *
	 * @param string $str
	 * @return boolean
	 */
	function isvaliddate($str, $sep='-')
	{

		if( mb_strlen($str) == 0 || $str  == '0000-00-00' )
		{
			return FALSE;
		}

		$d_arr = @explode( $sep, $str );
		$y = ( isset($d_arr[0]) ) ? $d_arr[0] : '';
		$m = ( isset($d_arr[1]) ) ? $d_arr[1] : '';
		$d = ( isset($d_arr[2]) ) ? $d_arr[2] : '';

		return checkdate($m,$d,$y);
	}
}

if( ! function_exists('isvaliddatetime') )
{
	/**
	 * Validate datetime format
	 *
	 * @param string $str
	 * @param string $format Default format: Y-m-d H:i:s
	 * @return boolean
	 */
	function isvaliddatetime($str, $format = 'Y-m-d H:i:s')
	{
		if( mb_strlen($str) == 0 || $str  == '0000-00-00 00:00:00' )
		{
			return FALSE;
		}
		
	    $d = DateTime::createFromFormat($format, $str);
	    return $d && $d->format($format) == $str;
	}
}

if( ! function_exists('modifydate') )
{
	/**
	 * Increment or decrement date
	 *
	 * @param string $date
	 * @param integer $addyear
	 * @param integer $addmonth
	 * @param integer $addday
	 * @return date in Y-m-d format
	 */
	function modifydate($date, $addyear, $addmonth, $addday)
	{
		list($year, $month, $day) = explode("-", $date);

		$ts = mktime(0, 0, 0, $month+$addmonth, $day+$addday, $year+$addyear);
		$date = date("Y-m-d", $ts);

		return $date;
	}
}

if( ! function_exists('modifydatetime') )
{
	/**
	 * Increment or decrement datetime
	 *
	 * @param string $datetime
	 * @param integer $addyear
	 * @param integer $addmonth
	 * @param integer $addday
	 * @param integer $addhour
	 * @param integer $addmin
	 * @param integer $addsec
	 * @return date in Y-m-d H:i:s format
	 */
	function modifydatetime($datetime, $addyear, $addmonth, $addday, $addhour, $addmin, $addsec)
	{
		list($date, $time) = explode(" ", $datetime);
		list($year, $month, $day) = explode("-", $date);
		list($hour, $min, $sec) = explode(":", $time);

		$ts = mktime($hour+$addhour, $min+$addmin, $sec+$addsec, $month+$addmonth, $day+$addday, $year+$addyear);
		$datetime = date("Y-m-d H:i:s", $ts);

		return $datetime;
	}
}

if( ! function_exists('rand_str') )
{
	/**
	 * Get random string
	 *
	 * @param integer $length Length of strings
	 * @param string $chars Used char map
	 * @return string
	 */
	function rand_str($length = 32, $chars = 'abcdefghijklmnopqrstuvwxyz1234567890')
	{
		$chars_length = (strlen($chars) - 1);
		$string = $chars{rand(0, $chars_length)};
		for ($i = 1; $i < $length; $i = strlen($string))
		{
			$r = $chars{rand(0, $chars_length)};
			if ($r != $string{$i - 1}) $string .=  $r;
		}
		return $string;
	}
}

if( ! function_exists('input_to_db') )
{
	/**
	 * Escape string
	 *
	 * @param string $val
	 * @return string
	 */
	function input_to_db($val) 
	{
		$db = get_instance()->db->conn_id;
		$val = mysqli_real_escape_string($db, $val);
		return $val;
	}
}

if( ! function_exists('sec_str') )
{
	/**
	 * Strip tags
	 *
	 * @param string $val
	 * @return string
	 */
	function sec_str($str) 
	{
		$str = trim(strip_tags($str));
		$str = str_replace('"', '', $str);
		return $str;
	}
}

if( ! function_exists('file_extension') )
{
	/**
	 * 	get file extension
	 * 	
	 *	@param filename string
	 *	@return string
	 */
	function file_extension($filename)
	{
		return end(explode(".", $filename));
	}
}

if( ! function_exists('getfilename') )
{
	/**
	 *	@param s string
	 *	@return string
	 */
	function getfilename($s)
	{
		$arr=explode(".",$s);
		return strtolower($arr[0]);
	}
}

if( ! function_exists('get_day_name_hu') )
{
	function get_day_name_hu( $date )
	{
		$napok=Array("Vasárnap","Hétfő","Kedd","Szerda","Csütörtök","Péntek","Szombat");
		$date=explode("-",$date);
		$date=mktime(0,0,0,$date[1],$date['2'],$date['0']);
		$nr=date("w", $date);
		return $napok[$nr];
	}
}

if( ! function_exists('get_nameday_hu') )
{
	function get_nameday_hu()
	{
		$_januar = array("","ÚJÉV","Ábel","Genovéva","Titusz","Simon",
		"Boldizsár","Attila","Gyöngyvér","Marcell",
		"Melánia","Ágota","Ernő","Veronika",
		"Bódog","Lóránt","Gusztáv","Antal","Piroska",
		"Sára","Sebestyén","Ágnes","Vince","Zelma",
		"Timót","Pál","Vanda","Angelika","Károly,","Adél","Martina",
		"Marcella");

		$_februar =	array("","Ignác","Karolina","Balázs","Ráhel","Ágota",
		"Dóra","Tódor","Aranka","Abigél","Elvira",
		"Bertold","Lívia","Ella, Linda","Bálint","Kolos",
		"Julianna","Donát","Bernadett","Zsuzsanna","Álmos",
		"Eleonóra","Gerzson","Alfréd","Mátyás","Géza",
		"Edina","Ákos, Bátor","Elemér");

		$_marcius =	array("","Albin","Lujza","Kornélia","Kázmér","Adorján",
		"Leonóra","Tamás","Zoltán","Franciska","Ildikó",
		"Szilárd","Gergely","Krisztián, Ajtony","Matild","Kristóf",
		"Henrietta","Gertrúd","Sándor","József","Klaudia",
		"Benedek","Beáta","Emőke","Gábor","Irén",
		"Emánuel","Hajnalka","Gedeon","Auguszta","Zalán",
		"Árpád");

		$_aprilis =	array("","Hugó","Áron","Buda, Richárd","Izidor","Vince",
		"Vilmos, Bíborka","Herman","Dénes","Erhard","Zsolt",
		"Leó","Gyula","Ida","Tibor","Tas, Anasztázia",
		"Csongor","Rudolf","Andrea","Emma","Konrád, Tivadar",
		"Konrád","Csilla","Béla","György","Márk","Ervin", 
		"Zita","Valéria","Péter","Katalin, Kitti");

		$_majus	= array("","Fülöp","Zsigmond","Tímea","Mónika","Györgyi",
		"Ivett","Gizella","Mihály","Gergely","Ármin",
		"Ferenc","Pongrác","Szervác","Bonifác","Zsófia",
		"Botond, Mózes","Paszkál","Erik","Ivó, Milán","Bernát, Felícia",
		"Konstantin","Júlia, Rita","Dezső","Eszter",
		"Orbán","Fülöp","Hella","Emil, Csanád","Magdolna","Zsanett, Janka",
		"Angéla");

		$_junius = array("","Tünde","Anita, Kármen","Klotild","Bulcsú","Fatime", 
		"Norbert","Róbert","Medárd","Félix","Margit",
		"Barnabás","Villő","Antal, Anett","Vazul","Jolán",
		"Jusztin","Laura","Levente","Gyárfás","Rafael",
		"Alajos","Paulina","Zoltán","Iván","Vilmos",
		"János","László","Levente, Irén","Péter, Pál",
		"Pál");

		$_julius = array("","Annamária","Ottó","Kornél","Ulrik","Sarolta, Emese",
		"Csaba","Appolónia","Ellák","Lukrécia","Amália", 
		"Nóra, Lili","Izabella","Jenő","Őrs","Henrik",
		"Valter","Endre, Elek","Frigyes","Emília","Illés",
		"Dániel","Magdolna","Lenke","Kinga, Kincső",
		"Kristóf, Jakab","Anna, Anikó","Olga",
		"Szabolcs","Márta","Judit","Oszkár");

		$_augusztus	= array("","Boglárka","Lehel","Hermina","Domonkos","Krisztina",
		"Berta","Ibolya","László","Emőd","Lörinc",
		"Zsuzsanna","Klára","Ipoly","Marcell","Mária",
		"Ábrahám","Jácint","Ilona","Huba","István",
		"Sámuel","Menyhért","Bence","Bertalan","Lajos",
		"Izsó","Gáspár","Ágoston","Beatrix","Rózsa",
		"Erika","Egon");

		$_szeptember = array("","Egon","Rebeka","Hilda","Rozália","Viktor, Lőrinc",
		"Zakariás","Regina","Mária","Ádám","Nikolett, Hunor", 
		"Teodóra","Mária","Kornél","Szeréna","Enikő",
		"Edit","Zsófia","Diána","Vilhelmina","Friderika",
		"Máté","Móric","Tekla","Gellért","Eufrozina",
		"Jusztina","Adalbert","Vencel","Mihály","Jeromos");

		$_oktober =	array("","Malvin","Petra","Helga","Ferenc","Aurél",
		"Renáta","Amália","Koppány","Dénes","Gedeon",
		"Brigitta","Miksa","Kálmán","Helén","Teréz",
		"Gál","Hedvig","Lukács","Nándor","Vendel",
		"Orsolya","Előd","Gyöngyi","Salamon","Bianka",
		"Dömötör","Szabina","Simon","Nárcisz","Alfonz",
		"Farkas");

		$_november = array("","Marianna","Achilles","Győző","Károly","Imre",
		"Lénárd","Rezső","Zsombor","Tivadar","Réka",
		"Márton","Jónás, Renátó","Szilvia","Aliz",
		"Albert, Lipót","Ödön","Hortenzia, Gergő",
		"Jenő","Erzsébet","Jolán","Olivér","Cecília",
		"Kelemen","Emma","Katalin","Virág",
		"Virgil","Stefánia","Taksony","András, Andor");

		$_december = array("","Elza","Melinda","Ferenc","Barbara, Borbála","Vilma",
		"Miklós","Ambrus","Mária","Natália","Judit",
		"Árpád","Gabriella","Luca","Szilárda","Valér",
		"Etelka","Lázár","Auguszta","Viola","Teofil",
		"Tamás","Zéno","Viktória","Ádám, Éva","KARÁCSONY",
		"KARÁCSONY","János","Kamilla","Tamás","Dávid",
		"Szilveszter");

		$_nevnap = array("", $_januar, $_februar, $_marcius, $_aprilis, $_majus, $_junius,$_julius, $_augusztus, $_szeptember, $_oktober, $_november, $_december);

		$m = date('n');
		$d = date('j');

		$res = (isset($_nevnap[$m][$d])) ? $_nevnap[$m][$d] : '';
		return $res;
	}
}