<?php defined("BASEPATH") OR exit("No direct script access allowed");

class Frontend_game extends MCMS_Controller {
    
    public $frontend_theme;
    private $user_game_daylist = array();
    
    public function __construct(){
        parent::__construct();
        $this->frontend_theme = $this->config->item('frontend__theme')."/"; // config/mcms_config_frontend.php
        $this->get_calendar_user_game_list();
    }

    /**
     * *************************
     * game view
     * *************************
     */
    public function display(){
        if( false === $this->game_is_active ){
        	redirect('', 'refresh');
        }

        $slug = 'naptar';
		$cmsdata = $this->frontend_model->get_page($slug);
		if ( is_array( $cmsdata ) && count($cmsdata) > 0 ){
			$this->update_settings($cmsdata);

			// render output:
			$this->set_view_data(array(
				'meta_index'	=>	'index, follow, all',
				'cmsdata'		=> $cmsdata,
				'settings'		=> $this->frontend_settings,
				'html_header'	=> $this->frontend_model->get_header( $this->frontend_settings ),
				'slug'			=> $slug,
				'cms_block_type'=> false,
			));
			$this->render('game_display', $this->config->item('frontend__theme'));

		} else {
			redirect('404', 'refresh');
		}
    }

    public function calendar(){

        $this->check_game_available();
        
        $view_data = array();
        
        // játéknapok listája
        $days_list = $this->frontend_model->get_days_list();
        
        //bejelentkezett ügyfélhez tartozó játéknapok beolvasása
        $user_game_list = $this->user_game_list_by_dayid();
        
        $out = "";

        if($days_list){
            foreach( $days_list as $day ){
                
                //$viewed = !$this->account->is_guest()&&$this->db->get_where("user_games",array("user_id"=>$this->account->get_userid(),"game_id"=>$day->id))->row();
                
                $viewed = (!empty($user_game_list[$day->id]) ? 1 : 0);
                $dayview_settings = $this->prepare_dayview_settings($day->cal_day);

                /**
                 * kvíz tipusú nap nem jelenik meg a napok között ha bejelentlezés nélkül is lehet játszani
                 */
                if($day->type == 'quiz' && $this->frontend_settings["login_required"] === false)
                {
                    continue;
                }
                
                $view_data = array(
                    "day_id"            => $day->id,
                    "day_date"          => $day->cal_day,
                    "day_viewed"        => $viewed
                );
                $view_data = $view_data + $dayview_settings;
                
                $out .= $this->load->view($this->frontend_theme."game_daylist",$view_data,true);
            }
        }
        echo $out;
        die;
    }
    
    private function get_calendar_user_game_list()
    {
        if(!$this->account->is_guest() && $this->account->get_userid()>0)
        {
            $this->user_game_daylist = $this->frontend_model->get_user_game_days($this->account->get_userid());
        }
    }
    
    private function user_game_list_by_dayid()
    {
        //$return = new stdClass();
        $return = array();
        
        if($this->user_game_daylist){
            foreach ($this->user_game_daylist as $k => $v) {
                $return[$v->game_id] = $v;
            }
        }
        
        return $return;
    }
    
    private function prepare_dayview_settings($day_date)
    {
       
        $today_date = date("Y-m-d");
        $day_no = date("d",strtotime($day_date));
        $day_str = get_day_name_hu($day_date);
        
        $tooltip = false;
        $prev_days_class = '';
        $class = '';

        if(strtotime($today_date)>strtotime($day_date)){
            $class = "past";

            // visszamenoleg kitölthető?
            if(1 === (int)$this->frontend_settings['enable_prev_days'])
            {
                $prev_days_class = 'calendar-item--prv';
            }
            else{
                $tooltip = ("Sajnos erről a napról lemaradtál");
            }

        } elseif($today_date==$day_date){
             $class = "current";
        } elseif(date("Y-m-d", strtotime($today_date." +1 day"))==$day_date){
            $class = "next";
            $tooltip = lang("GAME_NEXT_DAY_TOOLTIP_MESSAGE");
        } else {
            $class = "future";
        }
        
        if( isset($_SESSION) && isset($_SESSION['admin_email']) && isset($_SESSION['admin_status']) && (int)$_SESSION['admin_status'] == 1 ){
            $prev_days_class = 'calendar-item--adm';
        }
        
        return array(
            "day_no"            => $day_no,
            "day_str"           => $day_str,
            "tooltip"           => $tooltip,
            "class"             => $class,
            "prev_days_class"   => $prev_days_class
        );
        
    }

    private function check_game_available()
    {
    	if (!$this->input->is_ajax_request()) {
    		die;
    	}

    	if( false === $this->game_is_active ){
        	die;
        }            
    }

    public function day($id){
        
        $this->check_game_available();
        
        $view_data = array();
        $view_data['settings'] = $this->frontend_settings;

        $day = $this->frontend_model->get_day_row($id);
        $view_data['day'] = $day;
        if(!$day)
            die;

        $viewed = $this->db->get_where("user_games",array("user_id"=>$this->account->get_userid(),"game_id"=>$day->id))->row();
        $view_data['viewed'] = $viewed;

        // nyero idopont jatek:
        // wintime - nyerő időpont törlés
        // FIGYELEM !!! Szándékosan tér vissza üresen, a nyerő időpont jelenleg inaktív        
        $wintime_res = $this->wintime_res($day, $viewed);
        $view_data['wintime_res'] = $wintime_res;
        
        /**
         * kvíz tipusú nap nem jelenik meg a napok között ha bejelentlezés nélkül is lehet játszani
         */
        $quiz_data = array();
        
        if( $day->type == 'quiz' && $this->account->get_userid() ){
            $quiz_question = $this->frontend_model->get_quiz_question_data($day->id);
            $view_data['quiz_data'] = array(
                "quiz_question" => $quiz_question['quiz_question']
            );
        }

        if( ! isset($user_info) ){
        	$user_info = array("user_id"=>$this->account->get_userid(),"game_id"=>$day->id);
        }
        
        if($day->type == 'puzzle'){
            $params = array(
                'puzzle_pcx' => $day->puzzle_pcx,
                'puzzle_pcy' => $day->puzzle_pcy
            );
            $view_data['puzzle_data'] = $this->prepare_puzzle_data($day->puzzle_image,$params);
        }    

        // save user view info:
        $save_view_info = 0;
        if( $this->account->get_userid() ){
        	$save_view_info = 1;
        }

        if( ! $viewed && $this->account->get_userid()) {
        	$this->db->insert("user_games",$user_info);
        	$save_view_info = 1;
        }

        if ($this->frontend_settings['login_required']){
            
            //van e már kupon eltárolva a userhez (ez azt jelenti, hogy már megnyitotta korábban a napot)
            $user_coupon_fieldname_num = $this->load_saved_coupon_num($day->id,$this->account->get_userid());

            //a userhez még nem tartozik kupon és van valid, akkor véletlenszerűen kiválasztunk egyet és eltároljuk (nem a kupont hanem csak a mezőnévhez tartozó számot!)
            if ($user_coupon_fieldname_num==0){
                
                //megnézzük van e kupon (legalább 1 legyen feltöltve és létezzen a fájl)
                $coupons_fieldname_exts = $this->check_fieldname_has_coupon($day);

                if(!empty($coupons_fieldname_exts))
                {
                    $coupon_fieldname_ext_num = array_rand($coupons_fieldname_exts);  // $coupons_fieldname_exts: tömb! A feltöltött kuponok közül véletlenszerűen választ egyet
                    $user_coupon_fieldname_num = $this->save_coupon_num($coupon_fieldname_ext_num,$day->id,$this->account->get_userid());
                }
            }
            
            if($this->account->get_userid()!=0)
            {
                $view_data['day']->coupon_url_download_hash = $this->get_coupon_url_download($user_coupon_fieldname_num,$day->id);
                $view_data['day']->coupon_url_email_hash = $this->get_coupon_url_email($user_coupon_fieldname_num,$day->id);
            }

        }
        
        
        /**
         * a válasz visszaadja a "game_dayview" sablont, amibe JS tölti bele az adott nap tartalmát először üresen
         * main.js -> $.get("ajax-day/"+day_clicked,function(ret){
         * ezen belül pedig: load_registration_form();
         * ez alapvetően a regisztrációt tölti be ha az ügyfél nincs bejelentkezve vagy login nélkül is játszható
         * Ha pedig megjelenthet a játéknap akkor a szintén a  "game_dayview" sablonban nézi meg hogy megjelenhet e
         * lásd: if($this->account->is_guest() && $settings['login_required']==true)
         */
        
        echo json_encode(array(
        	'html'                      => $this->load->view($this->frontend_theme."game_dayview",$view_data,true),
                'viewed'                    => $save_view_info,
                'login_required'            => $this->frontend_settings['login_required'],
                'GAME_DAY_VIEWED_MESSAGE'   => lang('GAME_DAY_VIEWED_MESSAGE'),
                'is_guest'                  => $this->account->is_guest() // figyelem: ha bejelenkezés nélkül is lehet kjátszani ez akkor is true
        ),JSON_HEX_QUOT | JSON_HEX_TAG);

        die;
    }
    
    public function prepare_puzzle_data($img_name,$params = array())
    {
        require_once 'assets/Mobile-Detect-2.8.34/Mobile_Detect.php';
        $detect = new Mobile_Detect;
        
        // params defaults
        $path = (isset($params['upload_path']) ? $params['upload_path'] : $this->config->item('backend__upload_path_puzzle').'/' );
        $pcx = (isset($params['puzzle_pcx']) ? $params['puzzle_pcx'] : 4 );
        $pcy = (isset($params['puzzle_pcy']) ? $params['puzzle_pcy'] : 3 );
        
        $device_type = "desktop";
        
        // Any mobile device (phones or tablets).
        if ( $detect->isMobile() ) {
            $device_type = 'mobile';
        }
        
        // Any tablet device.
        if( $detect->isTablet() ){
            $device_type = 'tablet';
        }

        switch ($device_type) {
            case 'mobile':
                $return = $this->get_images_for_puzzle($path, $img_name, $pcx, $pcy, 'mobile_');
                break;
            case 'tablet':
                $return = $this->get_images_for_puzzle($path, $img_name, $pcx, $pcy, 'tablet_');
                break;
            case 'desktop':
            default:
                $return = $this->get_images_for_puzzle($path, $img_name, $pcx, $pcy, '');
                break;
        }
        

        
        return $return;
    }
    
    public function get_images_for_puzzle($path,$img_name,$pcx,$pcy,$device='')
    {
        $return = array(
            'error' => ''
        );
        
        if(file_exists($path.$device.$img_name))
        {
            $img = $path.$device.$img_name;
            
            $img_info = getimagesize($img);
            $path_parts = pathinfo($img);
            $img_name_without_ext = $path_parts['filename'];
            
            if(false !== $img_info)
            {
                $img_width = $img_info[0];
                $img_height = $img_info[1];
                
                $crop_w = round($img_width / $pcx);
                $crop_h = round($img_height / $pcy);
                
                
                $return['global']['width'] = $img_width;
                $return['global']['height'] = $img_height;
                $return['global']['inline_css'] = "width: {$img_width}px; height: {$img_height}px";

                for ($x= 0; $x < $pcx; $x++) {
                    for ($y = 0; $y < $pcy; $y++) {
                        $x1 = ($x*$crop_w);
                        $x2 = ($x*$crop_w + $crop_w);

                        $y1 = ($y*$crop_h);
                        $y2 = ($y*$crop_h + $crop_h);

                        //$dest_file = $path.$img_name_without_ext.'/'.$x.$y.$img_name;
                        //$mobile_dest_file = $path."mobile_".$img_name_without_ext.'/'.$x.$y."mobile_".$img_name;
                        //$piece_data = getimagesize($mobile_dest_file);
                        $dest_file = $path.$img_name_without_ext.'/'.$x.$y.$device.$img_name;

                        $return['pieces'][] = array(
                            'file' => $dest_file,
                            'file_basename' => $img_name_without_ext,
                            'left' => $x1,
                            'top'  => $y1,
                            'inline_css' => "background-image: url('$dest_file'); left: {$x1}px; top: {$y1}px; width: {$crop_w}px; height: {$crop_h}px",
                        );

                    }
                }                    

            }
            else
            {
                $return['error'] = "Sajnáljuk, valami hiba történt. Gond van a feltöltött képpel, kérlek lépj kapcsolatba velünk.";
            }
            
        }
        else
        {
            $return['error'] = "Sajnáljuk, valami hiba történt. Nem találjuk a fájlt, kérlek lépj kapcsolatba velünk.";
            //die('nem létezik a fájl');
        }
        return $return;
    }

    public function quiz( $day_id, $answer_id ){

        $this->check_game_available();

        //$day = $this->db->where("id",$day_id)->get("calendar_days")->row();
        $day = $this->frontend_model->get_day_row($day_id);  // <--- itt felmerül a kérdés, hogy a "status=1" feltételnek kell e teljesülni/teljesülhet e
        if(!$day)
            die;

        $quiz_answer = $this->frontend_model->get_quiz_row($day_id, $answer_id);
        if(!$quiz_answer)
            die;

        $is_correct = $quiz_answer->is_correct==1;
        $fieldname = $quiz_answer->fieldname;

        $this->db
            ->where(array("user_id"=>$this->account->get_userid(),"game_id"=>$day->id))
            ->update("user_games",array("fieldname"=>$fieldname, "answer_id"=>$answer_id, "game_result"=>$is_correct?1:0));
        
        $msg = ( $is_correct ) ? $day->quiz_success : $day->quiz_failure;
        
        /**
         * nyertes találatok számának frissítése
         * Ha manuálisan kell az egészet frissíteni akkor elvileg ez az az update amivel lehet
         * 
            UPDATE mcms_calendar_days t2,
            (   SELECT COUNT(user_id) AS users, game_id from mcms_user_games WHERE game_result=1 GROUP BY game_id ) t1
            SET t2.quiz_correct_count = t1.users
            WHERE t1.game_id = t2.id
         * 
         */
        if($is_correct)
        {
            $this->db->where('id',$day->id);
            $this->db->set('quiz_correct_count', 'quiz_correct_count+1', FALSE);
            $this->db->update("calendar_days");
        }

        // correct ans id:
        $correct_ans_id = $answer_id;
        if( ! $is_correct ){
        	$this->db->select('id');
			$this->db->where("day_id", $day_id);
			$this->db->where("is_correct",1);
			$this->db->limit(1);
	        $query = $this->db->get("calendar_quiz");
	        $res = $query->result_array();
	        $correct_ans_id = $res[0]['id'];
        }

        echo json_encode(array("id"=>$correct_ans_id,	// helyes valasz id
                               "result"=>$this->load->view($this->frontend_theme."game_quiz_result",array("msg"=>$msg,"day"=>$day,"is_correct"=>$is_correct),true)));
        die;
    }
    
    public function wintime_res($day, $viewed)
    {
        $wintime_res = '';
        
        // wintime - nyerő időpont törlés
        // FIGYELEM !!! Szándékosan tér vissza üresen, a nyerő időpont jelenleg inaktív
        return $wintime_res; 
        
        if( $day->type == 'wintime' && $this->account->get_userid() ){
            /* // wintime - nyerő időpont törlés
             * // ne töröld, mert szándékosan csak kommentelve van */
        	if( ! $viewed ){
	        	// user adatok:
	        	$this->db->select('id,first_name,last_name,email_address');
				$this->db->where("id", $this->account->get_userid());
		        $query = $this->db->get("users");
		        $user = $query->result();
		        if( is_array($user) && count($user) > 0 ){

		        	// user nyert mar ma?
		        	$this->db->select('id');
					$this->db->where("user_id", $this->account->get_userid());
					$this->db->where("win_day", date('Y-m-d'));
			        $query = $this->db->get("user_games");
			        if( count($query->result_array()) == 0 ){

			        	// van nyero idopont?
			        	$this->db->select('id,prize_id,win_time');
						$this->db->where("user_id", '0');
						$this->db->where("win_time <=", date('Y-m-d H:i:s'));
						$this->db->order_by('win_time', 'ASC');
						$this->db->limit(1);
				        $query = $this->db->get("prize_win_times");
				        $win_time = $query->result();	// ezt nyerte meg...
				        if( count($win_time) == 1 ){

				        	$time_id = $win_time[0]->id;

				        	// mi a nyeremeny?
				        	$this->db->select('id,title');
							$this->db->where("id", $win_time[0]->prize_id);
							$this->db->limit(1);
					        $query = $this->db->get("prizes");
					        $prize = $query->result();

					        // idopont lista update:
					        $uid = $this->account->get_userid();
					        $this->db->query("UPDATE mcms_prize_win_times SET user_id='$uid' WHERE id='$time_id' LIMIT 1");

					        // view info update:
					        $user_info = array(
					        	"user_id"		=>	$this->account->get_userid(),
					        	"game_id"		=>	$day->id,
					        	"win_time_id" 	=>	$time_id,
					        	"win_prize_id" 	=>	$prize[0]->id,
					        	"win_prize_name" =>	$prize[0]->title,
					        	"win_day" 		=>	date('Y-m-d'),
					        );

					        // nyero szoveg:
					        $wintime_res = ( strlen($day->wintime_success) > 0 ) ? $day->wintime_success : 'Gratulálunk, nyertél!';;
					        $wintime_res = str_replace('{prize}', $prize[0]->title, $wintime_res);

					        // level:
					        if( isvalidemail($user[0]->email_address) ){

					        	$this->db->select('email_subject_won,email_content_won');
								$this->db->where("id", 1);
						        $query = $this->db->get("setup");
						        $mail = $query->result();

						        $message = $mail[0]->email_content_won;
						        $message = str_replace('{prize}',$prize[0]->title,$message);
						        $message = str_replace('{name}',$user[0]->last_name." ".$user[0]->first_name,$message);
						        $message = str_replace('{link}',base_url(),$message);

					        	$this->email->to($user[0]->email_address,$user[0]->last_name." ".$user[0]->first_name);
				                $this->email->subject($mail[0]->email_subject_won);
				                $this->email->message($message);
				                $this->email->send();
					        }

				        }
			        }
		        }
        	}
            // */
        }
        
        return $wintime_res;
    }
    
    /**
     * kupon-letoltes
     * 
     * @param type $hash
     */
    public function download_coupon($hash)
    {
        $data = json_decode(base64_decode($hash),true);
        $error = array();
        
        $day_id = (int)$data['day_id'];
        $coupon_num = (int)$data['coupon'];
        $mode = (!empty($data['mode']) ? $data['mode'] : 'download' );

        if($day_id>0){

            $day = $this->frontend_model->get_day_row($day_id);
            if(!$day)
            {
                $error[] = "Hiba a játéknap beolvasása során";
                die;
            }

            if(!empty($day->{"coupon_$coupon_num"})){
                if(file_exists($this->config->item('backend__upload_path_coupon').'/'.$day->{"coupon_$coupon_num"})){

                    if($mode == 'download'){
                        $this->download_file($day->{"coupon_$coupon_num"});
                        die();
                    }

                    if($mode == 'email'){
                        $user = $this->db->get_where("users",array("id"=>$this->account->get_userid()))->row();
                        $hash = $this->get_coupon_url_download($coupon_num, $day_id);
                        $mail_res = $this->send_coupon_in_mail($hash,$user);
                            echo json_encode(
                                    array("error"=>'',
                                          "res" => $mail_res,
                                          "message"=>'<div>A kupont elküldtük a regisztrált e-mail címre</div>'),true);
                        die();
                    }
                    
                    if($mode!=='download' || $mode!=='email'){
                        $error[] = "Hibás letöltési mód: --".$mode."--";
                    }
                }
            }            
        }
        else{
            $error[] = "Hibás játéknap azonosító";
        }
    }
    
    
    /**
     * kupon küldése emailtben
     * 
     * @param type $param
     */
    public function send_coupon_in_mail($hash,$user) {
        
            $this->email->to($user->email_address,$user->last_name." ".$user->first_name);
            $this->email->subject($this->frontend_settings['email_subject_coupon']);
            $this->email->message(str_replace("{kupon_link}",'<a href="'.base_url("kupon-letoltes/".$hash).'">Kupon letöltése</a>',$this->frontend_settings['email_content_coupon']));
            return $this->email->send();

    }
    
    private function download_file($filename)
    {
        $file = $this->config->item('backend__upload_path_coupon')."/".$filename;
        
        header("Content-Type: application/octet-stream");

        header("Content-Disposition: attachment; filename=" . urlencode($filename));   
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Description: File Transfer");            
        header("Content-Length: " . filesize($file));

        echo file_get_contents($file);
        die();
    }
    
    /**
     * van e feltöltve kupon.
     * legalább 1 kuponnak kell lennie, és egy tömb a visszatérési érték ami egy számot tartalmaz 1 v 2 v 3
     * azért ezt adja vissza, mert ezzel tudjuk azt hogy melyik kupon mezőben van tartalom! így a visszaolvasásnál ez használható
     * 
     * @param object $day nap
     * @return array kuponok mezőnév végződése (tehát "coupon_1" esetén "1")
     */
    private function check_fieldname_has_coupon($day)
    {
        $return = array();
        
        if(!empty($day->coupon_1)){
            if(file_exists($this->config->item('backend__upload_path_coupon').'/'.$day->coupon_1)){
                $return[1] = 1;
            }
        }
        if(!empty($day->coupon_2)){
            if(file_exists($this->config->item('backend__upload_path_coupon').'/'.$day->coupon_2)){
                $return[2] = 2;
            }
        }
        if(!empty($day->coupon_3)){
            if(file_exists($this->config->item('backend__upload_path_coupon').'/'.$day->coupon_3)){
                $return[3] = 3;
            }
        }
        
        return $return;
    }
    
    /**
     * kupon véletlenszerű kiválasztása (csak több kupon esetén) és mentése a felhasználóhoz
     * ha újra megnyitja a naptári napot, akkor ne egy másik kupont lásson
     * fontos: csak egy számot tárolunk el, ami a kupon mezőnevet azonosítja. 
     * 
     * Az alapján a mezőnév alapján kell a kupont visszaolvasni! Ha ezt törlik akkor onnantól nem lesz elérhető!
     * 
     * @param type $coupons
     */
    private function save_coupon_num($coupon_fieldname_ext_num,$day_id,$user_id)
    {
        if(!empty($coupon_fieldname_ext_num))
        {
            $saved = $this->frontend_model->save_user_coupon_num($coupon_fieldname_ext_num,$day_id,$user_id);
            if($saved>0){
                return $coupon_fieldname_ext_num;
            }
        }
        return 0;
    }
    
    private function load_saved_coupon_num($day_id,$user_id)
    {
        $return = $this->frontend_model->load_user_coupon_num($day_id,$user_id);
        return $return;
    }
    
    private function get_coupon_url_download($coupon_fieldname_ext,$day_id)
    {
        $return = '';
        
            if($coupon_fieldname_ext>0){
                $hash_data = array(
                    'day_id'    => $day_id,
                    'mode'      => 'download',
                    'coupon'    => $coupon_fieldname_ext,
                    'timestamp' => time()
                );
                $return = base64_encode(json_encode($hash_data, $assoc=true));
            }
        
        return $return;
    }
    
    private function get_coupon_url_email($coupon_fieldname_ext,$day_id)
    {
        $return = '';
        
            if($coupon_fieldname_ext>0){
                $hash_data = array(
                    'day_id'    => $day_id,
                    'mode'      => 'email',
                    'coupon'    => $coupon_fieldname_ext,
                    'timestamp' => time()
                );
                $return = base64_encode(json_encode($hash_data, $assoc=true));
            }
        
        return $return;        
    }
    
    private function get_coupon_url_download_from_email()
    {
        
    }
    
    
}

