<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ********************************
 * backend manage admin users
 * ********************************
 */
class Backend_admins extends MCMS_Controller 
{
	// setup:
	private $slug_pattern = 'admins';
	private $table_name = 'admins';

	function __construct()
	{
		parent::__construct();

		// check user role:
		if( ! $this->backend_model->user_has_right('list', $this->slug_pattern) )
		{
			redirect( site_url('admin/access_info'), 'refresh' );
			return;
		}
	}

	public function index()
	{
		redirect( site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh' );
	}

	/**
	 * *************************
	 * crud
	 * *************************
	 */
	public function crud()
	{
		// setup:
		$prefix_table_name = $this->db->dbprefix($this->table_name);
		$view_filename = 'admins';
		$pagetitle = lang('page_sidebar_admins');
		$sidebar_info = '';

		// id exists:
		if( $this->uri->segment(3) == 'edit' )
		{
			$id = $this->uri->segment(4);
			if( 
				! isvalidnumber($id,255) || 
				! $this->backend_model->record_exists($this->table_name, array( 'id' => $id ) ) )
			{
				redirect( site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh' );
			}
		}

		// init:
		$crud = $this->get_grocery_crud($prefix_table_name, '');
		$crud->set_crud_url_path(site_url('admin/'. $this->slug_pattern));
                
                //ha nem besocial email cím, akkor a besoical című adminisztrátorokat nem láthatja!
                if(!stristr($this->session->userdata["admin_email"], "@besocial.hu"))
                {
                    $crud->not_like('admin_email','@besocial.hu');
                }

		// list:
		$crud->columns('id', 'admin_name', 'admin_email', 'admin_level','admin_status');

		// add:
		$crud->add_fields('admin_name','admin_email','admin_password','admin_level','admin_create_time','admin_status');

		// edit:
		$crud->edit_fields('admin_name','admin_email','admin_password','admin_level','admin_create_time','admin_status');

		// disable:
		$crud->unset_read();
		$crud->unset_export();
		$crud->unset_clone();
		
		// spec field types:
		$crud->field_type('admin_status','dropdown', $this->config->item('backend__status_default'));
		$crud->field_type('admin_level','dropdown', $this->config->item('backend__admin_levels'));
		$crud->change_field_type('admin_password','password');
		$crud->field_type('admin_create_time', 'invisible');

		// display names:
		$crud->display_as( 'id', lang('id') )
             ->display_as( 'admin_name', lang('admin_name') )
             ->display_as( 'admin_email', lang('admin_email') )
             ->display_as( 'admin_password', lang('admin_password') )
             ->display_as( 'admin_level', lang('admin_level') )
             ->display_as( 'admin_status', lang('admin_status') );

		// validation:
		$crud->set_rules( 'admin_name', lang('admin_name'),'required|max_length[255]' )
             ->set_rules( 'admin_email', lang('admin_email'), 'required|valid_email|max_length[255]' )
             ->set_rules( 'admin_level', lang('admin_level'), 'required' ) ;

        // password:
		$state = $crud->getState();
		$state_info = $crud->getStateInfo();
		if($state == 'add' || $state == 'insert_validation' )
		{
			$crud->set_rules( 'admin_password', lang('admin_password'), 'required' );
		}
		
		$curr_id = 0;
		if($state == 'edit' )
		{
			$curr_id = (int)$this->uri->segment(4);
		}

		// callbacks:
		$crud->callback_before_insert(array($this, '_crud_before_insert'));
		$crud->callback_before_update(array($this, '_crud_before_update'));
		$crud->callback_after_insert(array($this, '_crud_after_insert'));
		$crud->callback_after_update(array($this, '_crud_after_update'));
		$crud->callback_after_delete(array($this, '_crud_after_delete'));

		// render output:
		$this->set_view_data(array(
			'crud_output'	=>	$crud->render(),
			'pagetitle'		=>	$pagetitle,	
			'sidebar_info'	=>	$sidebar_info,
			'curr_id'			=>	$curr_id,
		));
		$this->render( $view_filename, $this->config->item('backend__theme') );
	}


	/**
	 * *************************
	 * before insert callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 * @return array
	 */
	function _crud_before_insert($post_array)
	{
		// create date
		$post_array['admin_create_time'] = date("Y-m-d H:i:s");

		// pwd:
		$post_array['admin_password'] = sha1( $post_array['admin_password'] );

		// xss_clean:
		$post_array = $this->backend_model->xss_clean($post_array);

		return $post_array;
	}

	/**
	 * *************************
	 * before update callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 */
	function _crud_before_update($post_array, $primary_key)
	{
		// pwd:
		if( $post_array['admin_password'] != '' )
		{
			$post_array['admin_password'] = sha1( $post_array['admin_password'] );
		}
		else
		{
			$old_pwd_arr = $this->backend_model->get_val( $this->table_name, $primary_key, 'admin_password' );
			$old_pwd = $old_pwd_arr[0]['admin_password'];
			$post_array['admin_password'] = $old_pwd;
		}

		// xss_clean:
		$post_array = $this->backend_model->xss_clean($post_array);

		return $post_array;
	}

	/**
	 * *************************
	 * after insert callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 * @param integer $primary_key
	 * @return boolean
	 */
	function _crud_after_insert( $post_array, $primary_key )
	{
		// log:
		$this->backend_model->log($primary_key, array(
			"admin_id" 		=> $this->session->userdata('id'),
			"info" 			=> 'admin_insert: ' . $primary_key,
			"create_date" 	=> date('Y-m-d H:i:s')
		));

		return TRUE;
	}

	/**
	 * *************************
	 * after update callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 * @param integer $primary_key
	 * @return boolean
	 */
	function _crud_after_update( $post_array, $primary_key )
	{
		// log:
		$this->backend_model->log($primary_key, array(
			"admin_id" 		=> $this->session->userdata('id'),
			"info" 			=> 'admin_update: ' . $primary_key,
			"create_date" 	=> date('Y-m-d H:i:s')
		));

		return TRUE;
	}	

	/**
	 * *************************
	 * after delete callback
	 * *************************
	 * 
	 * @param integer $primary_key
	 * @return boolean
	 */
	public function _crud_after_delete($primary_key)
	{
		// log:
		return $this->backend_model->log($primary_key, array(
			"admin_id" 		=> $this->session->userdata('id'),
			"info" 			=> 'admin_delete: ' . $primary_key,
			"create_date" 	=> date('Y-m-d H:i:s')
		));
	}
}