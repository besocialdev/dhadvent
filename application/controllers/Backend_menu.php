<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ********************************
 * backend manage menu items
 * ********************************
 */
class Backend_menu extends MCMS_Controller 
{
	// setup:
	private $slug_pattern = 'menu';

	function __construct()
	{
		parent::__construct();

		// check user role:
		if( ! $this->backend_model->user_has_right('list', $this->slug_pattern) )
		{
			redirect( site_url('admin/access_info'), 'refresh' );
			return;
		}
	}

	public function index()
	{
		redirect( site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh' );
	}

	/**
	 * *************************
	 * crud
	 * *************************
	 */
	public function crud()
	{
		// setup:
		$table_name = 'menu_items';	
		$prefix_table_name = $this->db->dbprefix($table_name);
		$view_filename = 'menu';
		$pagetitle = lang('page_sidebar_menuitems');

		// id exists:
		if( $this->uri->segment(3) == 'edit' )
		{
			$id = $this->uri->segment(4);
			if( 
				! isvalidnumber($id,255) || 
				! $this->backend_model->record_exists($table_name, array( 'id' => $id ) ) )
			{
				redirect( site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh' );
			}
		}

		$pagetitle = ( $this->uri->segment(3) == 'edit' ) ? lang('edit_menuitem') : lang('add_menuitem');
		$crud = $this->get_grocery_crud($prefix_table_name, '');
		$crud->set_relation('menu_id','mcms_menu','label');
		//$crud->set_relation('parent_id','mcms_menu_items','item_label', array('parent_id'=>0,'menu_id'=>1));
		$crud->set_crud_url_path(site_url('admin/'. $this->slug_pattern));
		$crud->unset_export();
		$crud->columns('item_label','menu_id','item_url','item_status');
		$crud->add_fields('menu_id','item_status','item_label','item_url','item_url_target','item_css_class','item_order','create_date');
		$crud->edit_fields('menu_id','item_status','item_label','item_url','item_url_target','item_css_class','item_order','create_date');
		$crud->field_type('item_status','dropdown', $this->config->item('backend__status_default'));

		$crud->field_type('item_url_target','dropdown', array(
			'_self' 	=> lang('_self'), 
			'_blank' 	=> lang('_blank'),
		));

		$crud->field_type('create_date', 'invisible');

		// display names:
		$req = $this->config->item('backend__label_req');
		$crud->display_as( 'id', lang('id') )
             ->display_as( 'menu_id', lang('menu_id') . $req )
             ->display_as( 'item_label', lang('item_label') . $req )
             ->display_as( 'item_url', lang('item_url') . $req )
             ->display_as( 'item_url_target', lang('item_url_target') . $req )
             ->display_as( 'item_css_class', lang('css_class') )
             ->display_as( 'parent_id', 'Szülő menüpont' )
             ->display_as( 'item_order', lang('item_order') )
             ->display_as( 'create_date', lang('create_date') )
             ->display_as( 'item_status', lang('status') );


        // validation:
		$crud->set_rules( 'menu_id',lang('menu_id'),'required')
	         ->set_rules( 'item_label',lang('item_label'),'required|max_length[255]')
	         ->set_rules( 'item_css_class', lang('meta_title'), 'max_length[255]' )
	         ->set_rules( 'item_url_target',lang('item_url_target'),'required')
			 ->set_rules( 'item_url',lang('item_url'), 'required|max_length[255]');
		
		// roles:
		$state = $crud->getState();
		$state_info = $crud->getStateInfo();
		if( ! $this->backend_model->user_has_right('create', $this->slug_pattern) )
		{
			$crud->unset_add();
		}
		if( ! $this->backend_model->user_has_right('delete', $this->slug_pattern) )
		{
			$crud->unset_delete();
		}
		if( ! $this->backend_model->user_has_right('publish', $this->slug_pattern) )
		{
			if( $state == 'add' || $state == 'insert_validation' )
			{
				$crud->set_rules( 'item_status', lang('item_status'),'required|integer' );
				$crud->field_type('item_status','dropdown', $this->config->item('backend__status_author'));
			}
			if($state == 'edit' || $state == 'update_validation' )
			{
				$crud->set_rules( 'item_status', lang('item_status'),'integer' );
				$crud->field_type('item_status', 'invisible');
			}
		}
		else
		{
			$crud->set_rules( 'item_status', lang('item_status'),'required|integer' );
		}
		
		// callbacks:
		$crud->callback_before_insert(array($this, '_crud_before_insert'));
		$crud->callback_before_update(array($this, '_crud_before_update'));
		$crud->callback_after_insert(array($this, '_crud_after_insert'));
		$crud->callback_after_update(array($this, '_crud_after_update'));
		$crud->callback_after_delete(array($this, '_crud_after_delete'));

		// render output:
		$this->set_view_data(array(
			'crud_output'	=>	$crud->render(),
			'pagetitle'		=>	$pagetitle,	
		));
		$this->render( $view_filename, $this->config->item('backend__theme') );
	}

	/**
	 * *************************
	 * before insert callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 * @return array
	 */
	function _crud_before_insert($post_array)
	{
		// create date
		$post_array['create_date'] = date("Y-m-d H:i:s");

		// xss_clean:
		$post_array = $this->backend_model->xss_clean($post_array);

		return $post_array;
	}

	/**
	 * *************************
	 * before update callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 */
	function _crud_before_update($post_array)
	{
		// xss_clean:
		$post_array = $this->backend_model->xss_clean($post_array);

		return $post_array;
	}

	/**
	 * *************************
	 * after insert callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 * @param integer $primary_key
	 * @return boolean
	 */
	function _crud_after_insert( $post_array, $primary_key )
	{
		// log:
		return $this->backend_model->log($primary_key, array(
			"admin_id" 		=> $this->session->userdata('id'),
			"info" 			=> 'menu_item_insert: ' . $primary_key,
			"create_date" 	=> date('Y-m-d H:i:s')
		));
	}

	/**
	 * *************************
	 * after update callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 * @param integer $primary_key
	 * @return boolean
	 */
	function _crud_after_update( $post_array, $primary_key )
	{
		// log:
		return $this->backend_model->log($primary_key, array(
			"admin_id" 		=> $this->session->userdata('id'),
			"info" 			=> 'menu_item_update: ' . $primary_key,
			"create_date" 	=> date('Y-m-d H:i:s')
		));
	}	

	/**
	 * *************************
	 * after delete callback
	 * *************************
	 * 
	 * @param integer $primary_key
	 * @return boolean
	 */
	public function _crud_after_delete($primary_key)
	{
		// log:
		return $this->backend_model->log($primary_key, array(
			"admin_id" 		=> $this->session->userdata('id'),
			"info" 			=> 'menu_item_delete: ' . $primary_key,
			"create_date" 	=> date('Y-m-d H:i:s')
		));
	}
}