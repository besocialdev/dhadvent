<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ********************************
 * backend manage pages
 * ********************************
 */
class Backend_pages extends MCMS_Controller 
{
	// setup:
	private $slug_pattern = 'pages';
	private $table_name = 'pages';

	function __construct()
	{
		parent::__construct();

		// check user role:
		if( ! $this->backend_model->user_has_right('list', $this->slug_pattern) )
		{
			redirect( site_url('admin/access_info'), 'refresh' );
			return;
		}

		$this->load->model("backend_pages_model");
	}

	public function index()
	{
		redirect( site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh' );
	}

	/**
	 * *************************
	 * crud
	 * *************************
	 */
	public function crud()
	{
		// setup:
		$prefix_table_name = $this->db->dbprefix($this->table_name);
		$view_filename = 'pages';
		$pagetitle = lang('page_sidebar_sites');
		$sidebar_info = '';
		$curr_id = null;

		// id exists:
		if( $this->uri->segment(3) == 'edit' )
		{
			$id = $this->uri->segment(4);
			if( 
				! isvalidnumber($id,255) || 
				! $this->backend_model->record_exists($this->table_name, array( 'parent_id' => 0, 'id' => $id ) ) )
			{
				redirect( site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh' );
			}
		}

		// init:
		$crud = $this->get_grocery_crud($prefix_table_name, '');
		$crud->set_relation('lang_id','mcms_languages','lang_name', null, null, 1);
		$crud->where('parent_id', 0);
		$crud->where('mcms_pages.id !=', 3);
		$crud->set_crud_url_path(site_url('admin/'. $this->slug_pattern));
		$crud->unset_export();
		$crud->unset_read();
		$crud->unset_add();
		$crud->unset_clone();
		$crud->unset_delete();
		$crud->columns('note','title','is_mainpage','status');
		$crud->unset_texteditor('lead','cms_page_block_content','meta_description','og_description');
		$crud->field_type('status','dropdown', $this->config->item('backend__status_default'));
		$crud->field_type('cms_block_type','dropdown', array('image_on_left'=>'Kép a bal oldalon','image_on_right'=>'Kép a jobb oldalon')); // enum
		$crud->field_type('cms_block_mainpage_visible','dropdown', array(0=>'A blokk inaktív, nem jelenik meg',1=>'A blokk csak a nyitóoldalon jelenik meg', 2=>'A blokk csak az aloldal tartalmánál jelenik meg', 3=>'A blokk az aloldalon és a nyitóoldalon is megjelenik'));
		$crud->field_type('prizes_block_type','dropdown', array(0=>'Statikus tartalom (az itt kitöltött blokk elemek jelennek meg)', 1=>'Slider (a NYEREMÉNYEK\Nyeremények oldalra feltöltött nyereménylista sliderként jelenik meg)'));
		$crud->field_type('menu_zone_header','dropdown', array(0=>'Nem',1=>'Igen'));
		$crud->field_type('menu_zone_footer','dropdown', array(0=>'Nem',1=>'Igen'));
		$crud->field_type('create_date', 'invisible');
		$crud->field_type('parent_id', 'invisible');
		$crud->field_type('note', 'readonly');
		$crud->field_type('post_type', 'invisible');
		$crud->set_field_upload( 'og_image', $this->config->item('backend__upload_path') );
		$crud->set_field_upload( 'cover_image', $this->config->item('backend__upload_path') );
		$crud->set_field_upload( 'main_page_image', $this->config->item('backend__upload_path') );
                
                //ha "Vendég" módban van a játék, akkor bizonoys oldalaknak nem kell megjelenni
                if(false === $this->frontend_settings['login_required'] && !empty($this->disabled_login_required_pages) )
                {
                    //$crud->where('mcms_pages.menu_slug NOT IN', "('". implode("', '", $this->disabled_login_required_pages)."')" );
                    //$crud->where('mcms_pages.menu_slug NOT IN', $this->disabled_login_required_pages);
                    $crud->where_not_in('mcms_pages.menu_slug', $this->disabled_login_required_pages);
                }
                
		// display names:
		$req = $this->config->item('backend__label_req');
		$crud->display_as( 'id', lang('id') )
             ->display_as( 'lang_id', lang('lang_id') )
             ->display_as( 'is_mainpage', lang('is_mainpage') )
             ->display_as( 'title', 'Nyilvános cím' )
             ->display_as( 'lead', lang('lead') )
             ->display_as( 'prizes_block_type', 'Nyeremény blokk megjelenítése' )
             ->display_as( 'cms_page_block_pre_title', 'Blokk elő-cím' )
             ->display_as( 'cms_page_block_title', 'Blokk főcím' )
             ->display_as( 'cms_page_block_content', 'Blokk szöveg' )       
             ->display_as( 'cms_block_type', 'Blokk elrendezése' )
             ->display_as( 'cms_block_mainpage_visible', 'Blokk megjelenése:' )
             ->display_as( 'cms_block_mainpage_order', 'Blokk nyitóoldali sorrendje' )
             ->display_as( 'fooldal_also_blokk_cim', 'Blokk alsó címsor' )
             ->display_as( 'fooldal_also_blokk_szoveg', 'Blokk alsó szöveg' )
             ->display_as( 'menu_zone_header', 'Fejléc menüben megjelenik?' )
             ->display_as( 'menu_zone_footer', 'Lábléc menüben megjelenik?' )
             ->display_as( 'menu_order_header', 'Fejléc menü sorrend' )
             ->display_as( 'menu_order_footer', 'Lábléc menü sorrend' )
             ->display_as( 'content', lang('content') )
             ->display_as( 'menu_slug', lang('menu_slug') )
             ->display_as( 'css_class', lang('css_class') )
             ->display_as( 'css_id', lang('css_id') )
             ->display_as( 'cover_image', 'Borítókép (555x545px-re méretezve)' )
             ->display_as( 'main_page_image', 'Nyitóoldali kép (665x406px-re méretezve)' )
             ->display_as( 'meta_title', lang('meta_title') )
             ->display_as( 'meta_description', lang('meta_description') )
             ->display_as( 'og_title', lang('og_title') )
             ->display_as( 'og_description', lang('og_description') )
             ->display_as( 'og_image', lang('og_image') )
             ->display_as( 'main_image', lang('main_image') )
             ->display_as( 'create_date', lang('create_date') )
             ->display_as( 'mainpage_bg_image', 'Háttérkép (min. 1920x1080px, jpg vagy png)' )
             ->display_as( 'mainpage_bg_video', 'Háttér videó URL' )
             ->display_as( 'note', "Oldal tartalma" )
             ->display_as( 'status', lang('status') );
                
                // új mező: Blokk sátusz:
                // - blokk inaktív
                // - statikus blokk
                // - slider (nyeremények feltöltése kötelező)
                
                // nyitóoldalon jelenik meg
                // az aloldalon jelenik meg
                // a nyitóoldalon és az alodalon is megjelenik
                
        
		// callbacks:
		$crud->callback_before_insert(array($this, '_crud_before_insert'));
		$crud->callback_before_update(array($this, '_crud_before_update'));
		$crud->callback_after_insert(array($this, '_crud_after_insert'));
		$crud->callback_after_update(array($this, '_crud_after_update'));
		$crud->callback_before_delete(array($this, '_crud_before_delete'));
		$crud->callback_after_delete(array($this, '_crud_after_delete'));
		$crud->callback_after_upload(array($this,'_crud_after_upload'));

		// subpages:
		$state = $crud->getState();
		$state_info = $crud->getStateInfo();
		if($state == 'edit')
		{
			$pagetitle = lang('edit_page');
			$curr_id = (int)$this->uri->segment(4);
			$sidebar_arr = $this->backend_model->sidebar_info($this->table_name, $curr_id, 'create_date');
			
			// default edit fields:
			$crud->edit_fields(
				'note',
                                'title',
                                'status',
                                //'lead',
                                'content',
                                'cover_image',
                                'menu_zone_header',
                                'menu_zone_footer',
                                'menu_order_header',
                                'menu_order_footer',
                                'meta_title',
                                'meta_description',
                                'og_title',
                                'og_description',
                                'og_image');

			// nyeremények, nyertesek
                        if( in_array($curr_id,  array(36,37)) ){
                            
                            $edit_fields = array();
                            $edit_fields[] = 'note';
                            $edit_fields[] = 'title';
                            $edit_fields[] = 'status';

                            $edit_fields[] = 'content';
                            $edit_fields[] = 'cms_block_mainpage_visible';
                            
                            if($curr_id==36)
                            {
                                $edit_fields[] = 'prizes_block_type';
                            }
                            $edit_fields[] = 'cms_block_type';
                            $edit_fields[] = 'cms_block_mainpage_order';
                            
                            $edit_fields[] = 'cms_page_block_pre_title';
                            $edit_fields[] = 'cms_page_block_title';
                            $edit_fields[] = 'cms_page_block_content';
                            $edit_fields[] = 'cover_image';
                            
                            $edit_fields[] = 'menu_zone_header';
                            $edit_fields[] = 'menu_zone_footer';
                            $edit_fields[] = 'menu_order_header';
                            $edit_fields[] = 'menu_order_footer';
                            $edit_fields[] = 'meta_title';
                            $edit_fields[] = 'meta_description';
                            $edit_fields[] = 'og_title';
                            $edit_fields[] = 'og_description';
                            $edit_fields[] = 'og_image';
                            
                            // default edit fields:
                            $crud->edit_fields($edit_fields);
                        }                        

                        
			// reg. oldalak (adatmod, elf. jelszo, stb)
			if( in_array($curr_id,  array(45,44,43,42)) ){
                            $crud->edit_fields(
				'note',
                                'title',
                                'status',
                                'menu_zone_header',
                                'menu_zone_footer',
                                'menu_order_header',
                                'menu_order_footer',
                                'meta_title',
                                'meta_description',
                                'og_title',
                                'og_description',
                                'og_image');
			}
			
			// fooldal:
			if( $curr_id == 1 ){
				$pagetitle = "Főoldal szerkesztése";
				$crud->edit_fields(
                                        'note',
                                        'meta_title',
                                        'meta_description',
                                        'og_title',
                                        'og_description',
                                        'og_image',
                                        
                                        'cms_page_block_pre_title',
                                        'cms_page_block_title',
                                        'cms_page_block_content',
                                        'fooldal_also_blokk_cim',
                                        'fooldal_also_blokk_szoveg',
                                        'main_page_image'                                        
                                        );
			}



			// főoldal kiegészítések (intro + bejelentkezés blokk, csak kép és cím módosítható)
			/*if( in_array($curr_id,  array(2,3)) ){
				$crud->edit_fields(
                                        'cover_image',
                                        'cms_page_block_pre_title',
                                        'cms_page_block_title',
                                        'cms_page_block_content',
                                        'fooldal_also_blokk_cim',
                                        'fooldal_also_blokk_szoveg'
                                        );
			}*/
                        
			// spec aloldalak: //játék
			if( in_array($curr_id,  array(29)) ){
				$crud->edit_fields(
										'note',
                                        'title',
                                        'lead',
                                        'meta_title',
                                        'meta_description',
                                        'og_title',
                                        'og_description',
                                        'og_image'
                                        );
			}


			if( $curr_id != 2 ){
				$sidebar_info = $this->load->view( 'backend/_pages_widget', array(
					'sb_title'			=>	lang('widget_sidebar_title'),
					'w_create_date'		=>	$sidebar_arr[0]['create_date'],
					'curr_id'			=>	$curr_id,
				), true);
			}
			
		}

		if( $this->uri->segment(3) == 'ajax_list' )
		{
			if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest')
			{
				exit;
			}
		}

		// render output:
		$this->set_view_data(array(
			'crud_output'	=>	$crud->render(),
			'pagetitle'		=>	$pagetitle,	
			'sidebar_info'	=>	$sidebar_info,
			'curr_id'		=>	$curr_id,
		));
		$this->render( $view_filename, $this->config->item('backend__theme') );
	}

	/**
	 * *************************
	 * live url
	 * *************************
	 */
	public function get_live_url()
	{
		if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest')
		{
			exit;
		}

		$error_msg = 'Hiba a lekérés során.';
        $result = NULL;
		$id = (int)$this->input->get('id');

		if( $id >0 )
        {
        	$error_msg = '#OK#';

        	$result = $this->backend_pages_model->get_page_url($id);
        }

        echo json_encode(array(
            "error_msg" => $error_msg,
            "result"	=> $result,
        ));
        exit;
	}

	/**
	 * *************************
	 * ajax save preview
	 * *************************
	 */
	public function save_preview()
	{
		if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest')
		{
			exit;
		}

		$error_msg = 'Hiba a mentés során.';
        $result = NULL;
        $id = (int)$this->input->get('id');

        if( $_POST && $id >0 )
        {
            $this->backend_pages_model->update_preview(array(
                'parent_id'         =>  $id,
                'post_type'         =>  'preview',
                'title'				=>	trim($this->input->post("title")),
                'menu_slug'			=>	'preview/page/' . $id,
                'lead'				=>	trim(strip_tags($this->input->post("lead"))),
                'cms_page_block_pre_title'	=>	trim($this->input->post("cms_page_block_pre_title")),
                'cms_page_block_title'		=>	trim($this->input->post("cms_page_block_title")),
                'cms_page_block_content'	=>	trim(strip_tags($this->input->post("cms_page_block_content"))),
                'content'			=>	$this->input->post("content"),
                'status'			=>	(int)$this->input->post("status"),
                'meta_title'                    =>	$this->input->post("meta_title"),
                'meta_description'              =>	$this->input->post("meta_description"),
                'og_title'                      =>	$this->input->post("meta_title"),
                'og_description'                =>	$this->input->post("meta_description"),
                'lang_id'			=>	strip_tags($this->input->post("lang_id")),
            ), $id);

            $error_msg = '#OK#';
        }

        echo json_encode(array(
            "error_msg" => $error_msg,
            "result"	=> $result,
        ));
        exit;
	}

	/**
	 * *************************
	 * delete image
	 * *************************
	 * 
	 * @param  array $post_array 
	 * @return array
	 */
	public function delete_file($fieldname, $fieldval)
	{
		$fieldname = trim(addslashes($fieldname));
		$fieldval = trim(addslashes($fieldval));

		if( $fieldname != '' && $fieldval != '' )
		{
			$prefix_table_name = $this->db->dbprefix($this->table_name);
			$this->backend_model->empty_image_field( $prefix_table_name, $fieldname, $fieldval );
		}

		echo json_encode(array("success"=>true));
	}

	/**
	 * *************************
	 * resize images, etc...
	 * *************************
	 */
	function _crud_after_upload($uploader_response,$field_info, $files_to_upload)
	{
		$this->load->library('image_moo');
		$file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name; 

		if( $field_info->field_name == 'og_image' ){
			$this->image_moo
				->load($file_uploaded)
				->resize_crop(1200,630)
				->save($file_uploaded,true);
		}

		if( $field_info->field_name == 'cover_image' ){
                        $this->image_moo
                                ->load($file_uploaded)
                                ->resize(555,545) 
                                //->resize_crop(1920,265) 
                                ->save($file_uploaded,true);
		}

		if( $field_info->field_name == 'main_page_image' ){
                        $this->image_moo
                                ->load($file_uploaded)
                                ->resize(665,406) 
                                //->resize_crop(1920,265) 
                                ->save($file_uploaded,true);
		}
                
		return TRUE;
	}

	/**
	 * *************************
	 * before insert callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 * @return array
	 */
	function _crud_before_insert($post_array)
	{
		$post_array['create_date'] = date("Y-m-d H:i:s");
		$post_array['parent_id'] = 0;
		$post_array['post_type'] = 'live';
		$post_array = $this->backend_model->xss_clean($post_array, array('content'));
		return $post_array;
	}

	/**
	 * *************************
	 * before update callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 */
	function _crud_before_update($post_array, $primary_key)
	{
		$post_array = $this->backend_model->xss_clean($post_array, array('content'));

		// live:
		$post_array['parent_id'] = 0;
		$post_array['post_type'] = 'live';

		return $post_array;
	}

	/**
	 * *************************
	 * after insert callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 * @param integer $primary_key
	 * @return boolean
	 */
	function _crud_after_insert( $post_array, $primary_key )
	{
		$this->backend_model->log($primary_key, array(
			"admin_id" 		=> $this->session->userdata('id'),
			"info" 			=> 'cms_page_insert: ' . $primary_key,
			"create_date" 	=> date('Y-m-d H:i:s')
		));

		return TRUE;
	}

	/**
	 * *************************
	 * after update callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 * @param integer $primary_key
	 * @return boolean
	 */
	function _crud_after_update( $post_array, $primary_key )
	{
		$this->backend_model->log($primary_key, array(
			"admin_id" 		=> $this->session->userdata('id'),
			"info" 			=> 'cms_page_update: ' . $primary_key,
			"create_date" 	=> date('Y-m-d H:i:s')
		));

		return TRUE;
	}	

	/**
	 * prevent delete hub pages
	 */
	public function _crud_before_delete($primary_key)
	{
		$nodelete = array(1,29,35);
		if( in_array($primary_key, $nodelete) ){
			return false;
		}
		return true;
	}

	/**
	 * *************************
	 * after delete callback
	 * *************************
	 * 
	 * @param integer $primary_key
	 * @return boolean
	 */
	public function _crud_after_delete($primary_key)
	{
		// log:
		return $this->backend_model->log($primary_key, array(
			"admin_id" 		=> $this->session->userdata('id'),
			"info" 			=> 'page_delete: ' . $primary_key,
			"create_date" 	=> date('Y-m-d H:i:s')
		));
	}
}
