<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ********************************
 * backend manage languages
 * ********************************
 */
class Backend_languages extends MCMS_Controller 
{
	// setup:
	private $slug_pattern = 'languages';

	function __construct()
	{
		parent::__construct();

		// check user role:
		if( ! $this->backend_model->user_has_right('list', $this->slug_pattern) )
		{
			redirect( site_url('admin/access_info'), 'refresh' );
			return;
		}

		$this->load->model("backend_languages_model");
	}

	public function index()
	{
		redirect( site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh' );
	}

	/**
	 * *************************
	 * crud
	 * *************************
	 */
	public function crud()
	{
		// setup:
		$table_name = 'languages';	
		$prefix_table_name = $this->db->dbprefix($table_name);
		$view_filename = 'languages';
		$pagetitle = lang('page_sidebar_langs');
		$sidebar_info = '';

		// id exists:
		if( $this->uri->segment(3) == 'edit' )
		{
			$id = $this->uri->segment(4);
			if( 
				! isvalidnumber($id,255) || 
				! $this->backend_model->record_exists($table_name, array( 'id' => $id ) ) )
			{
				redirect( site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh' );
			}
		}

		$crud = $this->get_grocery_crud($prefix_table_name, '');
		$crud->set_crud_url_path(site_url('admin/'. $this->slug_pattern));
		$crud->unset_delete();
		$crud->unset_export();
		 $crud->unset_back_to_list();
		$crud->unset_add();
		$crud->unset_read();
		$crud->unset_texteditor('meta_description','og_description');
		$crud->columns('lang_name','meta_title','meta_description');
		$crud->add_fields('meta_title','meta_description','og_title','og_description','og_image');
		$crud->edit_fields('meta_title','meta_description','og_title','og_description','og_image');
		$crud->where('id', 3);

		// spec field types:
		$crud->field_type('status','dropdown', $this->config->item('backend__status_default'));
		$crud->field_type('is_default','dropdown', array(
			1 => lang('yes'), 
			0 => lang('no'),
		));

		$crud->field_type('create_date', 'invisible');
		//$crud->field_type('status', 'invisible');
		//$crud->field_type('is_default', 'invisible');

		// image fields:
		$crud->set_field_upload( 'og_image', $this->config->item('backend__upload_path') );

		// display names:
		$req = $this->config->item('backend__label_req');
		$crud->display_as( 'id', lang('id') )
             ->display_as( 'lang_name', lang('lang_name'). $req )
             ->display_as( 'lang_short_code', lang('lang_short_code'). $req )
             ->display_as( 'is_default', lang('is_default'). $req )
             ->display_as( 'lang_code', lang('lang_code'). $req )
             ->display_as( 'meta_title', lang('meta_title') )
             ->display_as( 'meta_description', lang('meta_description') )
             ->display_as( 'og_title', lang('og_title') )
             ->display_as( 'og_description', lang('og_description') )
             ->display_as( 'og_image', lang('og_image') )
             ->display_as( 'status', lang('status') . $req );

        // validation:
		$crud->set_rules( 'lang_short_code',lang('lang_short_code'),'max_length[10]')
			 ->set_rules( 'lang_name',lang('lang_name'),'max_length[255]')
			 ->set_rules( 'lang_code',lang('lang_code'),'max_length[10]')
			 ->set_rules( 'meta_title', lang('meta_title'), 'max_length[255]' )
             ->set_rules( 'meta_description', lang('meta_description'), 'max_length[2000]' )
             ->set_rules( 'og_title', lang('og_title'),'max_length[255]' )
             ->set_rules( 'og_description', lang('og_description'),'max_length[2000]' );
		
		// callbacks:
		$crud->callback_before_insert(array($this, '_crud_before_insert'));
		$crud->callback_before_update(array($this, '_crud_before_update'));
		$crud->callback_after_insert(array($this, '_crud_after_insert'));
		$crud->callback_after_update(array($this, '_crud_after_update'));
		$crud->callback_after_upload(array($this,'_crud_after_upload'));

		// roles:
		$state = $crud->getState();
		$state_info = $crud->getStateInfo();
		if( ! $this->backend_model->user_has_right('create', $this->slug_pattern) )
		{
			$crud->unset_add();
		}
		if( ! $this->backend_model->user_has_right('publish', $this->slug_pattern) )
		{
			if($state == 'add' || $state == 'insert_validation' )
			{
				//$crud->set_rules( 'status', lang('status'),'required|integer' );
				//$crud->field_type('status','dropdown', $this->config->item('backend__status_author'));
			}
			if($state == 'edit' || $state == 'update_validation' )
			{
				//$crud->set_rules( 'status', lang('status'),'integer' );
				//$crud->field_type('status', 'invisible');
			}
		}
		else
		{
			//$crud->set_rules( 'status', lang('status'),'required|integer' );
		}


		// subpages:
		if( $this->uri->segment(3) == 'edit' )
		{
			$pagetitle = lang('edit_lang');
			$curr_id = (int)$this->uri->segment(4);
		}
		if( $this->uri->segment(3) == 'add' )
		{
			$pagetitle = lang('add_lang');
		}

		// render output:
		$this->set_view_data(array(
			'crud_output'	=>	$crud->render(),
			'pagetitle'		=>	$pagetitle,
		));
		$this->render( $view_filename, $this->config->item('backend__theme') );
	}


	/**
	 * *************************
	 * delete image
	 * *************************
	 * 
	 * @param  array $post_array 
	 * @return array
	 */
	public function delete_file($fieldname, $fieldval)
	{
		$fieldname = trim(addslashes($fieldname));
		$fieldval = trim(addslashes($fieldval));

		if( $fieldname != '' && $fieldval != '' )
		{
			$prefix_table_name = $this->db->dbprefix('languages');
			$this->backend_model->empty_image_field( $prefix_table_name, $fieldname, $fieldval );
		}

		echo json_encode(array("success"=>true));
	}

	/**
	 * *************************
	 * resize images, etc...
	 * *************************
	 */
	function _crud_after_upload($uploader_response,$field_info, $files_to_upload)
	{
		$this->load->library('image_moo');
		$file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name; 

		if( $field_info->field_name == 'og_image' )
		{
			$this->image_moo
				->load($file_uploaded)
				->resize_crop(1200,630)
				->save($file_uploaded,true);
		}

		return TRUE;
	}

	/**
	 * *************************
	 * before insert callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 * @return array
	 */
	function _crud_before_insert($post_array)
	{
		// create date
		$post_array['create_date'] = date("Y-m-d H:i:s");

		// xss_clean:
		$post_array = $this->backend_model->xss_clean($post_array);

		return $post_array;
	}

	/**
	 * *************************
	 * before update callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 */
	function _crud_before_update($post_array)
	{
		// xss_clean:
		$post_array = $this->backend_model->xss_clean($post_array);

		return $post_array;
	}

	/**
	 * *************************
	 * after insert callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 * @param integer $primary_key
	 * @return boolean
	 */
	function _crud_after_insert( $post_array, $primary_key )
	{
		// is_default reset:
		if( (int)$post_array['is_default'] == 1 )
		{
			$this->backend_languages_model->reset_is_default($primary_key);
		}

		// log:
		return $this->backend_model->log($primary_key, array(
			"admin_id" 		=> $this->session->userdata('id'),	
			"info" 			=> 'lang_insert: ' . $primary_key,
			"create_date" 	=> date('Y-m-d H:i:s')
		));
	}

	/**
	 * *************************
	 * after update callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 * @param integer $primary_key
	 * @return boolean
	 */
	function _crud_after_update( $post_array, $primary_key )
	{
		// is_default reset:
		if( (int)$post_array['is_default'] == 1 )
		{
			$this->backend_languages_model->reset_is_default($primary_key);
		}

		// log:
		return $this->backend_model->log($primary_key, array(
			"admin_id" 		=> $this->session->userdata('id'),	
			"info" 			=> 'lang_update: ' . $primary_key,
			"create_date" 	=> date('Y-m-d H:i:s')
		));
	}	
}