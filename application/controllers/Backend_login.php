<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ********************************
 * backend login / logout
 * ********************************
 */

class Backend_login extends CI_Controller
{
	public function __construct(){
		parent::__construct();
		
		$this->config->load('mcms_config_backend');
		$this->load->model("backend_session_model");
		$this->lang->load('backend', 'hungarian');

		// cookie protection:
		if(0){
			$cookie_value = (int)$this->input->cookie( $this->config->item('backend__cookie_name'), TRUE );		
			if( $cookie_value != 1 ) {
				echo '404';
				exit;
			}
		}
		
		// check if admin user is logged in
		if( $this->backend_session_model->check_logged_status() == TRUE ){
			$this->backend_session_model->redirect_to_mainpage();
			return;
		}
	}

	/**
	 * Display main login screen
	 */
	public function index(){
		$this->login_display();
		return;
	}

	/**
	 * Display login screen
	 *
	 * @param null|string $error_msg Send error message to view files.
	 */
	public function login_display($error_msg = NULL){
		
		$this->load->view('backend/login', array(
			'error_msg'			=>	$error_msg,	
		));
		return;
	}

	/**
	 * Process login
	 */
	public function do_login(){
		$current_login_email = $this->input->post("admin_email");
		$current_login_password = $this->input->post("admin_password");

		if ( ! isvalidemail($current_login_email) ) {
			$this->login_display("error_email");
			return;
		}

		if ( strlen($current_login_password) < 1 ) {
			$this->login_display("error_password");
			return;
		}

		$result = $this->backend_session_model->login( $current_login_email, $current_login_password );
		if ( ! $result || ! $result[0]->id) {
			$this->login_display("error_user_not_exists");
			return;
		}

		$this->backend_session_model->create_admin_session($result);
		return;
	}

	/**
	 * Logout user
	 */
	public function logout(){
		$this->backend_session_model->destroy_admin_session();
		return;
	}
}