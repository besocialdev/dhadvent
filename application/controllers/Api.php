<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * *******************
 * Calendar API
 * *******************
 */
class Api extends MCMS_Controller 
{
	private $testing = true;	// just for testing the response, @todo: set to false

	public function __construct(){		
		parent::__construct();

		if( ENVIRONMENT === 'production' ){
			if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest'){
				$this->response(null, 'INVALID_REQUEST', null);
			}

			$this->testing = false;
		}
		
		$this->load->library('calendar');
		$this->calendar = new Calendar();
	}

	/**
	 * *******************
	 * not in use ...
	 * *******************
	 * 
	 * @return void
	 */
	public function index(){
		$this->response(null, 'INVALID_REQUEST', null);
	}

	/**
	 * *******************
	 * get available calendar items
	 * *******************
	 *
	 * path to list all: api/calendar-get
	 * path to list by id: api/calendar-get/1
	 *
	 * @todo (example):
	 * - if game is active:
	 * 		- all active items
	 * 			- order by rand / order by create date desc / etc..
	 *    	- item by id if $day_id is valid
	 *     - additional info:
	 * 		  - user data (logged in?)
	 * 		  - user has prev. game?
	 * - else
	 * 		- game end info (mcms_setup)
	 *
	 * @param $day_id integer
	 * 
	 * @return void
	 */
	public function calendar_get( $day_id = 0 ){
		
		// ......
		// if day id exists and is valid....
		// 
		// ......

		// we're done:
		$this->response('#OK#', $this->calendar->get( $day_id ), null);
	}

	/**
	 * *******************
	 * save user open
	 * *******************
	 *
	 * path: api/calendar-save/{day_id}
	 *
	 * @todo (example):
	 * - if game is active:
	 * 		- all active items
	 * 			- order by rand / order by create date desc / etc..
	 *    	- item by id if $id is valid
	 *     - additional info:
	 * 		  - user data (logged in?)
	 * 		  - user has prev. game?
	 * - else
	 * 		- game end info (mcms_setup)
	 * 
	 * @return void
	 */
	public function calendar_save( $day_id = null ){
		
		// by user cookie?
		// ......

		// we're done:
		$this->response('#OK#', array(), null);
	}


	// ... quiz save (post)
	// ... 


	/**
	 * *******************
	 * echo result
	 * *******************
	 * @param string $token
	 * @param  mixed $result  
	 * @param  string $error_msg
	 * @param array $info   (additional infos)
	 *
	 * @return void
	 */
	private function response($error_msg = null, $result = array(), $info = array()){
		if( true === $this->testing ){

			ini_set("xdebug.var_display_max_children", -1);
			ini_set("xdebug.var_display_max_data", -1);
			ini_set("xdebug.var_display_max_depth", -1);

			var_dump(json_encode(array(
				'error_msg'			=>	$error_msg,
				'csrf_hash'			=>	$this->security->get_csrf_hash(),
				'csrf_token_name'	=>	$this->security->get_csrf_token_name(),
				'result' 			=>	$result,
				'info'				=>	$info
			), JSON_PRETTY_PRINT)); 
		} else {
			echo json_encode(array(
				'error_msg'			=>	$error_msg,
				'csrf_hash'			=>	$this->security->get_csrf_hash(),
				'csrf_token_name'	=>	$this->security->get_csrf_token_name(),
				'result' 			=>	$result,
				'info'				=>	$info
			), JSON_PRETTY_PRINT);
		}
		exit;
	}
}