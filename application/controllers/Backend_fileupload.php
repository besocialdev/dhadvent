<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ********************************
 * backend file upload controller
 * wysiwyg editor file uploadd backend
 * ********************************
 *
 * - only ajax requests are allowed
 * - used for tinymce default image uploads
 */
class Backend_fileupload extends MCMS_Controller 
{

	public function __construct()
	{		
		parent::__construct();
	}

	public function index()
	{
        $image_folder = $this->config->item('backend__upload_path');

        reset ($_FILES);
		$temp = current($_FILES);

		if (is_uploaded_file($temp['tmp_name']) )
		{
			/*if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest')
			{
				exit;
			}

			if( stristr($_SERVER['HTTP_REFERER'], $this->config->item('base_url') ) === FALSE)
			{
				header("HTTP/1.1 403 Origin Denied");
				return;
			}*/

			/*
			if your script needs to receive cookies, set images_upload_credentials : true in
			the configuration and enable the following two headers.
			*/
			// header('Access-Control-Allow-Credentials: true');
			// header('P3P: CP="There is no P3P policy."');

			// sanitize input:
			if( preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name']) ) 
			{
				header("HTTP/1.1 400 Invalid file name.");
				return;
			}

			// verify extension:
			if (!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("gif", "jpg", "png"))) 
			{
				header("HTTP/1.1 400 Invalid extension.");
				return;
			}

			//$extension = file_extension( $temp['name'] );
			//$new_name = strtolower( md5( rand_str(32) . time() ) . "." . $extension );
			$new_name = $temp['name'];
			$filetowrite = $image_folder . '/' . $new_name;
			move_uploaded_file( $temp['tmp_name'], $filetowrite );

			// respond to the successful upload with JSON.
			// use a location key to specify the path to the saved image resource.
			// { location : '/your/uploaded/image/file'}
			echo json_encode( array(
				'location' => $this->config->item('base_url') . $filetowrite) 
			);
		} 
		else 
		{
			header("HTTP/1.1 500 Server Error");
		}

		exit;
	}
}