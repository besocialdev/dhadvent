<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ********************************
 * set cookie to access admin panel
 * ********************************
 *
 * base_url() . admsc/s={backend__setcookie_urlhash}
 * http://cms.local/admsc?s=sdkfjsuSDFSf23sdfsdf
 */
class Backend_setcookie extends MCMS_Controller 
{

	public function __construct()
	{		
		parent::__construct();
	}

	public function index()
	{
		$s = $this->input->get('s', TRUE);
		$cookie_name = $this->config->item('backend__cookie_name');
		$url_hash = $this->config->item('backend__setcookie_urlhash');
		$global__admin_root_slug = $this->config->item('global__admin_root_slug');

		if( $s === $url_hash )
		{
			setcookie( $cookie_name, 1, time()+60*60*24*30 );
			header('Location: ' . base_url() . $global__admin_root_slug);
		} 
		else 
		{
			echo "Invalid parameters for setting cookie.";
		}

		exit;
	}
}