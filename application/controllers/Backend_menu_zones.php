<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ********************************
 * backend manage menu zones
 * ********************************
 */
class Backend_menu_zones extends MCMS_Controller 
{
	// setup:
	private $slug_pattern = 'menu-zones';

	function __construct()
	{
		parent::__construct();

		// check user role:
		if( ! $this->backend_model->user_has_right('list', $this->slug_pattern) )
		{
			redirect( site_url('admin/access_info'), 'refresh' );
			return;
		}

		$this->load->model("backend_menu_model");
	}

	public function index()
	{
		redirect( site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh' );
	}

	/**
	 * *************************
	 * crud
	 * *************************
	 */
	public function crud()
	{
		// setup:
		$table_name = 'menu';	
		$prefix_table_name = $this->db->dbprefix($table_name);
		$view_filename = 'menu_zones';
		$pagetitle = lang('page_sidebar_menuzone');

		// id exists:
		if( $this->uri->segment(3) == 'edit' )
		{
			$id = $this->uri->segment(4);
			if( 
				! isvalidnumber($id,255) || 
				! $this->backend_model->record_exists($table_name, array( 'id' => $id ) ) )
			{
				redirect( site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh' );
			}
		}

		// set pagetitle:
		if( $this->uri->segment(3) == 'edit' )
		{
			$pagetitle = lang('edit_menuzone');
		}
		if( $this->uri->segment(3) == 'add' )
		{
			$pagetitle = lang('add_menuzone');
		}

		// init:
		$crud = $this->get_grocery_crud($prefix_table_name, '');
		//$crud->set_relation('lang_id','mcms_languages','lang_name', null, null, 1);
		$crud->set_crud_url_path(site_url('admin/'. $this->slug_pattern));

		// disable: 
		$crud->unset_export();
		$crud->unset_delete();
		$crud->columns('label','status');
		$crud->add_fields('label','create_date');
		$crud->edit_fields('label','create_date');

		// spec field types:
		$crud->field_type('status','dropdown', $this->config->item('backend__status_default'));
		$crud->field_type('create_date', 'invisible');

		// display names:
		$req = $this->config->item('backend__label_req');
		$crud->display_as( 'id', lang('id') )
             ->display_as( 'lang_id', lang('lang_id') . $req )
             ->display_as( 'label', lang('zone') . $req )
             ->display_as( 'status', lang('status') )
             ->display_as( 'css_class', lang('css_class') )
             ->display_as( 'create_date', lang('create_date') )
             ->display_as( 'css_id', lang('css_id') );

        // validation:
		$crud->set_rules('lang_id',lang('lang_id'),'integer|required')
		     //->set_rules( 'css_class', lang('css_class'), 'max_length[255]' )
             //->set_rules( 'css_id', lang('css_id'), 'max_length[255]' )
			 ->set_rules('label',lang('zone'), 'required');
		
		// callbacks:
		$crud->callback_before_insert(array($this, '_crud_before_insert'));
		$crud->callback_before_update(array($this, '_crud_before_update'));
		$crud->callback_after_insert(array($this, '_crud_after_insert'));
		$crud->callback_after_update(array($this, '_crud_after_update'));
		$crud->callback_after_delete(array($this, '_crud_after_delete'));

		// roles:
		$state = $crud->getState();
		$state_info = $crud->getStateInfo();
		if( ! $this->backend_model->user_has_right('create', $this->slug_pattern) )
		{
			$crud->unset_add();
		}
		if( ! $this->backend_model->user_has_right('delete', $this->slug_pattern) )
		{
			$crud->unset_delete();
		}
		if( ! $this->backend_model->user_has_right('publish', $this->slug_pattern) )
		{
			if( $state == 'add' || $state == 'insert_validation' )
			{
				$crud->set_rules( 'status', lang('status'),'required|integer' );
				$crud->field_type('status','dropdown', $this->config->item('backend__status_author'));
			}
			if($state == 'edit' || $state == 'update_validation' )
			{
				$crud->set_rules( 'status', lang('status'),'integer' );
				$crud->field_type('status', 'invisible');
			}
		}
		else
		{
			$crud->set_rules( 'status', lang('status'),'required|integer' );
		}

		// render output:
		$this->set_view_data(array(
			'crud_output'	=>	$crud->render(),
			'pagetitle'		=>	$pagetitle,	
		));
		$this->render( $view_filename, $this->config->item('backend__theme') );
	}

	/**
	 * *************************
	 * before insert callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 * @return array
	 */
	function _crud_before_insert($post_array)
	{
		// create date
		$post_array['create_date'] = date("Y-m-d H:i:s");

		// xss_clean:
		$post_array = $this->backend_model->xss_clean($post_array);

		return $post_array;
	}

	/**
	 * *************************
	 * before update callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 */
	function _crud_before_update($post_array)
	{
		// xss_clean:
		$post_array = $this->backend_model->xss_clean($post_array);

		return $post_array;
	}

	/**
	 * *************************
	 * after insert callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 * @param integer $primary_key
	 * @return boolean
	 */
	function _crud_after_insert( $post_array, $primary_key )
	{
		// log:
		return $this->backend_model->log($primary_key, array(
			"admin_id" 		=> $this->session->userdata('id'),
			"info" 			=> 'menu_zone_insert: ' . $primary_key,
			"create_date" 	=> date('Y-m-d H:i:s')
		));
	}

	/**
	 * *************************
	 * after update callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 * @param integer $primary_key
	 * @return boolean
	 */
	function _crud_after_update( $post_array, $primary_key )
	{
		// log:
		return $this->backend_model->log($primary_key, array(
			"admin_id" 		=> $this->session->userdata('id'),
			"info" 			=> 'menu_zone_update: ' . $primary_key,
			"create_date" 	=> date('Y-m-d H:i:s')
		));
	}	

	/**
	 * *************************
	 * after delete callback
	 * *************************
	 * 
	 * @param integer $primary_key
	 * @return boolean
	 */
	public function _crud_after_delete($primary_key)
	{
		// update menu items parent to 0:
		$this->backend_menu_model->reset_menu_id($primary_key);
		
		// log:
		return $this->backend_model->log($primary_key, array(
			"admin_id" 		=> $this->session->userdata('id'),
			"info" 			=> 'menu_zone_delete: ' . $primary_key,
			"create_date" 	=> date('Y-m-d H:i:s')
		));
	}
}