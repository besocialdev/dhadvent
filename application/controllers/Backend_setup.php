<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ********************************
 * cms settings
 * ********************************
 */
class Backend_setup extends MCMS_Controller
{
    // setup:
    private $slug_pattern = 'setup';
    private $table_name = 'setup';

    function __construct(){
        parent::__construct();

        // check user role:
        if( ! $this->backend_model->user_has_right('list', $this->slug_pattern) ){
            redirect( site_url('admin/access_info'), 'refresh' );
            return;
        }
    }

    public function index(){
        redirect( site_url('admin/' . $this->slug_pattern . '/edit/1'), 'refresh' );
    }

    /**
     * *************************
     * crud
     * *************************
     */
    public function crud(){
        // setup:
        $prefix_table_name = $this->db->dbprefix($this->table_name);
        $view_filename = 'setup';
        $pagetitle = lang('page_sidebar_setup');

        // id exists:
        if( $this->uri->segment(3) == 'edit' ){
            $id = $this->uri->segment(4);
            if( $id != 1 || strlen($id)>1  ){
                redirect( site_url('admin/' . $this->slug_pattern . '/edit/1'), 'refresh' );
            }
        }

        // init:
        $crud = $this->get_grocery_crud($prefix_table_name, '');
        $crud->where('id', 1);
        $crud->set_crud_url_path(site_url('admin/'. $this->slug_pattern));

        // disable delete:
        $crud->unset_delete();
        $crud->unset_back_to_list();
        $crud->columns();
        $crud->add_fields();
        $crud->unset_export();
        $crud->unset_add();

        $crud->set_field_upload( 'default_logo', $this->config->item('backend__upload_path') );
        $crud->set_field_upload( 'cookie_pdf', $this->config->item('backend__upload_path') );

        $crud_edit_fields = array(
                           'page_name',
                           'fb_appid',
                           'fb_appsecret',
                           'default_logo',
                           'login_required_setting',
                           'enable_prev_days',
//                           'game_start_date',
//                           'game_start_time',
//                           'game_end_date',
//                           'game_end_time',
                           'game_end_modal_title',
                           'game_end_modal_content',
                           'reg_title',
                         //  'share_title',
                         //  'share_text',
                           'social_fb',
                           'social_insta',
                           'social_twitter',
                           'site_status', 
                           'inactive_text',
                           'email_from',
                           'email_from_name',
                           'email_subject_forgotpass',
                           'email_subject_register',
                           //'email_subject_won', // wintime - nyerő időpont törlés
                           'email_content_forgotpass', 
                           'email_content_register',
                           //'email_content_won', // wintime - nyerő időpont törlés
        );
        //csak akkor van kupon feltöltés, ha belépéshez van kötve a játék
        if(true === $this->frontend_settings['login_required'])
        {
            $crud_edit_fields[] = 'email_subject_coupon';
            $crud_edit_fields[] = 'email_content_coupon';
        }
            
            $crud_edit_fields[] = 'cookie_modal_enabled';
            $crud_edit_fields[] = 'cookie_pdf';
            $crud_edit_fields[] = 'facebook_pixel_id';
            $crud_edit_fields[] = 'google_analytics_id';
            $crud_edit_fields[] = 'newsletter_api';
            $crud_edit_fields[] = 'mcapi_apikey';
            //$crud_edit_fields[] = 'mcapi_secure';
            $crud_edit_fields[] = 'mcapi_list_id';
        
        if(stristr($this->session->userdata["admin_email"], "@besocial.hu"))
        {
            $crud_edit_fields[] = 'mailgun_api_key';
            $crud_edit_fields[] = 'mailgun_api_url';
            $crud_edit_fields[] = 'extra_css';
            $crud_edit_fields[] = 'extra_js';
            $crud_edit_fields[] = 'pushalert';
            $crud_edit_fields[] = 'tracking_code_lvl1';
            $crud_edit_fields[] = 'tracking_code_lvl2';
            $crud_edit_fields[] = 'tracking_code_lvl3';
            

        }
         
        
        // edit:
        $crud->edit_fields($crud_edit_fields);
        


        // disable wysiwyg editor:
        $crud->unset_texteditor('tracking_code_lvl1','tracking_code_lvl2','tracking_code_lvl3','extra_css','extra_js','pushalert','game_end_modal_content','share_text','upload_success_text','upload_denied_text');

        $crud->field_type('site_status','dropdown', array(
            '1' => lang('site_status_active'),
            '0' => lang('site_status_inactive'),
        ));

        $crud->field_type('cookie_modal_enabled','dropdown', array(
            '1' => 'Igen',
            '0' => 'Nem',
        ));

        $crud->field_type('login_required_setting','dropdown', array(
            '1' => 'Bejelentkezéshez és regisztrációhoz kötött',
            '0' => 'Vendég mód: bárki játszhat, nincs szükség bejelentkezésre',
        ));
        
        $crud->field_type('enable_prev_days','dropdown', array(
            '1' => 'Igen',
            '0' => 'Nem',
        ));

        $crud->field_type('vote_enabled','dropdown', array(
            '1' => 'Igen',
            '0' => 'Nem',
        ));

        $crud->field_type('newsletter_api','dropdown', array(
            '0'    => 'Nincs mentés',
            '1' => lang('nl_null'),
            '2' => lang('nl_mailchimp'),
        ));


        // display names:
        $crud->display_as( 'id', lang('id') )
             ->display_as( 'fb_appid', 'Facebook App ID' )
             ->display_as( 'fb_appsecret', 'Facebook App Secret' )
             ->display_as( 'inactive_text', 'Karbantartás szöveg' )
             ->display_as( 'mailgun_api_key', 'Mailgun API kulcs' )
             ->display_as( 'mailgun_api_url', 'Mailgun API URL' )
             ->display_as( 'email_from', 'Feladó e-mail cím *' )
             ->display_as( 'login_required_setting', 'Játékmód')
             ->display_as( 'enable_prev_days', 'Korábbi napok elérhetőek?')
             ->display_as( 'email_subject_forgotpass', 'Levél tárgy (elfelejtett jelszó)' )
             ->display_as( 'email_subject_register', 'Levél tárgy (regisztráció)' )
             ->display_as( 'email_content_forgotpass', 'Levél szövege (elfelejtett jelszó)' )
             ->display_as( 'email_content_register', 'Levél szövege (regisztráció)' )
             ->display_as( 'page_name', 'Oldal (játék) címe' )
             ->display_as( 'default_logo', 'Logó' )
//             ->display_as('email_subject_won', 'Nyerő időpont levél tárgy') // wintime - nyerő időpont törlés
//             ->display_as('email_content_won', 'Nyerő időpont levél szöveg') // wintime - nyerő időpont törlés
             ->display_as('email_subject_coupon', 'Kupon küldés levél tárgy') 
             ->display_as('email_content_coupon', 'Kupon küldés levél tartalom') 
             ->display_as( 'social_fb', 'Facebook URL' )
             ->display_as( 'social_insta', 'Instagram URL' )
             ->display_as( 'social_twitter', 'Twitter URL' )
            // ->display_as( 'logo_text', 'Logó szöveg' )
             ->display_as( 'cookie_modal_enabled', 'Süti modal bekapcsolva?' )
             ->display_as( 'facebook_pixel_id', 'Facebook pixel ID' )
             ->display_as( 'google_analytics_id', 'Google Analytics ID' )
             ->display_as( 'email_from_name', lang('email_from') )
//             ->display_as( 'game_start_date', 'Játék kezdő dátum' )
             ->display_as( 'vote_enabled', 'Szavazás bekapcsolva?' )
             ->display_as( 'vote_nr', 'Szavazatok száma / felhasználó (0: korlátlan)' )
             ->display_as( 'upload_limit', 'Beküldhető pályázatok száma (0: korlátlan)' )
//             ->display_as( 'game_start_time', 'Játék kezdő időpont (00:00:00)' )
//             ->display_as( 'game_end_time', 'Játék vége időpont (00:00:00)' )
//             ->display_as( 'game_end_date', 'Játék vége dátum' )
             ->display_as( 'game_end_modal_title', 'Játék vége infó - cím' )
             ->display_as( 'game_end_modal_content', 'Játék vége infó - szöveg' )
             ->display_as( 'reg_title', 'Regisztráció főcím' )                
             ->display_as( 'share_title', 'Megosztás cím' )
             ->display_as( 'share_text', 'Megosztás szöveg' )
             ->display_as( 'upload_success_text', 'Sikeres feltöltés szöveg' )
             ->display_as( 'upload_denied_text', 'Feltöltés limit szöveg' )
             ->display_as( 'site_status', lang('site_status') )
             ->display_as( 'email_from_name', lang('email_from_name') )
             /**
             ->display_as( 'tracking_code_lvl1', 'Tracking kód (1-es szint, alap működés)' )
             ->display_as( 'tracking_code_lvl2', 'Tracking kód (2-es szint, statisztika)' )
             ->display_as( 'tracking_code_lvl3', 'Tracking kód (3-as szint, reklám/remarketing)' )
              //*/
             ->display_as( 'cookie_pdf', 'Süti tájékoztató (pdf)' )
             ->display_as( 'extra_css', lang('extra_css') )
             ->display_as( 'extra_js', lang('extra_js') )
             ->display_as( 'pushalert', lang('pushalert') )
             ->display_as( 'newsletter_api', lang('newsletter_api') )
             ->display_as( 'mcapi_apikey', lang('mcapi_apikey') )
             ->display_as( 'mcapi_secure', lang('mcapi_secure') )
             ->display_as( 'mcapi_list_id', lang('mcapi_list_id') );

        // validation:
        $crud->set_rules( 'email_from', lang('email_from'),'valid_email|required' )
            ->set_rules( 'email_from_name', lang('email_from_name'),'max_length[255]' )
            ->set_rules( 'mcapi_apikey', lang('mcapi_apikey'),'max_length[255]' )
            ->set_rules( 'mcapi_secure', lang('mcapi_secure'),'max_length[255]' )
            ->set_rules( 'mcapi_list_id', lang('mcapi_list_id'),'max_length[255]' );

        // callbacks:
        $crud->callback_before_update(array($this, '_crud_before_update'));
        $crud->callback_after_update(array($this, '_crud_after_update'));
        $crud->callback_after_upload(array($this,'_crud_after_upload'));

        if( $this->uri->segment(3) == 'edit' ){

        }
        if( $this->uri->segment(3) == 'add' ){
            return false;
        }

        // render output:
        $this->set_view_data(array(
            'crud_output'    =>    $crud->render(),
            'pagetitle'        =>    $pagetitle,
        ));
        $this->render( $view_filename, $this->config->item('backend__theme') );
    }

    /**
     * *************************
     * before update callback
     * *************************
     *
     * @param  array $post_array
     */
    function _crud_before_update($post_array, $primary_key){
        // valid date:
//        $game_start = $post_array['game_start_date'] . ' ' . $post_array['game_start_time'];
//        $game_end = $post_array['game_end_date'] . ' ' . $post_array['game_end_time'];
//        if( ! isvaliddatetime($game_start) || ! isvaliddatetime($game_end) || $game_start > $game_end ){
//            return false;
//        }

        // xss_clean:
        $post_array = $this->backend_model->xss_clean($post_array, array('tracking_code_lvl1','tracking_code_lvl2','tracking_code_lvl3','extra_css','extra_js','pushalert','game_end_modal_content','reg_title','share_text','upload_success_text','upload_denied_text'));

        return $post_array;
    }

    /**
     * *************************
     * after update callback
     * *************************
     *
     * @param  array $post_array
     * @param integer $primary_key
     * @return boolean
     */
    function _crud_after_update( $post_array, $primary_key ){
        // log:
        $this->backend_model->log($primary_key, array(
            "admin_id"         => $this->session->userdata('id'),
            "info"             => 'settings_changed: ' . $primary_key,
            "create_date"     => date('Y-m-d H:i:s')
        ));

        return TRUE;
    }

    /**
     * *************************
     * delete image
     * *************************
     *
     * @param  array $post_array
     * @return array
     */
    public function delete_file($fieldname, $fieldval){
        $fieldname = trim(addslashes($fieldname));
        $fieldval = trim(addslashes($fieldval));

        if( $fieldname != '' && $fieldval != '' ){
            $prefix_table_name = $this->db->dbprefix($this->table_name);
            $this->backend_model->empty_image_field( $prefix_table_name, $fieldname, $fieldval );
        }

        echo json_encode(array("success"=>true));
    }

    /**
     * *************************
     * resize images, etc...
     * *************************
     */
    function _crud_after_upload($uploader_response,$field_info, $files_to_upload){
        $this->load->library('image_moo');
        $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name;

        if( $field_info->field_name == 'default_logo' ){
            $this->image_moo
                ->load($file_uploaded)
                ->resize(500)
                ->save($file_uploaded,true);
        }

        return TRUE;
    }
}