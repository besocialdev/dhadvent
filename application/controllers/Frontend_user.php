<?php

defined("BASEPATH") OR exit("No direct script access allowed");

spl_autoload_register(function ($class){
    $prefix = "Facebook\\";
    $base_dir = "application/third_party/Facebook/";
    $len = strlen($prefix);
    if(strncmp($prefix, $class, $len) !== 0)
        return;

    $relative_class = substr($class, $len);
    $file = $base_dir . str_replace("\\", "/", $relative_class) . ".php";

    if(file_exists($file))
        require $file;
});

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;

class Frontend_user extends MCMS_Controller {

    public $frontend_theme;

    public function __construct(){
        parent::__construct();

        if(in_array($this->uri->segment(1), $this->disabled_login_required_pages))
        {
            redirect('404', 'refresh');
        }

        $this->frontend_theme = $this->config->item('frontend__theme')."/"; // config/mcms_config_frontend.php
    }

    public function login(){
        if(!$this->account->is_guest())
            redirect("adatmodositas");

        $slug = 'bejelentkezes';

		$cmsdata = $this->frontend_model->get_page($slug);
		if( ! is_array($cmsdata) || count($cmsdata) == 0 ){
        	redirect('404','refresh');
                }

		if ( is_array( $cmsdata ) && count($cmsdata) > 0 ){
			$this->update_settings($cmsdata);
			$success = true;

			if($this->input->post("email_address")){
	            if($this->account->login($this->input->post("email_address"),$this->input->post("password"))){
	                $this->session->set_flashdata("modal_success", lang("SIGN_IN_SUCCESSFUL"));
	                redirect("");
	            }
	            else{
	            	$success = false;
	            }
	        }

			// render output:
			$this->set_view_data(array(
				"success"		=>	$success,
				'meta_index'	=>	'index, follow, all',
				'cmsdata'		=> $cmsdata,
				'settings'		=> $this->frontend_settings,
				'html_header'	=> $this->frontend_model->get_header( $this->frontend_settings ),
				'slug'			=> $slug,
				'cms_block_type'=> false,
			));
			$this->render('login', $this->config->item('frontend__theme'));

		} else {
			redirect('404', 'refresh');
		}
    }

    public function fb_login(){
        FacebookSession::setDefaultApplication( $this->frontend_settings['fb_appid'], $this->frontend_settings['fb_appsecret'] );
        $helper = new FacebookRedirectLoginHelper(base_url()."fb-login");

        $session = false;
        try {
            $session = $helper->getSessionFromRedirect();
        } catch( Exception $e ){}

        if( $session ){
            $request = new FacebookRequest( $session, "GET", "/me?fields=name,email,first_name,last_name" );
            $response = $request->execute();

            $graph = $response->getGraphObject();
            $fb_data = array();

            $fb_data["fb_id"] = $graph->getProperty("id");
            $fb_data["fb_name"] = $graph->getProperty("name");
            $fb_data["fb_email"] =  $graph->getProperty("email");
            if(is_string($graph->getProperty("first_name"))){
                $fb_data["fb_first_name"] =  $graph->getProperty("first_name");
                $fb_data["fb_last_name"] =  $graph->getProperty("last_name");
            }
            elseif(strstr($graph->getProperty("name"), " ")){
                $pc = explode(" ", $graph->getProperty("name"));
                $fb_data["fb_first_name"] =  $pc[0];
                $fb_data["fb_last_name"] =  $pc[1];
            }
            else {
                $fb_data["fb_first_name"] =  $graph->getProperty("name");
                $fb_data["fb_last_name"] =  "";
            }

            if($this->account->login_fb($fb_data)){
                $this->session->set_flashdata("modal_success", lang("SUCCESSFUL_FACEBOOK_SIGN_IN"));
                redirect("");
            }
            else {
                $this->session->set_flashdata("modal_failure", lang("UNSUCCESSFUL_FACEBOOK_SIGN_IN"));
                redirect("bejelentkezes");
            }
        }
        else {
            header("Location: ".$helper->getLoginUrl(array("email")));
        }
    }

    public function form($form){

        if($form=="datamod")
            $this->account->require_login();
        elseif(!$this->account->is_guest())
            redirect("adatmodositas");

        $slug = 'regisztracio';
        if($form=="datamod") {
            $slug = 'adatmodositas';
        }

		$cmsdata = $this->frontend_model->get_page($slug);
		if( ! is_array($cmsdata) || count($cmsdata) == 0 ){
                    redirect('404','refresh');
                }

                if ( is_array( $cmsdata ) && count($cmsdata) > 0 ){
			$this->update_settings($cmsdata);

			$validation_errors = false;
			$user_data = $this->db->get_where("users",array("id"=>$this->account->get_userid()))->row_array();

			if($this->input->post("email_address")){
                            try {
                                $this->account->save($form);
                                if($form=="datamod") {
                                    $this->session->set_flashdata("modal_success", lang("SUCCESSFUL_ACCOUNT_MODIFY"));
                                    redirect("adatmodositas");
                                }
                                else {
                                    $this->session->set_flashdata("modal_success", lang("SUCCESSFUL_ACCOUNT_CREATE"));
                                    redirect("");
                                }
                            } catch (Exception $e) {
                                $user_data = $_POST;
                                $validation_errors = $e->getMessage();
//                                print '<pre>';
//                                print_r($validation_errors);
//                                print '</pre>';
//                                die('---- ERROR DEBUG ----');
                            }
                        }



			// render output:
			$this->set_view_data(array(
				"validation_errors"	=>	$validation_errors,
				"user_data"		=> $user_data,
				'meta_index'	=> 'noindex',
				'cmsdata'		=> $cmsdata,
				"type"			=> $form,
				'settings'		=> $this->frontend_settings,
				'html_header'	=> $this->frontend_model->get_header( $this->frontend_settings ),
				'slug'			=> $slug,
				'cms_block_type'=> false,
			));
			$this->render('dataform', $this->config->item('frontend__theme'));

		} else {
			redirect('404', 'refresh');
		}
    }

    public function get_reg_form(){
    	echo $this->load->view($this->frontend_theme."game_form",array("step"=>"register","settings"=>$this->frontend_settings),true);
    	die;
    }

    public function get_login_form(){
    	echo $this->load->view($this->frontend_theme."game_form",array("step"=>"login","settings"=>$this->frontend_settings),true);
    	exit;
    }

    public function game($what=false){
        if($this->account->is_guest() && $this->frontend_settings['login_required']){
            // var_dump($_POST);
            if(!isset($_POST["email_address"]))
               // echo $this->load->view($this->frontend_theme."game_form",array("step"=>"email","settings"=>$this->frontend_settings),true);
            	echo $this->load->view($this->frontend_theme."game_form",array("step"=>"login","settings"=>$this->frontend_settings),true);
            elseif(isset($_POST["last_name"])){
                try {
                    $this->account->save("register");
                    echo $this->load->view($this->frontend_theme."game_play",array('redirect'=>1),true);
                } catch (Exception $e) {
                    echo $this->load->view($this->frontend_theme."game_form",array("step"=>"register","settings"=>$this->frontend_settings),true);
                }
            }
            elseif(isset($_POST["password"])){
                $this->form_validation->set_rules("email_address",lang("FORM_EMAIL"),"required|valid_email|max_length[255]");
                $this->form_validation->set_rules('password', lang("FORM_PASSWORD"), 'required|min_length[6]');

                if(isset($_POST['becaptcha']) && $_POST['becaptcha']!="")
                {
                    die();
                }

                if(!$this->form_validation->run())
                    echo $this->load->view($this->frontend_theme."game_form",array("step"=>"login", "settings"=>$this->frontend_settings),true);
                else {
                    $success = $this->account->login($this->input->post("email_address"),$this->input->post("password"));
                    if($success)
                        echo $this->load->view($this->frontend_theme."game_play",array('redirect'=>1),true);
                    else
                        echo $this->load->view($this->frontend_theme."game_form",array("step"=>"login","validation_errors"=>lang("FORM_PASSWORD_ERROR"), "settings"=>$this->frontend_settings),true);
                }
            }
            else {
                $this->form_validation->set_rules("email_address",lang("FORM_EMAIL"),"required|valid_email|max_length[255]");
               // $this->form_validation->set_rules('accept_tos', lang("FORM_GAME_RULES_CHECKBOX_ERROR_MESSAGE_FIELD"), 'callback__accept_checkbox_gamerules');
               // $this->form_validation->set_rules('accept_privacy', lang("FORM_PRIVACY_POLICY_CHECKBOX_ERROR_MESSAGE_FIELD"), 'callback__accept_checkbox_privacy');

                if(isset($_POST['becaptcha']) && $_POST['becaptcha']!="")
                {
                    die();
                }

                if(!$this->form_validation->run())
                //    echo $this->load->view($this->frontend_theme."game_form",array("step"=>"email", "settings"=>$this->frontend_settings),true);
                	echo $this->load->view($this->frontend_theme."game_form",array("step"=>"login", "settings"=>$this->frontend_settings),true);
                else {
                    $user = $this->db->get_where("users",array("email_address"=>$this->input->post("email_address")))->row();
                    if($user)
                        echo $this->load->view($this->frontend_theme."game_form",array("step"=>"login", "settings"=>$this->frontend_settings),true);
                    else
                        echo $this->load->view($this->frontend_theme."game_form",array("step"=>"register","settings"=>$this->frontend_settings),true);
                }
            }
        }
        else
        {
            echo $this->load->view($this->frontend_theme."game_play",array('redirect'=>0, "settings"=>$this->frontend_settings),true);
        }

        die;
    }

    public function emailtest(){
        $this->email->to("basabence@gmail.com","Basa Bence");
        $this->email->subject("Új jelszó");
        $this->email->message("Az új jelszavadat <a href='".base_url()."user/new_pass/asd'>ide kattintva</a> tudod beállítani!");
        $this->email->send();
    }

    public function logout(){
        $this->account->logout();
        $this->session->set_flashdata("modal_success", lang("SUCCESSFUL_SIGN_OUT"));
        redirect("");
    }

    public function forgot_pass($code=false){

       	$slug = 'elfelejtett-jelszo';
       	$cmsdata = $this->frontend_model->get_page($slug);
        if( ! is_array($cmsdata) || count($cmsdata) == 0 ){
        	redirect('404','refresh');
        }

        if ( is_array( $cmsdata ) && count($cmsdata) > 0 ){
                $this->update_settings($cmsdata);
        }

        $this->set_view_data(array(
                'meta_index'	=> 'noindex',
                'cmsdata'		=> $cmsdata,
                'settings'		=> $this->frontend_settings,
                'html_header'	=> $this->frontend_model->get_header( $this->frontend_settings ),
                'slug'			=> $slug,
                'cms_block_type'=> false,
        ));


        if($code){ // email megerősítés
            $user = $this->db->get_where("users",array("password_reset_code"=>$code))->row();
            if(!$user)
            {
                show_404();
            }

            if(isset($_POST["password"])){
                $this->form_validation->set_rules("password", lang("FORM_PASSWORD"), "required|min_length[6]");
                $this->form_validation->set_rules("passconf", lang("FORM_PASSWORD_AGAIN"), "required|matches[password]");

                if(isset($_POST['becaptcha']) && $_POST['becaptcha']!="")
                {
                    die();
                }

                if( $this->form_validation->run() ){
                    $this->db->where(array("id"=>$user->id))->update("users",array("password"=>password_hash($_POST["password"], PASSWORD_DEFAULT), "password_reset_code"=>''));
                    $this->account->login($user->email_address,$_POST["password"]);
                    $this->session->set_flashdata("modal_success", lang("FORM_SUCCESSFUL_PASSWORD_CHANGE"));
                    redirect("");
                }
            }

            $this->render("new_pass", $this->config->item('frontend__theme'));
            return;
        }
        elseif(isset($_POST["email_address"])){

            $this->form_validation->set_rules("email_address",lang('FORM_EMAIL'),"required|valid_email|max_length[255]|callback__exists[users.email_address]");

            if(isset($_POST['becaptcha']) && $_POST['becaptcha']!="")
            {
                die();
            }

            if($this->form_validation->run()){
                $user = $this->db->get_where("users",array("email_address"=>$this->input->post("email_address")))->row();
                $code = $this->account->new_password($user->id);

                $this->email->to($user->email_address,$user->last_name." ".$user->first_name);
                $this->email->subject($this->frontend_settings['email_subject_forgotpass']);
                $this->email->message(str_replace("{link}","<a href='".base_url()."elfelejtett-jelszo/".$code."'>".base_url()."elfelejtett-jelszo/".$code."</a>",$this->frontend_settings['email_content_forgotpass']));
                $this->email->send();

                $this->session->set_flashdata("modal_success", lang("FORM_SUCCESSFUL_PASSWORD_CHANGE_TODOS_SENT_IN_EMAIL"));
                redirect("");
            }
        }

        $this->render("forgot_pass", $this->config->item('frontend__theme'));

    }

    public function _exists($str,$value){
        list($table, $column) = explode('.', $value, 2);
       /* echo $value; echo '<br>';
        echo $table;echo '<br>';
        echo $column;echo '<br>';
        echo $str; echo '<br>';
        echo $this->db->where($column,$str)->count_all_results($table);
        exit;*/
        return ($this->db->where($column,$str)->count_all_results($table) > 0);
    }

    public function _accept_checkbox_gamerules($val){
        return !empty($val);
    }
    public function _accept_checkbox_privacy($val){
        return !empty($val);
    }
}