<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Backend_winnergen extends MCMS_Controller {

    // setup:
    private $slug_pattern = 'winnergen';
    private $table_name = 'user_winners';

    function __construct() {
        parent::__construct();

        // check user role:
        if (!$this->backend_model->user_has_right('list', $this->slug_pattern)) {
            redirect(site_url('admin/access_info'), 'refresh');
            return;
        }
    }

    public function index() {
        redirect(site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh');
    }

    /**
     * *************************
     * crud
     * *************************
     */
    public function crud() {
        // setup:
        $prefix_table_name = $this->db->dbprefix($this->table_name);
        $view_filename = 'winnergen';
        $pagetitle = lang('page_sidebar_winnergen');
        $sidebar_info = '';
        $curr_id = null;

        // id exists:
        if ($this->uri->segment(3) == 'edit') {
            $id = $this->uri->segment(4);
            if (!isvalidnumber($id, 255) || !$this->backend_model->record_exists($this->table_name, array('id' => $id))) {
                redirect(site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh');
            }
        }

        // init:
        $crud = $this->get_grocery_crud($prefix_table_name, '');
        $crud->set_relation('user_id', 'mcms_users', '{last_name} {first_name} - {email_address}', null, null, 1);
        $crud->set_crud_url_path(site_url('admin/' . $this->slug_pattern));
        $crud->unset_read();
        $crud->unset_edit();
		$crud->unset_add();
        $crud->unset_clone();
		//$crud->unset_delete();
        $crud->columns('user_id');
        $crud->display_as('user_id', 'Felhasználó');

        if ($this->uri->segment(3) == 'ajax_list') {
            if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
                exit;
            }
        }

        // render output:
        $this->set_view_data(array(
            'crud_output' 	=> $crud->render(),
            'pagetitle' 	=> $pagetitle,
            'sidebar_info' 	=> ''
            //'all_time'		=> $this->backend_model->wintimes_count() /* // wintime - nyerő időpont törlés */
        ));
        $this->render($view_filename, $this->config->item('backend__theme'));
    }

    // nyero idopontok:
    public function gen(){
        if(!$this->input->get("h")||!is_numeric($this->input->get("winner_nr")))
            exit;

        $winner_nr = $this->input->get("winner_nr")>0?$this->input->get("winner_nr"):1;
        $weighted = $this->input->get("weighted")=="igen";
        $this->backend_model->set_winners($winner_nr,$weighted);

        redirect(base_url('admin/winnergen/crud?genmsg='.$winner_nr),'refresh');
    }

    /**
     * *************************
     * delete image
     * *************************
     *
     * @param  array $post_array
     * @return array
     */
    public function delete_file($fieldname, $fieldval) {
        $fieldname = trim(addslashes($fieldname));
        $fieldval = trim(addslashes($fieldval));

        if ($fieldname != '' && $fieldval != '') {
            $prefix_table_name = $this->db->dbprefix($this->table_name);
            $this->backend_model->empty_image_field($prefix_table_name, $fieldname, $fieldval);
        }

        echo json_encode(array("success" => true));
    }
}
