<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ********************************
 * cookie settings
 * ********************************
 */
class Backend_cpm extends MCMS_Controller
{
    // setup:
    private $slug_pattern = 'cpm';
    private $table_name = 'setup';

    function __construct(){
        parent::__construct();

        // check user role:
        if( ! $this->backend_model->user_has_right('list', $this->slug_pattern) ){
            redirect( site_url('admin/access_info'), 'refresh' );
            return;
        }
    }

    public function index(){
        redirect( site_url('admin/' . $this->slug_pattern . '/edit/1'), 'refresh' );
    }

    /**
     * *************************
     * crud
     * *************************
     */
    public function crud(){
        // setup:
        $prefix_table_name = $this->db->dbprefix($this->table_name);
        $view_filename = 'cpm';
        $pagetitle = lang('page_sidebar_cpm');

        // id exists:
        if( $this->uri->segment(3) == 'edit' ){
            $id = $this->uri->segment(4);
            if( $id != 1 || strlen($id)>1  ){
                redirect( site_url('admin/' . $this->slug_pattern . '/edit/1'), 'refresh' );
            }
        }

        // init:
        $crud = $this->get_grocery_crud($prefix_table_name, '');
        $crud->where('id', 1);
        $crud->set_crud_url_path(site_url('admin/'. $this->slug_pattern));

        // disable delete:
        $crud->unset_delete();
        $crud->unset_back_to_list();
        $crud->columns();
        $crud->add_fields();
        $crud->unset_export();
        $crud->unset_add();

        // edit:
        $crud->edit_fields('cpm_layer','cpm_modal_infotext','cpm_level1','cpm_level2','cpm_level3');
        $crud->unset_texteditor('cpm_modal_infotext','cpm_level1','cpm_level2','cpm_level3');

        // display names:
        $crud->display_as( 'id', lang('id') )
        	->display_as( 'cpm_modal_infotext', 'Felugró ablak, rövid tájékozató' )
        	->display_as( 'cpm_level1', 'Alap működéshez szükséges sütik leírása' )
        	->display_as( 'cpm_level2', 'Statisztikai sütik leírása' )
        	->display_as( 'cpm_level3', 'Statisztikai és reklám célú sütik leírása' )
            ->display_as( 'cpm_layer', 'Alsó sáv szövege' );

        // validation:
        $crud->set_rules( 'cpm_layer', lang('cpm_layer'),'max_length[255]' );

        // callbacks:
        $crud->callback_before_update(array($this, '_crud_before_update'));
        $crud->callback_after_update(array($this, '_crud_after_update'));
        $crud->callback_after_upload(array($this,'_crud_after_upload'));

        if( $this->uri->segment(3) == 'edit' ){

        }
        if( $this->uri->segment(3) == 'add' ){
            return false;
        }
        
        $crud->set_js("assets/grocery_crud/texteditor/ckeditor5-build-classic/ckeditor.js");

        // render output:
        $this->set_view_data(array(
            'crud_output'    =>    $crud->render(),
            'pagetitle'        =>    $pagetitle,
        ));
        $this->render( $view_filename, $this->config->item('backend__theme') );
    }

    /**
     * *************************
     * before update callback
     * *************************
     *
     * @param  array $post_array
     */
    function _crud_before_update($post_array, $primary_key){
      

        // xss_clean:
        $post_array = $this->backend_model->xss_clean($post_array);

        return $post_array;
    }

    /**
     * *************************
     * after update callback
     * *************************
     *
     * @param  array $post_array
     * @param integer $primary_key
     * @return boolean
     */
    function _crud_after_update( $post_array, $primary_key ){
        // log:
        $this->backend_model->log($primary_key, array(
            "admin_id"         => $this->session->userdata('id'),
            "info"             => 'cpmsettings_changed: ' . $primary_key,
            "create_date"     => date('Y-m-d H:i:s')
        ));

        return TRUE;
    }

    /**
     * *************************
     * delete image
     * *************************
     *
     * @param  array $post_array
     * @return array
     */
    public function delete_file($fieldname, $fieldval){
        $fieldname = trim(addslashes($fieldname));
        $fieldval = trim(addslashes($fieldval));

        if( $fieldname != '' && $fieldval != '' ){
            $prefix_table_name = $this->db->dbprefix($this->table_name);
            $this->backend_model->empty_image_field( $prefix_table_name, $fieldname, $fieldval );
        }

        echo json_encode(array("success"=>true));
    }
}