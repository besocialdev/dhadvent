<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend_home extends MCMS_Controller {

	public function __construct(){
		parent::__construct();
	}

	/**
	 * ********************************
	 * render cms pages
	 * ********************************
	 *
	 * @param string $slug
	 * @param string $slug2
	 */
	public function index( $slug='', $slug2 = '' ){

		$slug = _str($slug);
		$slug2 = _str($slug2);
		$cmsdata = $this->frontend_model->get_page($slug);

		if ( is_array( $cmsdata ) && count($cmsdata) > 0 ){

                $cmsdata = $this->set_cover_image_url($cmsdata); // set path url to images

			// lvl1:
			if( $slug2 == '' ){
				$this->update_settings($cmsdata);
				$content_template = ($cmsdata['content_template'] != '') ? $cmsdata['content_template'] : 'cms_page';

				$view_data = array(
					'meta_index'		=>	'index, follow, all',
					'cmsdata'		=> $cmsdata,
					'settings'		=> $this->frontend_settings,
					'q' 			=> ($this->input->get('q')) ? trim(strip_tags(stripslashes($this->input->get('q')))) : '',
					'p' 			=> ($this->input->get('p')) ? (int)$this->input->get('p') : 1,
					'order'			=> ($this->input->get('order') && in_array($this->input->get('order'), array(0,1,2)) ) ? (int)$this->input->get('order') : 0,
					'html_header'	=> $this->frontend_model->get_header( $this->frontend_settings ),
					'slug'			=> $slug,
					'slug2'			=> $slug2,
					'modal_success'	=> ($this->session->flashdata('modal_success')) ? $this->session->flashdata('modal_success') : '',
					'cms_block_type'	=> (!empty($cmsdata['cms_block_type']) ?  $cmsdata['cms_block_type'] : 'image_on_right' ), // admin felületen lehet állítani, hogy a kép melyik oldalra kerüljön (csak CMS oldal, enum tipusú mezőben tárolódik)
				);

                // nyitóoldali kiegészítések
                if($cmsdata['content_template']=='mainpage') {
                    $view_data['intro_block'] = $this->load_mainpage_intro_block_to_mainpage(); // nyitóoldal intro blokk
                    $view_data['mainpage_cms_blocks'] = $this->load_cms_blocks_to_mainpage(); // nyitóoldalon megjelenő blokkok
                }
                
                // díjak
                $view_data['prizes_list'] = $this->load_prizes();
                $view_data['prizes_slider_enabled'] = ( $this->prizes_slider_enabled($view_data) ? true : false );

				// render output:
				$this->set_view_data($view_data);
				$this->render($content_template, $this->config->item('frontend__theme'));
			}

			// lvl2:
			/*else {
				$record = $this->frontend_model->get_record($cmsdata['content_template'], $slug2);
				if( is_array($record) && count($record) > 0 ){
					$this->update_settings($record);

					// set view template:
					$content_template = 'cms_page';
					if( $cmsdata['content_template'] != '' ){
						$content_template = $cmsdata['content_template'] . '_page';
					}

					// render output:
					$this->set_view_data(array(
						'meta_index'		=>	( $slug != '404' ) ? 'index, follow, all' : 'noindex',
						'cmsdata'			=>	$cmsdata,
						'settings'		=> $this->frontend_settings,
						'html_header'		=>	$this->frontend_model->get_header( $this->frontend_settings, $cmsdata['content_template'], $record['image_name'] ),
						'record'			=>	$record,
						'slug'				=>	$slug,
						'slug2'				=>	$slug2,
					));
					$this->render($content_template, $this->config->item('frontend__theme'));
				} else {
					redirect('404', 'refresh');
				}
			}*/
		} else {
			redirect('404', 'refresh');
		}
	}

	/**
	 * ********************************
	 * set 404 page
	 * ********************************
	 *
	 * route: /404
	 */
	public function route_404(){
		$this->output->set_status_header(404);
		$cmsdata = $this->frontend_model->get_page('404');
		if ( is_array( $cmsdata ) && count($cmsdata) > 0 ){
			$this->update_settings($cmsdata);

			// render output:
			$this->set_view_data(array(
				'meta_index'		=>	'noindex',
				'html_lang'			=>	$this->frontend_settings['lang_code'],
				'cmsdata'			=>	$cmsdata,
				'settings'		=> $this->frontend_settings,
				'q' 				=> '',
				'p' 				=> 1,
				'frontend_settings'	=>	$this->frontend_settings,
				'html_header'		=>	$this->frontend_model->get_header( $this->frontend_settings ),
				'slug'				=>	'404',
			));
			$this->render('cms_page', $this->config->item('frontend__theme'));


		} else {
			echo 'A keresett oldal nem található.';
			exit;
		}
	}
        private function prizes_slider_enabled($view_data=array())
        {
            $return = false;
            
            $prizes_block_type = $this->frontend_model->get_prizes_block_type();
            
            if (!empty($view_data['prizes_list']) && count($view_data['prizes_list'])>1 && $prizes_block_type[0]['prizes_block_type']==1)
            {
                $return = true;
            }
            
            return $return;
        }

        /**
	 * ********************************
	 * set url to images
	 * ********************************
         *
         * @param array $cmsdata
         * @param array $img_indexes
         * @return array $cmsdata images fields extended with url
         */
        private function set_cover_image_url($cmsdata,$img_indexes=array('cover_image','main_page_image')) {

            $return = $cmsdata;

                foreach ( $img_indexes as $img_index ) {

                    if( !empty($cmsdata[$img_index]) ){

                        $img_with_url = $this->frontend_model->image_url($cmsdata[$img_index]);

                        $return[$img_index] = $img_with_url;

                    }
                }

            return $return;

        }

        private function load_cms_blocks_to_mainpage() {

            $res = $this->frontend_model->get_cms_blocks_to_mainpage();
            $return = $this->prepare_pages_to_view($res);

            //az utolsó elemet bejelöljük, mert oda mennek a ikonok (icon i001)
            if(!empty($return))
            {
                $c = count($return);
                $return[$c-1]['last_item'] = 1;
            }

            return $return;

        }

        private function load_mainpage_intro_block_to_mainpage() {

            $res = $this->frontend_model->get_mainpage_intro_block();
            $return = $this->prepare_pages_to_view($res);

            return $return;

        }
        
        private function load_prizes() {

            $res = $this->frontend_model->get_prizes_list();
            $return = $this->prepare_pages_to_view($res);

            return $return;

        }

        private function prepare_pages_to_view($res)
        {
            $return = array();

            if(!empty($res)) {
                foreach ($res as $v) {
                    $v['cms_page_block_content'] = (!empty($v['cms_page_block_content']) ? '<span>'. str_replace("\n", "</span>\n<br><span>", ($v['cms_page_block_content'])).'</span>' : '' );
                    $v['cms_block_type'] = (!empty($v['cms_block_type']) ?  $v['cms_block_type'] : 'image_on_right' ); // admin felületen lehet állítani, hogy a kép melyik oldalra kerüljön (csak CMS oldal, enum tipusú mezőben tárolódik)
                    $return[] = $this->set_cover_image_url( $v ); // set path url to images
                }
            }

            return $return;
        }
}