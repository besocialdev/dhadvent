<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ********************************
 * backend home controller
 * ********************************
 */
class Backend_home extends MCMS_Controller {

	public function __construct(){		
		parent::__construct();
	}

	/**
	 * ********************************
	 * frontend index page
	 * ********************************
	 */
	public function index(){
		// render output:
		$this->set_view_data(array(
			'widget1'	=>	$this->backend_model->mp_get_users(),
			'users_count'	=>	$this->backend_model->users_count(),
			'chart_reg'	=>	$this->backend_model->chart_reg(),
		));
                $this->set_view_data($this->tpb_view_data());
		$this->render('mainpage',$this->config->item('backend__theme'));
	}
}