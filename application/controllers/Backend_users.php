<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ********************************
 * backend manage users
 * ********************************
 */
class Backend_users extends MCMS_Controller
{
	// setup:
	private $slug_pattern = 'users';
	private $table_name = 'users';

	function __construct()
	{
		parent::__construct();

		// check user role:
		if( ! $this->backend_model->user_has_right('list', $this->slug_pattern) )
		{
			redirect( site_url('admin/access_info'), 'refresh' );
			return;
		}
	}

	public function index()
	{
		redirect( site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh' );
	}

	/**
	 * *************************
	 * crud
	 * *************************
	 */
	public function crud()
	{
		// setup:
		$prefix_table_name = $this->db->dbprefix($this->table_name);
		$view_filename = 'users';
		$pagetitle = lang('page_sidebar_users');
		$sidebar_info = '';

		// id exists:
		if( $this->uri->segment(3) == 'edit' )
		{
			$id = $this->uri->segment(4);
			if(
				! isvalidnumber($id,255) ||
				! $this->backend_model->record_exists($this->table_name, array( 'id' => $id ) ) )
			{
				redirect( site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh' );
			}
		}

		// init:
		$crud = $this->get_grocery_crud($prefix_table_name, '');
		$crud->set_crud_url_path(site_url('admin/'. $this->slug_pattern));
		$crud->columns('first_name', 'last_name', 'email_address', 'create_date', 'newsletter','games_num');
		$crud->add_fields();
		$crud->unset_add();
		$crud->unset_read();
		$crud->edit_fields('last_name','first_name','email_address','games_nn','newsletter','status');

  		$crud->callback_column('weighted_sum',array($this,'_callback_weighted_sum'));
  		$crud->callback_column('games_num',array($this,'_callback_count_games'));
		$crud->set_relation_n_n( 'games_nn', 'mcms_user_games', 'mcms_calendar_days', 'user_id', 'game_id', 'cal_day' );

		$crud->field_type('status','dropdown', array(
			'1' => lang('active'),
			'0' => lang('inactive'),
		));

		$crud->field_type('newsletter','dropdown', array(
			'igen' => lang('yes'),
			'nem' => lang('no'),
		));

		// display names:
		$crud->display_as( 'id', lang('id') )
             ->display_as( 'first_name', lang('first_name') )
             ->display_as( 'last_name', lang('last_name') )
             ->display_as( 'create_date', lang('date_reg') )
             ->display_as( 'games_nn', 'Játéknapok' )
             ->display_as( 'email_address', lang('email_address') )
             ->display_as( 'newsletter', lang('newsletter') )
             ->display_as( 'games_num', "Nyitott napok száma" )
             ->display_as( 'weighted_sum', "Súlyozott ID" )
             ->display_as( 'status', lang('status') );

		// validation:
		$crud->set_rules( 'first_name', lang('first_name'),'max_length[255]' )
             ->set_rules( 'last_name', lang('last_name'),'max_length[255]' )
             ->set_rules( 'email_address', lang('email_address'), 'valid_email|max_length[255]' )
             ->set_rules( 'newsletter', lang('newsletter'), 'max_length[255]' )
             ->set_rules( 'status', lang('status'), 'required' );

		// callbacks:
		$crud->callback_before_update(array($this, '_crud_before_update'));
		$crud->callback_after_update(array($this, '_crud_after_update'));
		$crud->callback_after_delete(array($this, '_crud_after_delete'));

		// states:
		$state = $crud->getState();
		$state_info = $crud->getStateInfo();

		if($state == 'add')
		{

		}

		if($state == 'edit')
		{
			$primary_key = $state_info->primary_key;
			$pagetitle = lang('page_sidebar_users_edit');
			$sidebar_arr = $this->backend_model->sidebar_info($this->table_name, $primary_key, 'create_date');

			$sidebar_info = $this->load->view( 'backend/_users_widget', array(
				'sb_title'			=>	lang('widget_sidebar_title'),
				'w_create_date'		=>	$sidebar_arr[0]['create_date']
			), true);
		}

		if( $state == 'export' )
		{
			$crud->columns('id','first_name','last_name','email_address','newsletter','status','create_date','games_num','weighted_sum');
		}

		// subpages:
		/*if( $this->uri->segment(3) == 'edit' )
		{
			$pagetitle = lang('page_sidebar_users_edit');
			$curr_id = (int)$this->uri->segment(4);
			$sidebar_arr = $this->backend_model->sidebar_info($this->table_name, $curr_id, 'create_date');

			$sidebar_info = $this->load->view( 'backend/_users_widget', array(
				'sb_title'			=>	lang('widget_sidebar_title'),
				'w_create_date'		=>	$sidebar_arr[0]['create_date']
			), true);
		}
		if( $this->uri->segment(3) == 'add' )
		{

		}*/

                if (true === $this->frontend_settings['login_required'])
                {
                    $crud_output = $crud->render();
                }
                else
                {
                    //a játék jelenleg "Vendég" módban fut, regisztráció és bejelentkezés nélkül is lehet használni. Ebben az esetben a játékosok menüpont nem elérhető.
                    // views/backend/users.php
                    $crud_output = new stdClass();
                    $crud_output->output = '';

                }



		// render output:
		$this->set_view_data(array(
			'crud_output'	=>	$crud_output,
			'pagetitle'		=>	$pagetitle,
			'sidebar_info'	=>	$sidebar_info,
			'primary_key'	=>	(isset($primary_key)) ? $primary_key : 0,
		));
		$this->render( $view_filename, $this->config->item('backend__theme') );
	}

	public function _callback_count_games($value, $row)
	{
		$data = get_instance()->db->select("user_id, COUNT(*) as cnt")->group_by("user_id")->having(array("user_id"=>$row->id))->get("mcms_user_games")->row();
		return ($data)?$data->cnt:0;
	}

	public function _callback_weighted_sum($value, $row)
	{
		if(get_instance()->config->item("weighted_sum")===NULL)
			get_instance()->config->set_item("weighted_sum",0);

		$new_weight = get_instance()->config->item("weighted_sum")+$row->games_num;

		if($row->newsletter!="Nem")
			$new_weight++;

		get_instance()->config->set_item("weighted_sum",$new_weight);

		return $new_weight;
	}

	/**
	 * *************************
	 * before update callback
	 * *************************
	 *
	 * @param  array $post_array
	 */
	function _crud_before_update($post_array, $primary_key)
	{
		// xss_clean:
		$post_array = $this->backend_model->xss_clean($post_array, array('games_nn'));

		return $post_array;
	}

	/**
	 * *************************
	 * after update callback
	 * *************************
	 *
	 * @param  array $post_array
	 * @param integer $primary_key
	 * @return boolean
	 */
	function _crud_after_update( $post_array, $primary_key )
	{
		// log:
		$this->backend_model->log($primary_key, array(
			"admin_id" 		=> $this->session->userdata('id'),
			"info" 			=> 'user_data_update: ' . $primary_key,
			"create_date" 	=> date('Y-m-d H:i:s')
		));

		return TRUE;
	}

	/**
	 * *************************
	 * after delete callback
	 * *************************
	 *
	 * @param integer $primary_key
	 * @return boolean
	 */
	public function _crud_after_delete($primary_key)
	{
		// log:
		return $this->backend_model->log($primary_key, array(
			"admin_id" 		=> $this->session->userdata('id'),
			"info" 			=> 'user_delete: ' . $primary_key,
			"create_date" 	=> date('Y-m-d H:i:s')
		));
	}
}