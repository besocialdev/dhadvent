<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Backend_wintimes extends MCMS_Controller {

    // setup:
    private $slug_pattern = 'wintimes';
    private $table_name = 'prize_win_times';

    function __construct() {
        parent::__construct();

        // check user role:
        if (!$this->backend_model->user_has_right('list', $this->slug_pattern)) {
            redirect(site_url('admin/access_info'), 'refresh');
            return;
        }
    }

    public function index() {
        redirect(site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh');
    }

    /**
     * *************************
     * crud
     * *************************
     */
    public function crud() {
        // setup:
        $prefix_table_name = $this->db->dbprefix($this->table_name);
        $view_filename = 'wintimes';
        $pagetitle = lang('page_sidebar_wintimes');
        $sidebar_info = '';
        $curr_id = null;

        // id exists:
        if ($this->uri->segment(3) == 'edit') {
            $id = $this->uri->segment(4);
            if (!isvalidnumber($id, 255) || !$this->backend_model->record_exists($this->table_name, array('id' => $id))) {
                redirect(site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh');
            }
        }

        // init:
        $crud = $this->get_grocery_crud($prefix_table_name, '');
        $crud->set_relation('prize_id', 'mcms_prizes', 'title', null, null, 1);
        $crud->set_relation('user_id', 'mcms_users', 'email_address', null, null, 1);
        $crud->set_crud_url_path(site_url('admin/' . $this->slug_pattern));
	    //$crud->unset_export();
        $crud->unset_read();
        $crud->unset_edit();
		$crud->unset_add();
        $crud->unset_clone();
		//$crud->unset_delete();
        $crud->columns('prize_id', 'user_id');
        $crud->display_as('prize_id', 'Nyeremény')
                ->display_as('user_id', 'Felhasználó');

        // callbacks:
        $crud->callback_before_insert(array($this, '_crud_before_insert'));
        $crud->callback_before_update(array($this, '_crud_before_update'));
        $crud->callback_after_insert(array($this, '_crud_after_insert'));
        $crud->callback_after_update(array($this, '_crud_after_update'));
        $crud->callback_before_delete(array($this, '_crud_before_delete'));
        $crud->callback_after_delete(array($this, '_crud_after_delete'));

        // subpages:
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        if ($state == 'edit') {
            $pagetitle = lang('edit_prize');
            $curr_id = (int) $this->uri->segment(4);
        }

        if ($this->uri->segment(3) == 'ajax_list') {
            if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
                exit;
            }
        }

        // render output:
        $this->set_view_data(array(
            'crud_output' 	=> $crud->render(),
            'pagetitle' 	=> $pagetitle,
            'sidebar_info' 	=> '',
            'curr_id' 		=> $curr_id,
            'all_time'		=> $this->backend_model->wintimes_count()
        ));
        $this->render($view_filename, $this->config->item('backend__theme'));
    }

    // nyero idopontok:
    public function gen(){
    	if( ! isset($_GET['h']) ){
    		exit;
    	}
    	$c = $this->backend_model->gen_times();
    	redirect(base_url('admin/wintimes/crud?genmsg='.$c),'refresh');
    }

    // nyero idopontok torlese:
    public function del(){
    	if( ! ($_GET['h']) ){
    		exit;
    	}
    	$this->backend_model->del_times();
    	redirect(base_url('admin/wintimes/crud?delmsg=ok'),'refresh');
    }

    public function import(){

    	$msg = 'err';
    	$config['upload_path']    = './' . $this->config->item('backend__upload_path');
        $config['allowed_types']  = 'csv';
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('csv')){
        	
        } else  {
        	$csv = @file_get_contents( './' . $this->config->item('backend__upload_path') . '/' . $_FILES['csv']['name'] );
        	$msg = $this->backend_model->import($csv);
        }
    	redirect(base_url('admin/wintimes/crud?importmsg='.$msg),'refresh');
    }


    /**
     * *************************
     * delete image
     * *************************
     * 
     * @param  array $post_array 
     * @return array
     */
    public function delete_file($fieldname, $fieldval) {
        $fieldname = trim(addslashes($fieldname));
        $fieldval = trim(addslashes($fieldval));

        if ($fieldname != '' && $fieldval != '') {
            $prefix_table_name = $this->db->dbprefix($this->table_name);
            $this->backend_model->empty_image_field($prefix_table_name, $fieldname, $fieldval);
        }

        echo json_encode(array("success" => true));
    }

    /**
     * *************************
     * resize images, etc...
     * *************************
     */
    function _crud_after_upload($uploader_response, $field_info, $files_to_upload) {
        $this->load->library('image_moo');
        $file_uploaded = $field_info->upload_path . '/' . $uploader_response[0]->name;

       /* if ($field_info->field_name == 'og_image') {
            $this->image_moo
                    ->load($file_uploaded)
                    ->resize_crop(1200, 630)
                    ->save($file_uploaded, true);
        }*/

        return TRUE;
    }

    /**
     * *************************
     * before insert callback
     * *************************
     * 
     * @param  array $post_array 
     * @return array
     */
    function _crud_before_insert($post_array) {
       
        $post_array['create_date'] = date("Y-m-d H:i:s");
        $post_array['post_type'] = 'live';
        $post_array = $this->backend_model->xss_clean($post_array);
        
        return $post_array;
    }

    /**
     * *************************
     * before update callback
     * *************************
     * 
     * @param  array $post_array 
     */
    function _crud_before_update($post_array, $primary_key) {

        $post_array = $this->backend_model->xss_clean($post_array);
        $post_array['post_type'] = 'live';

        return $post_array;
    }

    /**
     * *************************
     * after insert callback
     * *************************
     * 
     * @param  array $post_array 
     * @param integer $primary_key
     * @return boolean
     */
    function _crud_after_insert($post_array, $primary_key) {

        $this->backend_model->log($primary_key, array(
            "admin_id" => $this->session->userdata('id'),
            "info" => 'wintime_insert: ' . $primary_key,
            "create_date" => date('Y-m-d H:i:s')
        ));

        return TRUE;
    }

    /**
     * *************************
     * after update callback
     * *************************
     * 
     * @param  array $post_array 
     * @param integer $primary_key
     * @return boolean
     */
    function _crud_after_update($post_array, $primary_key) {
        // mainpage reset:
        if ((int) $post_array['is_mainpage'] == 1) {
            $this->backend_prizes_model->reset_mainpage($primary_key);
        }

        // log:
        $this->backend_model->log($primary_key, array(
            "admin_id" => $this->session->userdata('id'),
            "info" => 'wintime_update: ' . $primary_key,
            "create_date" => date('Y-m-d H:i:s')
        ));

        return TRUE;
    }

    /**
     * prevent delete hub prizes
     */
    public function _crud_before_delete($primary_key) {
        $nodelete = array();
        if (in_array($primary_key, $nodelete)) {
            return false;
        }
        return true;
    }

    /**
     * *************************
     * after delete callback
     * *************************
     * 
     * @param integer $primary_key
     * @return boolean
     */
    public function _crud_after_delete($primary_key) {
        // log:
        return $this->backend_model->log($primary_key, array(
                    "admin_id" => $this->session->userdata('id'),
                    "info" => 'wintime_delete: ' . $primary_key,
                    "create_date" => date('Y-m-d H:i:s')
        ));
    }

}
