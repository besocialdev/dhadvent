<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ********************************
 * backend access denied
 * ********************************
 */
class Backend_access_info extends MCMS_Controller 
{

	public function __construct()
	{		
		parent::__construct();
	}

	/**
	 * ********************************
	 * frontend index page
	 * ********************************
	 */
	public function index()
	{

		// render output:
		$this->render('access_denied',$this->config->item('backend__theme'));
	}
}