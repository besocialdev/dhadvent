<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ********************************
 * backend home controller
 * ********************************
 */
class Backend_activate extends MCMS_Controller {

	public function __construct(){		
		parent::__construct();
	}

	/**
	 * ********************************
	 * frontend index page
	 * ********************************
	 */
	public function index(){
            
                $view_data = $this->tpb_view_data();
            
		// render output:
		$this->set_view_data($view_data);
		$this->render('activate',$this->config->item('backend__theme'));
	}

	public function status(){
		
		if($_POST){
                    
			if( isset($_POST['action']) && $_POST['action'] == 'start' ){
				//$this->thepromobuilder->project_start( $_POST['game_start_date'] . ' ' . $_POST['game_start_time'] );
				$this->thepromobuilder->project_start( $_POST['game_start_date'] );
                                // $_POST['game_end_date'] // < elő van készítve
			}

			if( isset($_POST['action']) && $_POST['action'] == 'stop' ){
				$this->thepromobuilder->project_stop();
			}

		}
                
		redirect('admin/activate','refresh');
	}
}