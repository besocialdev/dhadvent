<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ********************************
 * backend contact form
 * ********************************
 */
class Backend_contact_form extends MCMS_Controller 
{
	// setup:
	private $slug_pattern = 'contact-form';
	private $table_name = 'contact_form';

	function __construct()
	{
		parent::__construct();

		// check user role:
		if( ! $this->backend_model->user_has_right('list', $this->slug_pattern) )
		{
			redirect( site_url('admin/access_info'), 'refresh' );
			return;
		}
	}

	public function index()
	{
		redirect( site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh' );
	}

	/**
	 * *************************
	 * crud
	 * *************************
	 */
	public function crud()
	{
		// setup:
		$prefix_table_name = $this->db->dbprefix($this->table_name);
		$view_filename = 'contact_form';
		$pagetitle = lang('sidebar_contact');
		$sidebar_info = '';

		// id exists:
		if( $this->uri->segment(3) == 'edit' )
		{
			$id = $this->uri->segment(4);
			if( 
				! isvalidnumber($id,255) || 
				! $this->backend_model->record_exists($this->table_name, array( 'id' => $id ) ) )
			{
				redirect( site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh' );
			}
		}
		
		// init:
		// id	job_id	fullname	email_address	phone_nr	message	create_date	resume
		$crud = $this->get_grocery_crud($prefix_table_name, '');
		$crud->set_crud_url_path(site_url('admin/'. $this->slug_pattern));
		$crud->unset_add();
		$crud->columns('id','job_id','fullname','email_address','phone_nr','create_date');
		$crud->add_fields();
		$crud->edit_fields('job_id','fullname','email_address','phone_nr','message','resume','create_date');
		$crud->unset_texteditor('message');
		$crud->field_type('create_date', 'invisible');
		$crud->set_relation('job_id','mcms_positions','title');

		// display names:
		$crud->display_as( 'id', lang('id') )
             ->display_as( 'fullname', 'Név' )
             ->display_as( 'job_id', 'Pozíció' )
             ->display_as( 'u_type', 'Típus' )
             ->display_as( 'email_address', lang('email_address') )
             ->display_as( 'phone_nr', lang('phone_nr') )
             ->display_as( 'resume', 'CV' )
             ->display_as( 'message', lang('message') )
             ->display_as( 'create_date', lang('create_date') );

		// callbacks:
		$crud->callback_after_delete(array($this, '_crud_after_delete'));

		// states:
		$state = $crud->getState();
		$state_info = $crud->getStateInfo();
		if( $state == 'export' )
		{
			$crud->columns('job_id','fullname','email_address','phone_nr','message','resume','create_date');
		}

		// render output:
		$this->set_view_data(array(
			'crud_output'	=>	$crud->render(),
			'pagetitle'		=>	$pagetitle,	
		));
		$this->render( $view_filename, $this->config->item('backend__theme') );
	}

	/**
	 * *************************
	 * after delete callback
	 * *************************
	 * 
	 * @param integer $primary_key
	 * @return boolean
	 */
	public function _crud_after_delete($primary_key)
	{
		// log:
		return $this->backend_model->log($primary_key, array(
			"admin_id" 		=> $this->session->userdata('id'),
			"info" 			=> 'contact_message_delete: ' . $primary_key,
			"create_date" 	=> date('Y-m-d H:i:s')
		));
	}
}