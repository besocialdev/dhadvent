<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ********************************
 * backend manage users
 * ********************************
 */
class Backend_my_profile extends MCMS_Controller 
{
	// setup:
	private $slug_pattern = 'my-profile';
	private $table_name = 'admins';

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		redirect( site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh' );
	}

	/**
	 * *************************
	 * crud
	 * *************************
	 */
	public function crud()
	{
		// setup:
		$prefix_table_name = $this->db->dbprefix($this->table_name);
		$view_filename = 'my_profile';
		$pagetitle = lang('page_sidebar_my_profile');
		$sidebar_info = '';

		// id exists:
		if( $this->uri->segment(3) == 'edit' )
		{
			$id = $this->uri->segment(4);
			if( ! is_numeric($id)  )
			{
				redirect( site_url('admin/'), 'refresh' );
			}

			if( $id != $this->session->userdata('id') )
			{
				redirect( site_url('admin/'), 'refresh' );
			}
		}

		// init:
		$crud = $this->get_grocery_crud($prefix_table_name, '');
		$crud->set_crud_url_path(site_url('admin/'. $this->slug_pattern));
		$crud->unset_delete();
		$crud->unset_back_to_list();
		$crud->columns();
		$crud->add_fields();
		$crud->unset_export();
		$crud->unset_add();
		$crud->edit_fields('admin_name','admin_email','admin_password');

		// display names:
		$crud->display_as( 'id', lang('id') )
             ->display_as( 'admin_name', lang('admin_name') )
             ->display_as( 'admin_email', lang('admin_email') )
             ->display_as( 'admin_password', lang('admin_password') )
             ->display_as( 'admin_level', lang('admin_level') )
             ->display_as( 'admin_status', lang('admin_status') );

		// validation:
		$crud->set_rules( 'admin_name', lang('admin_name'),'required|max_length[255]' )
             ->set_rules( 'admin_email', lang('admin_email'), 'required|valid_email|max_length[255]' )
             ->set_rules( 'admin_password', lang('admin_password'), 'max_length[255]' );

        $crud->change_field_type('admin_password','password');

		// callbacks:
		$crud->callback_before_update(array($this, '_crud_before_update'));
		$crud->callback_after_update(array($this, '_crud_after_update'));

		// states:
		$state = $crud->getState();
		$state_info = $crud->getStateInfo();

		// render output:
		$this->set_view_data(array(
			'crud_output'	=>	$crud->render(),
			'pagetitle'		=>	$pagetitle,	
		));
		$this->render( $view_filename, $this->config->item('backend__theme') );
	}

	/**
	 * *************************
	 * before update callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 */
	function _crud_before_update($post_array, $primary_key)
	{
		// pwd:
		if( $post_array['admin_password'] != '' )
		{
			$post_array['admin_password'] = sha1( $post_array['admin_password'] );
		}
		else
		{
			$old_pwd_arr = $this->backend_model->get_val( $this->table_name, $primary_key, 'admin_password' );
			$old_pwd = $old_pwd_arr[0]['admin_password'];
			$post_array['admin_password'] = $old_pwd;
		}

		// xss_clean:
		$post_array = $this->backend_model->xss_clean($post_array);

		return $post_array;
	}

	/**
	 * *************************
	 * after update callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 * @param integer $primary_key
	 * @return boolean
	 */
	function _crud_after_update( $post_array, $primary_key )
	{
		// log:
		$this->backend_model->log($primary_key, array(
			"admin_id" 		=> $this->session->userdata('id'),
			"info" 			=> 'my_profile_update: ' . $primary_key,
			"create_date" 	=> date('Y-m-d H:i:s')
		));

		return TRUE;
	}	
}