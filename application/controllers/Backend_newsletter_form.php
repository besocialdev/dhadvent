<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ********************************
 * backend newsletter form
 * ********************************
 */
class Backend_newsletter_form extends MCMS_Controller 
{
	// setup:
	private $slug_pattern = 'newsletter-form';
	private $table_name = 'newsletter_form';

	function __construct()
	{
		parent::__construct();

		// check user role:
		if( ! $this->backend_model->user_has_right('list', $this->slug_pattern) )
		{
			redirect( site_url('admin/access_info'), 'refresh' );
			return;
		}
	}

	public function index()
	{
		redirect( site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh' );
	}

	/**
	 * *************************
	 * crud
	 * *************************
	 */
	public function crud()
	{
		// setup:
		$prefix_table_name = $this->db->dbprefix($this->table_name);
		$view_filename = 'newsletter_form';
		$pagetitle = lang('sidebar_newsletter');
		$sidebar_info = '';

		// id exists:
		if( $this->uri->segment(3) == 'edit' )
		{
			$id = $this->uri->segment(4);
			if( 
				! isvalidnumber($id,255) || 
				! $this->backend_model->record_exists($this->table_name, array( 'id' => $id ) ) )
			{
				redirect( site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh' );
			}
		}
		
		// init:
		$crud = $this->get_grocery_crud($prefix_table_name, '');
		$crud->set_crud_url_path(site_url('admin/'. $this->slug_pattern));
		$crud->unset_add();
		$crud->columns('first_name','last_name','email_address','create_date');
		$crud->add_fields();
		$crud->edit_fields('first_name','last_name','email_address','create_date');
		$crud->field_type('create_date', 'invisible');

		// display names:
		$crud->display_as( 'id', lang('id') )
             ->display_as( 'first_name', lang('first_name') )
             ->display_as( 'last_name', lang('last_name') )
             ->display_as( 'email_address', lang('email_address') )
             ->display_as( 'create_date', lang('create_date') );

		// callbacks:
		$crud->callback_after_delete(array($this, '_crud_after_delete'));

		// states:
		$state = $crud->getState();
		$state_info = $crud->getStateInfo();
		if( $state == 'export' )
		{
			$crud->columns('id','first_name','last_name','email_address','create_date');
		}

		// render output:
		$this->set_view_data(array(
			'crud_output'	=>	$crud->render(),
			'pagetitle'		=>	$pagetitle,	
		));
		$this->render( $view_filename, $this->config->item('backend__theme') );
	}

	/**
	 * *************************
	 * after delete callback
	 * *************************
	 * 
	 * @param integer $primary_key
	 * @return boolean
	 */
	public function _crud_after_delete($primary_key)
	{
		// log:
		return $this->backend_model->log($primary_key, array(
			"admin_id" 		=> $this->session->userdata('id'),
			"info" 			=> 'newsletter_subscribe_delete: ' . $primary_key,
			"create_date" 	=> date('Y-m-d H:i:s')
		));
	}
}