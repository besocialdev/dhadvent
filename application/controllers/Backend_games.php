<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ********************************
 * backend manage games
 * ********************************
 */
class Backend_games extends MCMS_Controller 
{
	// setup:
	private $slug_pattern = 'games';
	private $table_name = 'user_games';

	function __construct()
	{
		parent::__construct();

		// check user role:
		if( ! $this->backend_model->user_has_right('list', $this->slug_pattern) )
		{
			redirect( site_url('admin/access_info'), 'refresh' );
			return;
		}
	}

	public function index()
	{
		redirect( site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh' );
	}

	/**
	 * *************************
	 * crud
	 * *************************
	 */
	public function crud()
	{
		// setup:
		$prefix_table_name = $this->db->dbprefix($this->table_name);
		$view_filename = 'games';
		$pagetitle = lang('page_sidebar_games');
		$sidebar_info = '';

		// id exists:
		if( $this->uri->segment(3) == 'edit' )
		{
			$id = $this->uri->segment(4);
			if( 
				! isvalidnumber($id,255) || 
				! $this->backend_model->record_exists($this->table_name, array( 'id' => $id ) ) )
			{
				redirect( site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh' );
			}
		}

		// init:
		$crud = $this->get_grocery_crud($prefix_table_name, '');
		$crud->set_crud_url_path(site_url('admin/'. $this->slug_pattern));
		$crud->columns('user_id', 'game_id', 'game_datetime', 'answer_id' /** // wintime - nyerő időpont törlés ,'win_prize_name' */);
		$crud->add_fields();
		$crud->unset_add();
		$crud->unset_edit();
		$crud->unset_read();
		$crud->edit_fields('user_id','game_id','game_datetime','answer_id' /** // wintime - nyerő időpont törlés ,'win_prize_name' */);
		$crud->set_relation('user_id','users','{last_name} {first_name} - {email_address}');
		$crud->set_relation('game_id','calendar_days','cal_day');
		$crud->set_relation('answer_id','calendar_quiz','body');
		$crud->field_type('game_datetime','readonly');
		$crud->unset_texteditor('answer_id');

		// display names:
		$crud->display_as( 'id', lang('id') )
             ->display_as( 'first_name', lang('first_name') )
             ->display_as( 'last_name', lang('last_name') )
             ->display_as( 'game_datetime', 'Játék időpont' )
             ->display_as( 'user_id', 'Játékos' )
             // ->display_as( 'win_prize_name', 'Nyerő időpont nyeremény' ) // wintime - nyerő időpont törlés
             ->display_as( 'game_id', 'Kártya dátuma' )
             ->display_as( 'answer_id', 'Kvíz válasz' )
             ->display_as( 'email_address', lang('email_address') );

		// validation:
	/*	$crud->set_rules( 'first_name', lang('first_name'),'max_length[255]' )
             ->set_rules( 'last_name', lang('last_name'),'max_length[255]' )
             ->set_rules( 'email_address', lang('email_address'), 'valid_email|max_length[255]' )
             ->set_rules( 'newsletter', lang('newsletter'), 'max_length[255]' )
             ->set_rules( 'status', lang('status'), 'required' );*/

		// callbacks:
		$crud->callback_before_update(array($this, '_crud_before_update'));
		$crud->callback_after_update(array($this, '_crud_after_update'));
		$crud->callback_after_delete(array($this, '_crud_after_delete'));

		// states:
		$state = $crud->getState();
		$state_info = $crud->getStateInfo();

		if($state == 'add')
		{

		}

		if($state == 'edit')
		{
			$primary_key = $state_info->primary_key;
			$sidebar_info = '';
		}

		if( $state == 'export' )
		{
			$crud->columns('id','user_id','game_id','game_datetime','game_result');
		}
                
                // 
                if (true === $this->frontend_settings['login_required'])
                {
                    $crud_output = $crud->render();
                }
                else
                {
                    //a játék jelenleg "Vendég" módban fut, regisztráció és bejelentkezés nélkül is lehet használni. Ebben az esetben a játékok menüpont nem elérhető.
                    // views/backend/games.php
                    $crud_output = new stdClass();
                    $crud_output->output = '';

                }                

		// render output:
		$this->set_view_data(array(
			'crud_output'	=>	$crud_output,
			'pagetitle'		=>	$pagetitle,	
			'sidebar_info'	=>	$sidebar_info,
			'primary_key'	=>	(isset($primary_key)) ? $primary_key : 0,
		));
		$this->render( $view_filename, $this->config->item('backend__theme') );
	}


	/**
	 * *************************
	 * before update callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 */
	function _crud_before_update($post_array, $primary_key)
	{
		// xss_clean:
		$post_array = $this->backend_model->xss_clean($post_array);
		return $post_array;
	}

	/**
	 * *************************
	 * after update callback
	 * *************************
	 * 
	 * @param  array $post_array 
	 * @param integer $primary_key
	 * @return boolean
	 */
	function _crud_after_update( $post_array, $primary_key )
	{
		// log:
		$this->backend_model->log($primary_key, array(
			"admin_id" 		=> $this->session->userdata('id'),
			"info" 			=> 'game_data_update: ' . $primary_key,
			"create_date" 	=> date('Y-m-d H:i:s')
		));

		return TRUE;
	}	

	/**
	 * *************************
	 * after delete callback
	 * *************************
	 * 
	 * @param integer $primary_key
	 * @return boolean
	 */
	public function _crud_after_delete($primary_key)
	{
		// log:
		return $this->backend_model->log($primary_key, array(
			"admin_id" 		=> $this->session->userdata('id'),
			"info" 			=> 'game_delete: ' . $primary_key,
			"create_date" 	=> date('Y-m-d H:i:s')
		));
	}
}