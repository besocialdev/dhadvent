<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * ********************************
 * backend manage prizes
 * ********************************
 */
class Backend_prizes extends MCMS_Controller {

    // setup:
    private $slug_pattern = 'prizes';
    private $table_name = 'prizes';

    function __construct() {
        parent::__construct();

        // check user role:
        if (!$this->backend_model->user_has_right('list', $this->slug_pattern)) {
            redirect(site_url('admin/access_info'), 'refresh');
            return;
        }

        $this->load->model("backend_prizes_model");
    }

    public function index() {
        redirect(site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh');
    }

    /**
     * *************************
     * crud
     * *************************
     */
    public function crud() {
        // setup:
        $prefix_table_name = $this->db->dbprefix($this->table_name);
        $view_filename = 'prizes';
        $pagetitle = lang('page_sidebar_prizes');
        $sidebar_info = '';
        $curr_id = null;

        // id exists:
        if ($this->uri->segment(3) == 'edit') {
            $id = $this->uri->segment(4);
            if (!isvalidnumber($id, 255) || !$this->backend_model->record_exists($this->table_name, array('parent_id' => 0, 'id' => $id))) {
                redirect(site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh');
            }
        }

        // init:
        $crud = $this->get_grocery_crud($prefix_table_name, '');
        $crud->set_relation('lang_id', 'mcms_languages', 'lang_name', null, null, 1);
        $crud->where('parent_id', 0);
        $crud->set_crud_url_path(site_url('admin/' . $this->slug_pattern));
        $crud->unset_export();
        $crud->unset_read();
//		$crud->unset_add();
        $crud->unset_clone();
//		$crud->unset_delete();
        $crud->columns('cover_image','title', 'orderby', 'status' /** ,'item_nr'*/ ,'id');
        
        $crud->field_type('status', 'dropdown', $this->config->item('backend__status_default'));
        $crud->field_type('create_date', 'invisible');
        $crud->field_type('parent_id', 'invisible');
        $crud->field_type('post_type', 'invisible');
        $crud->field_type('menu_slug', 'invisible');
        
        $crud->set_field_upload('cover_image', $this->config->item('backend__upload_path'));
        //$crud->unset_texteditor('lead','cms_page_block_content','meta_description','og_description');
        //$crud->field_type('cms_block_type','dropdown', array('image_on_left'=>'Kép a bal oldalon','image_on_right'=>'Kép a jobb oldalon')); // enum
        //$crud->field_type('cms_block_mainpage_visible','dropdown', array(0=>'Nem',1=>'Igen'));
        //$crud->field_type('menu_zone_header','dropdown', array(0=>'Nem',1=>'Igen'));
        //$crud->field_type('menu_zone_footer','dropdown', array(0=>'Nem',1=>'Igen'));
        //$crud->set_field_upload( 'og_image', $this->config->item('backend__upload_path') );

        $crud->add_fields(
                'title', 
                'orderby',
                'status', 
                //'item_nr', 
                'cover_image', 
                'cms_page_block_pre_title', 
                'cms_page_block_title', 
                'cms_page_block_content', 
                'create_date', 
                'menu_slug',
                'parent_id',
                'post_type');

        // display names:
        $crud->required_fields('title');
        $req = $this->config->item('backend__label_req');

        $crud->display_as('title', 'Nyeremény elnevezése' . $req)
                ->display_as('orderby', 'Sorrend')
                ->display_as('cms_page_block_pre_title', 'Blokk elő-cím')
                ->display_as('cms_page_block_title', 'Blokk főcím')
                ->display_as('cms_page_block_content', 'Blokk szöveg')
                ->display_as('menu_slug', lang('menu_slug'))
                // ->display_as('item_nr','Nyeremény darabszáma') 
                ->display_as('cover_image', 'Kép (555x545px-re méretezve)')
                ->display_as('create_date', lang('create_date'))
                ->display_as('id', 'Nyeremény azonosító')
                ->display_as('status', lang('status'));

        // callbacks:
        $crud->callback_before_insert(array($this, '_crud_before_insert'));
        $crud->callback_before_update(array($this, '_crud_before_update'));
        $crud->callback_after_insert(array($this, '_crud_after_insert'));
        $crud->callback_after_update(array($this, '_crud_after_update'));
        $crud->callback_before_delete(array($this, '_crud_before_delete'));
        $crud->callback_after_delete(array($this, '_crud_after_delete'));
        $crud->callback_after_upload(array($this, '_crud_after_upload'));

        // subpages:
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        if ($state == 'edit') {
            $pagetitle = lang('edit_prize');
            $curr_id = (int) $this->uri->segment(4);
            $sidebar_arr = $this->backend_model->sidebar_info($this->table_name, $curr_id, 'create_date');
            $crud->edit_fields(
                    'title', 
                    'orderby',
                    'status', 
                    //'item_nr',
                    'cover_image', 
                    'cms_page_block_pre_title', 
                    'cms_page_block_title', 
                    'cms_page_block_content');

            $sidebar_info = $this->load->view('backend/_pages_widget', array(
                'sb_title' => lang('widget_sidebar_title'),
                'w_create_date' => $sidebar_arr[0]['create_date'],
                'curr_id' => $curr_id,
                    ), true);
        }

        if ($this->uri->segment(3) == 'ajax_list') {
            if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
                exit;
            }
        }

        // render output:
        $this->set_view_data(array(
            'crud_output' => $crud->render(),
            'pagetitle' => $pagetitle,
            'sidebar_info' => '',
            'curr_id' => $curr_id,
        ));
        $this->render($view_filename, $this->config->item('backend__theme'));
    }

    /**
     * *************************
     * delete image
     * *************************
     * 
     * @param  array $post_array 
     * @return array
     */
    public function delete_file($fieldname, $fieldval) {
        $fieldname = trim(addslashes($fieldname));
        $fieldval = trim(addslashes($fieldval));

        if ($fieldname != '' && $fieldval != '') {
            $prefix_table_name = $this->db->dbprefix($this->table_name);
            $this->backend_model->empty_image_field($prefix_table_name, $fieldname, $fieldval);
        }

        echo json_encode(array("success" => true));
    }

    /**
     * *************************
     * resize images, etc...
     * *************************
     */
    function _crud_after_upload($uploader_response, $field_info, $files_to_upload) {
        $this->load->library('image_moo');
        $file_uploaded = $field_info->upload_path . '/' . $uploader_response[0]->name;

        /*if ($field_info->field_name == 'og_image') {
            $this->image_moo
                    ->load($file_uploaded)
                    ->resize_crop(1200, 630)
                    ->save($file_uploaded, true);
        }*/

        if ($field_info->field_name == 'cover_image') {
            $this->image_moo
                    ->load($file_uploaded)
                    ->resize(555, 545)
                    //->resize_crop(1920,265) 
                    ->save($file_uploaded, true);
        }

        return TRUE;
    }

    /**
     * *************************
     * before insert callback
     * *************************
     * 
     * @param  array $post_array 
     * @return array
     */
    function _crud_before_insert($post_array) {
        // create date
        $post_array['create_date'] = date("Y-m-d H:i:s");
        $post_array['parent_id'] = 0;
        $post_array['post_type'] = 'live';
        $post_array = $this->backend_model->xss_clean($post_array, array());
        return $post_array;
    }

    /**
     * *************************
     * before update callback
     * *************************
     * 
     * @param  array $post_array 
     */
    function _crud_before_update($post_array, $primary_key) {

        $post_array = $this->backend_model->xss_clean($post_array, array());
        $post_array['parent_id'] = 0;
        $post_array['post_type'] = 'live';

        return $post_array;
    }

    /**
     * *************************
     * after insert callback
     * *************************
     * 
     * @param  array $post_array 
     * @param integer $primary_key
     * @return boolean
     */
    function _crud_after_insert($post_array, $primary_key) {

        $this->backend_model->log($primary_key, array(
            "admin_id" => $this->session->userdata('id'),
            "info" => 'prize_insert: ' . $primary_key,
            "create_date" => date('Y-m-d H:i:s')
        ));

        return TRUE;
    }

    /**
     * *************************
     * after update callback
     * *************************
     * 
     * @param  array $post_array 
     * @param integer $primary_key
     * @return boolean
     */
    function _crud_after_update($post_array, $primary_key) {

        $this->backend_model->log($primary_key, array(
            "admin_id" => $this->session->userdata('id'),
            "info" => 'prize_update: ' . $primary_key,
            "create_date" => date('Y-m-d H:i:s')
        ));

        return TRUE;
    }

    /**
     * prevent delete hub prizes
     */
    public function _crud_before_delete($primary_key) {
       
        return true;
    }

    /**
     * *************************
     * after delete callback
     * *************************
     * 
     * @param integer $primary_key
     * @return boolean
     */
    public function _crud_after_delete($primary_key) {
        // log:
        return $this->backend_model->log($primary_key, array(
                    "admin_id" => $this->session->userdata('id'),
                    "info" => 'prize_delete: ' . $primary_key,
                    "create_date" => date('Y-m-d H:i:s')
        ));
    }

}
