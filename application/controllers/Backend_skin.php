<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Backend_skin extends MCMS_Controller
{
    // setup:
    private $slug_pattern = 'skin';
    private $table_name = 'setup';

    function __construct(){
        parent::__construct();

        // check user role:
        if( ! $this->backend_model->user_has_right('list', $this->slug_pattern) ){
            redirect( site_url('admin/access_info'), 'refresh' );
            return;
        }
    }

    public function index(){
        redirect( site_url('admin/' . $this->slug_pattern . '/edit/1'), 'refresh' );
    }

    /**
     * *************************
     * crud
     * *************************
     */
    public function crud(){

        // setup:
        $prefix_table_name = $this->db->dbprefix($this->table_name);
        $view_filename = 'skin';
        $pagetitle = lang('page_sidebar_skin');

        // id exists:
        if( $this->uri->segment(3) == 'edit' ){
            $id = $this->uri->segment(4);
            if( $id != 1 || strlen($id)>1  ){
                redirect( site_url('admin/' . $this->slug_pattern . '/edit/1'), 'refresh' );
            }
        }

        // init:
        $crud = $this->get_grocery_crud($prefix_table_name, '');
        $crud->where('id', 1);
        $crud->set_crud_url_path(site_url('admin/'. $this->slug_pattern));

        // disable delete:
        $crud->unset_delete();
        $crud->unset_back_to_list();
        $crud->columns();
        $crud->add_fields();
        $crud->unset_export();
        $crud->unset_add();

        // edit:
        //$crud->edit_fields('skin_main_color','skin_card_modal_tile_color','skin_mainarea_heading_color','skin_mainarea_text_color','skin_mainarea_heading_font_type','skin_btn_background','skin_btn_text_color','skin_bottom_svg_color');
        $crud->edit_fields('skin_main_color','skin_mainarea_heading_color','skin_mainarea_text_color','skin_mainarea_heading_font_type','skin_btn_background','skin_btn_text_color','skin_btn_2_background','skin_btn_2_text_color','skin_bottom_svg_color');


        //$google_api_key = 'AIzaSyCBHofb945zx8D4GpkQgYLGUXBcqBV0VHY';
       	//$fonts = file_get_contents('https://www.googleapis.com/webfonts/v1/webfonts?key=' . $google_api_key);
        //pre($fonts);
        
        $select_webfonts = array();
        $fonts = json_decode(file_get_contents( './assets/static/google_webfonts.json' ),true);
        if( is_array($fonts) ){
        	foreach ($fonts['items'] as $key => $font) {
        		$select_webfonts[$font['family']] = $font['family'];
        	}
        }

        $crud->field_type('skin_mainarea_heading_font_type','dropdown', $select_webfonts);

        /*$crud->field_type('skin_mainarea_heading_font_type','dropdown', array(
            'Covered By Your Grace' => "Covered By Your Grace",
            'Open Sans'				=> "Open Sans",
        ));*/

        // display names:
        $crud->display_as( 'id', lang('id') )
        	->display_as( 'skin_main_color', 'Elsődleges háttérszín' )
        	->display_as( 'skin_mainarea_heading_color', 'Főoldal kiemelt terület címsor szín' )
        	->display_as( 'skin_mainarea_text_color', 'Főoldal kiemelt terület betű szín' )
        	->display_as( 'skin_mainarea_heading_font_type', 'Főoldal kiemelt terület betűtípus' )
        	->display_as( 'skin_btn_background', 'Gomb háttérszín' )
        	->display_as( 'skin_btn_text_color', 'Gomb felirat színe' )
        	->display_as( 'skin_btn_2_background', 'Játék gomb háttérszín' )
        	->display_as( 'skin_btn_2_text_color', 'Játék gomb felirat színe' )
        	->display_as( 'skin_bottom_svg_color', 'Alsó kép színe' );
        	//->display_as( 'skin_card_modal_tile_color', 'Kártya adatlap, címsor betűszín' );


         $crud->set_rules( 'skin_main_color', 'Elsődleges háttérszín','max_length[7]' );
            //->set_rules( 'skin_card_modal_tile_color', 'Kártya adatlap, címsor betűszín','max_length[7]' );

        // callbacks:
        $crud->callback_before_update(array($this, '_crud_before_update'));
        $crud->callback_after_update(array($this, '_crud_after_update'));
        $crud->callback_after_upload(array($this,'_crud_after_upload'));

        if( $this->uri->segment(3) == 'edit' ){

        }
        if( $this->uri->segment(3) == 'add' ){
            return false;
        }

        // render output:
        $this->set_view_data(array(
            'crud_output'    =>    $crud->render(),
            'pagetitle'        =>    $pagetitle,
        ));
        $this->render( $view_filename, $this->config->item('backend__theme') );
    }

    /**
     * *************************
     * before update callback
     * *************************
     *
     * @param  array $post_array
     */
    function _crud_before_update($post_array, $primary_key){
     
        // xss_clean:
        $post_array = $this->backend_model->xss_clean($post_array);

        return $post_array;
    }

    function url_get_contents ($Url) {
	    if (!function_exists('curl_init')){ 
	        die('CURL is not installed!');
	    }
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $Url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    $output = curl_exec($ch);
	    curl_close($ch);
	    return $output;
	}

    /**
     * *************************
     * after update callback
     * *************************
     *
     * @param  array $post_array
     * @param integer $primary_key
     * @return boolean
     */
    function _crud_after_update( $post_array, $primary_key ){

    	// generate css and svg files:
    	$css_template = file_get_contents( './assets/static/style_settings_template.css' );
    	foreach ($post_array as $key => $value) {
    		$css_template = str_replace('%' . $key . '%', $value, $css_template);
    	}
    	
    	$css_template = str_replace('%skin_mainarea_heading_font_type%', $post_array['skin_mainarea_heading_font_type'], $css_template);
    	file_put_contents( './assets/static/style_settings.css',  $css_template);

		if ($handle = opendir('./assets/static/style_settings/svg_templates')) {
			while (false !== ($entry = readdir($handle))) {
				if ($entry != "." && $entry != "..") {
				    $svg_file = file_get_contents( './assets/static/style_settings/svg_templates/' . $entry );
				    foreach ($post_array as $key => $value) {
			    		$svg_file = str_replace('%' . $key . '%', $value, $svg_file);
			    	}
			    	file_put_contents( './assets/static/style_settings/' . $entry,  $svg_file);
				}
			}
			closedir($handle);
		}

        // log:
        $this->backend_model->log($primary_key, array(
            "admin_id"         => $this->session->userdata('id'),
            "info"             => 'skinsettings_changed: ' . $primary_key,
            "create_date"     => date('Y-m-d H:i:s')
        ));

        return TRUE;
    }

    /**
     * *************************
     * delete image
     * *************************
     *
     * @param  array $post_array
     * @return array
     */
    public function delete_file($fieldname, $fieldval){
        $fieldname = trim(addslashes($fieldname));
        $fieldval = trim(addslashes($fieldval));

        if( $fieldname != '' && $fieldval != '' ){
            $prefix_table_name = $this->db->dbprefix($this->table_name);
            $this->backend_model->empty_image_field( $prefix_table_name, $fieldname, $fieldval );
        }

        echo json_encode(array("success"=>true));
    }
}