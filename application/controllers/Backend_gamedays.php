<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * ********************************
 * backend manage gamedays
 * ********************************
 */
class Backend_gamedays extends MCMS_Controller {

    // setup:
    private $slug_pattern = 'gamedays';
    private $table_name = 'calendar_days';

    private $quiz_answers_array = array();
    private $quiz_answers_array_to_save = array();

    // calendar_quiz table
    // ENUM type field
    private $answer_fieldnames = array(
        "answer_1",
        "answer_2",
        "answer_3",
        "answer_4",
        "answer_5",
    );

    function __construct() {
        parent::__construct();

        // check user role:
        if (!$this->backend_model->user_has_right('list', $this->slug_pattern)) {
            redirect(site_url('admin/access_info'), 'refresh');
            return;
        }

        $this->load->model("backend_gamedays_model");
    }

    public function index() {
        redirect(site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh');
    }


    private function load_quiz_answers($curr_id=0)
    {
        $result = $this->backend_gamedays_model->read_quiz_answers($curr_id);
        if(!empty($result)) {

            $tmp = array();
            foreach ($result as $o) {
                $tmp[$o['fieldname']] = $o;
            }

            // csak a meghatározott mezőnevek lehetnek
            foreach ($this->answer_fieldnames as $fieldname) {
                if(!empty($tmp[$fieldname])) {
                    $this->quiz_answers_array[$fieldname] = $tmp[$fieldname];
                }
                else {
                    $this->quiz_answers_array[$fieldname] = array();
                }
            }
        }
    }

    public function crud(){
        // setup:
        $prefix_table_name = $this->db->dbprefix($this->table_name);
        $view_filename = 'gamedays';
        $pagetitle = lang('page_sidebar_gamedays');
        $sidebar_info = '';
        $curr_id = null;

        // init:
        $crud = $this->get_grocery_crud($prefix_table_name, '');
        $crud->set_relation('lang_id', 'mcms_languages', 'lang_name', null, null, 1);

        // id exists:
        $state = $crud->getState();
        //$state_info = $crud->getStateInfo();
        if($state == 'edit')
        {
            $pagetitle = lang('edit_page');
            $curr_id = (int)$this->uri->segment(4);

            if (!isvalidnumber($curr_id, 255) || !$this->backend_model->record_exists($this->table_name, array('id' => $curr_id))) {
                redirect(site_url('admin/' . $this->slug_pattern . '/crud'), 'refresh');
            }

            $this->load_quiz_answers($curr_id);

        }


        $crud->set_crud_url_path(site_url('admin/' . $this->slug_pattern));
        $crud->unset_export();
        $crud->unset_read();
        $crud->unset_clone();
        //$crud->order_by("cal_day", "ASC");
        $list_clumns = array('cal_day', 'headline_text', 'type','status');

        $day_types = array(
            'text'=>"Szöveges, képes tartalom (szerkesztővel)",
            'youtube'=>"Csak Youtube videó (csak a linket kell megadni)",
            'puzzle'=>"Puzzle",
            /*'image'=>"Kép"*/
            // 'wintime'=>'Nyerő időpont' // wintime - nyerő időpont törlés
            );

        //csak akkor van kvíz tipus, ha belépéshez van kötve a játék
        if(true === $this->frontend_settings['login_required'])
        {
            $day_types['quiz'] = "Kvíz";
            $list_clumns[] = 'quiz_correct_count';
        }
        else
        {
            //csak akkor van kvíz tipus, ha belépéshez van kötve a játék
            //ha nincs belépéshez kötve, akkor quiz tipusú napokat meg sem jelenítjük
            // kérdés jó e így
            $crud->where('type !=','quiz');
        }
        $crud->field_type('type','dropdown',$day_types);

        $crud->columns($list_clumns);


        // required fields
        $crud->required_fields('cal_day','type');
        $req = $this->config->item('backend__label_req'); // van beépített (required, css-ben: display:none) is de ezt használom

        $crud_fields = array(
            'cal_day',
            'type',
            'status',
            'title',
            'headline_text',
            'subtitle',
            'youtube_link',
            'puzzle_image',
            'puzzle_pcx',
            'puzzle_pcy',
            //'wintime_success',
            'quiz_question',
            'quiz_success',
            'quiz_failure',
            '_quiz_answers_input_possibilities_'
        );

        //csak akkor van kupon feltöltés, ha belépéshez van kötve a játék
        if(true === $this->frontend_settings['login_required'])
        {
            $crud_fields[] = 'coupon_1';
            $crud_fields[] = 'coupon_2';
            $crud_fields[] = 'coupon_3';

            $crud->set_field_upload( 'coupon_1', $this->config->item('backend__upload_path_coupon') );
            $crud->display_as( 'coupon_1', 'Kupon 1' );

            $crud->set_field_upload( 'coupon_2', $this->config->item('backend__upload_path_coupon') );
            $crud->display_as( 'coupon_2', 'Kupon 2' );

            $crud->set_field_upload( 'coupon_3', $this->config->item('backend__upload_path_coupon') );
            $crud->display_as( 'coupon_3', 'Kupon 3' );

        }

        $crud_fields[] = 'body';

        //$crud_fields = array_merge($crud_fields, $this->answer_fieldnames);
        $crud->unset_texteditor('quiz_success','quiz_failure');


        $crud->field_type('status','dropdown', array(
            1 => 'Aktív',
            0 => 'Inaktív',
        ));

        //$crud->unset_texteditor('wintime_success');  // wintime - nyerő időpont törlés

        $crud->callback_field('_quiz_answers_input_possibilities_',array($this,'quiz_answers_field_callback'));
        $crud->display_as('_quiz_answers_input_possibilities_', 'Kvíz típus esetén válaszlehetőségek:');



        $crud->fields($crud_fields);
        // display names:
        $crud->display_as('cal_day', 'Kiválasztott dátum'.$req);
        $crud->display_as('type', 'Játéktípus'.$req);
       //  $crud->display_as('type', 'Játéktípus'.$req.'<br><span style="font-weight:normal; color: #999;">Figyelem: a játék típusa befolyásolja a megjelenő adatokat</span>');
        $crud->display_as('title', 'Dátumsorhoz tartozó cím');
        $crud->display_as('headline_text', 'Főcímsor');
        $crud->display_as('subtitle', 'Alcím');
        $crud->display_as('status', 'Publikus oldalon megjelenik?');
        $crud->display_as('body', 'Tényleges tartalom'.'<br><span style="font-weight:normal; color: #999;">Csak bejelentkezés után látható</span>');
        $crud->display_as('quiz_success', 'A helyes válasz üzenete');
        $crud->display_as('quiz_failure', 'A hibás válasz üzenete');
        $crud->display_as('quiz_correct_count', 'Kvíz helyes találatok száma');
        //$crud->unsetSearchColumns(['quiz_correct_count']);

        //$crud->display_as('wintime_success', 'Nyerő időpont üzenet, a felhasználó nyert.');  // wintime - nyerő időpont törlés

        $crud->display_as('quiz_question', 'Kvíz kérdés');
        $crud->display_as('lang_id', 'Nyelv');


        $crud->set_field_upload( 'puzzle_image', $this->config->item('backend__upload_path_puzzle') );
        $crud->display_as( 'puzzle_image', 'Puzzle kép' );
        $crud->display_as( 'puzzle_pcx', 'Oszlopok száma' );
        $crud->display_as( 'puzzle_pcy', 'Sorok száma' );


        $crud->field_type('puzzle_pcx','dropdown', array(
            2 => '2',
            3 => '3',
            4 => '4',
            5 => '5',
        ));

        $crud->field_type('puzzle_pcy','dropdown', array(
            2 => '2',
            3 => '3',
            4 => '4',
            5 => '5',
        ));

        $crud->set_rules('youtube_link', 'Youtube link' ,'trim|callback_youtubelink_empty_check|callback_youtubelink_valid_check');

        $crud->set_rules('quiz_question', 'Kvíz kérdés' ,'trim|callback_quiz_question_empty_check');
        $crud->set_rules('quiz_success', 'A helyes válasz üzenete' ,'trim|callback_quiz_success_empty_check');
        $crud->set_rules('quiz_failure', 'A hibás válasz üzenete' ,'trim|callback_quiz_failure_empty_check');

        $crud->set_rules('cal_day', 'Játék nap' ,'callback_valid_date');
        //$crud->set_rules('youtube_link', 'Youtube link' ,'trim');

        // callbacks:
        $crud->callback_before_insert(array($this, '_crud_before_insert'));
        $crud->callback_before_update(array($this, '_crud_before_update'));
        $crud->callback_after_insert(array($this, '_crud_after_insert'));
        $crud->callback_after_update(array($this, '_crud_after_update'));
        $crud->callback_before_delete(array($this, '_crud_before_delete'));
        $crud->callback_after_delete(array($this, '_crud_after_delete'));
        $crud->callback_after_upload(array($this, '_crud_after_upload'));

        $crud->set_js("assets/grocery_crud/texteditor/ckeditor5-build-classic/ckeditor.js");

        // render output:
        $this->set_view_data(array(
            'crud_output' => $crud->render(),
            'pagetitle' => $pagetitle,
            'sidebar_info' => $sidebar_info,
            'curr_id' => $curr_id,
            'crud_state' => $state
        ));
        $this->render($view_filename, $this->config->item('backend__theme'));
    }

    /**
     * tooltip megjelenítéssel kéne!
     */
    public function show_help(){

    }

    public function valid_date($value){
        $primary_key = $this->uri->segment(4);
        if( $this->backend_model->game_day_exists($value, $primary_key) > 0 ){
            $this->form_validation->set_message(__FUNCTION__, 'Ezzel a dátummal már létezik kártya: ' . $value);
            return false;
        }
        return true;
    }

    public function quiz_failure_empty_check($value){
        $return = true;
        if(!empty($_POST['type']) && $_POST['type']=='quiz'){
            if(empty($value)){
                $this->form_validation->set_message(__FUNCTION__, 'Kvíz típusú játék esetén a {field} mező kitöltése kötelező.');
                $return = false;
            }
        }
        return $return;
    }

    public function quiz_success_empty_check($value){
        $return = true;
        if(!empty($_POST['type']) && $_POST['type']=='quiz'){
            if(empty($value)){
                $this->form_validation->set_message(__FUNCTION__, 'Kvíz típusú játék esetén a {field} mező kitöltése kötelező.');
                $return = false;
            }
        }
        return $return;
    }


    public function quiz_question_empty_check($value){
        $return = true;
        if(!empty($_POST['type']) && $_POST['type']=='quiz'){
            if(empty($value)){
                $this->form_validation->set_message(__FUNCTION__, 'Kvíz típusú játék esetén a {field} mező kitöltése kötelező.');
                $return = false;
            }
        }
        return $return;
    }


    public function youtubelink_empty_check($value){

        $return = true;

        if(!empty($_POST['type']) && $_POST['type']=='youtube')
        {
            if(empty($value))
            {
                $this->form_validation->set_message(__FUNCTION__, 'Youtube típusú játék esetén a {field} mező kitöltése kötelező.');
                $return = false;
            }
        }

        return $return;

    }

    public function youtubelink_valid_check($value){

        $return = true;

        if(!empty($_POST['type']) && $_POST['type']=='youtube')
        {
            //preg_match("/^(https?\:\/\/)?(www\.)?(youtube\.com|youtu\.?be)\/.+$/", $value, $matches);
            preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $value, $matches);

            if(empty($matches)){
                $this->form_validation->set_message(__FUNCTION__, 'A megadott {field}:<br><i>'.$value.'</i><br> nem tűnik valósnak.<br>Kérlek ellenőrizd!');
                return false;
            }

            if(!empty($matches[1]) && strlen($matches[1])>11)
            {
                $this->form_validation->set_message(__FUNCTION__, 'A megadott {field}:<br><i>'.$value.'</i><br> azonosítója nem tűnik valósnak.<br>Kérlek ellenőrizd!');
                return false;
            }

        }
        //$this->form_validation->set_message(__FUNCTION__, '{field}, {param}, '.$value.', aaaaaaaaaaaaaa'.print_r($_POST,1));

        return $return;

    }


    /**
     * *************************
     * live url
     * *************************
     */
    public function get_live_url() {
        if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
            exit;
        }

        $error_msg = 'Hiba a lekérés során.';
        $result = NULL;
        $id = (int) $this->input->get('id');
        if ($id > 0) {
            $error_msg = '#OK#';

            $result = $this->backend_gamedays_model->get_page_url($id);
        }

        echo json_encode(array(
            "error_msg" => $error_msg,
            "result" => $result,
        ));
        exit;
    }

    /**
     * *************************
     * ajax save preview
     * *************************
     */
    public function save_preview() {
        if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
            exit;
        }

        $error_msg = 'Hiba a mentés során.';
        $result = NULL;
        $id = (int) $this->input->get('id');

        if ($_POST && $id > 0) {
            $this->backend_gamedays_model->update_preview(array(
                'post_type' => 'preview',
                'title' => trim($this->input->post("title")),
                'menu_slug' => 'preview/page/' . $id,
                'cms_page_block_pre_title' => trim($this->input->post("cms_page_block_pre_title")),
                'cms_page_block_title' => trim($this->input->post("cms_page_block_title")),
                'cms_page_block_content' => trim(strip_tags($this->input->post("cms_page_block_content"))),
                'content' => $this->input->post("content"),
                'status' => (int) $this->input->post("status")
                    ), $id);

            $error_msg = '#OK#';
        }

        echo json_encode(array(
            "error_msg" => $error_msg,
            "result" => $result,
        ));
        exit;
    }

    /**
     * *************************
     * delete image
     * *************************
     *
     * @param  array $post_array
     * @return array
     */
    public function delete_file($fieldname, $fieldval) {
        $fieldname = trim(addslashes($fieldname));
        $fieldval = trim(addslashes($fieldval));

        if ($fieldname != '' && $fieldval != '') {
            $prefix_table_name = $this->db->dbprefix($this->table_name);
            $this->backend_model->empty_image_field($prefix_table_name, $fieldname, $fieldval);

            if($fieldname=='puzzle_image'){
                $this->delete_prev_puzzle_images($fieldval);
            }
            if($fieldname=='coupon_1' || $fieldname=='coupon_2' || $fieldname=='coupon_3'){
                $img = $this->config->item('backend__upload_path_coupon').'/'.$fieldval;
                if(file_exists($img))
                {
                    unlink($img);
                }
            }
        }

        echo json_encode(array("success" => true));
    }

    /**
     * *************************
     * resize images, etc...
     * *************************
     */
    function _crud_after_upload($uploader_response, $field_info, $files_to_upload) {
        $this->load->library('image_moo');
        $file_uploaded = $field_info->upload_path . '/' . $uploader_response[0]->name;

        if ($field_info->field_name == 'og_image') {
            $this->image_moo
                    ->load($file_uploaded)
                    ->resize_crop(1200, 630)
                    ->save($file_uploaded, true);
        }

        if ($field_info->field_name == 'cover_image') {
            $this->image_moo
                    ->load($file_uploaded)
                    ->resize(555, 545)
                    //->resize_crop(1920,265)
                    ->save($file_uploaded, true);
        }

        if ( $field_info->field_name == 'coupon_1'
                || $field_info->field_name == 'coupon_2'
                || $field_info->field_name == 'coupon_3'
            ) {
            $this->image_moo->load($file_uploaded)
                            ->resize(400, 400)
                            ->save($file_uploaded, true);
        }


        if ($field_info->field_name == 'puzzle_image') {
            $this->image_moo->load($file_uploaded)
                            ->resize(640, 640)
                            ->save($file_uploaded, true)

                            ->resize(400, 400)
                            ->save_pa('tablet_')

                            ->resize(270, 270)
                            ->save_pa('mobile_');
        }

        return TRUE;
    }

    /**
     * *************************
     * before insert callback
     * *************************
     *
     * @param  array $post_array
     * @return array
     */
    function _crud_before_insert($post_array) {

        // a kvíz válaszok összegyűjtése
        // azért itt mert a gyűjtéskor az answer_{n} tipusú post adatokat töröljük, mert a calendar_days táblában nincs ilyen
        $post_array = $this->collect_quiz_answers($post_array);

        if($post_array['type']=='youtube' || $post_array['type']=='image' || $post_array['type']=='puzzle')
        {
            $post_array['body'] = ($post_array['body']);
        }

        /**
         * ha puzzle játékmód de nincs feltöltve kp vagy nincs megadva x, y érték akkor automatikusan inaktív!!!
         */
        if($post_array['type']=='puzzle')
        {
            if($post_array['puzzle_image']=='' || $post_array['puzzle_pcx']=='' || $post_array['puzzle_pcy']=='')
            {
                $post_array['status'] = 0;
                $_POST['status'] = 0;
            }
        }

        // create date
        //$post_array['create_date'] = date("Y-m-d H:i:s");

        // xss_clean:
        $post_array = $this->backend_model->xss_clean($post_array, array('body'));

        return $post_array;
    }

    /**
     * *************************
     * before update callback
     * *************************
     *
     * @param  array $post_array
     */
    function _crud_before_update($post_array, $primary_key) {

        // a kvíz válaszok összegyűjtése
        // azért itt mert a gyűjtéskor az answer_{n} tipusú post adatokat töröljük, mert a calendar_days táblában nincs ilyen
        $post_array = $this->collect_quiz_answers($post_array, $primary_key);

        if($post_array['type']=='youtube' || $post_array['type']=='image' || $post_array['type']=='puzzle')
        {
            $post_array['body'] = ($post_array['body']);
        }

        /**
         * ha puzzle játékmód de nincs feltöltve kp vagy nincs megadva x, y érték akkor automatikusan inaktív!!!
         */
        if($post_array['type']=='puzzle')
        {
            if($post_array['puzzle_image']=='' || $post_array['puzzle_pcx']=='' || $post_array['puzzle_pcy']=='')
            {
                $post_array['status'] = 0;
                $_POST['status'] = 0;
            }
        }

        // xss_clean:
        $post_array = $this->backend_model->xss_clean($post_array, array('body'));

        return $post_array;
    }

    /**
     * *************************
     * after insert callback
     * *************************
     *
     * @param  array $post_array
     * @param integer $primary_key
     * @return boolean
     */
    function _crud_after_insert($post_array, $primary_key) {


        if(!empty($this->quiz_answers_array_to_save))
        {
            $this->save_quiz_answers($primary_key);
        }

        if(!empty($post_array['puzzle_image']))
        {
            $puzzle_img_name = $post_array['puzzle_image'];
            $params = array(
                "pcx" => $post_array['puzzle_pcx'],
                "pcy" => $post_array['puzzle_pcy']
            );

            $this->crop_puzzle_img($puzzle_img_name,$params);
            $this->crop_puzzle_img('mobile_'.$puzzle_img_name,$params);
            $this->crop_puzzle_img('tablet_'.$puzzle_img_name,$params);
        }

        // log:
        $this->backend_model->log($primary_key, array(
            "admin_id" => $this->session->userdata('id'),
            "info" => 'cms_page_insert: ' . $primary_key,
            "create_date" => date('Y-m-d H:i:s')
        ));

        return TRUE;
    }

    /**
     * *************************
     * after update callback
     * *************************
     *
     * @param  array $post_array
     * @param integer $primary_key
     * @return boolean
     */
    function _crud_after_update($post_array, $primary_key) {

        if(!empty($this->quiz_answers_array_to_save))
        {
            $this->save_quiz_answers();
        }

        if(!empty($post_array['puzzle_image']))
        {
            // fontos, hogy az előzőleg darabolt képeket törölni kell, de az eredeti nagy kép marad!
            $this->delete_prev_puzzle_images($post_array['puzzle_image'],$delete_original=false);

            $puzzle_img_name = $post_array['puzzle_image'];
            $params = array(
                "pcx" => $post_array['puzzle_pcx'],
                "pcy" => $post_array['puzzle_pcy']
            );

            $this->crop_puzzle_img($puzzle_img_name,$params);
            $this->crop_puzzle_img('mobile_'.$puzzle_img_name,$params);
            $this->crop_puzzle_img('tablet_'.$puzzle_img_name,$params);
        }



        // log:
        $this->backend_model->log($primary_key, array(
            "admin_id" => $this->session->userdata('id'),
            "info" => 'cms_page_update: ' . $primary_key,
            "create_date" => date('Y-m-d H:i:s')
        ));

        return TRUE;
    }

    /**
     * prevent delete hub gamedays
     */
    public function _crud_before_delete($primary_key) {
        //$nodelete = array(1,29,35);
        $nodelete = array();
        if (in_array($primary_key, $nodelete)) {
            return false;
        }
        return true;
    }

    /**
     * *************************
     * after delete callback
     * *************************
     *
     * @param integer $primary_key
     * @return boolean
     */
    public function _crud_after_delete($primary_key) {

        $this->backend_gamedays_model->delete_quiz_answers($primary_key);

        // log:
        return $this->backend_model->log($primary_key, array(
                    "admin_id" => $this->session->userdata('id'),
                    "info" => 'page_delete: ' . $primary_key,
                    "create_date" => date('Y-m-d H:i:s')
        ));
    }

    /**
     * collect quiz answers to save
     *
     * collect: crud_before_update or crud_before_insert
     * run: crud_after_update or crud_after_insert
     *
     * @param type $post_array
     * @param type $primary_key
     * @return type
     */
    private function collect_quiz_answers($post_array,$primary_key=null)
    {
        //load saved values
        $this->load_quiz_answers($primary_key);

        foreach ($this->answer_fieldnames as $fieldname) {

            $this->quiz_answers_array_to_save[$fieldname] = array();

            if(isset($this->quiz_answers_array[$fieldname])) {
                $this->quiz_answers_array_to_save[$fieldname] = $this->quiz_answers_array[$fieldname];
            }

            $this->quiz_answers_array_to_save[$fieldname]['day_id'] = $primary_key;
            $this->quiz_answers_array_to_save[$fieldname]['body'] = (!empty($post_array[$fieldname]) ? $post_array[$fieldname] : '');
            $this->quiz_answers_array_to_save[$fieldname]['fieldname'] = $fieldname;
            $this->quiz_answers_array_to_save[$fieldname]['is_correct'] = ($fieldname == 'answer_1' ? 1 : 0);

            // post adatot ki kell szedni
            if(isset($post_array[$fieldname]))
            {
                unset($post_array[$fieldname]);
            }
        }

        return $post_array;
    }

    /**
     * Save quiz answers to database
     *
     * @param $day_id value only when _crud_after_insert
     */
    private function save_quiz_answers($day_id=null){
        if(!empty($this->quiz_answers_array_to_save))
        {
            foreach ($this->quiz_answers_array_to_save as $inputname => $sql_fields) {

                // _crud_after_insert
                if($day_id!=null) {
                    $sql_fields['day_id'] = $day_id;
                }

                //update
                if(!empty($sql_fields['id'])){

                    $id = $sql_fields['id']; // calendar_quiz -> id
                    unset($sql_fields['id']); // unset to update

                    $this->backend_gamedays_model->update_quiz_answers($sql_fields, $id);
                }
                //insert
                else{
                    $this->backend_gamedays_model->insert_quiz_answers($sql_fields);
                }

            }
        }
    }



    /**
     * callback function for quiz answers html field
     *
     * @param type $fieldname
     * @param type $primary_key
     * @return type
     */
    public function quiz_answers_field_callback($fieldname = '', $primary_key = null)
    {
        /**
         * válasz lehetőségek elnevezései
         */
        $return = '';
        foreach ($this->answer_fieldnames as $k => $v) {

            $params = array();
            if($k==0) {
                $params['input_label'] = 'HELYES válasz' ;
            }
            else {
                $params['input_label'] = 'Alternatív válasz '.$k;
            }

            $params['input_fieldname'] = $v;
            $params['input_value'] = (!empty($this->quiz_answers_array[$v]['body']) ? $this->quiz_answers_array[$v]['body'] : '' );


            $return .= $this->answer_manual_input_row($params);

        }

        return $return;
    }

    /**
     * generate quiz answer input field html row
     *
     * @param array $params
     * [input_label]        => label of input<br>
     * [input_fieldname]    => name of input<br>
     * [input_value]        => value of input (default: empty)<br>
     * @return text html code of input row
     */
    private function answer_manual_input_row($params=array()){
        $return = '';
        if(!empty($params)){
            $return .= '<div class="form-group '.$params['input_fieldname'].'_form_group">';
                    $return .= '<label class="col-sm-2 control-label" style="text-align:left;">';
                        $return .= $params['input_label'];
                    $return .= '</label>';

                    $return .= '<div class="col-sm-10">';
                        $return .= '<input id="field-'.$params['input_fieldname'].'" class="form-control" name="'.$params['input_fieldname'].'" type="text" value="'.(!empty($params['input_value']) ? $params['input_value'] : '').'">';
                    $return .= '</div>';
            $return .= '</div>';
        }

        return $return;
    }

    /**
     * kép darabolása a puzzle játékhoz
     *
     * @param string $img_name kép neve elérési út nélkül
     * @param array $params egyéb paraméterek
     * <br><b>[upload_path]</b> => kép helye alapértelmezetten: $this->config->item('backend__upload_path_puzzle')
     * <br><b>[pcx]</b> => hány oszlopra legyen vágva a kép
     * <br><b>[pxy]</b> => hány sorra legyen vágva a kép
     * @return type
     */

    public function crop_puzzle_img($img_name,$params = array())
    {
        // params defaults
        $path = (isset($params['upload_path']) ? $params['upload_path'] : $this->config->item('backend__upload_path_puzzle').'/' );
        $pcx = (!empty($params['pcx']) ? $params['pcx'] : 4 ); // ha 0-t ad meg akkor a 4 az alapértelemzett
        $pcy = (!empty($params['pcy']) ? $params['pcy'] : 3 ); // ha 0-t ad meg akkor a 3 az alapértelemzett

        $this->load->library('image_moo');

        if(file_exists($path.$img_name))
        {
            $img = $path.$img_name;

            $img_info = getimagesize($img);
            $path_parts = pathinfo($img);
            $img_name_without_ext = $path_parts['filename'];

            if(false !== $img_info)
            {
                $img_width = $img_info[0];
                $img_height = $img_info[1];

                $crop_w = round($img_width / $pcx);
                $crop_h = round($img_height / $pcy);



                $this->image_moo->load($img);

                if (!file_exists($path.$img_name_without_ext)) {
                    $mkdir_res = mkdir($path.$img_name_without_ext);
                }
                else{
                    $mkdir_res = true;
                }

                if($mkdir_res)
                {
                    for ($x= 0; $x < $pcx; $x++) {
                        for ($y = 0; $y < $pcy; $y++) {
                            $x1 = ($x*$crop_w);
                            $x2 = ($x*$crop_w + $crop_w);

                            $y1 = ($y*$crop_h);
                            $y2 = ($y*$crop_h + $crop_h);

                            $dest_file = $path.$img_name_without_ext.'/'.$x.$y.$img_name;

                            $this->image_moo->crop($x1, $y1, $x2, $y2)
                                    //->set_background_colour("#TRANSPARENT")
                                    ->save($dest_file, true);
                        }
                    }
                }


            }

        }
        else
        {
            die('nem létezik a fájl');
        }

        //die('---debug: crop_puzzle_img');
    }

    /**
     * a korábban tárolt puzzle kép és a hozzá tartozó elemek törlése
     * (azért kell mindenképpen törölni, mert ha megváltozik a darabszám akkor is újra kel generálni)
     *
     * @param type $post_array
     * @param type $primary_key
     */
    private function delete_prev_puzzle_images($prev_puzzle_img,$delete_original = true)
    {

        if($prev_puzzle_img!='')
        {
            $img = $this->config->item('backend__upload_path_puzzle').'/'.$prev_puzzle_img;
            if(true==$delete_original && file_exists($img))
            {
                unlink($img);
            }
            $path_parts = pathinfo($img);
            $img_name_without_ext = $path_parts['filename'];

            $dir = $this->config->item('backend__upload_path_puzzle').'/'.$img_name_without_ext;
            if(file_exists($dir))
            {
                self::delTree($dir);
            }

            // mobilra vonatkozó kép törlés
            $img_mobile = $this->config->item('backend__upload_path_puzzle').'/mobile_'.$prev_puzzle_img;
            if(true==$delete_original && file_exists($img_mobile))
            {
                unlink($img_mobile);
            }

            $path_parts = pathinfo($img_mobile);
            $img_name_without_ext = $path_parts['filename'];

            $dir = $this->config->item('backend__upload_path_puzzle').'/'.$img_name_without_ext;
            if(file_exists($dir))
            {
                self::delTree($dir);
            }

            // tabletre vonatkozó kép törlés
            $img_tablet = $this->config->item('backend__upload_path_puzzle').'/mobile_'.$prev_puzzle_img;
            if(true==$delete_original && file_exists($img_tablet))
            {
                unlink($img_tablet);
            }

            $path_parts = pathinfo($img_tablet);
            $img_name_without_ext = $path_parts['filename'];

            $dir = $this->config->item('backend__upload_path_puzzle').'/'.$img_name_without_ext;
            if(file_exists($dir))
            {
                self::delTree($dir);
            }

        }
    }

    public static function delTree($dir) {
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }

}
