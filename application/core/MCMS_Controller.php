<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ********************************
 * mcms main controller
 * ********************************
 */
class MCMS_Controller extends CI_Controller {
	protected $view_data = array();
	public $frontend_settings = array();
	protected $game_is_active = false;
        protected $project_info = array();

        /**
         * ebben vannak azok az oldal slug értékek, amelyek regisztráció függőek
         * ezek az oldalak az adminon nem jelennek meg, illetve frontenden 404-re ugratnak,
         * amennyiben a játékmód "vendég" tipusú (azaz belépés nélkül is használható), ellenkező esetben alapértelmezetten üres
         * azokat kell felsorolni amik tiltva vannak!
         *
         * @var array page slugs
         * @see MCMS_Controller->set_disabled_frontend_user_login_page_slugs()
         */
        public $disabled_login_required_pages = array();

        public function __construct(){
		parent::__construct();
		// $this->output->enable_profiler(TRUE);

                if(empty($this->config->item('project_id')))
                {
                    die("mcms_config_global error - project_id not exists");
                }

		$project_info = $this->thepromobuilder->project_info();
                $this->project_info = $project_info;

                //megy e a játék vagy sem!!
                $this->game_is_active = $this->is_game_active($project_info);
		
		// settings for backend:
		if(  in_array( $this->uri->segment(1), $this->config->item('global__admin_slugs') ) ){
			$this->config->load('mcms_config_backend');
			$this->load->library('grocery_CRUD');
			$this->lang->load('backend', 'hungarian');
			$this->load->model("backend_model");
			$this->load->model("backend_session_model");

			// protected area:
			if( ! in_array( $this->uri->segment(1), $this->config->item('global__admin_not_protected_slugs') ) ){
				// cookie protection (disabled):
				if( 0 ){
					$cookie_value = (int)$this->input->cookie( $this->config->item('backend__cookie_name'), TRUE );
					if( $cookie_value != 1 ) {
						echo '404';
						exit;
					}
				}

				// check logged status:
				if( TRUE === $this->backend_session_model->check_logged_status() ){
					// ...
				} else {
					$this->backend_session_model->redirect_to_login();
					exit;
				}
			}

			// get settings:
			$this->frontend_settings = $this->backend_model->get_settings();
                        $this->frontend_settings["theme"] = $this->config->item("frontend__theme");

                        // regisztrációhoz kötött játék kapcsoló (ha nincs belépéshez kötve a játék akkor nincs kvíz tipus)
                        $this->frontend_settings["login_required"] = (isset($this->frontend_settings["login_required_setting"]) && $this->frontend_settings["login_required_setting"]==1) ? true : false;
                        $this->set_disabled_frontend_user_login_page_slugs();

			// set global view vars for admin panel:
			$this->set_view_data( array(
				'backend_menu'		=>	$this->gen_backend_menu($this->frontend_settings['page_name']),
				'frontend_settings'	=>	$this->frontend_settings,
				'project_info'		=>	$project_info,
                                'days_available'        =>      (!empty($project_info['data']['days_available']) && $project_info['data']['days_available']>0) ? $project_info['data']['days_available'] : 0
			));
		}

		// settings for frontend:
		else {
                        $this->load->model("backend_session_model");
			$this->config->load('mcms_config_frontend');
			$this->load->model("frontend_model");
			//$this->output->cache(10);
                        
                        //adminon be van jelentkezve akkor is akítva játék
                        if(TRUE === $this->backend_session_model->check_logged_status())
                        {
                                $this->game_is_active = true;
                        }
                        

			$this->lang->load('calendarapp_frontend', 'hungarian');
			//$this->lang->load('calendarapp_frontend', 'english');

			$this->frontend_settings = $this->frontend_model->get_settings();
                        $this->frontend_settings["theme"] = $this->config->item("frontend__theme");

                        // regisztrációhoz kötött játék kapcsoló
                        $this->frontend_settings["login_required"] = (isset($this->frontend_settings["login_required_setting"]) && $this->frontend_settings["login_required_setting"]==1) ? true : false;
                        $this->set_disabled_frontend_user_login_page_slugs();

                        // cookie modal beállítások
                        $this->frontend_settings['cookie_codes'] = $this->cookie_modal_script_codes();

                        $this->frontend_settings['pushalert'] = ''; // Szilvi kérésére egyelőre nincs push alert // Imi

			// under construction:
			if( (int)$this->frontend_settings['site_status'] != 1 ){
				//$this->load->model("backend_session_model");
				if( FALSE === $this->backend_session_model->check_logged_status() ){
					echo $this->load->view('under_construction', array('inactive_text'=>$this->frontend_settings['inactive_text']), TRUE);
					exit;
				}
			}

                        /*
                         * ezek helyett csak a thepromobuilder api választ kell figyelni
			// game is active?
			$game_start = $this->frontend_settings['game_start_date'] . ' ' . $this->frontend_settings['game_start_time'];
			$game_end = $this->frontend_settings['game_end_date'] . ' ' . $this->frontend_settings['game_end_time'];
			$now = date("Y-m-d H:i:s");
			if( $game_start <= $now && $game_end >= $now ){
				$this->game_is_active = true;
			}



			if( $project_info['data']['status'] != 'ongoing' || (int)$project_info['data']['days_available'] <=0 ){
				$this->load->model("backend_session_model");
				if( FALSE === $this->backend_session_model->check_logged_status() ){
					$this->game_is_active = false;
				}
			}
                        // */

			if( ! $this->input->is_ajax_request() ){

				// cookie modal:
				$cookie_modal_html = '';
				if( (int)$this->frontend_settings['cookie_modal_enabled'] === 1 ){
					$cookie_modal_html = $this->load->view('_cookie_modal', '', true);
					$cookie_pdf = base_url( 'assets/uploads/files/' . $this->frontend_settings['cookie_pdf'] );
					$cookie_modal_html = str_replace( '{{cookie_pdf}}', $cookie_pdf, $cookie_modal_html );
					$cookie_modal_html = str_replace( '{{cpm_modal_infotext}}', $this->frontend_settings['cpm_modal_infotext'], $cookie_modal_html );
					$cookie_modal_html = str_replace( '{{cpm_level1}}', $this->frontend_settings['cpm_level1'], $cookie_modal_html );
					$cookie_modal_html = str_replace( '{{cpm_level2}}', $this->frontend_settings['cpm_level2'], $cookie_modal_html );
					$cookie_modal_html = str_replace( '{{cpm_level3}}', $this->frontend_settings['cpm_level3'], $cookie_modal_html );
				}

				// set global view vars for frontend:
				$this->set_view_data( array(
//					'menu_header'		=>	$this->frontend_model->get_menu('menu_zone_header'),
//					'menu_footer'		=>	$this->frontend_model->get_menu('menu_zone_footer'),
					'menu_header'		=>	$this->get_header_menu(),
					'menu_footer'		=>	$this->get_footer_menu(),
					'extra_css'		=>	($this->frontend_settings['extra_css'] != '') ? '<style> ' . $this->frontend_settings['extra_css'] . '</style>' : '',
					'extra_js'		=>	($this->frontend_settings['extra_js'] != '') ? '<script> ' . $this->frontend_settings['extra_js'] . ' </script>' : '',
					'fb_app_id'		=>	$this->config->item('frontend__fb_app_id'),
					'csrf_token_name'	=>	$this->security->get_csrf_token_name(),
					'csrf_hash'		=>	$this->security->get_csrf_hash(),
					'frontend_settings'	=>	$this->frontend_settings,
					'cookie_modal_html'	=>	$cookie_modal_html,
					'game_is_active'	=>	$this->game_is_active,
					'project_info'		=>	$project_info,
					'homepage_menu_label'   => ( true === $this->game_is_active ) ? lang('GAME') : lang('HOMEPAGE'),
					//'js_templates' 	=>  $this->get_js_templates()
				));
			}
		}
	}

	/**
	 * *************************
	 * update settings
	 * *************************
	 *
	 * @param array $page_settings
	 * @return array
	 */
	protected function update_settings($page_settings){
		foreach ($this->frontend_settings as $key => $value) {
			$update_val = ( isset($page_settings[$key]) ) ? $page_settings[$key] : '';
			if( strlen($update_val) > 0 ){
				$this->frontend_settings[$key] = $update_val;
			}
		}
	}

	/**
	 * *************************
	 * generate view_data array
	 * *************************
	 *
	 * @param array $view_arr
	 * @return boolean
	 */
	protected function set_view_data( $view_arr = array() ){
		if( ! is_array( $view_arr ) ){
			return FALSE;
		}
		foreach ($view_arr as $key => $value) {
			$this->view_data[$key] = $value;
		}
		return TRUE;
	}

	/**
	 * *************************
	 * get underscore.js template strings
	 * *************************
	 *
	 * @param null|string $folder
	 * @return string
	 */
	private function get_js_templates($folder = null){
		$path = FCPATH . $folder;
		$ret = '';

		if ($handle = opendir($path)){
			while (false !== ($file = readdir($handle))) {

				if (stripos($file, '.html') === false){
					continue;
				}

				$id = str_replace('.html', '', $file);

				// set templates:
				$ret .= '<script type="text/template" id="'.$id.'">' . "\n";
					$ret .= @file_get_contents($path.$file);
				$ret .= "\n</script>\n";

			}
			closedir($handle);
		}

		return $ret;

	}

	/**
	 * *************************
	 * render view
	 * *************************
	 *
	 * @param  string $current_content view file name
	 * @param  string $template_name
	 * @return boolean
	 */
	protected function render( $current_content = '', $template_name = '' ){
		$this->set_view_data(array(
			'current_content' => $current_content,
		));
		$this->load->view($template_name, $this->view_data);
		return TRUE;
	}

	/**
	 * *************************
	 * generate crud ojbect
	 * *************************
	 *
	 * @param  string $table_name db table name
	 * @param  string $subject crud subject
	 * @return object
	 */
	protected function get_grocery_crud( $table_name, $subject ){
		$crud = new grocery_CRUD();
		$crud->set_table($table_name);
		$crud->set_subject($subject);
		$crud->unset_print();
		//$crud->unset_jquery();
		return $crud;
	}

	/**
	 * *************************
	 * generate backend menu
	 * *************************
	 *
	 * @todo user roles
	 */
	protected function gen_backend_menu($page_name)
	{
		$menu_items_html = '';
		$menu_items = $this->config->item('admin_menu_items');
		$slug_2 = $this->uri->segment(2);
		$count = 0;

		$admin_level = (int)$this->session->userdata('admin_level');
		$admin_rights = $this->config->item('backend__admin_rights');

		foreach($menu_items as $key => $item){
			$list_level = (int)$admin_rights[$item['list_level']]['list'];
			if( $admin_level >= $list_level ){
				$active = '';
				$aria_expanded = '';
				$collapse = 'collapse';
				if( $slug_2 == $key  ){
					$active =  ' class="active"';
					$aria_expanded = ' aria-expanded="true"';
					$collapse = 'collapse in';
				}
				if( is_array($item['submenu']) && count( $item['submenu'] ) > 0 ){
					foreach ($item['submenu'] as $key3 => $item3) {
						if( $item3['slug'] == $this->uri->uri_string ){
							$active =  ' class="active"';
							$aria_expanded = ' aria-expanded="true"';
							$collapse = 'collapse in';
						}
					}
				}

				$menu_items_html .= '<li ' . $active . '>';

				// submenu
				if( is_array($item['submenu']) && count( $item['submenu'] ) > 0 ){
					$menu_items_html .= '	<a href="javascript:;" data-toggle="collapse" ' . $aria_expanded . ' data-target="#dropdown-' . $count . '"><i class="fa fa-fw fa-' . $item['icon'] . '"></i> ' . lang($item['label']) . ' <i class="fa fa-fw fa-caret-down"></i></a>';

					$menu_items_html .= '<ul id="dropdown-' . $count . '" class="' . $collapse .'">';
					foreach ($item['submenu'] as $key2 => $item2) {
						$list_level2 = (int)$admin_rights[$item2['list_level']]['list'];
						if( $admin_level >= $list_level2 ){
							$menu_items_html .= '<li><a href="' . base_url($item2['slug']) . '">' . lang($item2['label']) . '</a></li>';
						}
					}
					$menu_items_html .= '</ul>';
				} else {
					$menu_items_html .= '	<a href="' . base_url($item['slug']) . '"><i class="fa fa-fw fa-' . $item['icon'] . '"></i> ' . lang($item['label']) . '</a>';
				}

				$menu_items_html .= '</li>';
				$count++;
			}
		}
		$view_arr = array(
			'menu_items_html' => $menu_items_html,
			'page_name'		  => $page_name,
		);

		$menu = $this->load->view( 'backend/_menu.php', $view_arr, TRUE );
		return $menu;
	}

        public function is_game_active($project_info)
        {
            $return = false;
            if($project_info['status']['message']!='OK')
            {
                return false;
            }
            if($project_info['data']['status']=='ongoing')
            {
                return true;
            }

            return $return;

//            if( $project_info['data']['status'] != 'ongoing' || (int)$project_info['data']['days_available'] <=0 ){
//                    $this->load->model("backend_session_model");
//                    if( FALSE === $this->backend_session_model->check_logged_status() ){
//                            $this->game_is_active = false;
//                    }
//            }
        }

        public function get_header_menu()
        {
            $menu_elements = $this->frontend_model->get_menu('menu_zone_header');
            $skip_items = $this->get_menu_exceptions(array());

            return $this->finalize_menu_items($menu_elements, $skip_items);
        }


        public function get_footer_menu()
        {
            $menu_elements = $this->frontend_model->get_menu('menu_zone_footer');
            $skip_items = $this->get_menu_exceptions(array());

            return $this->finalize_menu_items($menu_elements, $skip_items);

        }

        private function finalize_menu_items($menu_elements = array(), $skip_items = array())
        {
            $return = array();

            foreach ($menu_elements as $k => $v)
            {
                if( in_array($v['menu_slug'], $skip_items) )
                {
                    continue;
                }
                $return[$k] = $v;
            }

            return $return;
        }

        /**
         * A header és footer menükben vannak állandó szabályok a megjelenítésre
         *
         * A visszaolvasott menülistát ki lehet szedni ha kell a slug megadásával
         *
         * @param type $more_exceptions
         * @return string
         */
        private function get_menu_exceptions($more_exceptions=array())
        {
            // TODO !
            // fontos:
            // a regisztráció és a belépés aloldalakat mindenképp kiszedjük, mert nem érvényesülnek az adminon állítható színek!
            // + ellenőrizni kell: CSRF token + becaptcha megfelelő e
            // adatmódosítás oldalon CSRF és becaptcha oké, de a színek itt sem érvényesülnek!
            // UPDATE: 20200326-án javítva. kommit: 2020.03.30
            //
            // A belépés és a reg alapvetően csak vendégeknek jelenhet meg hiába aktívra van állítva
            // míg az adatmódosítás csak belépés után!
            //
            // A $skip_items tartalmazza azokat a "menu_slug" neveket amiket kiszedünk a visszaolvasott listából!
            // A paraméterben átadható $more_exceptions tömbben tovbbi elemek átadhatóak
            //
            // mivel kevés elemű tömbről van szó, az ismétlődssel nem foglalkoztam, persze lehetne :)
            //

            $skip_items = array();

            // ha nem aktív a játék akkor nincs se reg, se belépés, se adatmódosítás
            if( false === $this->game_is_active )
            {
                $skip_items[] = 'bejelentkezes';
                $skip_items[] = 'regisztracio';
                $skip_items[] = 'adatmodositas';
            }
            else
            {
                if($this->account->is_guest())
                {
                    $skip_items[] = 'adatmodositas';
                }
                else
                {
                    $skip_items[] = 'bejelentkezes';
                    $skip_items[] = 'regisztracio';
                }
            }

            //további kivételek ha szükséges
            $skip_items = $skip_items + $more_exceptions;


            return $skip_items;
        }

        /**
         * a thepromobuilder API által visszadott adatok feldolgozása view adatok előállítása
         *  az admin main_page és activate view-ek számára
         *
         * @return array view üzenetek
         */
        public function tpb_view_data()
        {
            $return = array();

            $error = $this->check_tpb_api_response();
            if(!empty($error))
            {
                $return = $error;
                $return['title'] = lang('error');

            }
            else
            {
                $return = $this->tpb_set_project_message();
            }

            return $return;
        }

        public function tpb_set_project_message()
        {
            $return = array();

            // ide csak akkor jut el, ha OK a status és $this->project_info['data']['status'] nem üres

            $return['title'] = '';
            $return['message_main'] = '';
            $return['message_valid_from'] = '';
            $return['form_type'] = '';

            switch ($this->project_info['data']['status']) {
                case 'ongoing':
                    $return['title'] = lang('activate_stop_game');
                    $return['message_main'] = '<h4>'.lang('activate_game_is_active').'</h4>';
                    $return['form_type'] = 'form_stop';
                break;

                case 'scheduled':
                    $return['title'] = 'Játék időzítve';

                    $return['message_valid_from'] = (!empty($this->project_info['data']['valid_from'])) ? '<div>Indítás időpontja: <b>'.$this->project_info['data']['valid_from'].'</b></div>' : '';
                    $return['form_type'] = 'form_stop.php';
                break;

                case 'new':
                case 'paused':
                    $return['title'] = 'Játék aktiválása - kezdő dátum beállítása';
                    $return['message_main'] = '<h4>A játék jelenleg nem aktív.</h4><div style="margin: 15px 0px;">Az indításhoz kattintson az alábbi gombra</div>';
                    $return['form_type'] = 'form_start.php';
                break;

                default:
                    break;
            }

            $return['message_days_total'] = (!empty($this->project_info['data']['days_total'])) ? '<div>Megrendelt napok száma: <b>'.$this->project_info['data']['days_total'].' nap</b></div>' : '';
            $return['message_days_available'] = (!empty($this->project_info['data']['days_available'])) ? '<div>Felhasználható napok száma: <b>'.$this->project_info['data']['days_available'].' nap</b></div>' : '';

            return $return;

        }

        private function check_tpb_api_response()
        {
            $return = array();

            // ha nincs válasz
            if(empty($this->project_info)){
                $return['message_main'] = $this->set_tpb_error_message('1.1');
                return $return;
            }

            // ha a status message mezőben nem "OK" a válasz
            if(!empty($this->project_info['status']['message']) && $this->project_info['status']['message']!='OK'){

                // van üzenet?
                if(!empty($this->project_info['status']['message']))
                {
                    $return['message_main'] = $this->set_tpb_error_message('1.2');
                }
                else
                {
                    $return['message_main'] = $this->set_tpb_error_message('1.1');
                }
                return $return;
            }

            // ha az adatoknál nincs status
            if(empty($this->project_info['data']['status']))
            {
                $return['message_main'] = $this->set_tpb_error_message('1.3');
                return $return;
            }

            return $return;

        }


        private function set_tpb_error_message($code = '')
        {
            $message = '';
            if(!empty($code))
            {
                $message .= '<div class="activate_error"><b>';
                switch ($code) {
                    case '1.1':
                        $message .= '#ActivateError 1.1 - API kapcsolódási hiba. Kérjük, <a href="'.$this->config->item('thepromobuilder_url').'" target="_blank">írj nekünk</a>';
                        break;

                    case '1.2':
                        $message .= '#ActivateError 1.2 - '.$this->project_info['status']['message'];
                        break;

                    case '1.3':
                        $message .= '#ActivateError 1.3 - Érvénytelen státusz. Kérjük, <a href="'.$this->config->item('thepromobuilder_url').'" target="_blank">írj nekünk</a>';
                        break;

                    default:
                        break;
                }
                $message .= '</b></div>';
            }

            return $message;
        }

        public function set_disabled_frontend_user_login_page_slugs(){

            if(false === $this->frontend_settings["login_required"]){

                $this->disabled_login_required_pages[] = 'bejelentkezes';
                $this->disabled_login_required_pages[] = 'kijelentkezes';
                $this->disabled_login_required_pages[] = 'fb-login';
                $this->disabled_login_required_pages[] = 'regisztracio';
                $this->disabled_login_required_pages[] = 'adatmodositas';
                $this->disabled_login_required_pages[] = 'elfelejtett-jelszo';

            }

            if(false === $this->game_is_active){

                $this->disabled_login_required_pages[] = 'bejelentkezes';
                $this->disabled_login_required_pages[] = 'kijelentkezes';
                $this->disabled_login_required_pages[] = 'fb-login';
                $this->disabled_login_required_pages[] = 'regisztracio';
                $this->disabled_login_required_pages[] = 'adatmodositas';
                $this->disabled_login_required_pages[] = 'elfelejtett-jelszo';

            }


        }

        public function cookie_modal_script_codes()
        {

            $return = '';
                // cookie modal:
                if( (int)$this->frontend_settings['cookie_modal_enabled'] === 1 ){
                        $cpm_cookie = ( $this->input->cookie('cpm_cookie_o', TRUE) ) ? ( $this->input->cookie('cpm_cookie_o', TRUE) ) : 1;

                        // besocial extra kódok
                        $current_level_codes = ( in_array( $cpm_cookie, array(1,2,3) ) ) ? 'tracking_code_lvl' . $cpm_cookie : 'tracking_code_lvl1';
                        $return .= $this->frontend_settings[$current_level_codes];

                        // google analytics
                        $return .= $this->get_google_analytics_code($cpm_cookie);

                        // facebook pixel
                        $return .= $this->get_facebook_pixel_code($cpm_cookie);


                        $return .= '<script> var cpm_cookie=' . $cpm_cookie . ' </script>';
                } else {
                        $return .= '<script> var cpm_cookie=1; </script>';
                }
            return $return;
        }

        private function get_google_analytics_code($cpm_cookie=1)
        {
            $return = '';
            if(!empty($this->frontend_settings['google_analytics_id']))
            {
                $ga_id = $this->frontend_settings['google_analytics_id'];

                $return  .= '
                            <!-- Global site tag (gtag.js) - Google Analytics -->
                            <script async src="https://www.googletagmanager.com/gtag/js?id='.$ga_id.'"></script>
                            <script>';

                switch ($cpm_cookie) {
                    case 3:
                        $return .= "
                                    window.dataLayer = window.dataLayer || [];
                                    function gtag(){dataLayer.push(arguments);}
                                    gtag('js', new Date());
                                    gtag('config', '".$ga_id."');";
                        break;
                    case 2:
                        $return .= "
                                    window.dataLayer = window.dataLayer || [];
                                    function gtag(){dataLayer.push(arguments);}
                                    gtag('js', new Date());
                                    gtag('config', '".$ga_id."', {
                                                  'allow_ad_personalization_signals': false,
                                          });";
                        break;
                    case 1:
                    default:
                        $return .= "
                                    window.dataLayer = window.dataLayer || [];
                                    function gtag(){dataLayer.push(arguments);}
                                    gtag('js', new Date());
                                    gtag('config', '".$ga_id."', {
                                                  'allow_ad_personalization_signals': false,
                                                  'anonymize_ip': true
                                          });";
                        break;
                }

                $return .= '
                            </script>';

            }
            return $return;
        }
        private function get_facebook_pixel_code($cpm_cookie=1)
        {
            $return = '';
            if(!empty($this->frontend_settings['facebook_pixel_id']))
            {
                $fb_pixel_id = $this->frontend_settings['facebook_pixel_id'];

                switch ($cpm_cookie) {
                    case 3:
                        $return .= "
                            <!-- Facebook Pixel Code -->
                            <script>
                                !function(f,b,e,v,n,t,s)
                                {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                                n.queue=[];t=b.createElement(e);t.async=!0;
                                t.src=v;s=b.getElementsByTagName(e)[0];
                                s.parentNode.insertBefore(t,s)}(window,document,'script',
                                'https://connect.facebook.net/en_US/fbevents.js');
                                 fbq('init', '".$fb_pixel_id."');
                                fbq('track', 'PageView');
                            </script>
                            ";

                             $return .= '<noscript>
                                    <img height="1" width="1"
                                    src="https://www.facebook.com/tr?id='.$fb_pixel_id.'&ev=PageView
                                    &noscript=1"/>
                            </noscript>
                            <!-- End Facebook Pixel Code -->';
                        break;
                    case 2:
                    case 1:
                    default:
                        break;
                }


            }
            return $return;
        }
}