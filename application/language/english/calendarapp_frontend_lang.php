<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2018, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['URL_SIGN_IN'] = 'belepes';
$lang['SIGN_IN'] = 'Login';
$lang['SUCCESSFUL_SIGN_IN'] = 'Successful login';
$lang['UNSUCCESSFUL_SIGN_IN'] = 'Failed login';
$lang['SUCCESSFUL_ACCOUNT_CREATE'] = 'Successful registration';
$lang['SUCCESSFUL_ACCOUNT_MODIFY'] = 'successful data modification';
$lang['SUCCESSFUL_FACEBOOK_SIGN_IN'] = 'Successful login with facebook profile';
$lang['UNSUCCESSFUL_FACEBOOK_SIGN_IN'] = 'Login, something is wrong with your profile';
$lang['SIGN_OUT'] = 'Logout';
$lang['SUCCESSFUL_SIGN_OUT'] = 'Successful sign out';
$lang['CREATE_ACCOUNT'] = 'Create account';
$lang['MODIFY_ACCOUNT'] = 'Modify account';
$lang['TURN_ON_NOTIFICATION'] = 'Turn on notifications so you don\'t miss a thing!';
$lang['HOW_TO_PLAY_BLOCK_TITLE'] = '- How do you play? -';
$lang['HOW_TO_PLAY_BLOCK_LEADTEXT'] = 'Discover the content of every day! <br> You just need to register to start the game. Good luck!';
$lang['BUTTON_NEXT'] = 'Next';
$lang['GAME'] = 'Game';
$lang['HOMEPAGE'] = 'Homepage';
$lang['FORM_EMAIL'] = 'E-mail';
$lang['FORM_LASTNAME'] = 'Last name';
$lang['FORM_FIRSTNAME'] = 'First name';
$lang['FORM_PASSWORD'] = 'Password';
$lang['FORM_PASSWORD_AGAIN'] = 'Password again';
$lang['FORM_PASSWORD_ERROR'] = 'Incorrect password';
$lang['FORM_SUCCESSFUL_PASSWORD_CHANGE'] = 'Successful password change';
$lang['FORM_SUCCESSFUL_PASSWORD_CHANGE_TODOS_SENT_IN_EMAIL'] = 'Successful password reset, more info sent via email!';
$lang['FORM_SUBSCRIBE_NEWSLETTER'] = 'I subscribe to the newsletter';
$lang['FORM_SUBMIT_BUTTON_MODIFY_ACCOUNT'] = 'Modify account';
$lang['FORM_SUBMIT_BUTTON_CREATE_ACCOUNT'] = 'Create account';
$lang['FORM_MAIN_TITLE_PLAY_FOR_REWARDS'] = 'I\'ll play for the prizes!';
$lang['FORM_START_GAME_LINK'] = 'Click here to start the game.';
$lang['FORM_GAME_RULES_CHECKBOX_ERROR_MESSAGE_FIELD'] = 'Rules of the game';
$lang['FORM_PRIVACY_POLICY_CHECKBOX_ERROR_MESSAGE_FIELD'] = 'Privacy Statement';
$lang['ACCEPT_GAME_RULES'] = 'I accept the <a href="{url_token_game_rules}" target="_blank"> rules of the game </a>.';
$lang['ACCEPT_PRIVACY_POLICY'] = 'I accept the <a href="{url_token_privacy_policy}" target="_blank"> Privacy Statement </a>.';
$lang['FOOTER_WEBSHOP_USE_COOKIES'] = 'This site uses cookies.';
$lang['FOOTER_COOKIE_SETTINGS'] = 'Further settings';
$lang['FORGOTTEN_PASSWORD_TITLE'] = 'Forgotten password'; 
$lang['GAME_DAY_VIEWED_MESSAGE'] = 'Already seen';
$lang['GAME_DAY_OPEN_MESSAGE'] = 'Open';
$lang['GAME_NEXT_DAY_TOOLTIP_MESSAGE'] = 'What does tomorrow have in store? Come back and find out';
$lang['SHOW_QUIZ_RESULT_BUTTON_TITLE'] = 'Let\'s see';
$lang['FORGOTTEN_PWD'] = 'Forgotten password';
$lang['LOGIN_WITH_FB'] = 'Fogin with Facebook ';
$lang['LOGIN_TITLE'] = 'Login';
$lang['LOGIN_BTN'] = 'Login';
$lang['LOGIN_ERROR'] = 'Login error.';
$lang['NEXT_STEP'] = "Next";
$lang['INVALID_EMAIL'] = 'Wrong e-mail address';
$lang['NEW_PASS'] = 'New password';
$lang['PASSWORD'] = 'Password';
$lang['PASSWORD_2'] = 'Password again';
$lang['copy'] = 'Copyright ©';
$lang['besocial'] = 'Be Social. All rights reserved.';
$lang['cookie_settings'] = 'Cookie settings';
$lang['CLOSE'] = 'Close';
$lang['OK'] = 'Accept';
$lang['TIPP1'] = 'Tip: Click a';
$lang['TIPP2'] = 'icon to keep up with nothing!';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';