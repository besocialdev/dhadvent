<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2018, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['SIGN_IN'] = 'Belépés';
$lang['SUCCESSFUL_SIGN_IN'] = 'Sikeres belépés';
$lang['UNSUCCESSFUL_SIGN_IN'] = 'Sikertelen belépés';
$lang['SUCCESSFUL_ACCOUNT_CREATE'] = 'Sikeres regisztráció';
$lang['SUCCESSFUL_ACCOUNT_MODIFY'] = 'Sikeres adatmódosítás';
$lang['SUCCESSFUL_FACEBOOK_SIGN_IN'] = 'Sikeres bejelentkezés facebook profillal';
$lang['UNSUCCESSFUL_FACEBOOK_SIGN_IN'] = 'Belépés, valami nem stimmel a profiloddal';
$lang['SIGN_OUT'] = 'Kilépés';
$lang['SUCCESSFUL_SIGN_OUT'] = 'Sikeres kilépés';
$lang['CREATE_ACCOUNT'] = 'Regisztráció';
$lang['MODIFY_ACCOUNT'] = 'Adatmódosítás';
$lang['TURN_ON_NOTIFICATION'] = 'Kapcsold be az értesítéseket, hogy ne maradj le semmiről!';
$lang['HOW_TO_PLAY_BLOCK_TITLE'] = '- Hogyan játssz? -';
$lang['HOW_TO_PLAY_BLOCK_LEADTEXT'] = 'Fedezd fel minden nap tartalmát! <br>A játék kezdéséhet pusztán regisztrálnod kell. Sok sikert!';
$lang['BUTTON_NEXT'] = 'Tovább';
$lang['GAME'] = 'Játék';
$lang['HOMEPAGE'] = 'Kezdőlap';
$lang['FORM_EMAIL'] = 'E-mail';
$lang['FORM_LASTNAME'] = 'Vezetéknév';
$lang['FORM_FIRSTNAME'] = 'Keresztnév';
$lang['FORM_PASSWORD'] = 'Jelszó';
$lang['FORM_PASSWORD_AGAIN'] = 'Jelszó megerősítés';
$lang['FORM_PASSWORD_ERROR'] = 'Hibás jelszó';
$lang['FORM_SUCCESSFUL_PASSWORD_CHANGE'] = 'Sikeres jelszómódosítás';
$lang['FORM_SUCCESSFUL_PASSWORD_CHANGE_TODOS_SENT_IN_EMAIL'] = 'Sikeres jelszó átállítás, a további tudnivalókat elküldtük e-mailben!';
$lang['FORM_SUBSCRIBE_NEWSLETTER'] = 'Feliratkozom a hírlevélre';
$lang['FORM_SUBMIT_BUTTON_MODIFY_ACCOUNT'] = 'Módosítom az adataimat';
$lang['FORM_SUBMIT_BUTTON_CREATE_ACCOUNT'] = 'Regisztrálok!';
$lang['FORM_MAIN_TITLE_PLAY_FOR_REWARDS'] = 'Játszom a nyereményekért!';
$lang['FORM_START_GAME_LINK'] = 'A játék megkezdéséhez kattints ide.';
$lang['FORM_GAME_RULES_CHECKBOX_ERROR_MESSAGE_FIELD'] = 'Játékszabályzat';
$lang['FORM_PRIVACY_POLICY_CHECKBOX_ERROR_MESSAGE_FIELD'] = 'Adatkezelési tájékoztató';

$lang['ACCEPT_GAME_RULES'] = 'Elolvastam és elfogadom a <a href="{url_token_game_rules}" target="_blank">játékszabályzatot</a>.';
$lang['ACCEPT_PRIVACY_POLICY'] = 'Elolvastam és elfogadom az <a href="{url_token_privacy_policy}" target="_blank">adatkezelési tájékoztatót</a>.';
$lang['FOOTER_WEBSHOP_USE_COOKIES'] = 'A honlap sütiket használ.';
$lang['FOOTER_COOKIE_SETTINGS'] = 'További beállítások';
$lang['FORGOTTEN_PASSWORD_TITLE'] = 'Elfelejtett jelszó';
$lang['GAME_DAY_VIEWED_MESSAGE'] = 'Ezt már láttad';
$lang['GAME_DAY_OPEN_MESSAGE'] = 'Kinyitom';
$lang['GAME_NEXT_DAY_TOOLTIP_MESSAGE'] = 'Vajon a holnap mit tartogat? Látogass vissza és kiderül';
$lang['SHOW_QUIZ_RESULT_BUTTON_TITLE'] = 'Lássuk';
$lang['FORGOTTEN_PWD'] = 'Elfelejtett jelszó';
$lang['LOGIN_WITH_FB'] = 'Belépés Facebookkal ';
$lang['LOGIN_TITLE'] = 'Belépés';
$lang['LOGIN_BTN'] = 'Belépés';
$lang['LOGIN_ERROR'] = 'Sikertelen belépés.';
$lang['NEXT_STEP'] = "Tovább";
$lang['INVALID_EMAIL'] = 'Hibás e-mail cím';
$lang['NEW_PASS'] = 'Új jelszó';
$lang['PASSWORD'] = 'Jelszó';
$lang['PASSWORD_2'] = 'Jelszó még egyszer';
$lang['copy'] = 'Copyright ©';
$lang['besocial'] = 'Be Social. Minden jog fenntarva.';
$lang['cookie_settings'] = 'Süti beállítások';
$lang['CLOSE'] = 'Bezárás';
$lang['OK'] = 'Rendben';
$lang['TIPP1'] = 'Tipp: kattints a';
$lang['TIPP2'] = 'ikonra hogy ne maradj le semmiről!';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';
$lang[''] = '';



$lang[''] = '';