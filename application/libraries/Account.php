<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Account {

    protected static $guest=true;
    protected static $data=false;
    protected $CI=false;

    public function __construct() {
        $this->CI =& get_instance();
        if(isset($_SESSION["user_data"])&&!empty($_SESSION["user_data"])&&!self::$data){
            $user = $this->CI->db->where("id",$_SESSION["user_data"]->id)->select()->get("users")->row();

            self::$guest = false;
            $_SESSION["user_data"] = self::$data = $user;
        }
    }

    public function require_login(){
        if(self::is_guest())
            redirect("bejelentkezes");
    }

    public function data(){
        return self::$data;
    }

    public function get_userid(){
        if(self::$data)
            return self::$data->id;
        else return 0;
    }

    public function is_guest(){
        return self::$guest;
    }

    public function login( $email, $password ){
        //$user = $this->CI->db->where("email_address",$email)->select()->get("users")->row();

        $user = $this->CI->db->where(array("email_address"=>$email, "status"=>1))->select()->get("users")->row();
        if($user&&password_verify($password,$user->password)){
            self::$guest = false;
            $_SESSION["user_data"] = self::$data = $user;
            return true;
        }

        return false;
    }

    public function login_fb($fb_data){
       	$user = $this->CI->db
            ->where( array("email_address"=>$fb_data['fb_email'], "status"=>1 ) )
            ->or_where("fb_id",$fb_data["fb_id"])
            ->select()
            ->get("users")
            ->row();
        if(empty($user)){
            $this->CI->db->insert("users",array("email_address"=>$fb_data["fb_email"],
                                                "fb_id"=>$fb_data["fb_id"],
                                                "last_name"=>$fb_data["fb_last_name"],
                                                "first_name"=>$fb_data["fb_first_name"]));

            $new_user = $this->CI->db->where("fb_id",$fb_data['fb_id'])->select()->get("users")->row();

            self::$guest = false;
            $_SESSION["user_data"] = self::$data = $new_user;
        }
        else {
            self::$guest = false;
            $_SESSION["user_data"] = self::$data = $user;
        }
        return true;
    }

    public function logout(){
        self::$guest = true;
        $_SESSION["user_data"] = self::$data = false;

        unset($_SESSION["user_data"]);
    }

    public function new_password( $id ){
        $this->CI->load->helper("string");
        $code = strtolower(random_string("alnum",16));

        $this->CI->db->where(array("id"=>$id))->update("users",array("password_reset_code"=>$code));

        return $code;
    }

    public function save($form){
        $this->CI->form_validation->set_rules("last_name","Vezetéknév","required|min_length[3]|max_length[255]");
        $this->CI->form_validation->set_rules("first_name","Keresztnév","required|min_length[3]|max_length[255]");

        if($form=="datamod"){
            $this->CI->form_validation->set_rules("email_address","E-mail","required|valid_email|max_length[255]");
            if(strlen($this->CI->input->post("password"))>0){
                $this->CI->form_validation->set_rules('password', 'Jelszó', 'required|min_length[6]');
                $this->CI->form_validation->set_rules('passconf', 'Jelszó megerősítés', 'required|matches[password]');
            }
        }
        elseif($form=="register"){
            $this->CI->form_validation->set_rules("email_address","E-mail","required|valid_email|is_unique[users.email_address]|max_length[255]");
            $this->CI->form_validation->set_rules('password', 'Jelszó', 'required|min_length[6]');
            $this->CI->form_validation->set_rules('passconf', 'Jelszó megerősítés', 'required|matches[password]');

            $this->CI->form_validation->set_rules('accept_tos', 'Játékszabályzat', 'required');
            $this->CI->form_validation->set_rules('accept_privacy', 'Adatkezelési tájékoztató', 'required');
        }

        if(isset($_POST['becaptcha']) && $_POST['becaptcha']!="")
        {
            die();
        }

        if( !$data=$this->CI->form_validation->run() )
            throw new Exception(validation_errors());

        if(isset($data["passconf"])){
            $data["password"] = password_hash($data["password"], PASSWORD_DEFAULT);
            unset($data["passconf"]);
        }

        if(isset($data["accept_tos"])){
        	unset($data["accept_tos"]);
        }

        if(isset($data["accept_privacy"])){
        	unset($data["accept_privacy"]);
        }

        $data["newsletter"] = $this->CI->input->post("newsletter")?'igen':'nem';

        $data['last_name'] = strip_tags($data['last_name']);
        $data['first_name'] = strip_tags($data['first_name']);
        $data['email_address'] = strip_tags($data['email_address']);

        if($form=="datamod") {
            $this->CI->db->where("id",$this->get_userid())->update("users",$data);
        }
        else {
            $this->CI->db->insert("users",$data);
            $insert_id = $this->CI->db->insert_id();
            if($insert_id>0){
                $this->login($data["email_address"],$this->CI->input->post("password"));

                $this->CI->email->to($this->CI->input->post("email_address"),$this->CI->input->post("last_name")." ".$this->CI->input->post("first_name"));
                $this->CI->email->subject($this->CI->frontend_settings['email_subject_register']);
                $this->CI->email->message($this->CI->frontend_settings['email_content_register']);
                $this->CI->email->send();
            }
        }

        // mailchimp:
        $settings = $this->CI->db->where("id",1)->select()->get("setup")->row();
        if( (int)$settings->newsletter_api == 2 && $settings->mcapi_apikey != '' && $settings->mcapi_list_id != '' ){

        	$this->CI->load->library('MailChimp');

        	// sub:
        	if( $data["newsletter"] == 'igen' ){
        		try {
					$result = $this->mailchimp->post("lists/" . $settings->mcapi_list_id . "/members", array(
			            'email_address' => $this->CI->input->post("email_address"),
			            'merge_fields' 	=> ['FNAME'=>$this->CI->input->post("first_name"), 'LNAME'=>$this->CI->input->post("last_name")],
			            'status'        => 'subscribed',
			        ));

			        /*if( is_array($result) ){
			        	if( isset( $result['unique_email_id'] ) ){
			        		// ok ...
			        	} else {
			        		if( isset( $result['title'] ) && $result['title'] == 'Member Exists' ){
			        			// user exists
			        		} else {
			        			// ??
			        		}
			        	}
			        }*/
				} catch (Exception $e) {
					//echo $e->errorMessage();
				}
        	}

        	// unsub:
        	if( $data["newsletter"] == 'nem' ){
        		try {
        			$h = md5(strtolower($this->CI->input->post("email_address")));
					$result = $this->mailchimp->delete("lists/" . $settings->mcapi_list_id . "/members/" . $h);
				} catch (Exception $e) {
					//echo $e->errorMessage();
				}
        	}
        }
    }
}