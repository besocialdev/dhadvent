<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MCMS_Form_validation extends CI_Form_validation {
    /**
     * Felülírva, hogy a visszatérési érték a validált tömb legyen
     * Így ez kerül mentésre $_POST helyett (nem kerülhet bele olyan mező ami nincs validálva)
     */
    public function run($group = ''){
        $result_array = array();
        $validation_array = empty($this->validation_data)
            ? $_POST
            : $this->validation_data;

        if (count($this->_field_data) === 0){
            if (count($this->_config_rules) === 0){
                return FALSE;
            }

            if (empty($group)){
                $group = trim($this->CI->uri->ruri_string(), '/');
                isset($this->_config_rules[$group]) OR $group = $this->CI->router->class.'/'.$this->CI->router->method;
            }

            $this->set_rules(isset($this->_config_rules[$group]) ? $this->_config_rules[$group] : $this->_config_rules);

            if (count($this->_field_data) === 0){
                log_message('debug', 'Unable to find validation rules');
                return FALSE;
            }
        }

        $this->CI->lang->load('form_validation');

        foreach ($this->_field_data as $field => &$row){
            if ($row['is_array'] === TRUE){
                $this->_field_data[$field]['postdata'] = $this->_reduce_array($validation_array, $row['keys']);
            }
            elseif (isset($validation_array[$field])){
                $this->_field_data[$field]['postdata'] = $validation_array[$field];
            }
        }

        foreach ($this->_field_data as $field => &$row){
            if (empty($row['rules']))
                continue;
            /*if( $row["postdata"]===null){
                $this->_error_array[$row['field']] = $this->_build_error_msg($this->_get_error_message("is_present", $row['field']), $this->_translate_fieldname($row['label']));
                continue;
            }*/

            $this->_execute($row, $row['rules'], $row['postdata']);
            if(strstr($field, "[")){
                if(!isset($result_array[substr($field, 0, strpos($field, "["))]))
                    $result_array[substr($field, 0, strpos($field, "["))] = array();
                $result_array[substr($field, 0, strpos($field, "["))][substr($field, strpos($field, "[")+1,-1)] = $row["postdata"];
            }
            else
                $result_array[$field] = $row['postdata'];
        }

        $total_errors = count($this->_error_array);
        if ($total_errors > 0){
            $this->_safe_form_data = TRUE;
        }

        empty($this->validation_data) && $this->_reset_post_array();

        return $total_errors === 0 ? $result_array : false;
    }
}