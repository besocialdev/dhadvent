<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ***************************
 * calendar reg.
 * ***************************
 *
 */
class Calendar 
{
	protected $CI;

	public function __construct(){		
		$this->CI =& get_instance();
	}

	/**
	 * *******************
	 * get calendar items
	 * *******************
	 *
	 * @param $id integer
	 * 
	 * @return array
	 */
	public function get( $day_id = 0 ){
		$day_id*=1;

		$this->CI->db->select('*');
		if($day_id > 0){
			$this->CI->db->where("id", $day_id);
			$this->CI->db->limit(1);
		}
		
		$query = $this->CI->db->get('calendar_days');
		return $query->result_array();
	}

	public function save_view(){
		
	}

	
	public function save_quiz(){
		
	}
}

?>