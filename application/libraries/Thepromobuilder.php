<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Thepromobuilder {

	protected $project_id = null;
	protected $public_api_salt = null;
	protected $public_api_url = null;

    public function __construct() {
        $this->CI =& get_instance();

        $this->project_id = $this->CI->config->item('project_id');
        $this->public_api_salt = $this->CI->config->item('public_api_salt');
        $this->public_api_url = $this->CI->config->item('public_api_url');
    }

    public function project_stop(){
    	$post_arr = array(
    		"action"		=>	"stop",
    		"project_id"	=>	(int)$this->project_id,
    	);

    	$token = $this->create_token($post_arr);
    	$post_arr["token"] = $token;

		$ch = curl_init($this->public_api_url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_USERPWD, "thepromobuilder:thepromobuilder012");
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_arr);
		$result = json_decode( curl_exec($ch), true );
		return $result;
    }

    public function project_start($valid_from){

    	$valid_from = $valid_from . ' 00:00:00';
    	$post_arr = array(
    		"action"		=>	"start",
    		"project_id"	=>	(int)$this->project_id,
    		"valid_from"	=>	$valid_from,
    	);

    	$token = $this->create_token($post_arr);
    	$post_arr["token"] = $token;

		$ch = curl_init($this->public_api_url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_USERPWD, "thepromobuilder:thepromobuilder012");
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_arr);
		$result = json_decode( curl_exec($ch), true );
		return $result;
    }

    public function project_info(){

    	$token = $this->create_token(array(
    		"project_id"	=>	$this->project_id
    	));
    	$url = $this->public_api_url . '?project_id=' . $this->project_id . '&token=' . $token;

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_USERPWD, "thepromobuilder:thepromobuilder012");
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = json_decode( curl_exec($ch), true );

		/*if( $_SERVER['REMOTE_ADDR'] == '178.48.225.242' ){
			pre($result);
			exit;
		}*/

		return $result;
    }

	protected function create_token($data) {

		if (!$data || !is_array($data)) {
			return '';
		}

		// create params and token:
		$params = array();
		foreach ($data as $key => $value) {
			$params[] = $key . '=' . $value;
		}

		// add salt:
		$params[] = 'salt=' . $this->public_api_salt;

		// alphabetic sort
		sort($params, SORT_STRING);

		$token = sha1(join('&', $params));
		//var_dump($token); exit;

		// return result:
		return $token;

	}
}