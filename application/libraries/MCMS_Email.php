<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MCMS_Email extends CI_Email {

    public $_name = "";
    public $_real_subject = "";

    public function subject($subject){
        $this->_real_subject = $subject;
        $this->set_header('Subject', $this->_prep_q_encoding($subject));
        return $this;
    }

    public function to($to,$name=false){
        $to = $this->_str_to_array($to);
        $to = $this->clean_email($to);

        if ($this->validate)
            $this->validate_email($to);
        if ($this->_get_protocol() !== 'mail')
            $this->set_header('To', implode(', ', $to));
        if($name)
            $this->_name = $name;

        $this->_recipients = $to;

        return $this;
    }

    public function send($auto_clear = TRUE){

    	$CI =& get_instance();

        if( $CI->frontend_settings["mailgun_api_key"] != '' && $CI->frontend_settings['mailgun_api_url'] != '' ){

        	$postfields = array( "from" => $CI->frontend_settings['email_from_name']." <".$CI->frontend_settings['email_from'].">",
                             "to" => $this->_recipients[0],
                             "subject" => $this->_real_subject,
                             "html" => str_replace("{name}",$this->_name,$this->_body));

        	$ch = curl_init();
	        curl_setopt($ch, CURLOPT_POST, 1);
	        curl_setopt($ch, CURLOPT_HEADER, 1);
	        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	        curl_setopt($ch, CURLOPT_USERPWD, "api:".$CI->frontend_settings["mailgun_api_key"]);
	        curl_setopt($ch, CURLOPT_URL, $CI->frontend_settings['mailgun_api_url']);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
	        $res = curl_exec($ch);
	        curl_close($ch);

            $CI->db->insert("mail_log",array("subject"=>$this->_real_subject,"api_answer"=>$res));

	        return $res;
        } else {
			$this->from($CI->frontend_settings['email_from'], $CI->frontend_settings['email_from_name']);
			$this->to($this->_recipients[0]);
			$this->set_mailtype("html");
			$this->subject($this->_real_subject);
			$this->message(str_replace("{name}",$this->_name,$this->_body));
			parent::send();

            $CI->db->insert("mail_log",array("subject"=>$this->_real_subject));

			return true;
        }
    }
}