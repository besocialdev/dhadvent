<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_Session_model Extends CI_Model 
{
	function __construct()
	{
        parent::__construct();
    }

     /**
     * *************************
     * Check current admin level
     * *************************
     * 
     * @return integer
     */
    function check_logged_admin_level()
    {
        if (
            $this->session->userdata("id") && 
            $this->session->userdata("admin_level")
        ) {
            return intval($this->session->userdata("admin_level"));
        }
        return 0;
    }

    /**
     * *************************
     * Check current admin session
     * *************************
     * 
     * @return [boolean] true if the user logged in, false if not
     */
    function check_logged_status()
    {
    	$admin_logged_in = FALSE;
    	if (
            $this->session->userdata("id") && 
            $this->session->userdata("admin_email") && 
            $this->session->userdata("admin_login_date")
        ) {
    		$admin_logged_in = TRUE;
    	}
    	return $admin_logged_in;
    }

    /**
     * *************************
     * Log in current user
     * *************************
     *
     * @param string $email
     * @param string $password
     * @return array Return logged in user data
     */
    function login($email, $password)
    {
    	$login_details = array(
    		"admin_email"     => $email,
    		"admin_password"  => sha1($password),
            "admin_status"    => 1
    	);

    	$admin = $this->db->get_where("admins", $login_details, 1);
    	$admin_data = $admin->result();

    	return $admin_data;
    }

    /**
     * *************************
     * Redirect the user to the login screen
     * *************************
     */
    function redirect_to_login($status = NULL)
    {
    	header("Location: " . base_url('admin/login/') . urldecode($status), 'refresh');
    	return;
    }

    /**
     * *************************
     * Display mainpage for logged in user
     * *************************
     */
    function redirect_to_mainpage()
    {
    	header( "Location: " . base_url('admin') );
    	return;
    }

    /**
     * *************************
     * Create session for current user
     * *************************
     * 
     * @param  [array] $user_data current logged in user data
     * @return null    just redirect the user to the main dashboard
     */
    function create_admin_session($admin_data)
    {
    	$new_admin_data = get_object_vars($admin_data[0]);
    	unset($new_admin_data["admin_password"]);
    	$new_admin_data["admin_login_date"] = date("Y.m.d. H:i:s");

    	$this->session->set_userdata($new_admin_data);
    	session_regenerate_id();
    	
    	$this->redirect_to_mainpage();
    	return;
    }

    /**
     * *************************
     * Clear current logged in user sessions
     * *************************
     */
    function destroy_admin_session()
    {
    	$this->session->unset_userdata("id");
    	$this->session->unset_userdata("admin_email");
    	$this->session->unset_userdata("admin_login_date");

		session_unset();
		session_destroy();
		session_regenerate_id();

    	$this->redirect_to_login();
    	return;
    }

    /**
     * *************************
     * Check logged in user level
     * *************************
     * 
     * @return integer
     */
    function check_admin_level()
    {
        $current_admin_level = FALSE;
        
        if (
            $this->session->userdata("id") && 
            $this->session->userdata("admin_email") && 
            $this->session->userdata("admin_login_date")
        ) {
            $current_admin_level = $this->session->userdata("admin_level");
        }
        return $current_admin_level;
    }

    /**
     * *************************
     * Return current admin ID
     * *************************
     * 
     * @return integer
     */
    function get_admin_id()
    {
        $res = 0;
        if (
            $this->session->userdata("id") &&
            $this->session->userdata("admin_email") &&
            $this->session->userdata("admin_login_date")
        ) {
            $res = (int)$this->session->userdata("id");
        }
        return $res;
    }
}