<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ********************************
 * languages backend model
 * ********************************
 */
class Backend_Languages_Model Extends CI_Model 
{
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * *************************
	 * set is_default to 0 except $id
	 * *************************
	 *
	 * @param integer $id
	 * $return boolean
	 */
	public function reset_is_default( $id )
	{
		$id = (int)$id;
		if( $id > 0 )
		{
			$this->db->where("id !=", $id);
			$this->db->update("languages", array('is_default'=>0));
		}
		return;
	}
}