<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ********************************
 * main backend model
 * ********************************
 */
class Backend_Model Extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function chart_reg(){
		$sql = "select DATE(create_date) as day, count(create_date) as value from mcms_users GROUP BY day ORDER BY day";
		$res = $this->db->query($sql);
		$arr = $res->result_array();
		if( ! is_array($arr) ){
			$arr = array();
		}
		return json_encode($arr);
	}

	public function set_winners($winner_nr,$weighted=false){
		$res = $this->db->query("SELECT id, newsletter, (SELECT COUNT(*) as cnt FROM mcms_user_games GROUP BY user_id HAVING user_id=mcms_users.id) as games_num FROM mcms_users WHERE status='1'")->result_array();
		$pool = array();
		$weight = 0;
		foreach( $res as $row ){
			if($weighted){
				$weight += $row["games_num"];
				if(strtolower($row["newsletter"])!="nem")
					$weight++;
			}
			else
				$weight++;

			$row["weight"] = $weight;
			$pool[] = $row;
		}

		$winners = array();
		for($i=0;$i<$winner_nr;$i++){
			$rand = rand(1,$weight);
			foreach( $pool as $row ){
				if( $row["weight"] >= $rand && !isset($winners[$row["id"]]) ){
					$row["rand"] = $rand;
					$winners[$row["id"]] = $row;
					break;
				}
			}
		}

		$this->db->query("DELETE FROM mcms_user_winners");
		foreach($winners as $row) {
			$this->db->insert("mcms_user_winners",array("user_id"=>$row["id"]));
		}
	}

	public function import($csv){
		$m = 0;
		$rows = explode(PHP_EOL, $csv);
    	if( is_array($rows) && count($rows) > 0 ){
    		foreach ($rows as $key => $row) {
    			$cols = explode(';', $row);
    			$t = $cols[0];
    			$id = $cols[1];
    			$this->db->query("INSERT INTO mcms_prize_win_times SET prize_id='$id', user_id='0', win_time='$t'");
    			$m++;
    		}
    	}
    	return $m;
	}

	public function wintimes_count(){
		$this->db->select('id');
        $query = $this->db->get("prize_win_times");
        return count( $query->result_array() );
	}

	public function del_times(){
		//$this->db->query("DELETE FROM mcms_prize_win_times WHERE user_id='0'");
		$this->db->query("DELETE FROM mcms_prize_win_times");
		return true;
	}

	public function gen_times()
        {
            // get start and end time:
            $this->db->select('game_start_date, game_end_date, game_start_time, game_end_time');
            $this->db->where("id", 1);
            $query = $this->db->get("setup");
            $settings = $query->result_array();
            $m = 0;
            if (is_array($settings)) {

                $game_start = $settings[0]['game_start_date'] . ' ' . $settings[0]['game_start_time'];
                $game_end = $settings[0]['game_end_date'] . ' ' . $settings[0]['game_end_time'];

                if ($game_start > $game_end) {
                    return false;
                }

                // get prize nrs:
                $this->db->select('id, item_nr');
                $this->db->where("status", 1);
                $query = $this->db->get("prizes");
                $prizes = $query->result_array();
                if (is_array($prizes) && count($prizes) > 0) {
                    foreach ($prizes as $key => $prize) {
                        $prize_id = $prize['id'];
                        $item_nr = $prize['item_nr'];
                        for ($i = 1; $i <= $item_nr; $i++) {
                            $wintime = $this->get_random_datetime($game_start, $game_end);
                            $this->db->query("INSERT INTO mcms_prize_win_times SET prize_id='$prize_id', user_id='0', win_time='$wintime'");
                            $m++;
                        }
                    }
                }
            }
            return $m;
        }

    function get_random_datetime($sStartDate, $sEndDate, $sFormat = 'Y-m-d H:i:s')
	{
	    $fMin = strtotime($sStartDate);
	    $fMax = strtotime($sEndDate);
	    $fVal = mt_rand($fMin, $fMax);
	    return date($sFormat, $fVal);
	}


	public function game_day_exists($value, $primary_key = 0)
	{
		if( (int)$primary_key > 0 ){
			$this->db->where("id !=", $primary_key);
		}
		$this->db->select('id');
		$this->db->where("cal_day", $value);
		//$this->db->where("status", 1);
        $query = $this->db->get("calendar_days");
        return count( $query->result_array() );
	}


        /**
         * összeszámolja a regisztált felhasználókat
         * @return type
         */
	public function users_count(){
            $this->db->select('id');
            $query = $this->db->get("users");
            return count( $query->result_array() );
	}

	/**
	 * mainpage get users
	 */
	public function mp_get_users(){
		$this->db->limit(10);
		$this->db->order_by('id', 'DESC');
        $this->db->select('id, first_name, last_name, email_address, create_date');
        $query = $this->db->get("users");
        $list = $query->result_array();
        $result = array();

        if( is_array($list) && count($list) > 0 ){
			$baseurl = 'admin/users/edit';

			foreach ($list as $key => $value) {
				$result[$value['id']] = $value;

				$name = trim(strip_tags(stripslashes($value['first_name'] . " " . $value['last_name'])));
				$email = trim(strip_tags(stripslashes($value['email_address'])));
				$create_date = trim(strip_tags(stripslashes($value['create_date'])));

				$html = '<tr>
                            <td>'.$name.'</td>
                            <td>'.$email.'</td>
                            <td>'.$create_date.'</td>
                            <td>
								<a href="'.$baseurl.'/'.$value['id'].'">részletek &raquo;</a>
                            </td>
                        </tr>';

				$result[$value['id']]['html'] = $html;
			}
		}

		return $result;
	}

	/**
	 * *************************
	 * get default settings form db
	 * *************************
	 *
	 * @return array
	 */
	public function get_settings()
	{
		// lang:
		$lang_id = 3;	// @todo language settings
		$lang = $this->get_list( 'languages', 'id,lang_short_code,lang_code,meta_title,meta_description,og_title,og_description,og_image', '', 'ASC', array(
			0	=>	array(
				'field'		=>	'status',
				'operator'	=>	'',
				'val'		=>	1,
			),
			1	=>	array(
				'field'		=>	'id',
				'operator'	=>	'',
				'val'		=>	$lang_id,
			),
		));

		// settings:
		$settings = $this->get_list( 'setup', '', '', 'ASC', array(
			0	=>	array(
				'field'		=>	'id',
				'operator'	=>	'',
				'val'		=>	1,
			),
		));

		return array_merge( $lang[0], $settings[0] );
	}

	/**
	 * *************************
	 * get list
	 * *************************
	 *
	 * get_list('bnrs', 'id,name', array(0=>array('field'=>'status', 'operator'=>'!=', 'val'=>0)))
	 *
	 * @param  string $table
	 * @param  array $where
	 * @param  string $fields (empty: *)
	 * @return array
	 */
	public function get_list( $table, $fields = '', $order_by='', $order_type='ASC', $where = array(), $limit=null, $offset=null )
	{
		if( $table == '' )
		{
			return array();
		}

		if( $fields != '' )
		{
			$this->db->select($fields);
		}

		if( $order_by != '' )
		{
			$this->db->order_by($order_by, $order_type);
		}

		if( $limit != null && $offset == null )
		{
			$this->db->limit($limit);
		}

		if( $limit != null && $offset != null )
		{
			$this->db->limit($limit, $offset);
		}

		if( is_array($where) && count($where)>0 )
		{
			foreach ($where as $key => $field_set)
			{
				$field = $field_set['field'];
				$operator = $field_set['operator'];
				$val = $field_set['val'];

				$this->db->where("$field $operator", $val);
			}
		}

		$query = $this->db->get($table);
        return $query->result_array();
	}

	/**
	 * *************************
	 * update image field to empty
	 * *************************
	 */
	public function empty_image_field( $table, $fieldname, $fieldval )
	{

		$sql = "UPDATE $table SET $fieldname='' WHERE $fieldname='$fieldval' LIMIT 1";
		$this->db->query($sql);

		return TRUE;
	}

	/**
	 * *************************
	 * admin log
	 * *************************
	 *
	 * @param array $log_arr
	 * @param integer $primary_key
	 * @return boolean
	 */
	public function log( $primary_key, $log_arr )
	{
		$primary_key = (int)$primary_key;
		if( $primary_key <= 0 )
		{
			return FALSE;
		}

		if( ! is_array( $log_arr ) )
		{
			return FALSE;
		}

		$this->db->insert('admin_log', $log_arr);

		return TRUE;
	}

	/**
	 * *************************
	 * count records in db table
	 * *************************
	 *
	 * @param $db_table string
	 * @param $where array
	 * @return mixed
	 */
	public function record_count( $db_table, $where = array() )
	{
		$db_table = trim(addslashes($db_table));

		if( strlen($db_table) == 0 )
		{
			return FALSE;
		}

		if( ! is_array($where) )
		{
			return FALSE;
		}

		if( count($where) > 0 )
		{
			foreach ($where as $key => $value)
			{
				$this->db->where($key, $value);
			}
		}

		$this->db->from($db_table);
		return $this->db->count_all_results();
	}

	/**
	 * *************************
	 * check if record exists
	 * *************************
	 *
	 * @param $db_table string
	 * @param $where array
	 * @return mixed
	 */
	public function record_exists( $db_table, $where = array() )
	{
		$db_table = trim($db_table);

		if( strlen($db_table) == 0 )
		{
			return FALSE;
		}

		if( ! is_array($where) )
		{
			return FALSE;
		}

		if( count($where) > 0 )
		{
			foreach ($where as $key => $value)
			{
				$this->db->where($key, $value);
			}
		}

		$this->db->from($db_table);
		$count = $this->db->count_all_results();

		if( $count == 0 )
		{
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * *************************
	 * try to remove spec chars
	 * from slug
	 * *************************
	 *
	 * @param  string $str
	 * @return string
	 */
	function clean_slug($str) {
		$str = strtolower($str);
		$str = str_replace(' ', '-', $str);
		$str = preg_replace('/[^A-Za-z0-9\-]/', '', $str);

		return preg_replace('/-+/', '-', $str);
	}

	/**
	 * *************************
	 * set unique menu slug
	 * *************************
	 *
	 * @param  string $incoming_menu_slug
	 * @param int $id
	 * @param string $db_table
	 * @return string
	 */
	public function generate_slug( $incoming_menu_slug, $current_db_table, $id=null, $slug_field = 'menu_slug' ){

		$incoming_menu_slug = $this->clean_slug($incoming_menu_slug);

		if( $incoming_menu_slug == '' ){
			return 'cms-' . rand_str(5);
		}

		if( $incoming_menu_slug == 'admin' ){
			return 'admin-' . rand_str(5);
		}

		$result = $incoming_menu_slug;
        $table = $all = array();
        $new_menu_slug = '';
        $separator = '-';

        if( in_array($current_db_table, array('pages','competitions'))  ){
        	$this->db->where('post_type', 'live');
        }

        if( $id != null ) {
            $this->db->where("id !=", $id);
        }

        $this->db->like($slug_field, $incoming_menu_slug, 'after');
        $this->db->select($slug_field);
        $query = $this->db->get($current_db_table);
        $table = $query->result_array();
        if(count($table)>0) {
            foreach ($table as $k => $v) {
                $all[] = $v[$slug_field];
            }
        }

        if(count($all)>0) {
            foreach ($all as $i => $slug) {
                $urlold = $new_menu_slug = $slug;
                $first = 1;
                while ( $new_menu_slug == $urlold ) {
                    preg_match('/(.+)'.$separator.'([0-9]+)$/', $new_menu_slug, $match);
                    $new_menu_slug = isset($match[2]) ? $match[1].$separator.($match[2] + 1) : $urlold.$separator.$first;
                    $first++;
                }
            }
        }

        $result = ($new_menu_slug == '') ? $incoming_menu_slug : $new_menu_slug;
        return $result;
	}

	/**
	 * *************************
	 * xss clean post vars
	 * *************************
	 *
	 * @param array $post_arr
	 * @param array $skip
	 *
	 * @return array
	 */
	public function xss_clean( $post_array, $skip = array() )
	{
		if( ! is_array( $post_array ) )
		{
			return $post_array;
		}

		if( count($post_array) == 0 )
		{
			return $post_array;
		}

		foreach ($post_array as $key => $value)
		{
			if( ! in_array( $key, $skip ) )
			{
				$post_array[$key] = trim( $this->security->xss_clean($value) );
			}
		}

		return $post_array;
	}

	/**
	 * *************************
	 * get info for sidebar box
	 * *************************
	 *
	 * @param  string $table
	 * @param  integer $id
	 * @param  string $fields
	 * @return array
	 */
	public function sidebar_info( $table, $id, $fields )
	{
		if( $table == '' )
		{
			return array();
		}

		$id = (int)$id;
		if( $id <= 0 )
		{
			return array();
		}

		if( $fields == '' )
		{
			return array();
		}

		$this->db->select($fields);
		$this->db->where("id", $id);
		$query = $this->db->get($table);
        return $query->result_array();
	}

	/**
	 * *************************
	 * get val from table
	 * *************************
	 *
	 * @param  string $table
	 * @param  integer $id
	 * @param  string $field
	 * @return array
	 */
	public function get_val( $table, $id, $field )
	{
		if( $table == '' )
		{
			return array();
		}

		$id = (int)$id;
		if( $id <= 0 )
		{
			return array();
		}

		if( $field == '' )
		{
			return array();
		}

		$this->db->select($field);
		$this->db->where("id", $id);
		$query = $this->db->get($table);
		$this->db->limit(1);
        return $query->result_array();
	}

	/**
	 * *************************
	 * check admin role
	 * *************************
	 *
	 * @param  string $role_type
	 * @param string $current_page
	 * @return boolean
	 */
	public function user_has_right( $role_type, $current_page )
	{
		$admin_level = (int)$this->session->userdata('admin_level');
		$admin_rights = $this->config->item('backend__admin_rights');
		$minimum_right = (int)$admin_rights[$current_page][$role_type];

		if( $admin_level < $minimum_right )
		{
			return FALSE;
		}

		return TRUE;
	}
}