<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ********************************
 * pages backend model
 * ********************************
 */
class Backend_Pages_Model Extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * *************************
     * set mainpage to 0 except $id
     * *************************
     *
     * @param integer $id
     * $return boolean
     */
    public function reset_mainpage($id) {
        $id = (int) $id;
        if ($id > 0) {
            $this->db->where("id !=", $id);
            $this->db->where("parent_id", 0);
            $this->db->update("pages", array('is_mainpage' => 0));
        }
        return;
    }

    /**
     * *************************
     * update preview
     * *************************
     *
     * @param array $db_arr
     * @param integer $id
     * @return boolean
     */
    public function update_preview($db_arr, $id) {
        // preview exists?
        $this->db->where('parent_id', $id);
        $this->db->where('post_type', 'preview');
        $this->db->select('id');
        $query = $this->db->get("pages");
        $count = $query->num_rows();

        if ($count == 0) {
            $this->db->insert("pages", $db_arr);
            $this->db->insert_id();
        } else {
            $this->db->where("parent_id", $id);
            $this->db->limit(1);
            $this->db->update("pages", $db_arr);
        }

        return TRUE;
    }

    /**
     * *************************
     * get page url by id
     * *************************
     */
    public function get_page_url($id) {
        $id *= 1;

        $this->db->where('id', $id);
        $query = $this->db->get("pages");
        $res = $query->result_array();

        if (isset($res[0]['menu_slug'])) {
            return base_url() . $res[0]['menu_slug'];
        } else {
            return '';
        }
    }

}
