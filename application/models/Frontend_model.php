<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ********************************
 * main frontend model
 * ********************************
 */
class Frontend_Model Extends CI_Model
{
	function __construct(){
		parent::__construct();
	}

	/**
	 * *************************
	 * gallery data:
	 * *************************
	 *
	 * @param  int $gallery_id
	 * @return array
	 */
	public function get_gallery_data($gallery_id){
		$result = array();
                $sql = "SELECT * FROM mcms_gallery WHERE status='1' AND id='$gallery_id' LIMIT 1";
		$query = $this->db->query($sql);
		$gallery = $query->result_array();
		if( is_array($gallery) && isset($gallery[0]) ){
			$result = $gallery[0];
			$result['imgs'] = array();
			$gallery_images = $this->get_gallery_images($gallery_id);
			if( is_array($gallery_images) && count($gallery_images) > 0 ){
				foreach ($gallery_images as $key => $value) {
					$result['imgs'][] = $value;
				}
			}
		}
		return $result;
	}

	/**
	 * *************************
	 * count records in db table
	 * *************************
	 *
	 * @param $db_table string
	 * @param $where array
	 * @return mixed
	 */
	public function record_count( $db_table, $where = array() ){
		$db_table = trim(addslashes($db_table));
		if( strlen($db_table) == 0 || ! is_array($where) ){
			return FALSE;
		}
		if( count($where) > 0 ){
			foreach ($where as $key => $value) {
				$this->db->where($key, $value);
			}
		}
		$this->db->from($db_table);
		return $this->db->count_all_results();
	}

	/**
	 * *************************
	 * check if record exists
	 * *************************
	 *
	 * @param $db_table string
	 * @param $where array
	 * @return boolean
	 */
	public function record_exists( $db_table, $where = array() ){
		$db_table = trim($db_table);
		if( strlen($db_table) == 0 || ! is_array($where) ){
			return FALSE;
		}

		if( count($where) > 0 ){
			foreach ($where as $key => $value) {
				$this->db->where($key, $value);
			}
		}
		$this->db->from($db_table);
		$count = $this->db->count_all_results();
		if( $count == 0 ){
			return FALSE;
		}
		return TRUE;
	}

	/**
	 * *************************
	 * get val from table
	 * *************************
	 *
	 * @param  string $table
	 * @param  integer $id
	 * @param  string $field
	 * @return array
	 */
	public function get_val( $table, $id, $field ){
		if( $table == '' ){
			return array();
		}
		$id = (int)$id;
		if( $id <= 0 ){
			return array();
		}
		if( strlen($field) == 0 ){
			return array();
		}

		$this->db->select($field);
		$this->db->where("id", $id);
		$this->db->limit(1);

		$query = $this->db->get($table);
        return $query->result_array();
	}

	/**
	 * *************************
	 * get list
	 * *************************
	 *
	 * get_list('bnrs', 'id,name', array(0=>array('field'=>'status', 'operator'=>'!=', 'val'=>0)))
	 *
	 * @param  string $table
	 * @param  array $where
	 * @param  string $fields (empty: *)
	 * @return array
	 */
	public function get_list( $table, $fields = '', $order_by='', $order_type='ASC', $where = array(), $limit=null, $offset=null ){
		if( $table == '' ){
			return array();
		}

		if( $fields != '' ){
			$this->db->select($fields);
		}
		if( $order_by != '' ){
			$this->db->order_by($order_by, $order_type);
		}
		if( $limit != null && $offset == null ){
			$this->db->limit($limit);
		}
		if( $limit != null && $offset != null ){
			$this->db->limit($limit, $offset);
		}

		if( is_array($where) && count($where)>0 ){
			foreach ($where as $key => $field_set) {
				$field = $field_set['field'];
				$operator = $field_set['operator'];
				$val = $field_set['val'];
				$this->db->where("$field $operator", $val);
			}
		}
		$query = $this->db->get($table);
        return $query->result_array();
	}

	/**
	 * *************************
	 * get one record by slug
	 * *************************
	 *
	 * @param  string $content_template type of record
	 * @param  string $slug2
	 * @return array
	 */
	function get_record($content_template, $slug2){

		if( $content_template == '' || $content_template == 'mainpage' || $content_template == 'cms_page' ){
			return array();
		}

		if( ! isset($slug_arr[$content_template]) ){
			return false;
		}

		$slug_db = $slug_arr[$content_template];
		$slug2 = addslashes($slug2);

		$query = $this->db->query("SELECT * FROM mcms_$content_template WHERE $slug_db='$slug2' LIMIT 1");
		$res = $query->result_array();
		if( isset($res[0]) ){
			return $res[0];
		}

		return false;
	}

	/**
	 * *************************
	 * get menu
	 * *************************
	 *
	 * @param integer $menu_id menu zone ID
	 * @return array
	 */
	public function get_menu($type){
		$order_field = ($type=='menu_zone_header') ? 'menu_order_header' : 'menu_order_footer';
		$result = array();
		$query = $this->db->query("SELECT id,title,menu_slug FROM mcms_pages WHERE status='1' AND $type='1' ORDER BY $order_field");
		$res = $query->result_array();

		if( is_array($res) ){
			foreach ($res as $key => $value) {
				$result[$value['id']] = $value;
			}
		}

		return $result;
	}

	/**
	 * *************************
	 * get page data by slug
	 * *************************
	 *
	 * @param  boolean $slug [description]
	 * @return array
	 */
	public function get_page($slug){
		$query = $this->db->get_where('pages', array(
			'menu_slug' => $slug,
			'status' 	=> 1,
			'post_type' => 'live'
		));
		$this->db->limit(1);
		$page = $query->result_array();
		if( ! $page ){
			return FALSE;
		}

		$res = $page[0];

                /* 
                // erre nincs szükség már, de átmenetileg benne hagyom hogyha valami hiba lenne akkor vissza lehessen követni //Imi 20200331
		// tmp data:
		$query = $this->db->get_where('pages', array(
			'id' => 1,
			'status' 	=> 1,
			'post_type' => 'live'
		));
		$this->db->limit(1);
		$tmp = $query->result_array();

		$res['fooldal_also_blokk_cim'] = $tmp[0]['fooldal_also_blokk_cim'];
		$res['fooldal_also_blokk_szoveg'] = $tmp[0]['fooldal_also_blokk_szoveg'];
                // */
                
		return $res;
	}

	/**
	 * *************************
	 * get category data by slug
	 * *************************
	 *
	 * @param  boolean $slug [description]
	 * @return array
	 */
	public function get_category($slug){
		$query = $this->db->get_where('post_categories', array(
			'menu_slug' => $slug,
			'status' 	=> 1,
			'lang_id' 	=> 1
		));
		$this->db->limit(1);
		$page = $query->result_array();
		if( ! $page ){
			return FALSE;
		}
		return $page[0];
	}


	/**
	 * *************************
	 * get default settings form db
	 * *************************
	 *
	 * @return array
	 */
	public function get_settings()
	{
		// lang:
		$lang_id = 3;	// @todo language settings
		$lang = $this->get_list( 'languages', 'id,lang_short_code,lang_code,meta_title,meta_description,og_title,og_description,og_image', '', 'ASC', array(
			0	=>	array(
				'field'		=>	'status',
				'operator'	=>	'',
				'val'		=>	1,
			),
			1	=>	array(
				'field'		=>	'id',
				'operator'	=>	'',
				'val'		=>	$lang_id,
			),
		));

		// settings:
		$settings = $this->get_list( 'setup', '', '', 'ASC', array(
			0	=>	array(
				'field'		=>	'id',
				'operator'	=>	'',
				'val'		=>	1,
			),
		));

		return array_merge( $lang[0], $settings[0] );
	}

	/**
	 * *************************
	 * html header
	 * *************************
	 *
	 * @return [type] [description]
	 */
	public function get_header($settings, $content_template='', $user_og_image = '')
	{
		if($content_template == ''){
			$meta_title = strip_tags(trim(stripslashes($settings['meta_title'])));
			$meta_description = strip_tags(trim(stripslashes($settings['meta_description'])));
			$og_title = strip_tags(trim(stripslashes($settings['og_title'])));
			$og_description = strip_tags(trim(stripslashes($settings['og_description'])));
			$og_url = current_url();
			$og_image = '';
			$og_image_width = 0;
			$og_image_height = 0;

			if( $settings['og_image'] != '' ){
				$path = $this->config->item('backend__upload_path');
				$og_image = base_url( $path . '/' . $settings['og_image'] );

				$size = getimagesize( $path . '/' . $settings['og_image'] );
				$og_image_width = $size[0];
				$og_image_height = $size[1];
			}
		}

		return "<meta content='$meta_description' name='description'>
                <meta content='$og_title' property='og:title'>
                <meta content='$og_url' property='og:url'>
                <meta content='$og_image' property='og:image'>
                <meta content='$og_image_width' property='og:image:width'>
                <meta content='$og_image_height' property='og:image:height'>
                <meta content='$og_description' property='og:description'>

                <title>$meta_title</title>";
	}

	/**
	 * *************************
	 * get default settings form db
	 * *************************
	 *
	 * @return array
	 */
	public function get_cms_blocks_to_mainpage()
	{

            // Miért van LIMI 1... ???? nem értem :(
//		$res = $this->get_list( 'pages', 'id,title,cover_image,cms_block_type', 'cms_block_mainpage_order', 'ASC', array(
//			0	=>	array(
//				'field'		=>	'status',
//				'operator'	=>	'',
//				'val'		=>	1,
//			),
//			1	=>	array(
//				'field'		=>	'cms_block_mainpage_visible',
//				'operator'	=>	'',
//				'val'		=>	'1',
//			),
//		),0,100);
//
       //$sql = "SELECT * FROM mcms_pages WHERE status='1' AND (cms_block_mainpage_visible=1 OR cms_block_mainpage_visible=3)ORDER BY cms_block_mainpage_order ASC";
        $sql = "SELECT * FROM mcms_pages WHERE (cms_block_mainpage_visible=1 OR cms_block_mainpage_visible=3)ORDER BY cms_block_mainpage_order ASC";
		$query = $this->db->query($sql);
		$res = $query->result_array();

		return $res;
	}
        
        
	public function get_mainpage_intro_block()
	{
        //$sql = "SELECT * FROM mcms_pages WHERE status='1' AND menu_slug='---mainpageintro---' ";
        $sql = "SELECT * FROM mcms_pages WHERE  menu_slug='' ";
		$query = $this->db->query($sql);
		$res = $query->result_array();

		return $res;
	}
        
        
	public function get_prizes_block_type()
	{
        //$sql = "SELECT prizes_block_type FROM mcms_pages WHERE status='1' AND menu_slug='nyeremenyek' ";
        $sql = "SELECT prizes_block_type FROM mcms_pages WHERE menu_slug='nyeremenyek' ";
		$query = $this->db->query($sql);
		$res = $query->result_array();

		return $res;
	}
        
	public function get_prizes_list()
	{
                $sql = "SELECT * FROM mcms_prizes WHERE status='1' ORDER BY orderby ASC,title ASC ";
		$query = $this->db->query($sql);
		$res = $query->result_array();

		return $res;
	}
        
        public function get_days_list()
        {
            $this->db->where("status", 1);
            $this->db->order_by("cal_day ASC");
            $query = $this->db->get("calendar_days");
            return $query->result();
        }
        
        public function get_day_row($day_id=0)
        {
            $this->db->where("status", 1);
            $this->db->where("id", $day_id);
            $query = $this->db->get("calendar_days");
            return $day = $query->row();
        }
        
        public function get_quiz_row($day_id,$answer_id)
        {
            return $this->db->get_where("calendar_quiz",array("id"=>$answer_id,"day_id"=>$day_id))->row();
        }
        
        public function get_user_game_days($user_id=0)
        {
            $this->db->where("user_id", $user_id);
            $query = $this->db->get("user_games");
            return $query->result();
            //$this->db->get_where("user_games",array("user_id"=>$this->account->get_userid(),"game_id"=>$day->id))
        }
        
        public function get_quiz_question_data($day_id = 0)
        {
            $return = array();
            if(!empty($day_id))
            {
                $this->db->select('quiz_question');
                $this->db->where("id", $day_id);
                $query = $this->db->get("calendar_days");
                $res = $query->result_array();
                if (is_array($res) && isset($res[0])) {
                    //echo '<h2>' . htmlspecialchars_decode($res[0]['quiz_question']) . '</h2>';
                    $return = $res[0];
                }
            }
            return $return;
        }
        
        public function save_user_coupon_num($coupon_num, $day_id, $user_id)
        {
            $data = array(
                "day_id"                => $day_id,
                "user_id"               => $user_id,
                "coupon_fieldname_num"  => $coupon_num
            );
            $res = $this->db->insert("users_coupons_connections", $data);
            return $result["id"] = $this->db->insert_id();
        }
        
        public function load_user_coupon_num($day_id, $user_id)
        {
            $return = 0;
            if(!empty($day_id) && !empty($user_id))
            {
                $this->db->select('coupon_fieldname_num');
                $this->db->where("day_id", $day_id);
                $this->db->where("user_id", $user_id);
                $this->db->limit(1);
                
                $query = $this->db->get("users_coupons_connections");
                
                $res = $query->result_array();
                if (is_array($res) && isset($res[0]['coupon_fieldname_num'])) {
                    $return = $res[0]['coupon_fieldname_num'];
                }
            }
            return $return;
            
        }

	/**
	 * *************************
	 * newsletter - email exists?
	 * *************************
	 *
	 * @return boolean
	 */
	public function nl_email_exists($email_address){
		$res = $this->record_exists('newsletter_form', array('email_address'=>$email_address));
		return $res;
	}

	/**
	 * *************************
	 * newsletter - save to db
	 * *************************
	 */
	public function nl_save( $db_arr ){
		$res = $this->db->insert("newsletter_form", $db_arr);
		$result["id"] = $this->db->insert_id();
	}


	/**
	 * *************************
	 * get image url by name
	 * *************************
	 *
	 * @param  string $image_name
	 * @return string
	 */
	public function image_url( $image_name ){
		if( $image_name != '' ){
			$upload_path = $this->config->item('backend__upload_path');
			return base_url() . $upload_path . '/' . $image_name;
		}
		return '';
	}

	/**
	 * *************************
	 * get nice date
	 * *************************
	 *
	 * @param  datetime $datetime
	 * @return mixed
	 */
	public function get_create_date($datetime){
		if( ! isvaliddatetime($datetime) ){
			return '';
		}
		return strftime("%Y. %m. %d.", strtotime($datetime));
	}

	/**
	 * *************************
	 * generate category url
	 * *************************
	 *
	 * @param string $slug
	 * @return string
	 */
	public function get_category_url($slug){
		return base_url() . $slug;
	}

	/**
	 * *************************
	 * generate post url
	 * *************************
	 *
	 * @param string $cat_slug
	 * @param string $slug
	 * @return string
	 */
	public function get_post_url($cat_slug, $slug){
		return base_url() . $cat_slug . "/" . $slug;
	}
}