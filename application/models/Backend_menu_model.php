<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ********************************
 * menu backend model
 * ********************************
 */
class Backend_Menu_Model Extends CI_Model 
{
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * *************************
	 * set menu items menu_id to 0
	 * *************************
	 *
	 * @param integer $menu_id
	 * $return boolean
	 */
	public function reset_menu_id( $menu_id )
	{
		$menu_id = (int)$menu_id;
		if( $menu_id > 0 )
		{
			$this->db->where("menu_id", $menu_id);
			$this->db->update("menu_items", array('menu_id'=>0));
		}
		return;
	}
}