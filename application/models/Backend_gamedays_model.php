<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * ********************************
 * Gameday backend model
 * ********************************
 */
class Backend_Gamedays_Model Extends CI_Model {

    
    function __construct() {
        parent::__construct();
    }

    /**
     * *************************
     * set mainpage to 0 except $id
     * *************************
     *
     * @param integer $id
     * $return boolean
     */
    public function reset_mainpage($id) {

        $id = (int) $id;
        if ($id > 0) {
            $this->db->where("id !=", $id);
            $this->db->update("calendar_days", array('is_mainpage' => 0));
        }
        return;
    }
    
    public function read_quiz_answers($day_id=0)
    {
        if(!empty($day_id))
        {

            $this->db->where('day_id', $day_id);
            $query = $this->db->get("calendar_quiz");
            $count = $query->num_rows();

            if($count>0)
            {
                return $query->result('array');
            }
        }
        
        return false;
    }
    
    public function read_puzzle_data($id=0)
    {
        if(!empty($id))
        {

            $this->db->where('id', $id);
            $query = $this->db->get("calendar_days");
            $count = $query->num_rows();

            if($count>0)
            {
                $res = $query->result('array');
                return $res[0];
            }
        }
        
        return false;
    }
    
    
    /**
     * *************************
     * insert quiz anwsers
     * *************************
     *
     * @param integer $id
     * $return boolean
     */
    public function insert_quiz_answers($insert_fields) {

        if (!empty($insert_fields)) {
            $this->db->insert("calendar_quiz", $insert_fields);
            $this->db->insert_id();
        }
        return true;
    }    
    
    /**
     * *************************
     * insert quiz anwsers
     * *************************
     *
     * @param integer $id
     * $return boolean
     */
    public function delete_quiz_answers($day_id) {

        if (!empty($day_id)) {
            $this->db->where("day_id", $day_id);
            $this->db->delete("calendar_quiz");
        }
        return true;
    }    
    
    /**
     * *************************
     * insert quiz anwsers
     * *************************
     *
     * @param integer $id
     * $return boolean
     */
    public function update_quiz_answers($update_fields,$id) {
        
        if (!empty($update_fields)) {
            $this->db->where('id', $id);
            $this->db->update("calendar_quiz", $update_fields);
        }

        return true;
    }    
    

    /**
     * *************************
     * update preview
     * *************************
     *
     * @param array $db_arr
     * @param integer $id
     * @return boolean
     */
    public function update_preview($db_arr, $id) {
        // preview exists?
        $this->db->where('post_type', 'preview');
        $this->db->select('id');
        $query = $this->db->get("calendar_days");
        $count = $query->num_rows();

        if ($count == 0) {
            $this->db->insert("calendar_days", $db_arr);
            $this->db->insert_id();
        } else {
            $this->db->limit(1);
            $this->db->update("calendar_days", $db_arr);
        }

        return TRUE;
    }

    /**
     * *************************
     * get page url by id
     * *************************
     */
    public function get_page_url($id) {
        $id *= 1;
        
        $this->db->where('id', $id);
        $query = $this->db->get("calendar_days");
        $res = $query->result_array();

        if (isset($res[0]['menu_slug'])) {
            return base_url() . $res[0]['menu_slug'];
        } else {
            return '';
        }
    }

}
