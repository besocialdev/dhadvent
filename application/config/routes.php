<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'frontend_home/index';
$route['404_override'] = 'frontend_home/route_404';
$route['translate_uri_dashes'] = FALSE;

// **************
// FRONTEND felhasználó:
// **************
$route['bejelentkezes'] = 'frontend_user/login';
$route['kijelentkezes'] = 'frontend_user/logout';
$route['fb-login'] = 'frontend_user/fb_login';
$route['regisztracio'] = 'frontend_user/form/register';
$route['adatmodositas'] = 'frontend_user/form/datamod';
$route['elfelejtett-jelszo'] = 'frontend_user/forgot_pass';
$route['elfelejtett-jelszo/(:any)'] = 'frontend_user/forgot_pass/$1';

$route['jatek-urlap'] = 'frontend_user/game';
$route['get-reg-form'] = 'frontend_user/get_reg_form';
$route['get-login-form'] = 'frontend_user/get_login_form';

$route['naptar'] = 'frontend_game/display';
$route['kupon-letoltes/(:any)'] = 'frontend_game/download_coupon/$1';
$route['ajax-quiz/(:any)/(:any)'] = 'frontend_game/quiz/$1/$2';
$route['ajax-calendar'] = 'frontend_game/calendar';
$route['ajax-day/(:any)'] = 'frontend_game/day/$1';

// **************
// admin cookie:
// **************
$route['admsc'] = "backend_setcookie/index";

// **************
// admin file upload:
// **************
$route['admin/fileupload'] = "backend_fileupload/index";
$route["admin/fileupload/(:any)"] = "backend_fileupload/index/$1";

// **************
// admin routes:
// **************
$route["admin/pages"] = "backend_pages/index";
$route["admin/pages/save_preview"] = "backend_pages/save_preview";
$route["admin/pages/get_live_url"] = "backend_pages/get_live_url";
$route["admin/pages/delete_file/(:any)/(:any)"] = "backend_pages/delete_file/$1/$2";
$route["admin/pages/(:any)"] = "backend_pages/crud/$1";
$route["admin/pages/(:any)/(:any)"] = "backend_pages/crud/$1/$2";

$route["admin/prizes"] = "backend_prizes/index";
$route["admin/prizes/save_preview"] = "backend_prizes/save_preview";
$route["admin/prizes/get_live_url"] = "backend_prizes/get_live_url";
$route["admin/prizes/delete_file/(:any)/(:any)"] = "backend_prizes/delete_file/$1/$2";
$route["admin/prizes/(:any)"] = "backend_prizes/crud/$1";
$route["admin/prizes/(:any)/(:any)"] = "backend_prizes/crud/$1/$2";

/* // wintime - nyerő időpont törlés
$route["admin/wintimes"] = "backend_wintimes/index";
$route["admin/wintimes/delete_file/(:any)/(:any)"] = "backend_wintimes/delete_file/$1/$2";
$route["admin/wintimes/gen"] = "backend_wintimes/gen";
$route["admin/wintimes/del"] = "backend_wintimes/del";
$route["admin/wintimes/import"] = "backend_wintimes/import";
$route["admin/wintimes/(:any)"] = "backend_wintimes/crud/$1";
$route["admin/wintimes/(:any)/(:any)"] = "backend_wintimes/crud/$1/$2";
  */

$route["admin/winnergen"] = "backend_winnergen/index";
$route["admin/winnergen/delete_file/(:any)/(:any)"] = "backend_winnergen/delete_file/$1/$2";
$route["admin/winnergen/gen"] = "backend_winnergen/gen";
$route["admin/winnergen/(:any)"] = "backend_winnergen/crud/$1";
$route["admin/winnergen/(:any)/(:any)"] = "backend_winnergen/crud/$1/$2";

$route["admin/widgets"] = "backend_widgets/index";
$route["admin/widgets/(:any)"] = "backend_widgets/crud/$1";
$route["admin/widgets/(:any)/(:any)"] = "backend_widgets/crud/$1/$2";
$route["admin/gamedays"] = "backend_gamedays/index";
$route["admin/gamedays/save_preview"] = "backend_gamedays/save_preview";
$route["admin/gamedays/get_live_url"] = "backend_gamedays/get_live_url";
$route["admin/gamedays/delete_file/(:any)/(:any)"] = "backend_gamedays/delete_file/$1/$2";
$route["admin/gamedays/(:any)"] = "backend_gamedays/crud/$1";
$route["admin/gamedays/(:any)/(:any)"] = "backend_gamedays/crud/$1/$2";
$route["admin/languages"] = "backend_languages/index";
$route["admin/languages/delete_file/(:any)/(:any)"] = "backend_languages/delete_file/$1/$2";
$route["admin/languages/(:any)"] = "backend_languages/crud/$1";
$route["admin/languages/(:any)/(:any)"] = "backend_languages/crud/$1/$2";
$route["admin/login"] = "backend_login";
$route["admin/login/(:any)"] = "backend_login/$1";
$route["admin/dologin"] = "backend_login/do_login";
$route["admin/logout"] = "backend_login/logout";
$route["admin/setup/delete_file/(:any)/(:any)"] = "backend_setup/delete_file/$1/$2";
$route["admin/setup/(:any)/(:any)"] = "backend_setup/crud/$1/$2";
$route["admin/users"] = "backend_users/index";
$route["admin/users/(:any)"] = "backend_users/crud/$1";
$route["admin/users/(:any)/(:any)"] = "backend_users/crud/$1/$2";
$route["admin/games"] = "backend_games/index";
$route["admin/games/(:any)"] = "backend_games/crud/$1";
$route["admin/games/(:any)/(:any)"] = "backend_games/crud/$1/$2";
$route["admin/access_info"] = "backend_access_info/index";
$route["admin/admins"] = "backend_admins/index";
$route["admin/admins/(:any)"] = "backend_admins/crud/$1";
$route["admin/admins/(:any)/(:any)"] = "backend_admins/crud/$1/$2";
$route["admin/newsletter-form"] = "backend_newsletter_form/index";
$route["admin/newsletter-form/(:any)"] = "backend_newsletter_form/crud/$1";
$route["admin/newsletter-form/(:any)/(:any)"] = "backend_newsletter_form/crud/$1/$2";
$route["admin/my-profile/(:any)/(:any)"] = "backend_my_profile/crud/$1/$2";
$route["admin/activate/status"] = "backend_activate/status";
$route["admin/activate"] = "backend_activate/index";
$route['admin'] = "backend_home/index";

$route["admin/cpm/(:any)/(:any)"] = "backend_cpm/crud/$1/$2";
$route["admin/skin/(:any)/(:any)"] = "backend_skin/crud/$1/$2";

$route["api/calendar-get"] = "api/calendar_get";
$route["api/calendar-get/(:any)"] = "api/calendar_get/$1";
$route["api"] = "api/index";

$route["404"] = "frontend_home/route_404";
$route["(:any)"] = "frontend_home/index/$1";
$route["(:any)/(:any)"] = "frontend_home/index/$1/$2";