
<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * *****************
 * backend config
 * *****************
 */

// default admin theme:
$config['backend__theme'] 				= 'theme_backend';

// cookie validation:
$config['backend__cookie_name'] 		= 'cms_sga';
$config['backend__setcookie_urlhash'] 	= 'sdkfjsuSDFSf23sdfsdf';
$config['backend__setcookie_slug'] 		= 'admsc';

// file upload controller slug:
$config['backend__fileupload_slug']		= 'admin/fileupload';

// required label:
$config['backend__label_req'] = '<span class="required_star"> *</span>';

// status types:
$config['backend__status_default'] = array(
	1 => 'Aktív', 
	0 => 'Inaktív',
);

$config['backend__post_status_default'] = array(
	0 => 'Inaktív',
	1 => 'Aktív', 
);

$config['backend__status_author'] = array(
	0 => 'Inaktív',
);

// admin levels:
define('ADMIN_LEVEL_ADMIN', 	30);
define('ADMIN_LEVEL_ADMIN2', 	35);
define('ADMIN_LEVEL_ROOT', 		40);

// admin levels select:
$config['backend__admin_levels'] = array(
	ADMIN_LEVEL_ADMIN		=>	'Korlátozott jogosultság',
	ADMIN_LEVEL_ADMIN2		=>	'Teljes jogosultság (kivéve dizájn módosítás)',
	ADMIN_LEVEL_ROOT		=>	'Teljes jogosultság'
);

// set admin rights:
$config['backend__admin_rights'] = array(

	// CMS:
	'dashboard'	=>	array(
		'list'		=>	ADMIN_LEVEL_ADMIN,
	),
	'pages'	=>	array(
		'list'		=>	ADMIN_LEVEL_ADMIN2,
		'create'	=>	ADMIN_LEVEL_ADMIN2,
		'delete'	=>	ADMIN_LEVEL_ADMIN2,
		'publish'	=>	ADMIN_LEVEL_ADMIN2,
	),	
    'prizes' =>	array(
		'list'		=>	ADMIN_LEVEL_ADMIN2,
		'create'	=>	ADMIN_LEVEL_ADMIN2,
		'delete'	=>	ADMIN_LEVEL_ADMIN2,
		'publish'	=>	ADMIN_LEVEL_ADMIN2,
	),
    /* // wintime - nyerő időpont törlés
	'wintimes' =>	array(
		'list'		=>	ADMIN_LEVEL_ADMIN2,
		'create'	=>	ADMIN_LEVEL_ADMIN2,
		'delete'	=>	ADMIN_LEVEL_ADMIN2,
		'publish'	=>	ADMIN_LEVEL_ADMIN2,
	),
     */
	'winnergen' =>	array(
		'list'		=>	ADMIN_LEVEL_ADMIN2,
		'create'	=>	ADMIN_LEVEL_ADMIN2,
		'delete'	=>	ADMIN_LEVEL_ADMIN2,
		'publish'	=>	ADMIN_LEVEL_ADMIN2,
	),
    'gamedays' =>	array(
		'list'		=>	ADMIN_LEVEL_ADMIN2,
		'create'	=>	ADMIN_LEVEL_ADMIN2,
		'delete'	=>	ADMIN_LEVEL_ADMIN2,
		'publish'	=>	ADMIN_LEVEL_ADMIN2,
	),
	'setup'	=>	array(
		'list'		=>	ADMIN_LEVEL_ADMIN2,
		'create'	=>	null,
		'delete'	=>	null,
		'publish'	=>	null,
	),
	'cpm'	=>	array(
		'list'		=>	ADMIN_LEVEL_ADMIN2,
		'create'	=>	null,
		'delete'	=>	null,
		'publish'	=>	null,
	),
	'skin'	=>	array(
		'list'		=>	ADMIN_LEVEL_ADMIN2,
		'create'	=>	null,
		'delete'	=>	null,
		'publish'	=>	null,
	),
	'activate'	=>	array(
		'list'		=>	ADMIN_LEVEL_ADMIN2,
		'create'	=>	null,
		'delete'	=>	null,
		'publish'	=>	null,
	),
	'users'	=>	array(
		'list'		=>	ADMIN_LEVEL_ADMIN,
		'create'	=>	ADMIN_LEVEL_ADMIN,
		'delete'	=>	ADMIN_LEVEL_ADMIN,
		'publish'	=>	ADMIN_LEVEL_ADMIN,
	),
	'games'	=>	array(
		'list'		=>	ADMIN_LEVEL_ADMIN,
		'create'	=>	ADMIN_LEVEL_ADMIN,
		'delete'	=>	ADMIN_LEVEL_ADMIN,
		'publish'	=>	ADMIN_LEVEL_ADMIN,
	),
	'admins'	=>	array(
		'list'		=>	ADMIN_LEVEL_ADMIN2,
		'create'	=>	null,
		'delete'	=>	null,
		'publish'	=>	null,
	),
	'languages'	=>	array(
		'list'		=>	ADMIN_LEVEL_ADMIN2,
		'create'	=>	ADMIN_LEVEL_ADMIN2,
		'delete'	=>	ADMIN_LEVEL_ADMIN2,
		'publish'	=>	ADMIN_LEVEL_ADMIN2,
	),
);

// menu_items:
// https://fontawesome.bootstrapcheatsheets.com/
$config['admin_menu_items'] = array(

	// dashboard:
	''	=>	array(
		'label'		=>	'dashboard',
		'icon'		=>	'dashboard',
		'slug'		=>	'admin',
		'list_level'=>	'dashboard',
		'submenu'	=>	null 	// array
	),

	'activate'	=>	array(
		'label'		=>	'activate',
		'icon'		=>	'crosshairs',
		'slug'		=>	'admin/activate',
		'list_level'=>	'activate',
		'submenu'	=>	null 	// array
	),

	// cms pages:
	'pages'	=>	array(
		'label'		=>	'page_sidebar_sites',
		'icon'		=>	'file',
		'slug'		=>	'admin/pages/crud',
		'list_level'=>	'pages',
		'submenu'	=>	null 	// array
	),

	// cms prizes:
	/*'prizes'	=>	array(
		'label'		=>	'page_sidebar_prizes',
		'icon'		=>	'trophy',
		'slug'		=>	'admin/prizes/crud',
		'list_level'=>          'prizes',
		'submenu'	=>	null 	// array
	),*/


	'prizes'	=>	array(
		'label'		=>	'page_sidebar_prizes',
		'icon'		=>	'trophy',
		'slug'		=>	'admin/prizes/crud',
		'list_level'=>	'prizes',
		'submenu'	=>	array(	
			0	=>	array(
				'label'		=>	'page_sidebar_prizes',
				'list_level'=>	'prizes',
				'slug'		=>	'admin/prizes/crud',
			),
                    /* // wintime - nyerő időpont törlés
			1	=>	array(
				'label'		=>	'page_sidebar_wintimes',
				'list_level'=>	'prizes',
				'slug'		=>	'admin/wintimes/crud',
			),
                     */
			2	=>	array(
				'label'		=>	'page_sidebar_winnergen',
				'list_level'=>	'prizes',
				'slug'		=>	'admin/winnergen/crud',
			),
		)
	),


	// cms calendar days:
	'gamedays'	=>	array(
		'label'		=>	'page_sidebar_gamedays',
		'icon'		=>	'calendar',
		'slug'		=>	'admin/gamedays/crud',
		'list_level'=>          'gamedays',
		'submenu'	=>	null 	// array
	),

	'users'	=>	array(
		'label'		=>	'page_sidebar_users',
		'icon'		=>	'user',
		'slug'		=>	'admin/users/crud',
		'list_level'=>	'users',
		'submenu'	=>	null 	// array
	),

	'games'	=>	array(
		'label'		=>	'page_sidebar_games',
		'icon'		=>	'gamepad',
		'slug'		=>	'admin/games/crud',
		'list_level'=>	'games',
		'submenu'	=>	null 	// array
	),

	// settings:
	'setup'	=>	array(
		'label'		=>	'page_sidebar_setup',
		'icon'		=>	'cog',
		'slug'		=>	'admin/setup/crud',
		'list_level'=>	'setup',
		'submenu'	=>	array(
			
			0	=>	array(
				'label'		=>	'page_sidebar_setup2',
				'list_level'=>	'setup',
				'slug'		=>	'admin/setup/edit/1',
			),
			1	=>	array(
				'label'		=>	'page_sidebar_langs',
				'list_level'=>	'languages',
				'slug'		=>	'admin/languages/edit/3',
			),
			2	=>	array(
				'label'		=>	'page_sidebar_cpm',
				'list_level'=>	'cpm',
				'slug'		=>	'admin/cpm/edit/1',
			),
			3	=>	array(
				'label'		=>	'page_sidebar_skin',
				'list_level'=>	'skin',
				'slug'		=>	'admin/skin/edit/1',
			),
			4	=>	array(
				'label'		=>	'page_sidebar_admins',
				'list_level'=>	'admins',
				'slug'		=>	'admin/admins/crud',
			),
		)
	),
);
