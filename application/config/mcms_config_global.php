<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * *****************
 * global site config
 * *****************
 */

define("MCMS_VERSION", 0.5);
date_default_timezone_set('Europe/Budapest');

$config['global__admin_slugs']					= array('admin','admsc');
$config['global__admin_not_protected_slugs']	= array('admsc','login','logout');
$config['global__admin_root_slug'] 				= 'admin';
//$config['backend__upload_path'] = 'assets'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'files';
$config['backend__upload_path'] = 'assets/uploads/files';
$config['backend__upload_path_puzzle'] = 'assets/uploads/files/puzzle';
$config['backend__upload_path_coupon'] = 'assets/uploads/files/coupons';

// thepromobuilder settings:
$config['project_id'] = 12;															// @todo change project id (!)
$config['public_api_salt'] = '2rCr2YGkSu4XR5SUbEgPJhJ6b2dfzvpfVqM5CGX83HcgbXzw';

if(isset($_SERVER['CI_ENV'])&&($_SERVER['CI_ENV']=='development' || $_SERVER['CI_ENV']=='testing'))
{
    if($_SERVER['CI_ENV']=='development')
    {
        $config['project_id'] = 1;
    }
    $config['public_api_url'] = 'http://prodlanding.besocialdev.hu/public/api/projects/';		// test
    $config['thepromobuilder_url'] = 'http://prodlanding.besocialdev.hu/';		// test
}
else{
    $config['public_api_url'] = 'https://thepromobuilder.com/public/api/projects/';			// prod
    $config['thepromobuilder_url'] = 'https://thepromobuilder.com/calendarapp';		// prod
}
