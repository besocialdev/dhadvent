<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * *****************
 * frontend config
 * *****************
 */
$config['frontend__recaptcha_site_key'] = '';
$config['frontend__recaptcha_secret'] = '';
$config['frontend__google_maps_api_key'] = '';
$config['frontend__user_salt'] = '';
$config['frontend__pagination_limit'] = 20;
$config['frontend__pagination_items'] = 3;
$config['frontend__theme'] = 'frontend_v2';